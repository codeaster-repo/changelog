==================================================================
Version 15.1.13 (révision 9032ae2c3451) du 2020-03-31 09:06 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27561 YU Ting                  Étendre le cas-test SSNS115 au-delà de la charge limite
 29475 ZENTNER Irmela           [GENE_ACCE_SEISME] Problème d'interpolation du spectre cible
 29698 GRANGE Fabien            sdnd105 oscillations de raideur de choc pendant flambage
 28786 ABBAS Mickael            Ajouter FORMULATION à AFFE_MODELE
 28921 ABBAS Mickael            A2020.1.3 - Validation IFS sur sphère immergée
 22858 PIGNET Nicolas           [RUPT] Problème avec DEFI_FOND_FISS si noeuds double en amont de la fissure
 29589 COURTOIS Mathieu         ABNORMAL_ABORT sur calcul qui se termine bien
 29452 COURTOIS Mathieu         Alarme RECU_FONCTION
 29359 YU Ting                  DEFI_GROUP: Ajouter des mailles lors de la création de nouveaux goupes
 27614 YU Ting                  D124.19 - Détecter et traiter le flip-flop en LAC
 28171 GRANGE Fabien            Mise à jour des cas tests FLAMBAGE avec DYNA_VIBRA
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27561 DU 09/04/2018
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Étendre le cas-test SSNS115 au-delà de la charge limite
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | L'objectif de cette fiche est d'étendre le cas test ssns115 de gonflement d'une membrane au-delà de sa charge limite grâce au type
  | de chargement "SUIV_PILO". 
  | 
  | Concrètement, il faut modifier ssns115d/e/f, les 3 modélisations avec la loi de comportement Néo-Hookéenne, pour que le calcul
  | continue après la pression maximale. On comparera la partie de 'snap-through' avec la solution de référence (numérique) dans le
  | livre Le Van (en pièce jointe) afin de les valider. 
  | 
  | Remarque: Pour relever des valeurs sur la courbe de solution de référence, une solution est d'utiliser le logiciel G3Data Graph
  | Analyzer sur le scan de courbe (comme marqué dans 2.3 de V6.05.115).
  | 
  | 
  | Travail effectué
  | -------------------------
  | 
  | * Digitalisation des résultats de Le Van pour le cas de la loi Néo-Hookéenne (refait avec le nouveau scan).
  | 
  | * Pilotage en déplacement selon Z par une pression suiveuse pour les cas aster ssns115d/e/f.
  | 
  | * On pilote une pression de 1.N/m^3, afin d'avoir ETA_PILOTAGE en sortie de STAT_NON_LINE 
  |   qui correspond à une pression surfacique en N/m^3.
  | 
  | * Ajout des TEST_RESU sur ETA_PILOTAGE, les références sont interpolés sur la courbe digitalisée de Le Van 
  | aux déplacements DZ pilotes (10 incréments).
  | 
  | * MAJ doc V6.05.115. On en profite pour ajouter des figures et commenter. 
  | Les résultats sont très satisfaisant (en comparant avec le nouveau scan). 
  | Les erreurs sont dorénavant plutôt lié à les différences de discrétisation (maillage différents, taille de mailles, ordre) 
  | qu'à l'interpolation du scan. Les résultats sont très proches pour ssns115e (QUAD8, comme dans l'article). 
  | Dommage de ne pas avoir produit un maillage pareil à celui de l'article.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V6.05.115
- VALIDATION : ssns115d, ssns115e, ssns115f


================================================================================
                RESTITUTION FICHE 29475 DU 16/01/2020
================================================================================
- AUTEUR : ZENTNER Irmela
- TYPE : evolution concernant code_aster (VERSION 14.2)
- TITRE : [GENE_ACCE_SEISME] Problème d'interpolation du spectre cible
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | GENE_ACCE_SEIME ne tient pas compte de l'option d'interpolation des fonctions qu'elle prend en argument.
  | Cette commande n'utilise que des interpolations LIN/LIN.
  | 
  | Correction :
  | ==========
  | 
  | J'ai effectué des modifications dans la macro afin de prendre en compte le paramètre d'interpolation défini
  | par l'utilisateur lorsqu'il crée la fonction.
  | Ces propriétés modifiées étaient également utilisées à d'autres endroits notamment la routine SRO2DSP.
  | Pour cela j'ai du remettre en dur l'interpolation LIN/LIN.
  | 
  | Les modifications provoquent 4 tests NOOK en src et 3 dans validation. Tous utilisent GENE_ACCE_SEISME avec une fonction
  | d'interpolation LOG.
  | Il est donc normal que les résultats soient modifiés. Il y a seulement des valeurs de non-regression.
  | On supprime INTERPOL ='LOG' de ces cas-tests (qui ne sont pas intentionnels) sauf dans zzzz100d pour lequel supprimer LOG créer
  | d'autres NOOK.
  | 
  | Pour valider cette correction on ajoute une modélisation (H) à zzzz317 :
  | 
  | 1) Génération d'un accélérogramme sur un spectre EUR brut (INTERPOL='LIN')
  | 2) Génération d'un accélérogramme sur le même spectre EUR brut (INTERPOL='LOG')
  | 3) Génération d'un accélérogramme sur le même spectre EUR qui a été finement interpolé en log-log au préalable.
  | 
  | On calcule les 3 spectres de réponse correspondants.
  | 
  | Comparaison des spectres de réponse : 
  | Le spectre issu de 3) sert de référence. On constate que le spectre issu de 1) est très différents de celui issu de 3)
  | alors que le spectre issu de 2) lui est quasiment confondu.
  | 
  | Impact doc :
  | V1.01.317 : ajout de la modélisation H, avec description du but de la modélisation, la méthode et une figure de comparaison
  | des spectres de réponse
  | 
  | Irmela, merci de passer la fiche à résolu si tout te convient.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V1.01.317
- VALIDATION : zzzz317h


================================================================================
                RESTITUTION FICHE 29698 DU 23/03/2020
================================================================================
- AUTEUR : GRANGE Fabien
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : sdnd105 oscillations de raideur de choc pendant flambage
- FONCTIONNALITE :
  | Les cas test SDND105c et SDND105d (DYNA_VIBRA choc avec flambage) utilisent une 
  | évolution de raideur calculée à partir des paramètres renseignés dans la loi de 
  | flambage. 
  | 
  | Avec le jeu de paramètre renseignés, en traçant l'évolution de raideur de décharge 
  | on note des oscillations. Les résultats sont OK par rapport aux résultats 
  | analytiques, on décharge après les oscillations. 
  | 
  | Cependant, je propose de changer les paramètres de la loi de flambage pour 
  | supprimer ces oscillations. Les raideurs étant modifiées, les valeurs de référence 
  | sont également modifiées.
  | 
  | Impact sur le code :
  | --------------------
  | sdnd105c : Modifications des valeurs de référence et ajout d'un test 
  | supplémentaire 
  | sdnd105d : Modifications des valeurs de référence
  | 
  | Impact sur la doc:
  | --------------------
  | V5.01.105 :  Modifications des valeurs de référence

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V5.01.105
- VALIDATION : sdnd105c,d
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28786 DU 24/04/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Ajouter FORMULATION à AFFE_MODELE
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Nous devons introduire des nouvelles formulations IFS (issue27890), ce qui va impliquer de trouver des nouveaux mots-clefs pour
  | MODELISATION.
  | 
  | On trouve que c'est un peu pénible et qu'on pourrait peut-être introduire un troisième niveau de choix dans AFFE_MODELE:
  | PHENOMENE
  | MODELISATION
  | => FORMULATION
  | 
  | Par exemple
  | MECANIQUE / 3D_FLUIDE / U_P_PHI
  | MECANIQUE / 3D_FLUIDE / U_P
  | MECANIQUE / 3D_FLUIDE / U_PSI
  | 
  | Développement
  | -------------
  | 
  | Impact dans affe_modele
  |   on ajoute le mot-clef FORMULATION sous forme d'un bloc activable selon les modélisations
  |   modification de la programmation d'AFFE_MODELE (op0018)
  | 
  | Impact dans catalogues EF
  |   On ajoute un attribut "FORMULATION" sur les modelisations concernées
  |   Les noms des MODELISATION sont post-fixées par  #x où x est un numéro d'ordre dans phenomenes_modelisations.py + ajout de
  | l'attribut FORMULATION
  | 
  | Exemple: HHO
  |   Il existe quatre modélisations: 3D_HHO_121, 3D_HHO_222, D_PLAN_HHO_121, D_PLAN_HHO_222
  |   Ajout de l'attribut FORMULATION
  |     FORMULATION = Attribute(value=(  'HHO_LINE', 'HHO_QUAD',),)
  |   Modification du catalogue ohenomenes modelisations
  |     3D_HHO_121 devient 3D_HHO#1 + (AT.FORMULATION, 'HHO_LINE'),
  |     3D_HHO_222 devient 3D_HHO#2 + (AT.FORMULATION, 'HHO_QUAD'),
  |     D_PLAN_HHO_121 devient D_PLAN_HHO#1 + (AT.FORMULATION, 'HHO_LINE'),
  |     D_PLAN_HHO_222 devient D_PLAN_HHO#2 + (AT.FORMULATION, 'HHO_QUAD'),
  |   Modification du catalogue AFFE_MODELE
  |     3D_HHO_121 => MODELISATION = '3D_HHO' + FORMULATION = 'LINEAIRE'
  |     3D_HHO_222 => MODELISATION = '3D_HHO' + FORMULATION = 'QUADRATIQUE'
  |     D_PLAN_HHO_121 => MODELISATION = 'D_PLAN_HHO' + FORMULATION = 'LINEAIRE'
  |     D_PLAN_HHO_222 => MODELISATION = 'D_PLAN_HHO' + FORMULATION = 'QUADRATIQUE'
  |    => LINEAIRE est la valeur par défaut (bloc dans le catalogue)
  | 
  | Dans la programmation (affe_modele)
  |   La routine modelGetFEType gère les formulations. Pour les modélisations identifiées (en dur), on lit la FORMULATION et on ajoute
  | #1 ou #2 au nom de la modélisation
  |   Il a fallu modifier les impressions pour ajouter la colonne "FORMULATION3 (vide donc, sauf pour HHO)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.41.01
- VALIDATION : Tout HHO
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28921 DU 14/06/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : A2020.1.3 - Validation IFS sur sphère immergée
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Dans le cadre des travaux communs avec Naval Group, le doctorant a conçu un cas-test d'IFS avec solution analytique
  | 
  | C'est une sphère creuse en élasticité linéaire immergée dans un fluide infini
  | On propose de restituer ce test dans le dépot de validation
  | 
  | 
  | Développement
  | -------------
  | 
  | Le test est donc une sphère creuse en acier de diamètre interne 1.0m et externe 1,4m, elle est considérée élastique isotrope
  | linéaire (acier standard).
  | Le fluide infini est de l'eau (vitesse du son 1500 m/s). Pour simuler "l'infini", on ajoute une condition d'impédance (IMPE_ABSO)
  | sur le bord externe de l'eau, placé à 3m de distance de la structure.
  | 
  | L'IFS est modélisée par des élément U,P,PHI, maillage 3D (T10)
  | Le maillage est construit de sorte que la diamètre de la sphère circonscrite à l’élément le plus grand soit inférieure à 0.4m, ce
  | qui fait 28477 ddl.
  | 
  | 
  | Le chargement est une pression sur la surface interne de la sphère. Calcul harmonique de 10 à 3000Hz
  | En harmonique, on dispose d'une solution analytique avec les fonctions de Bessel. L’évaluation des fonctions de Bessel est faite par
  | scipy (module "special"). On propose donc d'ajouter le petit script Python qui le permet en fichier datg, sans l'exécuter.
  |  
  | 
  | On teste pression et déplacement sur les rayons, partie réelle et imaginaire. L'erreur est comprise entre 5% et 9%, mais on teste
  | des valeurs à une fréquence donnée qui correspond à un "pic", il vaut mieux regarder la courbe globale en fonction de la fréquence.
  | 
  | Ce test est assez long (plusieurs minutes) et ne teste rien de nouveau. On le met dans validation, mot-clef dans export: fluid_structure

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V8.22.303
- VALIDATION : AHLA303a
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 22858 DU 15/07/2014
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.4)
- TITRE : [RUPT] Problème avec DEFI_FOND_FISS si noeuds double en amont de la fissure
- FONCTIONNALITE :
  | Besoin
  | ------
  | Evolution de l'opérateur DEFI_FOND_FISS afin de prendre en considération le déboutonnage avec des fronts doubles
  | 
  | Solution
  | ----------
  | Cette Evolution n'est plus nécessaire, car il a été acté dans issue25709 que les fronts doubles ne pourront plus être utilisés dans 
  | DEFI_FOND_FISS
  | 
  | Impact Doc
  | -----------
  | Sans impact

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29589 DU 12/02/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : ABNORMAL_ABORT sur calcul qui se termine bien
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Un calcul exécuté sur Eole en 14.4 MPI est indiqué comme <F>_ABNORMAL_ABORT dans astk alors que le
  | fichier '.mess' semble indiquer OK.
  | 
  | 
  | Analyse
  | -------
  | 
  | Le fichier '.export' montre un calcul effectué sur 2 processeurs MPI.
  | Or les fichiers '.mess' et output font références à des calculs avec la version séquentielle.
  | Mais je pense que ce n'est pas l'origine du diagnostic erroné.
  | 
  | Le problème vient certainement de cette ligne en commentaire dans le fichier de commandes :
  | 
  | # -- CODE_ASTER -- VERSION : EXPLOITATION (stable) -- Version 14.4.0 modifiee le 28/06/2019
  | 
  | 
  | En effet, lors de la détermination du statut d'un calcul, on s'appuie sur le code retour de l'exécution
  | et aussi sur l'output.
  | En particulier, un test vérifie qu'il y a autant de passages dans DEBUT que dans FIN.
  | 
  | La chaine "-- CODE_ASTER -- VERSION" est utilisée pour marquer les passages dans DEBUT.
  | On a donc l'impression qu'il manque des passages dans FIN.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29452 DU 10/01/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : Alarme RECU_FONCTION
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Une alarme apparaît dans RECU_FONCTION si la fonction ne contient qu'un seul point.
  | 
  | Alarme:
  | 
  | Les points ne sont pas croissants.
  | 
  | 
  | Analyse
  | -------
  | 
  | Pas réussi à reproduire avec une fonction ne contenant qu'un point.
  | En l'absence de test pour reproduire : sans suite.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 29359 DU 04/12/2019
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : DEFI_GROUP: Ajouter des mailles lors de la création de nouveaux goupes
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Pendant l'étude Naval, Nicolas souhaite convertir un maillage de QUAD4 en QUAD8. 
  | Le problème est qu'on ajoute des nœuds qui ne sont pas rajoutés aux groupes de nœuds existants. 
  | Il est parfois impossible de les rajouter facilement dans les groupes existants avec la commandes défi_group.
  | 
  | 
  | Résultat 
  | -------------
  | 
  | Sans suite.
  | 
  | Solution existante dans code_aster :
  | 
  |     - créer un groupe des mailles à partir du groupe des nœuds spécifique dans le maillage linéaire
  | (DEFI_GROUP (CREA_GROUP_MA = ( GROUP_NO=... 
  |                                OPTION='APPUI',
  |                                TYPE_APPUI='TOUT/SOMMET/...',
  |                                TYPE_MAILLE='SEG2/TRIA3/...', )
  |              ...)
  | 
  |     - convertir le maillage en quadratique ( CREA_MAILLAGE )
  |   
  |     - créer un groupe des nœuds à partir du groupe des mailles, et il contient les nouveaux nœuds ajoutés pendant la conversion
  | (DEFI_GROUP / CREA_GROUP_NO)
  | 
  | Je l'ai testée et il marche (voici en pièce jointe).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27614 DU 19/04/2018
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : D124.19 - Détecter et traiter le flip-flop en LAC
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | L'objectif est d'implémentation du traitement du flip-flop pour la méthode LAC.
  | 
  | 
  | Résultat 
  | -------------
  | 
  | Sans suite.
  | 
  | Une feuille de route contact-frottement est en train de se préparer, et des fiches nécessaires seront ouvertes.
  | Pour le traitement des problèmes de flip-flop, on envisagera des nouvelles méthodes, par exemple, introduction 
  | d'un statut contact "rasant".

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans suite
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28171 DU 22/10/2018
================================================================================
- AUTEUR : GRANGE Fabien
- TYPE : evolution concernant code_aster (VERSION 14.2)
- TITRE : Mise à jour des cas tests FLAMBAGE avec DYNA_VIBRA
- FONCTIONNALITE :
  | Avec la fiche 28100, il a été détecté que l'amortissement de choc n'est pas testé 
  | lors des calculs DYNA_VIBRA avec flambage.
  | 
  | -----------------
  | Résolution : 
  | Correction déjà effectuée dans la fiche #28758

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnd105d
- NB_JOURS_TRAV  : 0.0

