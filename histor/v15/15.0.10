==================================================================
Version 15.0.10 (révision 61cde8403795) du 2019-10-07 10:28 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28950 LEFEBVRE Jean-Pierre     Messages d'erreurs MED en version unstable
 27926 LEFEBVRE Jean-Pierre     LIRE_MAILLAGE avec nom groupe > 24 caractères
 29110 DEGEILH Robin            XFEM + thermique + chargement de rotation
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28950 DU 26/06/2019
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : Messages d'erreurs MED en version unstable
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les tests et les études effectuant des impressions au format MED à l'aide de la commande IMPR_RESU émettent des messages d'erreur
  | intempestifs.
  | 
  | /ci/_MEDfield23nProfile30.c [95] : Erreur à l'ouverture du groupe
  | /ci/_MEDfield23nProfile30.c [95] : du champ résultat
  | /ci/_MEDfield23nProfile30.c [96] : fieldname = "RES_X1__DEPL"
  | /ci/_MEDfield23nProfile30.c [96] : _path = "/CHA/RES_X1__DEPL/0000000000000000000100000000000000000001/"
  | 
  | 
  | Analyse et correction du problème
  | ---------------------------------
  | 
  | Les messages sont émis par la routine MED mfdonp car l'existence du champ dans le fichier était testée à la fois sur le nom complet
  | (avec le numéro d'ordre) et sur le nom de base du résultat.
  | On supprime le second appel à mfioex.   
  | 
  | L'appel à mfdonp est conditionné à l'existence du nom complet du champ dans le fichier. 
  | 
  | Remarque : la documentation complète des API MED est disponible dans le paquet installé sous calibre ou scibian     
  | file:///opt/med/4.0serial/share/doc/med

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : wtnv147a + tests produisant le message d'erreur
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 27926 DU 30/07/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : evolution concernant code_aster (VERSION 13.4)
- TITRE : LIRE_MAILLAGE avec nom groupe > 24 caractères
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Lors de la lecture d'un maillage au format MED, pouvoir ignorer les groupes de mailles ou de noeuds dont le nom fait plus de 24
  | caractères. Aujourd(hui la commande LIRE_MAILLAGE s'arrête avec un message d'erreur (MED_7).
  | 
  | 
  | Développement
  | -------------
  | 
  | La routine lrmmfa est modifiée pour ignorer la lecture et la création dans la structure de données code_aster des groupes de mailles
  | ou de noeuds dont le nom fait plus de 24 caractères. Une alarme est émise en remplacement du message d'erreur. Par exemple :
  | 
  |   MAYA = LIRE_MAILLAGE(FORMAT='MED',
  |                        UNITE=20,
  |                        INFO_MED=1,
  |                        VERI_MAIL=_F(VERIF='OUI',
  |                                     APLAT=1.E-3,),
  |                        INFO=1,)  
  | !-------------------------------------------------------------------------------------------------------------!
  |    ! <A> <MED_7>                                                                                                 !
  |    !                                                                                                             !
  |    !   -> Le nom de groupe "grma_012345678901234567890123456789" est trop long. Il sera ignoré dans le maillage  !
  |    !      utilisé par le calcul.                                                                                 !
  |    !                                                                                                             !
  |    !   -> Conseil:                                                                                               !
  |    !      Si vous voulez utiliser ce groupe dans le calcul, il faut le renommer en limitant                      !
  |    !      à 24 caractères son nom.                                                                               !
  |    !                                                                                                             !
  |    !                                                                                                             !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette                                              !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !                                                      !
  |    !-------------------------------------------------------------------------------------------------------------!
  | 
  | Le maillage du test adlv100a est modifié pour introduire trois groupes dont les noms font plus de 24 caractères et un groupe dont le
  | nom fait exactement 24 caractères.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : adlv100a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29110 DU 09/09/2019
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : XFEM + thermique + chargement de rotation
- FONCTIONNALITE :
  | Probleme
  | ------------
  | 
  | 
  | Les calculs mêlant Xfem, thermique et chargement en rotation ne fonctionnement plus sur Code_Aster depuis la version 11.5. 
  | Les calculs tournent dans la version 11.4 mais au delà ce n'est plus possible. 
  | Dans les versions récentes : Xfem + thermique oui, rotation + thermique oui, Xfem+rotation oui, mais les 3 en même temps c'est impossible.
  | Voici le message d'erreur :
  | 
  |    !-----------------------------------------------------------------------!
  |    ! <EXCEPTION> <CALCUL_31>                                               !
  |    !                                                                       !
  |    !  Erreur de programmation :                                            !
  |    !  Pour la variable de commande TEMP, on cherche à utiliser la famille  !
  |    !  de points de Gauss 'FPG1'.                                           !
  |    !  Mais cette famille n'est pas prévue dans la famille "liste" (MATER). !
  |    !                                                                       !
  |    !  Contexte de l'erreur :                                               !
  |    !     option       : CHAR_MECA_ROTA_R                                   !
  |    !     type_élément : MECA_XH_TETRA4                                     !
  |    !                                                                       !
  |    ! Ce message est un message d'erreur développeur.                       !
  |    ! Contactez le support technique.                                       !
  |    !                                                                       !
  |    ! --------------------------------------------                          !
  |    ! Contexte du message :                                                 !
  |    !    Option         : CHAR_MECA_ROTA_R                                  !
  |    !    Type d'élément : MECA_XH_TETRA4                                    !
  |    !    Maillage       : mesh                                              !
  |    !    Maille         : M33865                                            !
  |    !    Type de maille : TETRA4                                            !
  |    !    Cette maille appartient aux groupes de mailles suivants :          !
  |    !       SETALL VDisq VFiss                                              !
  |    !    Position du centre de gravité de la maille :                       !
  |    !       x=0.452286 y=-0.008939 z=0.148240                               !
  |    !-----------------------------------------------------------------------!
  | 
  | Analyse
  | *********
  | Suite aux échanges eu avec les développeurs « mécanique de la rupture » dans Code_Aster, la solution retenue serait d'utiliser la version 11.4 de 
  | Code_Aster qui permet de réaliser ce type de calcul.
  | L'autre solution proposée par la R&D était d'utiliser Zcrack. Cette solution n'a pas été adoptée car l'étude est déjà entamé et le nombre de 
  | configuration à étudier est très important (plusieurs dizaines de fissures différentes).
  | 
  | Classée sans suite (donc résolue!)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Aucune
- NB_JOURS_TRAV  : 0.5

