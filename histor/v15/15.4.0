==================================================================
Version 15.4.0 (révision 0230a87df871) du 2021-07-05 09:14 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31227 COURTOIS Mathieu         Erreur system à la lecture d'une base
 31118 COURTOIS Mathieu         En version 15.3.22 - révision 15455e677bb8, le cas-test de vérification fdlv1...
 31234 COURTOIS Mathieu         Installation 14.8 native sur Cronos
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31227 DU 30/06/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : Erreur system à la lecture d'une base
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand on poursuit un calcul à partir d'une base compressée, si on produit une base compressée en résultat, on ne peut pas poursuivre ce dernier calcul.
  | 
  | 
  | Correction
  | ----------
  | 
  | En effet, les fichiers compressés de la base en donnée ne sont pas supprimés lors de la décompression.
  | Il y a donc dans le répertoire de la base résultat des fichiers `pick.XXX.gz` et `pick.XXX.gz.gz`.
  | Lors de la poursuite suivante, la décompression du fichier doublement compressé écrase le fichier `pick.XXX`.
  | 
  | Contournement : supprimer manuellement les fichiers `*.gz.gz`.
  | 
  | 
  | Validation
  | ----------
  | 
  | Lancement manuel de 3 étapes (DEBUT + 2 POURSUITE).
  | On ne peut pas intégrer ce type de test aujourd'hui.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : lancement manuel
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31118 DU 07/06/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.3.22 - révision 15455e677bb8, le cas-test de vérification fdlv112a est NOOK sur cronos
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test fdlv112a est NOOK à 1.3e-6.
  | 
  | 
  | Correction
  | ----------
  | 
  | Il s'agit du seul test de CALC_TRANSFERT.
  | Cette macro fait uniquement des appels à des commandes de base CALC_FONC_INTERP, CALC_FONCTION, CREA_TABLE et à linalg.solve de numpy.
  | 
  | L'écart est faible et il n'y a pas de test élémentaire de CALC_TRANSFERT pour creuser plus loin.
  | On prend les valeurs obtenues sur Cronos et on met TOLE_MACHINE à 1.5e-6 sur ces deux valeurs testées.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv112a
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31234 DU 02/07/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Installation 14.8 native sur Cronos
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le lancement de la version 14.8 sur Cronos depuis salome_meca 2020 échoue dès l'import du catalogue avec ce message :
  | 
  | AttributeError: module 'PyQt5.Qt' has no attribute 'Qt'
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans le catalogue (ce n'est plus le cas à partir de la version 15), on regarde si on est dans AsterStudy, auquel cas, on n'utilise les objets du langage de
  | commandes communs à AsterStudy et à la version 15, sinon on utilise les objets de l'ancien superviseur, celui de la version 14.
  | 
  | Or, sur Cronos, ce test ne peut se faire à cause d'un 'import PyQt5' qui provoque l'erreur mentionnée précédemment.
  | Ceci vient de la version salome_meca 2020 universelle sur Cronos (c'est ok sur Gaia, version scibian9).
  | 
  | La correction consiste à provoquer ImportError dans `code_aster/Cata/__init__.py` :
  | 
  | . . from DO_NOT_TRY_asterstudy.common.session import AsterStudySession
  | 
  | Fait dans l'installation des versions séquentielle et parallèle natives.
  | 
  | Fiche pour tracer, pas de révision LE DEPOT DE SOURCES car c'est lié à l'installation "particulière".

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : lancement distant via asterstudy
- NB_JOURS_TRAV  : 0.5

