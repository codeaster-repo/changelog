==================================================================
Version 15.2.12 (révision 055ced57cda8) du 2020-09-28 19:45 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 30201 MATHIEU Tanguy           [RUPT] [Chantier CALC_G] : calcul de l'objet .BASELOC de DEFI_FISSURE si CONF...
 27463 TARDIEU Nicolas          A102.19 - Extraction de la numérotation (NUME_DDL) en Python
 30271 COURTOIS Mathieu         Ajouter un enum pour le python
 29453 PIGNET Nicolas           Simplifier la gestion d'un matériau
 30191 PIGNET Nicolas           En version 15.2.8 - révision cf8271850e61, les cas-tests vérification ssnv251...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 30201 DU 01/09/2020
================================================================================
- AUTEUR : MATHIEU Tanguy
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [RUPT] [Chantier CALC_G] : calcul de l'objet .BASELOC de DEFI_FISSURE si CONFIG_INIT= DECOLLEE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Le fonctionnement actuel de DEFI_FOND_FISS/CALC_G est le suivant :
  |    - si les lèvres de fissures sont collées, (CONFIG_INIT = COLLEE dans DEFI_FOND_FISS), il est obligatoire de définir les lèvres de fissure et interdit de définir la normale. L'objet BASELOC est alors calculé, mais pas l'objet NORMALE.
  |   - si les lèvres de fissures sont décollées, (CONFIG_INIT = DECOLLEE dans DEFI_FOND_FISS, il est obligatoire de définir la normale et interdit de définir les lèvres de fissure. L'objet NORMALE est alors calculé, mais pas l'objet BASELOC.
  | 
  | Selon l'existence ou non de ces objets, CALC_G peut re-calculer la direction de propagation de la fissure (qui est pourtant déjà définie dans BASELOC quand il existe) de différentes manières.
  | 
  | Enfin, le cas où les lèvres sont décollées en 2D n'est pas couvert par les cas test et aboutit à un bug (issue30174).
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Les développements sont réalisés dans le nouvel opérateur de DEFI_FISSURE en chantier.
  | Pour cet opérateur, on fait les modifications de syntaxe suivantes :
  |    - la définition des lèvres est obligatoire dans tous les cas
  |    - la définition de la normale reste obligatoire dans le cas DECOLLEE et interdite dans le cas COLLEE
  | 
  | Dans le cas DECOLLEE, il s'agit de calculer la base locale en fond de fissure. La normale au plan de fissure est donnée par l'utilisateur parle mot clé NORMALE. La direction de propagation est calculée:
  |    - en 3D en utilisant la méthodologie de la routine gdinor.F90 de CALC_G
  |    - en 2D en utilisant la normale et les lèvres (grâce aux lèvres, on définit un vecteur allant vers la matière, auquel on soustrait sa composante selon la normale à la fissure).
  | 
  | Une fois ces deux composantes trouvées, BASLOC est calculée de manière analogue que l'on soit en COLLEE ou DECOLLE.
  | Cet objet est alors calculé dans tous les cas, contient les mêmes informations et pourra être utilisé ans CALC_H.
  | 
  | L'objet .NORMALE de la sd_fond_fissure reste sauvegardé. Il pourra être supprimé ultérieurement s'il s'avère inutile.
  | 
  | Le cas test zzzz117a est ajouté. En 2D, il teste l'intégralité des composantes de la structure de donnée .BASLOC et les compare à une solution analytique (triviale, car en 2D). Il dure 35 seconde sur calibre 9.
  | 
  | Le problème soulevé par issue30174 persiste toutefois et la présente solution n'est pas transposable en l'état compte tenue des différences entre les opérateurs.
  | 
  | Résultat faux
  | -------------
  | 
  | Sans objet

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V1.01.117
- VALIDATION : zzzz117
- NB_JOURS_TRAV  : 7.0


================================================================================
                RESTITUTION FICHE 27463 DU 08/03/2018
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : A102.19 - Extraction de la numérotation (NUME_DDL) en Python
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans le cadre d'études de méthodologies pour le recalage de modèle, il serait souhaitable de pouvoir accéder au format numpy aux
  | matrices éléments-finis et leurs numérotations associées (nume_ddls associés). 
  | => On traite dans cette fiche l'accès à la numérotation.
  | 
  | Développement
  | -------------
  | 
  | Pour explorer le contenu des numérotations, on ajoute des méthodes d'accès à l'objet C++ DOFNumbering et on les expose en Python. 
  | 
  | Différents chemins de consultation ont été utilisés : 
  | - consultation directe des objets JEVEUX depuis les attributs des objets GlobalEquationNumbering (i.e. NUME_EQUA), LocalEquationNumbering (i.e. NUML_EQUA) et FieldOnNodesDescription (i.e. PROF_CHNO) ;
  | - utilisation de routines utilitaires existantes avec ajout d'interfaces (rgndas, posddl)
  | - écriture d'une nouvelle routine utilitaire avec ajout d'interface (numeddl_get_components) ou ajout de questions dans la routine dismoi
  | 
  | Au final, les méthodes suivantes ont été ajoutées :
  | - useLagrangeMultipliers
  | - useSingleLagrangeMultipliers
  | - getPhysicalQuantity
  | - getComponentAssociatedToRow
  | - getComponentsAssociatedToNode
  | - getNodeAssociatedToRow
  | - getNumberOfDofs
  | - getRowAssociatedToNodeComponent
  | - getRowsAssociatedToPhysicalDofs
  | - getRowsAssociatedToLagrangeMultipliers
  | - getComponents
  | 
  | ATTENTION : tous les index en entrée et en sortie *commencent à 1*! C'est rappelé dans le docstring!
  | 
  | 
  | Validation
  | ----------
  | Un appel à toutes ces méthodes a été ajouté dans le test xxMacroMatrAjou, avec des vérifications croisées comme :
  | 
  | for row in NUSTR.getRowsAssociatedToLagrangeMultipliers():
  |    test.assertEqual( NUSTR.getComponentAssociatedToRow(row), 'LAGR' )
  | 
  | for row in NUSTR.getRowsAssociatedToPhysicalDofs():
  |    test.assertTrue( NUSTR.getComponentAssociatedToRow(row) in ['DX','DY'])

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxMacroMatrAjou
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 30271 DU 21/09/2020
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Ajouter un enum pour le python
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Actuellement dans DEFI_MATERIAU, les chaînes de caractères doivent être converties en réel pour être sauvegardées.
  | 
  | Pour l'instant, cette conversion est faite dans le c++ mais ce n'est pas pratique pour quelque chose d'aussi simple.
  | Dans issue29423, je propose de passer cette conversion dans le python pour considérablement simplifier les choses.
  | 
  | 
  | Développement
  | -------------
  | 
  | L'idée est de stocker cette information dans le catalogue du mot-clé.
  | 
  | Exemple :
  | ECROUISSAGE = SIMP(statut='o', typ='TXM', into=('ISOTROPE', 'CINEMATIQUE'), enum=(0, 1))
  | 
  | ce qui signifie : ISOTROPE=0, CINEMATIQUE=1
  | 
  | vocab01a vérifie la cohérence du catalogue len(into) == len(enum) et typ == 'TXM'.
  | 
  | Le fonctionnement est le suivant dans l'exécution des commandes :
  | - vérification de la syntaxe (inchangé, on vérifie que la chaîne de caractères est dans le champ 'into'),
  | - juste avant l'exécution, on substitue les chaînes de caractères par leur valeur entière.
  | 
  | En pratique, c'est le visiteur 'EnumVisitor' qui se charge de la substitution.
  | Une fonction utilitaire 'replace_enum' est fournie dans `code_aster.Supervis`.
  | 
  | NB : Parcourir les mots-clés est potentiellement coûteux. Comme ce n'est pas encore utilisé
  | et que la cible est, pour le moment, uniquement DEFI_MATERIAU, on ne l'appelle pas dans
  | la fonction générique `exec_` mais uniquement dans celle de DEFI_MATERIAU.
  | Dans `exec_`, la syntaxe a déjà été vérifiée, `replace_enum` le suppose.
  | 
  | C'est ensuite au développeur de savoir que ECROUISSAGE contient maintenant des entiers ou des réels.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : vocab01a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29453 DU 13/01/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION 15.2)
- TITRE : Simplifier la gestion d'un matériau
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | Le but de cette fiche est de réfléchir à comment simplifier la gestion des matériaux dans asterxx avant la fusion avec aster_legacy
  | 
  | 
  | Développement
  | ------------------------
  | Plusieurs modifications pour permettre de faciliter la gestion des loi de comportement
  | 
  | 1) on ajoute un enum dans le catalogue de DEFI_MATERIAU pour convertir automatiquement les chaines de caractères en réel à la création du matériau. On supprime ainsi les classes c++ suivantes:
  | BETON_DOUBLE_DP, BETON_RAG, CABLE_GAINE_FROT, DIS_ECRO_TRAC, DIS_CHOC_ENDO, ELAS_META, ELAS_META_FO, RUPT_FRAG, RUPT_FRAG_FO, CZM_LAB_MIX
  | 
  | 2) A la place de spécialiser des classes pour créer une courbe de traction ou d'enthalpie, je spécialise un peu MaterialPropertyClass en rajoutant les méthodes
  | - hasTractionFunction et hasEnthalpyFunction (faux par défaux). Dans le cas de TRACTION, META_TRACTIO et THER_NL, il suffit de spécifier que cette classe à une fonction à générer. C'est automatiquement à la création du matériau maintenant. On supprime les classes C++ suivantes:
  | TRACTION, META_TRACTIO et THER_NL
  | 
  | --> Il n'y a plus de classe spécifiques. Uniquement MaterialPropertyClass maintenant, ce qui rend plus lisible le code maintenant
  | 
  | 3) Je supprime le contenu de l'op0005 de DEFI_MATERIAU car c'est du pur python maintenant

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : astest/*
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 30191 DU 31/08/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 15.2)
- TITRE : En version 15.2.8 - révision cf8271850e61, les cas-tests vérification ssnv251f , ssnv254e et ssnv250b sont en FPE sur Scibian et clap0f0q
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | En version 15.2.8 - révision cf8271850e61, les cas-tests vérification ssnv251f , ssnv254e et ssnv250b sont en FPE sur Scibian et clap0f0q
  | 
  | 
  | Correction
  | ------------------------
  | 
  | J'applique la correction d'Eric: TE0545 crée une variable CODRET et la transmet aux routines appelées. A la fin, CODRET n'est 
  | recopié dans ZI(ICORET) que si l'adresse existe.
  | 
  | Les tests ssnv251f , ssnv254e et ssnv250b marchent maintenant
  | 
  | A reporter en v14

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv251f , ssnv254e et ssnv250b
- NB_JOURS_TRAV  : 0.5

