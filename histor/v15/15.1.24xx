==================================================================
Version 15.1.24 (révision 5ad8b33180c3) du 2020-06-15 15:01 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29925 PIGNET Nicolas           ssnv256a nook sur eole
 29927 PIGNET Nicolas           ssnl133a est nook sur eole
 29929 PIGNET Nicolas           sdnd123a est nook sur eole
 29932 PIGNET Nicolas           wtna101d, wtnv136c et wtnv100d en erreur
 29931 PIGNET Nicolas           zzzz337b est nook sur gaia
 29950 PIGNET Nicolas           OBSERVATION reste bloqué
 29958 PIGNET Nicolas           wtnv135a est nook
 29961 PIGNET Nicolas           sdls139a est nook sur eole
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29925 DU 29/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : ssnv256a nook sur eole
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | le test ssnv256a est nook sur eole.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Il y a aussi ssnv256b sur sciabian qui est nook.
  | 
  | Le modèle est GRAD_VARI et les résultats sont déjà assez sensibles car il y a des TOLE_MACHINE.
  | 
  | Le problème comprend plus de Lagrange (CL) que de DDL physiques. Je remplace tous les AFFE_CHAR_MECA par AFFE_CHAR_CINE et ça
  | corrige les nook et ça accélère le cas test. -> La matrice doit être mal conditionnée.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv256a, ssnv256b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29927 DU 29/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : ssnl133a est nook sur eole
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | ssnl133a est nook sur eole.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | C'est un test avec des poutres en grandes rotations. 
  | 
  | Le problème est difficile à résoudre car il converge mal (c'est marqué au début).
  | 
  | Si on change le solveur de LDLT à MUMPS, on s’arrête en erreur car la matrice est singulière.
  | 
  | Le remplace donc AFFE_CHAR_MECA par AFFE_CHAR_CINE et je passe de LDLT à MULT_FRONT. (pour MUMPS, il faut désactiver toutes les
  | vérifications mais sinon ça marche). LDLT manque de précision ici.
  | 
  | Ca redevient ok;

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnl133a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29929 DU 29/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : sdnd123a est nook sur eole
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | le sdnd123a est nook sur eole
  | 
  | 
  | Correction
  | ------------------------
  | 
  | 
  | C'est un test avec CALC_STABILITE qui est connu pour donner des résultats variables. 
  | 
  | Il y a déjà un TOLE_MACHINE et un commentaire pour corriger cela.
  | 
  | Je mets à jour la valeur de non-regression avec celle d'eole (aster5 avant).
  | 
  | Ca corrige le test sur toutes les machines ainsi.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnd123a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29932 DU 29/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : wtna101d, wtnv136c et wtnv100d en erreur
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Les tests wtna101d, wtnv136c et wtnv100d sont en erreur sur toutes les machines (tests parallèles)
  | 
  | Ils semblent être bloqués dans stat_non_line.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Je pense que ces tests non jamais marchés dans asterxx. C'est le nouveau bilan des tests avec junit qui semble plus complet
  | 
  | Le problème est dans typmat pour déterminer si la matrice est symétrique ou non. Le bug se produisait uniquement si la matrice était
  | non-symétrique et que les matr_elem étaient dans un ordre bien particulier en parallèle.
  | 
  | On restait bloquer dans une communication bloquante dans une boucle do car un proc était sorti de la boucle car la matrice était
  | détectée comme non-symétrique alors que l'autre proc continuait la boucle (pas de matrice non-symétrique détecté pour le moment).
  | 
  | Il faut synchroniser l'information sur la matrice pour que tout le monde sorte en même temps de la boucle.
  | 
  | Avec ça, les tests sont corrigés.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : wtna101d, wtnv136c, wtnv100d
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29931 DU 29/05/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 15.1)
- TITRE : zzzz337b est nook sur gaia
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | le test zzzz337b est nook sur gaia
  | 
  | Correction
  | ------------------------
  | 
  | Il y a un test sur 40 qui est nook. Ce test avait déjà un TOLE_MACHINE à 2e-6. Je l'augmente à 3e-6 pour que le test soit ok
  | (2.51e-6 sur gaia).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz337b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29950 DU 05/06/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : OBSERVATION reste bloqué
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Observation reste bloquer dans dyna_vibra si les noeuds ne font pas partie d'un des maillages.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | On détecte bien que certains maillages n'ont pas le groupe où faire l'observation mais on ne traite pas correctement ensuite s'il
  | n'a pas d'élément.
  | 
  | Si aucun noeud ou élément n'est présent dans le maillage courant, on fait comme s'il n'y a pas d'observation sur le proc courant.
  | 
  | 
  | Je modifie xxParallelLinearTransientDynamics001a pour rajouter OBSERVATION dans dyna_vibra
  | 
  | 
  | Rmq: pour ce prémunir de ces situations dans le futur, il faudra passer beaucoup de tests séquentiels en parallèles distribués car
  | la couverture du code doit pas être géniale

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : xxParallelLinearTransientDynamics001a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29958 DU 09/06/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : wtnv135a est nook
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | le test wtnv135a est nook sur différentes machines (et de manière variable).
  | 
  | L'écart se produit dès que le comportement n'est plus linéaire (à partir du 5eme pas de temps).
  | 
  | Valgrind ne détecte rien mais il y a quelque chose de suspect.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Je n'ai pas trouvé de problème dans le code. Le problème semble venir d'une dégénérescence progressive.
  | 
  | Il y a 1123 pas de temps et l'écart ne fait que croître entre les machines. Quelques points à noter:
  | 
  | - Passer de affe_char_meca à affe_char_cine rend le test NOOK sur ma machine (OK avant) ce qui sous-entend un problème pas très bien
  | conditionné.
  | - Si on regarde, l'estimation de l'erreur donnée sur le calcul de la solution par mumps, il y a jusqu'à un facteur 10 entre les
  | machines. De plus, il y a une différence de un sur le nombre de non-zéro dans la matrice dès le début. (Il y a un élément dans le
  | maillage ça permet de comparer facilement la matrice). Donc, j'ai bien peur que c'est des erreurs d'approximation successives qui se
  | cumulent.
  | 
  | -> C'est le chemin de convergence qui est différent entre les machines.
  | 
  | Solution:
  | - Passer par affe_char_cine pour imposer les conditions aux limites pour éviter la perturbation par les lagrange
  | - Passer resi_glob_rela de 1e-6 à 1e-8 pour limiter la propagation des erreurs et avoir un système bien convergé (augmentation de
  | 10% du temps de calcul < 60s) 
  | - Forcer les post-traitements dans le solveur avec mumps pour limiter l'approximation sur la solution.
  | 
  | Avec tout ça, on obtient le même résultat sur toutes les machines mais il faut même à jour la valeur de non régression.
  | 
  | J'ajoute un petit commentaire dans le cas-test pour expliquer.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : wtnv135a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29961 DU 11/06/2020
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant asterxx (VERSION 11.8)
- TITRE : sdls139a est nook sur eole
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | le test sdls139a est nook sur eole.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Les valeurs testées le sont avec zéro donc c'est facile d'être nook
  | 
  | C'est la résolution dans CALC_MODES qui est imprécise. 
  | 
  | Je modifie les paramètres du solveur. Je force les post_traitements, je passe à resi_rela=1e-8 et ELIM_LAGR='NON'.
  | 
  | Ca améliore considérable les résultats sur toutes les machines (d'un facteur 100)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdls139a est
- NB_JOURS_TRAV  : 0.5

