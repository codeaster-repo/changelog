==================================================================
Version 15.8.10 (révision a61a29db96f6) du 2023-12-12 16:34 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33497 MEUNIER Sébastien        Message alarme RESULT3_12 dans ssna108a
 33171 FERTÉ Guilhem            Acceleration absolue en sortie de REST_GENE_PHYS
 33334 NGUYEN Thuong Anh        Ajoute cas-test mult-appui décorrélé avec pseudo-mode
 33485 ABBAS Mickael            CALCUL n'est pas sympa avec l'utilisateur
 33461 SAMIR Zouhair            En version 16.4.16, le cas test ssnv231d est en erreur NOOK_TEST_RESU sur Cronos
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33497 DU 13/12/2023
================================================================================
- AUTEUR : MEUNIER Sébastien
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Message alarme RESULT3_12 dans ssna108a
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le test ssna108a a l'alarme RESULT3_12 dans la commande IMPR_RESU/FORMAT="MED" dans l'unité 8 :
  | 
  | Le fichier correspondant à l'unité logique renseignée pour l'écriture de résultats au format
  | MED est de type ASCII.
  | Cela peut engendrer l'affichage de messages intempestifs provenant de la bibliothèque MED. Il
  | n'y a toutefois aucun risque de résultats faux.
  | Pour supprimer l'émission de ce message d'alarme, il faut donner la valeur BINARY au  mot-clé
  | TYPE de DEFI_FICHIER.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | J'enlève l'unité 8. IMPR_RESU utilise donc l'unité par défaut, qui est 80.
  | Le test devient OK et non plus ALARM.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssna108a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33171 DU 14/09/2023
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Acceleration absolue en sortie de REST_GENE_PHYS
- FONCTIONNALITE :
  | en v15, on ne reporte que la correction de POST_GENE_PHYS

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.63.31
- VALIDATION : sdld04a + tests d'émission des messages


================================================================================
                RESTITUTION FICHE 33334 DU 03/11/2023
================================================================================
- AUTEUR : NGUYEN Thuong Anh
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ajoute cas-test mult-appui décorrélé avec pseudo-mode
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Ecart des tests en multi-appui décoléré avec pseudo-mode entre Aster (ancien) et Ansys car bug dans la prise en compte 
  | du 
  | pseudo-mode.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | - Ajoute d'un cas-test décoléré avec pseudo-mode
  | 
  | - les options à blinder dans aster V15/comb_sism_modal (ancien)
  | 
  | * MULTI_APPUI=’CORRELE’   + COMB_MULT_APPUI =  QUAD  ou non-saisie 
  | * MULTI_APPUI=’DECORRELE’ + MODE_CORR = acce
  | * MULTI_APPUI=’CORRELE’   + COMB_DEPL_APPUI =/ 'LINE' et / 'ABS'
  | * COMB_MODE =_F(TYPE = 'DSC')
  | * CORR_FREQ = 'OUI'
  | * MULTI_APPUI=’DECORRELE’ + OPTION = 'ACCE_ABSOLU'
  | 
  | En version 15
  | =============
  | 
  | On apporte les blindages suivants dans le catalogues de COMB_SISM_MODAL en v15 (plusieurs tests sont modifiés en 
  | conséquence) :
  | 
  | 
  | * MULTI_APPUI=’CORRELE’   + COMB_MULT_APPUI =  QUAD  ou non-saisie
  | => suppression de calculs dans sdld301b
  | 
  | * MULTI_APPUI=’DECORRELE’ + MODE_CORR
  | => suppression de calculs dans sdld30a et sdlx02a
  | => sdll131a dans validation : suppression
  |  
  | * MULTI_APPUI=’CORRELE’   + COMB_DEPL_APPUI =/ 'LINE' et / 'ABS'
  | => pas de tests cassés
  | 
  | * COMB_MODE =_F(TYPE = 'DSC')
  | => suppression de calculs dans sdlx301a, sdll23a, sdld301a, sdld301b, sdll23c et sdll23b
  | 
  | * CORR_FREQ = 'OUI'
  | => suppression de calculs dans sdlx02a et sdlx301a
  | => sdlv136a dans validation, on passe à CORR_FREQ = NON (rien d'autre à changer)
  | 
  | * MULTI_APPUI=’DECORRELE’ + OPTION = 'ACCE_ABSOLU'
  | pas de test présentant les deux mots-clés

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas-test


================================================================================
                RESTITUTION FICHE 33485 DU 08/12/2023
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CALCUL n'est pas sympa avec l'utilisateur
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Lors de l'utilisation de la commande CALCUL dans le test joint,  il y a deux problèmes:
  | 1/ Si DU et U n'ont pas la même numérotation (au hasard: si on oublié de numéroter les Lagrange), ça plante en FPE très salement au fond du Fortran. Il faut
  | mettre FIXE_X dans la commande NUME_DDL
  | 2/ La vérification de la cohérence des variables internes entre l'entrée dans VARI et celles créées dans CALCUL par le comportement semble fausse: même si on a
  | le même nombre de variables internes, l'alarme COMPOR2_23 est émise !
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | 1/ On ajoute une vérification de la cohérence des numérotations des champs DEPL et INCR_DEPL avec une erreur fatale en cas de discordance
  | 
  | 2/ Le problème est dans vrcomp_chck_cmp.F90
  | 
  | En effet, on a le bloc suivant:
  | 
  | ! --------- This element appears or disappears -> no problem
  | if ((nbVariPrev .eq. 0) .or. (nbVariCurr .eq. 0)) then
  |     l_modif_vari = ASTER_TRUE
  |     cycle
  | end if
  | 
  | Le commentaire est faux et le choix l'est aussi.
  | Un nombre de variables internes nul ne veut pas dire que l'élément apparit ou disparait, mais qu'il n'y a pas de variables interens sur l'élément. C'est le cas
  | typique des éléments de bords par exemple.
  | Il ne faut donc pas lever le drapeau l_modif_vari, mais simplement "sauter" cet élément (on garde le cycle)
  | Ca corrige l'alarme COMPOR2_23

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Le test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33461 DU 04/12/2023
================================================================================
- AUTEUR : SAMIR Zouhair
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.4.16, le cas test ssnv231d est en erreur NOOK_TEST_RESU sur Cronos
- FONCTIONNALITE :
  | Problème
  | --------
  | Le test est OK. Cependant, avec valgrind, un "Invalid read" est détecté 
  | dans la routine "lc0000.F90:322".
  | 
  | Il s'agit d'une copie d'un vecteur de contrainte à l'instant précédent 
  | "sigm(1:nsig) = sigm_all(1:nsig)" avec "nsig = 6", alors que "sigm_all" 
  | a été alloué avec une taille de 4.
  | 
  | 
  | Correction
  | ----------
  | On dimensionne sigm_ldc à 6 dans la routine nifism, et on met les éléments 
  | non utilisés à 0 pour les dimensions inférieures à 3 (sinon on a énormément 
  | de warnings valgrind). Ceci ne concerne que SIMO_MIEHE dont la résorption 
  | est prévue.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv231d
- NB_JOURS_TRAV  : 1.0

