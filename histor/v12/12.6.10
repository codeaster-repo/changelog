==================================================================
Version 12.6.10 (révision 1f32a0fa85f3) du 2016-10-06 14:21 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 25638 DU 03/10/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Suite issue25567, vérifier LIRE_FONCTION vs DEFI_FONCTION
FONCTIONNALITE
   Lors de la réalisation de issue25567, on redoutait un comportement suspect de LIRE_FONCTION.
   
   En reprenant les fichiers joints à issue25567, on compare la fonction issue de DEFI_FONCTION (via INCLUDE) et celle produite par
   LIRE_FONCTION. En faisant une bête différence des deux fonctions, l'écart max est de +/-5.e-6.
   
   En effet, les valeurs du fichier lus par LIRE_FONCTION semblent arrondis à une décimale de moins.
   Donc c'est tout à fait possible que les calculs ensuite puissent être différents.
   
   Si on "convertit" les valeurs du DEFI_FONCTION pour les relire avec LIRE_FONCTION, la différence est strictement nulle.
   
   Par ailleurs, ça me semble curieux de prolonger un accélérogramme, et encore plus avec un prolongement linéaire.
   
   Il y avait un problème lors du formatage des messages d'erreur en cas de problème de lecture (erreur du séparateur utilisé par
   exemple) que l'on corrige ici.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    accelero fournis dans la fiche
DEJA RESTITUE DANS : 13.2.14
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25625 DU 28/09/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    OBSERVATION : plantage avec champ ELGA et champ aux noeuds
FONCTIONNALITE
   Problème
   ========
   
   Dans ssnv226a, j'ajoute une observation sur le champ DEPL alors qu'il y en a déjà sur 3 champs ELGA. J'obtiens alors le message
   suivant :
   
      !---------------------------------------------------------------------!
      ! <F> <JEVEUX_26>                                                     !
      !                                                                     !
      ! Objet inexistant dans les bases ouvertes : &&NMCROB.OBSV  4   .MAIL !
      ! l'objet n'a pas été créé ou il a été détruit                        !
      ! Ce message est un message d'erreur développeur.                     !
      ! Contactez le support technique.                                     !
      !                                                                     !
      !                                                                     !
      ! Cette erreur est fatale. Le code s'arrête.                          !
      !---------------------------------------------------------------------!
   
   Solution
   ========
   
   Il y a un problème lorsqu'on crée les observations. Pour des raisons d'efficacité, on crée un LIGREL réduit pour estimer EPSI_ELGA.
   Or ce ligrel ne peut être crée que quand on fait des extraction sur des champs ELGA. On essayait de récupérer la liste des éléments
   sur DEPL alors que ce champ existe sur les noeuds.
   
   De plus, la routine nmextr_ligr était mal programmée: on crée un LIGREL pour tous les champs observés, alors que ce n'est utile que
   pou les champs "calculés" (c'est-à-dire tous ceux qu'on calcule explicitement et qui ne viennent pas naturellement en sortie de SNL
   comme VARI_ELGA et SIEF_ELGA).
   On modifie la programmation
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit+ssnv226a
DEJA RESTITUE DANS : 13.2.14
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25628 DU 29/09/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    XFEM : CALC_MATR_ELEM / MASS_MECA problème
FONCTIONNALITE
   Problème
   ========
   
   Fiche émise dans le cadre de l'AT.
   ---------------------------------
   Concerne les éléments XFEM avec un mix : poutre coque vol
   
   Je réalise actuellement un calcul sur une structure complexe comprenant des éléments poutres, coques et massifs. Lorsque je réalise
   mon calcul transitoire avec la commande DYNA_LINE_TRAN sans éléments XFEM, tout se passe bien. Malgré tout quand j'ajoute les
   éléments XFEM, il y a un blocage dans la commande CALC_MATR_ELEM.
   
   Message d'erreur:
      ! Erreur de programmation.                              !
      !                                                       !
      ! Condition non respectée:                              !
      !     .false.                                           !
      ! Fichier contex.F90, ligne 145  
   
   
   Solution
   ========
   
   Au vu de la programmation actuelle de la routine memame.F90, ce comportement est bien un bug.
   En effet, bizarrement, on a des champs d'entrée différents entre XFEM et pas XFEM. Or, en général, on ne procède pas comme ça, il
   suffit de regarder les autres cas (par exemple merimp) qui utilisent massivement une routine dédiée à XFEM et ajoutée par Sam
   (xajcin.F90)
   On modifie donc la routine MEMAME pour calculer simultanément dans le même modèle des éléments de structure et XFEM.
   
   Validation: 
   1/on a modifié légèrement sdls120b pour mettre des poutres. On ne s'arrête plus dans CALC_MATR_ELEM. Léger NOOK sur la fréquence du
   à la masse supplémentaire des poutres.
   2/ L'étude fournie tourne (JUSQU'À dyna_line_tran parce que je l'ai faite tourner en 13)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdls120b modifié + étude fournie
DEJA RESTITUE DANS : 13.2.14
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25597 DU 19/09/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.6)
TITRE
    Améliorer le message d'arrêt en cas d'incohérence entre modèle et chargement
FONCTIONNALITE
   Suite à la formation de septembre, si l'utilisateur oublie de modifier le nom du modèle, il est arrêté par un ASSERT(mesh.eq.new_mesh).
   
   En branche default, depuis issue18990, le message est clair.
   
   En v12, on affiche maintenant :
   
   """
    Il n'est pas possible d'avoir des maillages différents : MAIL et MAIL2.
   
    Conseil : Vérifier la cohérence entre les objets maillage, modèle, champ de matériau, etc.
   """
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    forma01b en erreur
NB_JOURS_TRAV  : 0.5

