==================================================================
Version 12.4.11 (révision 1b4a50e5bb7d) du 2015-11-19 11:18 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 24393 DU 20/10/2015
AUTEUR : DEGEILH Robin
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    CALC_G : option CALC_K_G
FONCTIONNALITE
   ***** PROBLEME *****
   Fiche émise dans le cadre de l'AT (A.Thibon, CIDEN)
   Retour "erreur numérique" sur le calcul des coefficients des contraintes.
   Nécessite une amélioration au niveau de la remontée d'erreur
   
   
   ***** ANALYSE *****
   Le problème vient d'une mauvaise utilisation de CALC_G par l'utilisateur.
   Dans le fichier de commande on peut lire les lignes suivantes : 
   
   mod=AFFE_MODELE(MAILLAGE=mail,
                   AFFE=_F(TOUT='OUI',
                           PHENOMENE='MECANIQUE',
                           MODELISATION='DKT',),);
   
   cara=AFFE_CARA_ELEM(MODELE=mod,
                       COQUE=_F(GROUP_MA='TOUT',
                                EPAIS=0.01,
                                COQUE_NCOU=2,),);
   
   -> il n'y a que des éléments de structure dans le modèle!
   
   Or la doc U2.05.01 "Notice d'utilisation des opérateurs de mécanique de la rupture pour l'approche classique (élasticité
   non-linéaire)" précise bien à la page 10, dans la section "Domaine de validité en général", ce qui suit :
   
   Fissure maillée : les opérateurs CALC_G et POST_K1_K2_K3 sont disponibles pour toutes les modélisations des milieux continus 2D et
   3D : déformations planes, contraintes planes, 2D axisymétrique et 3D.
   
   -> il faut affecter des éléments isoparamétriques dans AFFE_MODELE pour appeler CALC_G.
   
   
   ***** CORRECTION *****
   Ajout d'une nouvelle routine (cgvcmo.F90) parmi les vérifications des données d'entrée de CALC_G.
   Cette routine compare la dimension de la modélisation avec la dimension topologique des éléments
   en fond de fissure. Si elle est différente, c'est que l'utilisateur a appelé des éléments de structure.
   Une erreure fatale (RUPTURE0_40) arrête alors proprement le calcul et demande à l'utilisateur de ne pas
   utiliser d'éléments de structure.
   
   
   ***** VALIDATION *****
   Tous les cas tests appelant CALC_G ou CALC_THETA.
   Pas de nouveau test.
   Vérification que le cas proposé donne bien l'erreur RUPTURE0_40.
   
   
   ***** IMPACT DOC *****
   Pas de doc impactée. L'information sur les types d'éléments pouvant être utilisés est déjà dans la doc U2.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    Tous les cas tests appelant CALC_G ou CALC_THETA.
DEJA RESTITUE DANS : 13.0.11
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24379 DU 16/10/2015
AUTEUR : DEGEILH Robin
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    [RUPT] En version 13.0.9-4ec12113c343, les cas-tests hplp101a hplp311a sslp103b et sslp314d s'arrêtent en erreur fatale sur Calibre9 et en version 13.0.9-13e4bdb3df8d, les cas-tests hplp310b, sslp314c et sslp314f s'arrêtent en erreur fatale sur Calibre7
FONCTIONNALITE
   ***** PROBLEME *****
   En version 13.0.9-4ec12113c343, les cas-tests suivants s'arrêtent en erreur fatale sur la machine calibre 9:
   - hplp101a
   - hplp311a
   - sslp103b
   - sslp314d
   
   
   ***** ANALYSE *****
   Suite à la fiche concernant le calcul automatique de la direction du champ théta en 2D ([#22224]),
   la routine gcou2d a été modifiée. Auparavant, en FEM, si la direction n'était pas renseignée, une erreur fatale
   intervenait. Avec la modification la routine s'exécute entièrement.
   
   Or, à la fin de la routine, une structure de donnée liée à XFEM est détruite (par un "detrsd"). Le problème est que la variable k8
   contenant le nom de cet objet n'est pas initialisée dans le cas FEM.
   
   
   ***** CORRECTION *****
   Initialisation de la variable par une chaîne de caractère vide en début de routine.
   
   
   ***** VALIDATION *****
   Tous les cas tests appelant CALC_G ou CALC_THETA.
   Pas de nouveau test.
   
   
   ***** IMPACT DOC *****
   Pas de doc impactée.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    Cas tests appelant CALC_G ou CALC_THETA
DEJA RESTITUE DANS : 13.0.11
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24401 DU 22/10/2015
AUTEUR : BOITEAU Olivier
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    CALC_MODES en parallèle => <F> <MODAL_10>
FONCTIONNALITE
   PROBLEME
   ========
      Message d'erreur MODAL_10 pas assez clair.
   
   FONCTIONNALITE
   ==============
      Op. CALC_MODES + //MPI + SOLVEUR=_F(METHODE='MULT_FRONT' ou 'LDLT'
   
   ANALYSE
   =======
      Lorsqu'on utilise cet op en //MPI avec un solveur linéaire différent de MUMPS, comme mentionné ds la
      doc U4.52.02 (schéma fonctionnel 3.12-2), on ne peut utiliser qu'un nombre de processus MPI égale au
      nbre de sous-bandes. Sinon apparaît le message d'erreur MODAL_10.
      Dans le libellé actuel du msg on parle de (nbre de fréquences - 1) et de nbre de processeurs et on peut
      confondre , en lisant un peu vite, ces éléments avec le nbre de fréquences ! Pour éviter tout impair,
      on parle désormais de sous-bandes fréquentielles !
   
   !-------------------------------------------------------------------------------------------------------------------!
   ! <F> <MODAL_10> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! Opérateurs INFO_MODE ou CALC_MODES sur plusieurs sous-bandes, en mode parallèle: . . . . . . . . . . . . . . . . .!
   ! . Le nombre de processeurs, 4, et le nombre de fréquences, 4, sont incompatibles ! . . . . . . . . . . . . . . . .!
   ! . Avec le solveur linéaire MUMPS, ce nombre de processeurs peut être supérieur ou égale . . . . . . . . . . . . . !
   ! . (idéalement proportionnel) au nombre de fréquences - 1. . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . Avec les autres solveurs linéaires ces deux paramètres doivent être rigoureusement égaux. . . . . . . . . . . . !
   ! . Ici le solveur linéaire utilisé est MULT_FRONT. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . Conseils: . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . . * Ajuster le nombre de processeurs et/ou la distribution des sous-bandes et/ou . . . . . . . . . . . . . . . .!
   ! . . . le solveur linéaire en conséquence. . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . . * Sans toucher aux nombres de processeurs et de sous-bandes, vous pouvez profiter quand même du parallélisme. !
   ! . . . Il suffit de le limiter au seul solveur linéaire. Pour cela paramétrer le mot-clé NIVEAU_PARALLELISME à . . !
   ! . . . 'PARTIEL' et choisissez le solveur linéaire MUMPS. . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   !-------------------------------------------------------------------------------------------------------------------!
   
     Ce libellé en fréquence est du au partage de ce msg entre INFO_MODE et CALC_MODES. Ceux propres à CALC_MODES ne parlent
     que de sous-bandes (exemple: MODAL_9).
     Je change le libellé et parle désormais de sous-bandes pour être plus clair.
   
   !-------------------------------------------------------------------------------------------------------------------!
   ! <F> <MODAL_10> . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
   ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! Opérateurs INFO_MODE ou CALC_MODES sur plusieurs sous-bandes, en mode parallèle: . . . . . . . . . . . . . . . . .!
   ! . Le nombre de processeurs, 4, et le nombre de de sous-bandes fréquentielles, 3, sont incompatibles ! . . . . . . !
   ! . Avec le solveur linéaire MUMPS, ce nombre de processeurs peut être supérieur ou égale . . . . . . . . . . . . . !
   ! . (idéalement proportionnel) au nombre de sous-bandes.    . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
   ! . Avec les autres solveurs linéaires ces deux paramètres doivent être rigoureusement égaux. . . . . . . . . . . . !
   
      
   VERIFICATION
   ============
     sdnx101b sur 3 et 4 procs avec MULT_FRONT.
   
   SOURCES IMPACTEES
   =================
   CALM bibfor/op/op0032.F90
   M bibpyt/Messages/modal.py
   M bibpyt/Modal/calc_modes_multi_bandes.py
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    non-régression
DEJA RESTITUE DANS : 13.0.12
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24148 DU 21/08/2015
AUTEUR : MARTIN Alexandre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    X-FEM : bug dans le calcul de la norme L2 du dpélacement avec POST_ERREUR, pour les éléments XHC
FONCTIONNALITE
   Problème :
   ----------
   
   Le calcul de la norme L2 du déplacement avec l'opérateur POST_ERREUR s'appuie sur le
   calcul du champ de déplacement recomposé aux points de Gauss des éléments X-FEM. Ce calcul
   est effectué par te0566. Le champ de déplacement aux points de Gauss comporte le nombre
   maximum de composantes décrites dans le catalogue. Dans le cas des éléments
   multi-fissurés, le nombre de composantes correspond au cas avec le nombre maximal de
   fissures (soit 4). Dans le cas des éléments traversés par une seule fissure, le catalogue
   des éléments avec contact recouvre les éléments standards (XHC) et les éléments cohésifs
   (XHC3).
   
   La formule utilisée dans te0566 pour calculer le nombre de composantes du champ de
   déplacement recomposé aux points de Gauss est la suivante :
   
   !   calcul du nombre de ddl par point de Gauss, pour le champ OUT
       if (nfiss.eq.1) then
   !      cas mono-fissure : le nombre de ddls d'un noeud sommet
          ddlg=ddls
       else
   !      cas mono-fissure : le nombre de ddls pour un element voyant
   !      le nombre maximal de fissures
          ddlg=ndim*(1+nfissmx+nfe)+ddlc
       endif
   
   Elle est correcte pour les éléments XH, mais erronée pour les éléments XHC. En effet, le
   nombre de composantes utilisé est celui des éléments XHC3, puisque ces deux familles
   d'éléments sont décrites dans le même catalogue.
   
   Correction :
   ------------
   
   Il m'apparaît aujourd'hui que plutôt que de chercher à calculer le nombre de composantes
   du champ, il est plus simple et plus robuste d'utiliser la routine tecach :
   
       call tecach('OOO', 'PDEPLPG', 'E', iret, nval=7,&
                   itab=jtab2)
   
   !   calcul du nombre de ddl par point de Gauss, pour le champ OUT
       ddlg=jtab2(2)/jtab2(3)
   
   Ce bug était jusqu'à présent masqué pour les cas-tests restitués. En effet, il s'agit de
   tests de refermeture d'une fissure et où la solution attendue est celle d'une compression
   simple. Dans le cas d'une interface, l'erreur attendue est la précision machine. Dans le
   cas d'une fissure, du fait que les ddls Crack-Tip sont déasctivés en fond de fissure, on
   attend une erreur résiduelle.
   
   Validation :
   ------------
   
   Avec la version actuelle, pour le cas-test ssnv185u, l'erreur relative en termes de la
   norme L2 du déplacement est :
   
   0.00209091297219
   
   Après correction, elle passe à :
   
   4.69373996572E-06
   
   tandis que l'erreur en termes de la norme en énergie est de : 0.000356471568301.
   
   Risque de résultats faux :
   --------------------------
   
   Sans cette correction, le calcul de l'erreur en termes de la norme L2 est faux dans le cas d'un modèle X-FEM + contact présentant
   une seule fissure.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.3.15
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 12.3.15
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas-tests
DEJA RESTITUE DANS : 13.0.12
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24147 DU 21/08/2015
AUTEUR : MARTIN Alexandre
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TITRE
    X-FEM : bug dans la découpe des pyramides quadratiques (P13)
FONCTIONNALITE
   La découpe des éléments X-FEM s'appuie sur la possibilité de découper n'importe quel support géométrique en triangles ou en
   tétraèdres. Dans le cas des éléments quadratiques, cela n'est possible que pour les mailles complètes, i.e. on sait découper un
   HEXA27 mais pas un HEXA20, un PENTA18 mais pas un PENTA15. L'astuce pour traiter un élément incomplet, H20 ou P18, consiste à créer
   les points manquants pour en faire un élément complet et découper cet élément complet. Les coordonnées et les valeurs de la
   level-set des points créés sont obtenus par interpolation des valeurs nodales.
   
   Lorsqu'on recherche les points d'intersection entre l'iso-zéro de la level-set et les arêtes du maillages (routine xinter), on
   recherche le zéro de la level-set le long de l'arête en utilisant la méthode de Newton. La variable est un paramètre qui repère le
   point courant dans l'arête et les coordonnées du point sont exprimées dans le repère de
   l'élément de référence. On récupère les coordonnées des nœuds de l'élément complet par un appel à la routine xelrex. Cette routine
   encapsule un appel à elraca. La donnée d'entrée est le type de l'élément et elraca est appelé en utilisant l'élément complet, si on
   travaille avec un élément incomplet.
   
   La pyramide quadratique constitue un cas particulier. Il est nécessaire d'ajouter un point au centre de sa base afin de pouvoir la
   diviser en deux tétraèdres quadratiques, mais la pyramide à 14 nœuds n'est pas un élément fini d'Aster. Et le 14ième point n'est pas
   ajouté par la routine xelrex. En conséquence lorsque la routine xinter traite l'arête coupant la base de la pyramide, pour laquelle
   le 14ième point est le nœud milieu, les coordonnées du 14ième point lues n'ont pas été initialisées.
   
   La correction consiste à ajouter le bloc suivant à la fin de la routine xelrex :
   
       if (elrefp.eq.'P13') then
          x(ndime*(14-1)+1)=0.
          x(ndime*(14-1)+2)=0.
          x(ndime*(14-1)+3)=0.
       endif
   
   Les deux cas-tests de la base utilisant des pyramides quadratiques en X-FEM, ssnv186m et wtnv143f, ne permettent pas de mettre en
   évidence ce problème. Ce problème est apparu sur un maillage plus gros, lors d'un travail préparatoire sur la propagation de
   fissures X-FEM en présence d'éléments quadratiques.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas-tests
DEJA RESTITUE DANS : 13.0.12
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24423 DU 27/10/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    A203 - MACR_LIGN_COUPE ne doit pas créer des groupes de noeuds dans le dos de l'utilisateur
FONCTIONNALITE
   Problème :
   ----------
   Quand on modifie le test wtnp116a :
   +
   +DEFI_GROUP(reuse =MAIL_00, .MAILLAGE=MAIL_00,
   + . . . . . CREA_GROUP_NO= _F(GROUP_MA='GM3', NOM='GM3',))
   +
    V_X_00=MACR_LIGN_COUPE(RESULTAT=RESU_00, NOM_CHAM='DEPL', INST=400000.0,
    . . . . . . . . . . .LIGN_COUPE=(
    . . . . . . . . . . . . _F(TYPE='GROUP_MA', .MAILLAGE=MAIL_00, GROUP_MA='GM3',
   
   On se plante dans MACR_LIGN_COUPE avec le message :
   
    . !---------------------------------------------------------------------!
    . ! <F> <SOUSTRUC_37> . . . . . . . . . . . . . . . . . . . . . . . . . !
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . ! .Le groupe de noeuds 'GM3' existe déjà. . . . . . . . . . . . . . . !
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . ! .Conseil : . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
    . ! . . Si vous souhaitez utiliser un nom de groupe existant, il suffit !
    . ! . . de le détruire avec DEFI_GROUP / DETR_GROUP_NO. . . . . . . . . !
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . !---------------------------------------------------------------------!
   
   Analyse :
   ---------
   La commande MACR_LIGN_COUPE recopie le maillage MAIL_00 sous un autre nom (maillage temporaire de travail). Puis elle tente
   d'exécuter DEFI_GROUP / CREA_GROUP_NO mais le groupe de noeuds 'GM3' existe déjà dans le maillage. 
   La commande se plante.
   
   Correction :
   ------------
   Je modifie la commande MACR_LIGN_COUPE pour qu'elle détruise (dans le maillage "copie") le groupe de noeuds avant de le re-créer.
   
   Validation :
   ------------
   Je modifie le test wtnp116a pour "prouver" que la correction fonctionne.
   Je restitue le test modifié.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    wtnp116a
DEJA RESTITUE DANS : 13.0.12
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 24374 DU 15/10/2015
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    POST_RCCM : erreur dans le calcul du Sn de l'option UNITAIRE
FONCTIONNALITE
   Anomalie :
   Dans le cadre du post traitement suivant le rcc-m B3200, les grandeurs Sn (contraintes linéarisées) et Sp (contraintes totales)
   interviennent dans le calcul du facteur d'usage en fatigue.
   Dans l'option B3200_UNIT, par souci de gain de temps, les calculs ne sont pas effectués pour tous les instants en entrée mais
   seulement pour des extremas  tminSp et tmaxSp, déterminés sur les contraintes totales Sp.
   Les contraintes linéarisées Sn sont ensuite calculées pour ces deux instants tminSp et tmaxSp lors du calcul de dommage. Cette
   méthode s'avère non conservative puisque Sp et Sn n'atteignent pas nécessairement leurs extremas en même temps. 
   
   Travail réalisé :
   les extremas de sn sont aussi déterminés puis utilisés ensuite dans le calcul de fatigue.
   
   Cas test impacté : rccm01b dans src (deux test_resu à modifier)
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 11.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : R7.04.03, U2.09.03, V1.01.107
VALIDATION
    passage de cas tests rccm*
DEJA RESTITUE DANS : 13.0.11
NB_JOURS_TRAV  : 10.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24290 DU 30/09/2015
AUTEUR : MICHEL-PONNELLE Sylvie
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TMA : Necs
TITRE
    Erreurs avec DEFI_CABLE_BP
FONCTIONNALITE
   Pb
   ====
   Dans le cadre d'un calcul avec des câbles de précontrainte dans Europlexus, un utilisateur est arrêté pour cause d'erreur fatale
   avec  l'utilisation de
   DEFI_CABLE_BP sur un maillage ( pas d'erreur en STA12.2) en STA12.4/v13.0
   
   
   Travail effectué:
   ================
   
   1- Amélioration de la projection des nœuds de câble sur les segments ou sur les nœuds dans le cas des géométries concaves.
   
   Compte-tenu de l'étude fournie, certains seuils de tolérance étaient trop sévères ou mal adaptés. On passe d'un critère local à la
   maille à un critère qui mixe le local à la maille et le global au câble.
   
   2- On ajoute également un critère pour éviter la projection des nœuds sur des mailles trop éloignées (c'était le cas de plexu04a au
   niveau des ancrages). Désormais une maille candidate sera exclue si la distance entre le nœud et la maille est plus grande que la
   distance du nœud de câble avec son nœud de béton le plus proche.
   Avant cette correction, en imprimant les tables issues de DEFI_CABLE_BP dans plexu04a, on pouvait trouver dans la colonne
   EXCENTRICITE des valeurs à 0.75. Avec la correction, ces EXCENTRICITES sont comme les autres autour de 0.25.
   
   3- Avec ces corrections, l'étude fournie s'arrête toujours dans DEFI_CABLE_BP. Il s'agit en fait d'une zone où les mailles de béton
   sont trop déformées.
   On enrichit le message d'erreur pour avertir l'utilisateur de cette possibilité en cas d'échec de la projection d'un nœud de câble.
   
   La même étude avec un maillage régulier ne s'arrête plus dans DEFI_CABLE_BP.
   
   
   Tests :
   
   3 valeurs à changer dans zzzz111a et b sur des indices de projection qui passe de 14 à 12 (il s'agit d'une projection sur un côté,
   cet indice change selon que l'algo choisit la maille d'un côté ou de l'autre du segment).
   
   plexu04a : les valeurs sont modifiées avec la correction des mauvaises projections.
   
   Tous les autres tests passent.
   
   Impact doc :
   
   V1.01.111
   V5.06.109
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V1.01.111, V5.06.109
VALIDATION
    étude fournie + tests DEFI_CABLE_BP
DEJA RESTITUE DANS : 13.0.11

--------------------------------------------------------------------------------
RESTITUTION FICHE 24447 DU 02/11/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Y-a-t-il d'autres erreurs comme celle de issue24068 ?
FONCTIONNALITE
   Problème :
   ----------
   
   Suite à issue24068, je me pose la question :
    Y-a-t-il d'autres mots clés facteurs répétables pour lesquels on fait, par erreur, un 'call getvxx(MFAC, MSIMP, iocc=1, ...)' ?
   
   Réponse :
   =========
   
   Déjà traité :
   ~~~~~~~~~~~~
   
   * issue24451 :
   /postrele/rc32r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rc32r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rcZ2r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rcZ2r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   
   * issue22290 :
   /algorith/mdveri.F90: call getvtx('CHOC','SOUS_STRUC_1',iocc=1,nbval=0,nbret=n1)
   /algorith/mdveri.F90: call getvtx('CHOC','NOEUD_2',iocc=1,nbval=0,nbret=n1)
   /algorith/mdveri.F90: call getvtx('CHOC','SOUS_STRUC_2',iocc=1,nbval=0,nbret=n1)
   
   /algorith/mdtr74.F90: call getvis('PALIER_EDYOS','UNITE',iocc=1,scal=unitpa,nbret=n1)
   
   
   * issue24068 :
   /algorith/chrpel.F90: call getvr8('AFFE','VECT_X',iocc=1,nbval=3,vect=vectx,&
   /algorith/chrpel.F90: call getvr8('AFFE','VECT_Y',iocc=1,nbval=3,vect=vecty,&
   ...
   /algorith/chrpno.F90: call getvr8('AFFE','VECT_X',iocc=1,nbval=3,vect=vectx,&
   /algorith/chrpno.F90: call getvr8('AFFE','VECT_Y',iocc=1,nbval=3,vect=vecty,&
   ...
   
   
   Mots clés facteurs que l'on interdit de répéter :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Dans le catalogue de la commande, on supprime : "max='**'"
   
   >>>>> MASSIF
   /commande/simu_point_mat.capy
   /commande/test_compor.capy
   
   >>>>> CONVECTION
   /commande/affe_char_ther.capy
   /commande/affe_char_ther_f.capy
   
   >>>>> CYCLIQUE
   /commande/defi_squelette.capy
   /commande/rest_sous_struc.capy
   
   >>>>> PROJ_MODAL
   /commande/dyna_non_line.capy
   
   >>>>> DIAG_MASS
   /commande/defi_base_modale.capy
   
   >>>>> CARA_POUTRE
   /commande/post_elem.capy
   
   >>>>> REPERE
   /commande/crea_maillage.capy
   
   >>>>> MODE_INTERF
   /commande/mode_statique.capy
   
   >>>>> ORTHO_BASE
   /commande/defi_base_modale.capy
   
   
   Appels justifiés avec iocc=1 :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   J'ai regardé les routines suivantes et je me suis convaincu que le "iocc=1" n'était pas écrit par erreur.
   
   /utilitai/x195cb.F90: call getvid('COMB','CHAM_GD',iocc=1,scal=ch1,nbret=ib)
   
   /modelisa/acearp.F90: call getvtx('RIGI_PARASOL','EUROPLEXUS',iocc=1,scal=k8bid,nbret=ibid)
   /modelisa/acearp.F90: call getvtx('RIGI_PARASOL','EUROPLEXUS',iocc=1,scal=k8bid,nbret=ibid)
   
   /algorith/asefen.F90: call getvtx('DEPL_MULT_APPUI','NOM_CAS',iocc=1,nbval=0,nbret=ns)
   /algorith/asveri.F90: call getvtx('DEPL_MULT_APPUI','NOM_CAS',iocc=1,nbval=0,nbret=ns)
   /op/op0109.F90: call getvid('DEPL_MULT_APPUI','MODE_STAT',iocc=1,scal=stat,nbret=ns)
   
   /op/op0153.F90: call getvr8('SECTEUR','ANGL_INIT',iocc=1,scal=zr(idangt),nbret=na)
   
   /utilitai/pecage.F90: call getvtx('CARA_GEOM','SYME_X',iocc=1,scal=symex,nbret=ns1)
   /utilitai/pecage.F90: call getvtx('CARA_GEOM','SYME_Y',iocc=1,scal=symey,nbret=ns2)
   /utilitai/pecage.F90: call getvr8('CARA_GEOM','ORIG_INER',iocc=1,nbval=2,vect=xyp,&
   /utilitai/pecapo.F90: call getvid('CARA_POUTRE','CARA_GEOM',iocc=1,nbval=0,nbret=ntab)
   /utilitai/pecapo.F90: call getvid('CARA_POUTRE','CARA_GEOM',iocc=1,scal=nomtab,nbret=ntab)
   
   /algorith/crtype.F90: call getvid('AFFE','CHAM_GD',iocc=1,scal=champ,nbret=ier)
   /op/op0018.F90: call getvtx('AFFE','PHENOMENE',iocc=1,scal=phenom)
   
   /algorith/refe99.F90: call getvid('RITZ','MODE_MECA',iocc=1,nbval=0,nbret=nbg)
   /algorith/refe99.F90: call getvid('RITZ','BASE_MODALE',iocc=1,scal=resul1,nbret=ibmo)
   ...
   /algorith/ritz99.F90: call getvid('RITZ','BASE_MODALE',iocc=1,scal=resul1,nbret=ibmo)
   /algorith/ritz99.F90: call getvid('RITZ','MODE_MECA',iocc=1,nbval=0,nbret=nbgl)
   ...
   
   /calculel/crpcvg.F90: call getvtx('PERM_CHAM','GROUP_MA_FINAL',iocc=1,nbval=1,vect=nom_gr2,nbret=ibid)
   /calculel/crpcvg.F90: call getvtx('PERM_CHAM','GROUP_MA_INIT',iocc=1,nbval=1,vect=nom_gr1,nbret=ibid)
   
   /utilitai/peritr.F90: call getvtx('RICE_TRACEY', 'OPTION', iocc=1, scal=optcal(1), nbret=np)
   /utilitai/peritr.F90: call getvtx('RICE_TRACEY', 'LOCAL', iocc=1, scal=optcal(2), nbret=nq)
   
   /algorith/rec110.F90: call getvtx('RECO_GLOBAL', 'GROUP_NO_1', iocc=1, nbval=0, nbret=igr)
   /algorith/rec110.F90: call getvr8('RECO_GLOBAL', 'PRECISION', iocc=1, scal=prec, nbret=ibid)
   /algorith/rec110.F90: call getvr8('RECO_GLOBAL', 'DIST_REFE', iocc=1, scal=dist, nbret=ndist)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    rien de particulier
DEJA RESTITUE DANS : 13.0.13
NB_JOURS_TRAV  : 0.4

--------------------------------------------------------------------------------
RESTITUTION FICHE 24491 DU 12/11/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Message CONTACT_22 en formulation CONTINUE
FONCTIONNALITE
   Anomalie :
   Cette alarme n'a de sens que si la pénalisation est activée. Or actuellement elle se déclenche même pour la méthode standard. 
   
   Correction : 
   1. Désormais cette alarme ne se déclenchera que lorsque la méthode pénalisée est activée.
   2. Améliorer le conseil fourni
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    correction du source TE0364,TE0365
DEJA RESTITUE DANS : 13.0.13
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24332 DU 09/10/2015
AUTEUR : FAYOLLE Sebastien
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    OMARISI2016 - Dossier V&V GLRC_DM - SMART2013
FONCTIONNALITE
   Évolution
   ~~~~~~~~~~~
   
   Dans le cadre du projet OMARISI2016, on souhaite développer un dossier V&V de 
   la loi GLRC_DM. Cette fiche correspond à la restitution du benchmark SMART2013.
   
   
   Réalisation
   ~~~~~~~~~~~~~
   
   On restitue la modélisation A du cas test sdnv142.
   En entête du fichier, on ajoute un flag qui prend soit la valeur Benchmark soit la valeur Validation.
   Dans le cas Validation, on effectue le calcul sur les 10 premières secondes du run019 et on fait les TEST_RESU.
   Dans le cas Benchmark, on effectue le calcul sur l'ensemble des run et on réalise les post-traitements.
   
   La doc [V5.03.142] SDNV142 – Benchmark SMART 2008 est mise à jour.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V5.03.142
VALIDATION
    cas test
DEJA RESTITUE DANS : 13.0.11
NB_JOURS_TRAV  : 20.0

