==================================================================
Version 12.5.10 (révision 9b07bf719243) du 2016-04-13 15:19 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 20287 DU 30/01/2013
AUTEUR : DEGEILH Robin
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    RUPT : utiliser les maillages parametriques d'eprouvettes CT dans les cas tests
FONCTIONNALITE
   ***** PROBLEME *****
   On fait évoluer le plugin CT de salome_meca (issue21955). Ce nouveau plugin est restitué en même temps que cette fiche.
   La validation du nouveau plugin CT est faite en générant deux maillages et en les utilisant dans deux cas tests aster : perfe02a et
   ssnv108a.
   Ainsi on teste des valeurs mécaniques et pas simplement un maillage.
   
   Il faut toutefois adapter ces cas tests pour qu'ils soient directement compatibles avec les maillages issus du nouveau plugin CT.
   
   ***** REMARQUE ******
   Pour cette stabilisation, les deux maillages ont été générés en branchant le nouveau plugin CT à salome_meca 2015.2. On prend cette
   version pour être sûr que les maillages réalisés seront lisibles par code_aster, ce qui n'est actuellement pas le cas avec la
   dernière version disponible de salome (7.7.1).
   
   ***** MODIFICATIONS ******
   Les mises en donnée des deux cas tests ont été légèrement modifiées :
   - Remplacement des deux maillages et mise à jour des noms des groupes.
   - Suppression des appels à CALC_THETA suite à sa résorption.
   - Conversion LIST_REEL -> LIST_INST pour éviter Warning dans STAT_NON_LINE.
   - Mise à jour des valeurs de non-régression.
   
   Sur ce dernier point, les valeurs des tests peuvent être différentes. On rappelle que les maillages originaux sont grossiers, les
   nouveaux maillages sont donc à priori meilleurs. J'ai joint une archive contenant des vues des maillages avant/après pour illustrer la
   différence.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V1.03.119, V6.04.108
VALIDATION
    perfe02a, ssnv108a
DEJA RESTITUE DANS : 13.1.14
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24880 DU 25/02/2016
AUTEUR : DELMAS Josselin
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TMA : Necs
TITRE
    CREA_RESU/'MODE_MECA' conduit à une erreur IMPR_RESU/'GMSH'
FONCTIONNALITE
   Problème :
   ========
   En version 13.1, on ajoute un IMPR_RESU/'GMSH' d'un mode dans le cas test sdls121e (ci-joint) et on obtient l'erreur suivante:
   
     IMPR_RESU(INFO=1,
               FORMAT='GMSH',
               RESU=_F(RESULTAT=BASER,
                       TYPE_CHAM='VECT_3D',
                       NOM_CMP=('DX', 'DY', 'DZ'),
                       NOM_CHAM=('DEPL', ),
                       NUME_MODE=(1, ),),
               VERSION=1.2,
               UNITE=80,)
   
      
      !-------------------------------------------------------!
      ! <F> <DVP_1>                                           !
      !                                                       !
      ! Erreur de programmation.                              !
      !                                                       !
      ! Condition non respectée:                              !
      !     zr(ljeveu(i)).ne.rundef                           !
      ! Fichier rsadpa.F90, ligne 152                         !
      !                                                       !
      !                                                       !
      ! Cette erreur est fatale. Le code s'arrête.            !
      ! Il y a probablement une erreur dans la programmation. !
      ! Veuillez contacter votre assistance technique.        !
      !-------------------------------------------------------!
   
   L'erreur disparait si on rajoute le mot clé 'FREQ' lors de la création de la base modale CREA_RESU/'MODE_MECA'.
   La documentation U4.44.12 indique 'l'utilisateur doit indiquer NUME_MODE et FREQ pour chacun des champs', mais il n'y a pas
   d'émission d'erreur ni alarme pour alerter l'utilisation et le cas test sdls121e ne vérifie pas cette 'obligation'.
   
   Correction :
   ==========
   Il faut modifier un argument de rsadpa pour permettre de recopier des valeurs à undef.
   Dans irgmsh, on ajoute la paramètre d'entrée istop=0 à rsadspa dans le cas "FREQ" (on ne fait rien dans le cas "INST"). Avec cela le
   test fourni va au bout.
   On ajoute un IMPR_RESU/GMSH dans sdls121e.
   On rend obligatoire les opérandes FREQ et NUME_MODE dans le cas MODE_MECA.
   
   doc :
   ===
   
   U4.44.12
   Dans la doc, FREQ et NUME_MODE sont tous les deux décrits deux fois : 
   
   FREQ : $3.5.5 et $3.5.9
   NUME_MODE : $3.5.7 et $3.5.9
   
   On réorganise pour que chacun n'apparaisse qu'une fois. 
   
   Autre remarque :
   ==============
   Dans IMPR_RESU/ASTER, NUME_MODE n'est pas écrit si FREQ n'est pas donné dans CREA_RESU.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.44.12
VALIDATION
    sdls121e
DEJA RESTITUE DANS : 13.1.14

--------------------------------------------------------------------------------
RESTITUTION FICHE 24923 DU 09/03/2016
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TMA : Necs
TITRE
    D114 - Erreur SIGM et SIEQ pour DKT avec excentrement
FONCTIONNALITE
   Travail effectué:
   =================
   
   1. Correction de la contrainte de cisaillement dans le cas d'une plaque seule excentrée
      ------------------------------------------------------------------------------------
   - Ajout des explication de la méthode de calcul de la contrainte de cisaillement dans l'épaisseur dans la doc R3.07.03.
   - Correction du terme de d1i dans les routines dk*sie, ds*sie, t3gsie, q4gsie, dktnli :
     --> Avant 
     """ 
        d1i(1,1) = 3.d0/ (2.d0*epais) - zic*zic*6.d0/ ( epais*epais*epais)
     """ 
     --> Correction (après vérifications par Dzifa et veronique)
     """ 
        quotient = (1.d0*zmax*zmax*zmax-3*zmax*zmax*zmin + 3*zmax*zmin*zmin-1.d0*zmin*zmin*zmin)
        a        =  (-6.d0)/quotient
        b        =  (6.d0*(zmin+zmax))/quotient
        c        =  (-6.d0*zmax*zmin)/quotient   
        d1i(1,1) =  a*zic*zic + b*zic + c
   
     """ 
   
   2. Rajout d'un nouveau cas-test :
      ----------------------------
   
   Création du test ssll111o
   
   On modifie "test.comm" (copie de ssll111a) de sorte à valider uniquement une plaque homogène excentrée et une pseudo-plaque composite
   excentrée formée de deux couches identiques. Après avoir corrigé et lancé le test, les résultats de SIXZ entre ces deux méthodes
   sont identiques. L'avantage de ce test est donc de vérifier les contraintes de cisaillement à la fois pour les composites et les
   plaques simples. La plaque homogène sert de référence AUTRE_ASTER pour la plaque composite. 
   
   3. Conseils d'utilisation
      ----------------------
   Ajout de précisions d'utilisation et de post-traitement de SI*Z dans la doc U2.02.01
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U2.02.01, v3.03.111, R3.07.03
VALIDATION
    submit + ssls111o
DEJA RESTITUE DANS : 13.1.14

--------------------------------------------------------------------------------
RESTITUTION FICHE 24974 DU 24/03/2016
AUTEUR : HENDILI Sofiane
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    SPAR2 - Option de calcul REST_ECRO : extension à la loi VMIS_CIN2_CHAB
FONCTIONNALITE
   Proposition :
   =============
   
   L'option de calcul REST_ECRO permet de modéliser la restauration d'écrouissage en multipliant, à la fin de chaque pas de temps
   convergé de STAT_NON_LINE, les variables d'écrouissage par un coefficient compris entre 0 et 1. Ce coefficient, qui dépend de la
   température, est renseigné par l'utilisateur dans le fichier de commande.
    
   L'utilisation de REST_ECRO est actuellement limitée aux lois VMIS_ISOT_LINE, VMIS_ISOT_TRAC, VMIS_CINE_LINE et VMIS_ECMI_LINE.
   L'objectif de cette fiche est de compléter cette liste de lois de comportement on ajoutant les deux lois VMIS_CIN1_CHAB et
   VMIS_CIN2_CHAB.
    
   
   Réalisations :
   ==============
    
   1) Programmation : 
   ------------------
   
      - La liste des lois compatibles avec l'option REST_ECRO est complétée dans le catalogue  c_comportement.capy ;
      - Le te0116 qui réalise l'option de calcul REST_ECRO est modifié afin de prendre en compte les deux nouvelles lois de comportement.
   
   Remarque :
   ==========
   Une correction est également apportée dans le te : le calcul (rcvalb) de la valeur du coefficient de restauration était réalisée
   avant la boucle sur les points de Gauss. Hors, ce coefficient dépend de la température et peut donc varier d'un point de Gauss à
   l'autre. 
   La correction consiste à récupérer la valeur du coefficient en chaque point de Gauss.         
   
   
   2) Validation :
   ---------------
   
   La validation de l'option REST_ECRO est réalisée dans les cas tests hsnv140a/b/c/d/e. Le problème modèle est un essai de
   dilatométrie bloquée (comparaison avec des valeurs expérimentales obtenues pour l'acier 316L).
   Les deux cas tests suivants sont ajoutés en considérant le même problème modèle et les mêmes résultats expérimentaux :
      - hsnv140f : cas où la loi est VMIS_CIN1_CHAB ;
      - hsnv140g : cas où la loi est VMIS_CIN2_CHAB. 
   
   Impact documentaire :
   ===================== 
   
   v7.22.140
   U4.43.01 
   U4.51.11
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : v7.22.140, U4.43.01,  U4.51.11
VALIDATION
    cas test hsnv140f/g
DEJA RESTITUE DANS : 13.1.14
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24898 DU 02/03/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Problème d'archivage en reprise et avec découpe du pas de temps
FONCTIONNALITE
   Problème
   ========
   
   Fiche émise dans le cadre de l'AT (Julie Drouet)
   
   Lors d'une poursuite, l'utilisateur tombe sur le message "objet inexistant dans les bases ouvertes" dans 
   STAT_NON_LINE. Ce message fait suite à celui-ci "Numéro d'ordre inconnu 1007".
   
   La poursuite du calcul à partir d'un état initial et de sa précédente liste d'instants ne devraient pas générer ce 
   genre de problème.
   
   Analyse
   =======
   
   Note liminaire: l'étude est en V11. Cette version n'est plus maintenue, on ne pourra donc pas y corriger le bug. Il faudra donc la
   mettre à jour en V12.
   
   Le problème est assez sioux. On est en mode AUTO pour la gestion du pas de temps.
   
   Lors du calcul initial, on a échoué par manque de temps CPU pendant le processus de découpe du pas de temps. Le dernier calcul
   sauvegardé était:
   
   Instant  8.137812499992e-01 pour le numéro d'ordre 1006
   
   Lors de la reprise, l'instant 8.137812499992e-01 n'étant pas dans la liste initiale (à cause du redécoupage), STAT_NON_LINE prend
   l'instant le plus proche dans la liste prédéfinie, soit 8.14 E-1.
   L'état initial étant bien entendu l'instant précédemment sauvegardé (soit 8.137812499992e-01).
   Évidemment, le calcul échoue et le processus de découpe s'enclenche.
   C'est là que les ennuis commencent.
   
   A l'instant t=8.137539062492e-01, on a convergence mais Aster ne sauvegarde pas le pas de temps. En effet, depuis qu'on a imposé que
   la liste d'instant soit toujours strictement croissante, on a une protection qui empêche de sauvegarder au numéro d'ordre N+1 un
   instant inférieur au numéro d'ordre N.
   Ici : 
   N   => 8.137812499992e-01
   N+1 => 8.137539062492e-01
   La "protection" se déclenche normalement et le pas de temps n'est pas sauvegardé.
   Le calcul continue. Le pas de temps suivant convergé est 8.137578124992e-01
   Une fois de plus, il est inférieur au dernier pas de temps convergé. Mais la protection ne fonctionne pas à cause d'un bug.
   
   Ce bug est le suivant
   
   Lors de l'archivage, une routine utilitaire (dinuar) décide s'il faut archiver (selon les critères utilisateurs et d'autres critères
   internes). Mais la protection "ne pas archiver un instant non croissant" n'est pas faite au même endroit. Or la routine dinuar
   "anticipe" qu'on va réussir l'archivage en incrémentant un compteur pour le prochain archivage.
   
   Voici l'algo:
   Instant 8.137539062492e-01 
     Dans dinuar => on décide d'archiver pour le numéro d'ordre 1007
                    on pré-incrémente le numéro de pas de temps pour le prochain coup => 1008
     Dans nmarch => on a appelé dinuar
                    on décide d'archiver pour le numéro d'ordre 1007
                    on récupère l'instant précédent archivé en regardant à 1006
                    pour 1006, temps = 8.137539062492e-0
                    Comme 8.137539062492e-01 > 8.137812499992e-01, on n'archive pas => il n'y a rien de sauvegardé en 1007
   Instant 8.137578124992e-01
     Dans dinuar => on décide d'archiver pour le numéro d'ordre 1008
                    on pré-incrémente le numéro de pas de temps pour le prochain coup => 1009
     Dans nmarch => on a appelé dinuar
                    on décide d'archiver pour le numéro d'ordre 1008
                    on récupère l'instant précédent archivé en regardant à 1007 <= C'est là que ça buggue (forcément on a pas archivé)
   
   
   Correction
   ==========
   
   Il faut déplacer le dispositif de protection "ne pas archiver un instant non croissant" dans dinuar.
   C'est à faire en V12 et en V13
   
   Remarque: Ce dispositif de protection veut dire que si dans une reprise de calcul, on n'a aucun instants convergé supérieur au
   dernier instant convergé du précédent calcul, on ne pourra pas le reprendre (rien ne sera archivé), y compris en cas de manque de
   temps CPU.
   
   Néanmoins, je ne préconise pas de revenir sur ce dispositif de protection car bcp de choses dans la gestion de la liste repose sur
   ce principe de stricte croissance de la liste d'instants. Par contre, je propose de bien l'expliquer dans la doc.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.51.03
VALIDATION
    l'étude
DEJA RESTITUE DANS : 13.1.14
NB_JOURS_TRAV  : 1.0

