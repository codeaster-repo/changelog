========================================================================
Version 10.1.20 du : 19/04/2010
========================================================================


-----------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR abbas        ABBAS Mickael          DATE 19/04/2010 - 14:10:32

--------------------------------------------------------------------------------
RESTITUTION FICHE 014942 DU 2010-04-15 07:27:40
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TITRE
   XFEM cassxc3xa9 en 10.1.19
FONCTIONNALITE
   90 cas-tests XFEM sont cassés en 10.1.19 à cause de la routine d'interrogation du contenu
   de la liste des charges ISCHAR.
   La raison est un ASSERT mal placé car le cas d'XFEM avec un AFFE_CHAR_MECA uniquement
   constitué de LIAISON_XFEM='OUI' n'était pas prévu (charge nulle).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
   Les 90 cas-tests
NB_JOURS_TRAV  : 0.5
--------------------------------------------------------------------------------
RESTITUTION FICHE 014722 DU 2010-03-04 09:15:46
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TITRE
   En NEW10.1.13, le cas test ssnl127a s'arrete en <S>_Error sur Calibre 4 et Calibre 5
FONCTIONNALITE
   La variable LDCCVG (code retour intégration loi de comportement) n'est pas initialisée
   dans NMFCOR.
   Le code a l'impression de ne pas avoir pu intégrer la loi de comportement, alors qu'en
   fait, il ne l'a même pas intégré !
   
   Je modifie le cas-test pour supprimer l'alarme MECANONLINE5_46
   
      ! <A> <MECANONLINE5_46>                                                   !
      !                                                                         !
      ! -> La définition des paramètres RHO_MIN et RHO_EXCL est contradictoire. !
      !      On choisit de prendre RHO_MIN à RHO_EXCL.                          !
      !   -> Risque & Conseil :                                                 !
      !      RHO_MIN ne doit pas etre compris entre -RHO_EXCL et RHO_EXCL       !
   
   A faire en v9
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
   ssnl127a
NB_JOURS_TRAV  : 2.0
--------------------------------------------------------------------------------
RESTITUTION FICHE 014356 DU 2010-01-04 08:03:17
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TITRE
   En NEW10.1.4, le cas-test ssnp504b s'arrete en erreur_<S> sur calibre4.
FONCTIONNALITE
   En NEW10.1.4, le cas-test ssnp504b s'arrete en erreur_<S> sur calibre4.
   
   S'est corrigé tout seul.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
   rien
NB_JOURS_TRAV  : 0.1
--------------------------------------------------------------------------------
RESTITUTION FICHE 014355 DU 2010-01-04 07:57:20
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TITRE
   En NEW10.1.4, le cas-test ssnp503b s'arrete en erreur_<S> sur Rocks.
FONCTIONNALITE
   En NEW10.1.4, le cas-test ssnp503b s'arrete en erreur_<S> sur Rocks.
   
   S'est corrigé tout seul
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
   rien
NB_JOURS_TRAV  : 0.1
--------------------------------------------------------------------------------
RESTITUTION FICHE 014110 DU 2009-10-27 14:22:53
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TITRE
   'DEPL_CALCULE' et AFFE_CHAR_CINE
FONCTIONNALITE
   En DEPL_CALCULE (ou EXTRAPOL), l'incrément de dépalacement initial n'est pas calculé par
   K.DELTA_U = F mais donné par l'utilisateur -> on ne crée donc pas de matrice K.
   Les algos de contact ne peuvent donc pas travailler ensuite puisqu'il ne peuvent pas
   calculer le complément de Schur (sans matrice K, comment faire ? )
   
   La subtilité, c'est que, d'habitude, on crée quand même une matrice en DEPL_CALCULE pour
   vérifier que les champs sont cinématiquement admissibles, sauf s'il y a du AFFE_CHAR_CINE
   (voir l'alarme MECANONLINE5_48)
   C'est le cas de cet exemple. 
   
   Solution:
   Une évolution consistant à reprojeter les champs cinématiquement admissibles aussi avec
   AFFE_CHAR_CINE.
   La matrice est donc systématiquement construite en prédiction -> plus de bug
   
   Validation:
   Tous les cas-tests faisant du DEPL_CALCULE+PREDICTION
   Modification du zzzz237a (contact avec AFFE_CHAR_CINE) pour valider DEPL_CLACULE avec
   AFFE_CHAR_CINE (+ cotn act, ça ne mange pas de pain): on le coupe en deux, le deuxième
   commence avec un DEPL_CALCULE.
   
   Pour la V9:
   C'est finalement une évolution que l'on propose en v10.
   En v9, il faut interdire la combinaison DEPL_CALCULE+CONTACT+pas de AFFE_CHAR_MECA pour
   les DIRICHLET pour ne pas tomber sur l'objet JEVEUX inexistant.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
   comp002i,perfe02a,sdll133b,ssnv115a,forma03b,pynl01a,sdns106a,ssnv206a,zzzz237a
NB_JOURS_TRAV  : 4.0
--------------------------------------------------------------------------------



-----------------------------------------------------------------------

--------------------------------------------------------------------------------
--- AUTEUR desroches    DESROCHES Xavier       DATE 19/04/2010 - 13:36:58

--------------------------------------------------------------------------------
RESTITUTION FICHE 014888 DU 2010-04-01 08:25:44
TYPE anomalie concernant Code_Aster (VERSION 7.0)
TMA : DeltaCad
TITRE
   En NEW10.1.17 le cas-test sdls114a s'arrete en erreur fatale sur toutes les machines
FONCTIONNALITE
   En NEW10.1.17 le cas-test sdls114a s'arrete en erreur 
   fatale sur toutes les machines. Le message d'erreur est le 
   suivant:
   
   !------------------------------------------------------- 
   <F> <POSTRELE_65>                                         !
    La composante DZ n'existe pas pour le champ de type 
   DEPL  !
   du résultat MODE                                          !
   Cette erreur est fatale. Le code s'arrete.                !
   !-------------------------------------------------------
   
   La vérification est correcte, c'est le fichier de 
   commandes qui comporte une erreur : on demande 
   l'extraction de la composante DZ pour une modélisation 
   C_PLAN.
   On remplace dans POST_RELEVE_T les lignes : 
          NOM_CMP=('DX','DY','DZ',),
   par :  NOM_CMP=('DX','DY',),
   
   Passage du cas-test concerné
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
   sdls114a
--------------------------------------------------------------------------------



========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST MODIF comp002i                        abbas M.ABBAS            342      1      1
 CASTEST MODIF forma03b                        abbas M.ABBAS            342      1      1
 CASTEST MODIF perfe02a                        abbas M.ABBAS            249      1      1
 CASTEST MODIF pynl01a                         abbas M.ABBAS            412      1      1
 CASTEST MODIF sdll133b                        abbas M.ABBAS            368      1      1
 CASTEST MODIF sdls114a                    lebouvier F.LEBOUVIER        342      4      3
 CASTEST MODIF sdns106a                        abbas M.ABBAS            371      1      1
 CASTEST MODIF ssnl127a                        abbas M.ABBAS            456     40     23
 CASTEST MODIF ssnv115a                        abbas M.ABBAS            245      1      1
 CASTEST MODIF ssnv206a                        abbas M.ABBAS            330      1      1
 CASTEST MODIF zzzz237a                        abbas M.ABBAS            398     17      1
 FORTRAN AJOUT algorith/nmpcin                 abbas M.ABBAS             85     85      0
 FORTRAN AJOUT algorith/nmprca                 abbas M.ABBAS            214    214      0
 FORTRAN MODIF algorith/ischar                 abbas M.ABBAS            245      2      4
 FORTRAN MODIF algorith/nmconv                 abbas M.ABBAS            523      1     14
 FORTRAN MODIF algorith/nmimpm                 abbas M.ABBAS            757      2      2
 FORTRAN MODIF algorith/nmprdc                 abbas M.ABBAS            115     11     11
 FORTRAN MODIF algorith/nmprde                 abbas M.ABBAS            161     34    104
 FORTRAN MODIF algorith/nmpred                 abbas M.ABBAS            131     10     11
 FORTRAN MODIF algorith/nmprex                 abbas M.ABBAS            117     10     10
 FORTRAN MODIF algorith/nmresi                 abbas M.ABBAS            353      8     17
 FORTRAN MODIF algorith/op0070                 abbas M.ABBAS            553      5      5
  PYTHON MODIF Messages/mecanonline5           abbas M.ABBAS            222      1     22


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    2         299       299              +299
 MODIF :   21        7032       153     235       -82
 SUPPR :    0           0                 0        +0
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   23        7331       452     235      +217 
