==================================================================
Version 16.0.17 (révision b4c051073930) du 2021-11-25 09:08 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31578 COURTOIS Mathieu         Le test hsnv141a est en erreur dans CREA_RESU #2
 31575 COURTOIS Mathieu         Ne pas prendre toujours COMM_WORLD
 31566 COURTOIS Mathieu         Le test "sdll126e" est NOOK sur scibian en version 16.0.13
 31572 FLEJOU Jean Luc          Couverture de code DEPL_ELGA
 31583 COURTOIS Mathieu         Le test "sdls140b" est en ABNORMAL_ABORT sur cronos et gaia depuis la version...
 31589 COURTOIS Mathieu         Test mac3c04* et mac3c05a en erreur sur Gaia
 31581 ABBAS Mickael            Dégradation des performances d'AFFE_CHAR_MECA en v16.0.16
 31533 ABBAS Mickael            SIMU_POINT_MAT incompatible avec nouvelle prédiction tangente
 31365 ZAFATI Eliass            problème avec "FieldOnNodesComplex "  dans la version 15.4
 31568 COURTOIS Mathieu         De nombreux tests de validation en erreur sur gaia en version 15.4.7 et 16.0.13
 30678 GEOFFROY Dominique       possibilités de réutilisation pour un calcul de fatigue (i.e. sur structure f...
 31562 ABBAS Mickael            Convergence en analyse limite avec défaut plan maillé
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31578 DU 18/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test hsnv141a est en erreur dans CREA_RESU #2
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test hsnv141a est en erreur dans un CREA_RESU pour un problème similaire à issue31543.
  | 
  | On fait un CREA_CHAMP pour combiner 2 champs et la référence au maillage est perdue, avec un plantage dans CREA_RESU.
  | 
  | 
  | Correction
  | ----------
  | 
  | Le problème est dans CREA_CHAMP.
  | Quand on fait CREA_CHAMP/AFFE, le champ produit connaît bien la référence vers le maillage.
  | En revanche, après CREA_CHAMP/COMB, le champ n'a pas la référence vers le maillage.
  | CREA_RESU plante ensuite quand il essaie d'affecter l'attribut 'mesh'.
  | 
  | Correction dans le post_exec de CREA_CHAMP.
  | 
  | Reporter en v15 uniquement la correction dans crea_champ (pas dans crea_resu, c'est une revue de code).
  | 
  | Fiche émise (issue31580) pour revoir create_result/post_exec : create_result ne doit faire que son boulot de créer l'objet résultat de la commande, pas
  | commencer à remplir les attributs !
  | 
  | 
  | Correction de 'waf test ' :
  | En cas de NOOK, le diagnostic affiché en rouge prévalait sur toutes autres erreurs. On affichait 'exit nook' au lieu de 'exit 1'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joint + hsnv141a light
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31575 DU 18/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ne pas prendre toujours COMM_WORLD
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | On a fait en sorte de ne pas indiquer en dur le communicateur COMM_WORLD dans les sources. On doit demander quel est le "communicateur courant".
  | Le problème est qu'on ne peut pas démarrer code_aster autrement qu'avec le COMM_WORLD.
  | 
  | 
  | Correction
  | ----------
  | 
  | La première chose à faire est de ne plus exécuter `aster_mpi_init()` dès l'import du module 'aster' (inclus dans 'libaster') pour avoir la main sur
  | l'initialisation du communicateur.
  | On remonte cet appel lors de l'initialisation de Jeveux, par `libaster.jeveux_init()`. Cette fonction est appelée par `DEBUT` ou `code_aster.init()`.
  | - `DEBUT` utilisera toujours COMM_WORLD.
  | - `code_aster.init(comm=<mpi4py.MPI.Comm object>)` permet de choisir le sous communicateur.
  | 
  | 
  | Validation dans supv003k
  | ------------------------
  | 
  | On simule un cas de "couplage" entre codes où on démarre avec 3 processus MPI.
  | 
  | comm = MPI.COMM_WORLD
  | comm.Split(...)
  | Les procs #0 et #1 seront utilisés par code_aster, le proc #2 par un "superviseur" ou autre code.
  | 
  | On peut voir dans l'entête au démarrage que code_aster ne voit que 2 procs MPI :
  | Parallélisme MPI : actif
  | Rang du processeur courant : 0
  | Nombre de processeurs utilisés : 2
  | 
  | Les procs #0 et #1 lisent un maillage de 6 noeuds chacun. 
  | On récupère la numérotation globale des noeuds.
  | On fait une communication globale (Allgather) sur le sous communicateur.
  | On vérifie qu'il n'y a que 8 noeuds au total.
  | 
  | Le proc #2 définit 2 noeuds bidons.
  | 
  | On fait une communication globale sur le communicateur global.
  | On trouve bien 10 noeuds.
  | 
  | On peut aussi vérifier que 'glob.1' n'existe que sur le procs "code_aster".

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.01.316
- VALIDATION : supv003k + liste verification parallel
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31566 DU 08/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test "sdll126e" est NOOK sur scibian en version 16.0.13
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Suite à la montée de version de MUMPS, le test est légèrement NOOK.
  | 
  | NOOK  NON_REGRESSION   XXXX      5.719758862700E-04     5.719282597044E-04     8.3E-03%      3.0E-03%
  | 
  | 
  | Correction
  | ----------
  | 
  | cf. issue26485 pour l'ajout du test lors de corrections.
  | Les valeurs de non régressions sont toutes relâchées par TOLE_MACHINE (de 3 à 8e-5).
  | On met la valeur partout (1e-4).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdll126e
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31572 DU 16/11/2021
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 16.1)
- TITRE : Couverture de code DEPL_ELGA
- FONCTIONNALITE :
  | C'est en lien avec la fiche issue30125
  | 
  | Le script de couverture de code a détectée une non couverture des "GRILLE_EXCENTRE", c'est pas faux !!!
  | 
  | Le développement fait dans issue30125 ne concerne que les DKT et les GRILLE_EXCENTRE, c'est le même TE.
  | 
  | J'ajoute une modélisation f à zzzz413 :
  | * 'f' : c'est la 'e' avec une duplication d'un groupe de maille et affectation de GRILLE_EXCENTRE

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.413
- VALIDATION : passage cas test + études
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31583 DU 22/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test "sdls140b" est en ABNORMAL_ABORT sur cronos et gaia depuis la version 16.0.13
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | CREA_RESU, OPERATION='KUCV' échoue dans sdls140b, le maillage n'est pas renseigné dans l'objet Result produit.
  | 
  | 
  | Correction
  | ----------
  | 
  | En effet, KUCV n'est pas testé dans les tests de vérification donc il plante !
  | Quand on le corrige, ça plante encore dans CREA_RESU/CONV_RESU. Là, c'est l'utilisation CREA_RESU/CONV_RESU/MATR_RIGI qui n'est pas testée ailleurs.
  | => il faut des tests de verification : autre fiche à émettre.
  | 
  | La correction est simple, le maillage à utiliser est fourni par RESU_INIT sous les mots-clés facteurs CONV_RESU et KUCV.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdls140b
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31589 DU 23/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Test mac3c04* et mac3c05a en erreur sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les tests mac3c04* et mac3c05a échouent sur Gaia car ils testent la présence de matplotlib et dans ce cas, ils essaient de tracer un graphique interactif.
  | 
  | 
  | Correction
  | ----------
  | 
  | On modifie mac3c04a.com1, mac3c04b.com1 et mac3c05a.comm pour tester également la variable d'environnement DISPLAY.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mac4c04* et mac4c05a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31581 DU 22/11/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Dégradation des performances d'AFFE_CHAR_MECA en v16.0.16
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les performances de l'opérateur AFFE_CHAR_MECA se sont dégradées fortement suite à issue31524
  | 
  | Correction
  | ----------
  | 
  | la fiche issue31524 a modifié la programmation de DISMOI (sur les LIGREL), mais on en voit pas d'où peut venir une telle dégradation des performances.
  | Par contre, en faisant du DEBUG, on s’aperçoit qu'AFFE_CHAR_MECA utilise beaucoup la question DIM_GEOM pour les conditions limites dualisées.
  | En effet, la question est posée dans la routine AFDDLI, qui est appelée sous deux niveaux de boucle !
  | En V16.0, on passe 7s environ (perf005f) dans AFFE_CHAR_MECA. On double ce temps suite à issue31524.
  | 
  | En déplaçant la question DISMOI en dehors des boucles, on passe à 0.09s
  | 
  | Ce problème de performance est ancien et explique certainement la sous-optimalité constatée à maintes reprises par nos utilisateurs, en particulier ceux
  | utilisant un grand nombre d'occurrences de mots-clefs facteurs dans AFFE_CHAR_MECA
  | 
  | On reporte en V15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perf005f
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31533 DU 28/10/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : SIMU_POINT_MAT incompatible avec nouvelle prédiction tangente
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans SIMU_POINT_MAT en mode 'POINT', l'appel à une option (sans doute RIGI_MECA_TANG) se passe mal :
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <CALCUL_15>                                                                        ║
  |  ║                                                                                                ║
  |  ║ le paramètre: PCOPRED  n'est pas un paramètre de l'option:                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | En mode 'ELEMENT', il n'y a pas de problème (normal, c'est STAT_NON_LINE qui tourne).
  | 
  | Correction
  | ----------
  | 
  | Dans lc0076, on utilise la routine behaviourOption qui fait un JEVECH 
  | Cette routine ne doit servir que dans les TE, forcément, et c'est pas le cas de SIMU_POINT MAT en mode POINT (qui appelle directement nmcomp)
  | Alors qu'on voulait simplement les flags pour lMatr ...
  | Il vaut donc mieux que faire directement dans la loi:
  |     lVari = L_VARI(option)
  |     lSigm = L_SIGM(option)
  |     lVect = L_VECT(option)
  |     lMatr = L_MATR(option)
  | Avec le bon include pour avoir les define de L_VARI ...
  | #include "asterfort/Behaviour_type.h"

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31365 DU 02/09/2021
================================================================================
- AUTEUR : ZAFATI Eliass
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : problème avec "FieldOnNodesComplex "  dans la version 15.4
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Après la création d'un champ complexe du déplacement avec CH=CREA_CHAMP(...), la méthode CH.EXTR_COMP(...) n'est pas disponible 
  | pour la version 15.4 du code_aster. Pourtant, la méthode EXTR_COMP est disponible pour les champs réels.
  | 
  | Ce problème est bloquant pour corriger les cas test cassés de l'outil MT lorsqu'on fait la stabilization de la version 15.4 pour 
  | remplacer V14
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Après analyse EXTR_COMP existe uniquement pour FieldOnNodesReal et pas pour  FieldOnNodesComplex. Il existe plusieurs manières pour le faire mais la plus
  | simple est d'utiliser (comme FieldOnNodesReal) la fonction 'aster_prepcompcham' dans aster_module.c qui à son tour utilise la routine  prcoch.F90. 
  | 
  | J'ajoute une vérification dans zzzz505a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz505a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31568 DU 08/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : De nombreux tests de validation en erreur sur gaia en version 15.4.7 et 16.0.13
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | De nombreux tests échouent car ils sont tués par Slurm sur Gaia pour "Exceeded job memory limit".
  | 
  | 
  | Correction
  | ----------
  | 
  | On modifie le plugin qui ajuste les paramètres de job sur Gaia.
  | On fait au maximum comme pour Cronos.
  | 
  | - On ne passe plus systématiquement en exclusif si 'cpu_mpi > 1' mais si 'node_mpi > 1 or cpu_mpi >= 6'.
  | 
  | - Si la mémoire demandée par noeud (case mémoire x cpu_mpi) < 160 GB, on sélectionne la partition 'cn' sinon 'bm'.
  | 
  | - Sur 'bm' et 'exclusive', on fixe la limite maxi pour Slurm à 350 GB.
  | 
  | - Si 'ncpus' (nombre de threads) n'est pas renseigné, on prend 1 (et non 6 comme auparavant).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : plugin gaia
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 30678 DU 05/02/2021
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : aide utilisation concernant code_aster (VERSION 15.4)
- TITRE : possibilités de réutilisation pour un calcul de fatigue (i.e. sur structure fissurée) d’un résultat de calcul mécanique réalisé sur la structure saine tronquée
- FONCTIONNALITE :
  | Probleme
  | ===========
  | 
  | Quelles sont les possibilités de réutilisation pour un calcul de fatigue (i.e. sur structure fissurée) d’un résultat de calcul mécanique, réalisé 
  | sur la
  | structure saine, mais dont on ne connait les résultats que sur une partie de cette structure ?
  | Voici le problème en question :
  |  
  | •         OBJET DE L’ETUDE
  |  
  | Il s’agit ici de la modélisation avec code_aster d’une roue Pelton et de son chargement, pour le calcul en fatigue de ses augets (dans le cadre du 
  | projet
  | SSURRHYMI sur installations hydro-électriques).
  | Ci-joint un exemple de roue Pelton, muni de 23 augets.
  |  
  | Le chargement vu par la roue :
  | - L’effort centrifuge lorsque la roue est en rotation à vitesse nominale
  | - L’effort d’impact lorsque l’auget subit l’impact du jet (la roue est muni de plusieurs injecteurs)
  | Le fabriquant a réalisé des calculs avec ces deux chargements unitaires.
  |  
  | La fissure se situe en pied d’auget (trait rouge sur l’attache auget/roue sur la figure ci-jointe).
  |  
  | •         OBJECTIF ERMES
  |  
  | Fournir la valeur de ΔK = Kmax – Kmin, pour différentes profondeurs de fissures,
  | où Kmax et Kmin sont calculés respectivement aux instants tmax et tmin du transitoire de chargement,
  | instants déterminés par l’atteinte des valeurs max et min de la contrainte principale sur la surface en pied d’auget    
  |  
  | •         LES DONNEES D’ENTREE DONT NOUS DISPOSONS :
  |  
  | Nous n’avons pas accès à la géométrie complète de la roue Pelton avec ses augets pour réaliser les calculs mécaniques.
  | Le fabriquant ne nous fournit qu’une géométrie partielle, dans laquelle la géométrie des augets est tronquée (cf. Figures jointes).
  |  
  | Nous disposons d’un fichier med contenant le maillage tronqué non fissuré, et les champs de contraintes et de déplacement sur ce maillage, aux 
  | instants tmin et
  | tmax.
  |  
  | 
  | QUESTION : 
  | Connaissant les contraintes et/ou le déplacement sur le maillage non fissuré tronqué (et donc aussi sur la frontière tronquée de l’auget), peut-on 
  | retrouver
  | l’état de contrainte sur la structure fissurée ?
  | Autrement dit, comment pallier le manque de données sur le maillage sain pour réaliser le calcul de K sur le maillage fissuré ?
  | 
  | Solution
  | ===========
  | En absence de retour de la personne ayant émis la fiche, on classe la fiche.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31562 DU 04/11/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant code_aster (VERSION 15.4)
- TITRE : Convergence en analyse limite avec défaut plan maillé
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Afin de pouvoir compléter le chapitre III de l'annexe 5.3 du RSE-M, nous voulons mener des études paramétriques de type analyse limite sur un tuyau droit en
  | présence d'un défaut plan longitudinal débouchant en paroi interne ou externe.
  | Il s'agit d'analyse limite par borne inférieure, sous chargement de pression interne.
  | On en déduit alors le coefficient qp qui est le coefficient de correction de section par rapport à la structure saine.
  | Actuellement on évalue de manière enveloppe ce coefficient comme valant (t-a)/t avec t l'épaisseur et a la hauteur du défaut.
  | 
  | On a donc généré des maillages avec défaut maillé avec ZCracks.
  | 
  | Les calculs en analyse limite montrent alors des grosses difficultés de convergence et on n'arrive pas à les pousser suffisamment. Les calculs s'arrêtent donc
  | trop tôt : on est encore loin de la pression limite.
  | On peut le constater en observant la déformation plastique cumulée au pas ultime : elle reste très confinée au niveau du front de fissure semi-elliptique et les
  | valeurs maximales sont faibles : de l'ordre de 6% (ce qui est peu en analyse limite normalement). Cela mène à penser que le calcul pourrait aller plus loin.
  | Dans le cas d'un tuyau sain le calcul se passe bien : on retrouve les résultats analytiques (Plim=Sh.ln(Ri/Re) avec Sh la contrainte d'écoulement du modèle
  | plastique parfait, avec le critère de Tresca).
  | 
  | On a testé différentes choses (sachant qu'on est déjà en pilotage de type longueur d'arc) :
  | -utilisation de PAS_MINI_ELAS,
  | -définir une pente d'écrouissage non nulle (1/1000 du module d'Young),
  | -test de différents groupe pour le pilotage,
  | -paramètres du pilotage,
  | -stratégie de sous-découpage du pas...
  | Mais ça diverge toujours trop tôt.
  | 
  | Éléments de réponse
  | -------------------
  | 
  | Pour l'analyse limite, on utilise plutôt la méthodologie décrite dans u2.05.04, avec un comportement de Norton-Hoff et un pilotage spécifique.
  | 
  | Plastifier toute la structure est assez sévère, on recommande l'usage d'éléments finis ne présentant pas de verrouillage numérique intempestif (c'est d"ailleurs
  | une recommandation faite dans U2.05.04)
  | 
  | Les formulations INCO_UP et INCO_UPG (la première étant moins coûteuse que la seconde) sont une première piste. Malheureusement, le maillage contient des
  | PYRAM13 que ces éléments ne savent pas traiter. En modifiant le maillage, on atteint 11,6582 MPa.
  | 
  | On peut également utiliser une formulation 3D_SI, moins robuste en général, mais sur cette situation, avec des déformations raisonnables, le résultat est bon.
  | Par ailleurs ces éléments fonctionnent sur des PYRAM13 sans problèmes
  | 
  | Pour le cas étudié on a une limite supérieure à 12.1629 MPa et une limite inférieure estimée à 9.72802 MPa (instant valant 2,5156s). La pression limite (borne
  | inf) que l'on obtient en 3D_SI, c'est 12,059 MPa.
  | 
  | Pour s'assurer que tout se passe bien, on recommande de vérifier qu'il n'y a pas d'oscillations sur les contraintes, par exemple
  | calculer SIEQ_ELGA/VMIS_SG qui ne doit pas présenter de changements de signes intempestifs.
  | 
  | NB: sans pyramides, un calcul avec HHO pourrait donner des résultats intéressants. A tenter coté R&D ?

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : étude UTO
- NB_JOURS_TRAV  : 2.0

