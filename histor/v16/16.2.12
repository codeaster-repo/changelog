==================================================================
Version 16.2.12 (révision 43893a3f814e) du 2022-10-25 14:33 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32324 PIGNET Nicolas           Supprimer la condensation statique de HHO
 32338 PIGNET Nicolas           MECA_STATIQUE : rendre le mot-clé facteur EXCIT obligatoire ?
 32316 GANTIER Maxime           [RUPT] DEFI_FOND_FISS : bug dans le vecteur .BASLOC
 32231 GENIAUT Samuel           [RUPT] Revue du cas-test FORMA05 de la formation rupture
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32324 DU 14/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Supprimer la condensation statique de HHO
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | La condensation statique nous pose plus de problème qu'autre chose donc je propose de STAT_NON_LINE. En plus je vais faire en sorte que ça marche dans
  | MECA_NON_LINE comme ça
  | 
  | Correction/Développement
  | ------------------------
  | 
  | J'ai supprimé la condensation statique de STAT_NON_LINE ce qui permet de simplifier pas mal la programmation et de se mettre dans un cadre plus standard. On
  | peut en plus l'utiliser dans MECA_NON_LINE maintenant (sans certains options dans les TE)
  | 
  | J'en profite pour changer les fonctions de bases pour être plus robuste à l'aspect ratio
  | 
  | Il faut changer pas mal de valeurs de non-régression car le critère de convergence change.
  | 
  | Rmq: Il faudra voir comment passer des paramêtres en plus dans le c++

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests HHO
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 32338 DU 21/10/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : MECA_STATIQUE : rendre le mot-clé facteur EXCIT obligatoire ?
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Aussi étrange que cela puisse paraître, le mot-clé EXCIT de l'opérateur MECA_STATIQUE n'est pas obligatoire (il est facultatif donc :-) ).
  | 
  | En formation, 1/3 des stagiaires a oublié de renseigner ce mot-clé et cela a provoqué un plantage par pivot nul dans la matrice de raideur. Un mauvais blocage
  | des modes rigides a été soupçonné (à tort) et on a passé beaucoup de temps avant de s'apercevoir de l'oubli de EXCIT.
  | 
  | Je ne vois pas de raison à ce que ce mot-clé ne soit pas obligatoire dans MECA_STATIQUE. 
  | - S'il y en a une -> l'indiquer clairement dans la doc U4 de MECA_STATIQUE. Et éventuellement émettre une alarme si l'utilisateur ne le renseigne pas. (*)
  | - S'il n'y en a pas -> le rendre obligatoire
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Il y en n'as pas besoin avec AFFE_CARE_ELEM/RIGI_SOL. On oblige la présence de EXCIT s'il n'y a pas de CARA_ELEM

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32316 DU 10/10/2022
================================================================================
- AUTEUR : GANTIER Maxime
- TYPE : anomalie concernant code_aster (VERSION 16.4)
- TITRE : [RUPT] DEFI_FOND_FISS : bug dans le vecteur .BASLOC
- FONCTIONNALITE :
  | Problème
  | -----------------
  | Dans l'exemple à l'origine de cette fiche, les vecteurs normaux et/ou de propagation issus de 
  | DEFI_FOND_FISS sont faux pour 17 noeuds du front de fissures. 
  | 
  | 
  | Correction
  | ------------------------
  | L'origine de cette erreur se trouve dans la routine gdire3.F90, qui calcule et oriente le vecteur 
  | de propagation pour un segment du front à partir de trois noeuds (deux noeuds du segment du front, 
  | un noeud d'une maille de la lèvre). C'est le signe d'un produit scalaire entre deux vecteurs qui 
  | permet d'orienter le vecteur de propagation. 
  | Deux problèmes : 
  |  - les deux vecteurs n'étaient pas normalisés avant le produit scalaire
  |  - il y avait une comparaison à eps = 1e-6 pour déterminer l'orientation. 
  | On corrige en normalisant les vecteurs avant produit scalaire et en comparant à r8prem(). 
  | 
  | Validation : pas pour le moment. Des cas-tests de propagation sont prévus. 
  | 
  | Résultat faux
  | -------------
  | On pouvait avoir des résultats faux pour de la propagation avec CALC_G si les mailles du front 
  | sont de taille caractéristique inférieure ou égale à 1e-6. 
  | 
  | Faux depuis 7.0.1

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 32231 DU 23/08/2022
================================================================================
- AUTEUR : GENIAUT Samuel
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RUPT] Revue du cas-test FORMA05 de la formation rupture
- FONCTIONNALITE :
  | Suite à la nouvelle formation Rupture, on a revu le cas-test FORMA05 "Plaque fissurée en traction" :
  | - Modélisation A : mise en cohérence des noms des groupes avec le reste des modélisations
  | - Modélisation B : Modification de la procédure de maillage de la modélisation B (FEM, plaque entière sans symétrie). Le maillage actuel est obtenu par
  | duplication du maillage de la modélisation A (demi-plaque avec symétrie). On propose dans la formation de créer le maillage (libre) de la plaque entière ->
  | Ajout du script de maillage.
  | - Ajout d'une nouvelle modélisation D : FEM, demi-plaque avec symétrie, maillage rayonnant. Cette modélisation sera utilisée pour le "corrigé" de la formation.
  | - Interversion des modélisations A et B de manière à ce que la seule modélisation traitée par les stagiaires soit la modélisation A. 
  | - modifs des TEST_TABLE : calcul automatique des solutions des références, mise en cohérence des précisions...

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v3.02.111
- VALIDATION : forma05
- NB_JOURS_TRAV  : 3.0

