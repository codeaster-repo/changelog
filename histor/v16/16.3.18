==================================================================
Version 16.3.18 (révision baa0ae2f6336) du 2023-05-04 14:53 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32810 COURTOIS Mathieu         run_ctest : rerun-failed ne fonctionne pas bien
 32801 COURTOIS Mathieu         Gestion des inégalités strictes dans le catalogue
 32813 COURTOIS Mathieu         Le bilan d'un test de validation est toujours OK
 32799 ABBAS Mickael            Plantage dans THER_LINEAIRE en mode DEBUG de calcul
 32691 ESCOFFIER Florian        Evolution CALC_STAB_PENTE (ex CALC_SRM) ajout méthode Bishop Fellenius Spence...
 32636 FLEJOU Jean Luc          En version 16.3.8, le cas test ssnl201i est en erreur NO_CONVERGENCE sur CRON...
 32805 COURTOIS Mathieu         En version 16.3.15, le cas test perf901b est en ABNORMAL_ABORT
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32810 DU 21/04/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : run_ctest : rerun-failed ne fonctionne pas bien
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | 1. run_ctest ... => test cassé
  | 2. correction d'une erreur
  | 3. run_ctest --rerun-failed => le test apparaît encore 'Failed', lancé à la main, il est bien ok.
  | 
  | 
  | Le calcul passe correctement mais le diagnostic est incorrect.
  | Je suspecte que ce soit lié au changement récent pour ne pas écraser le '.mess'.
  | 
  | 
  | Correction
  | ----------
  | 
  | C'est effectivement la correction de l'écrasement du '.mess' en cas de '.comm' multiples qui conduit à cela
  | pour les tests avec un seul '.comm'. Les outputs sont cumulés. Le diagnostic peut donc être incorrect.
  | 
  | Si le '.mess' existe, il faut le supprimer quand on lance le 1er '.comm'.
  | 
  | On peut le vérifier car en cas de '.comm' multiple, on ajoute les paramètres 'step' et 'nbsteps'.
  | On est en train d'exécuter le 1er '.comm' si 'step' est égal à 0 ou non défini.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ahlv100t, zzzz143a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32801 DU 19/04/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Gestion des inégalités strictes dans le catalogue
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | val_min/val_max vérifient au sens large la validité de la valeur d'un mot-clé.
  | On souhaite avoir la possibilité d'exclure les bornes.
  | 
  | 
  | Développement
  | -------------
  | 
  | On ajoute les attributs 'val_min_included' et 'val_max_included' qui prennent les
  | valeurs 'True' (c'est le défaut et correspond au comportement actuel) ou 'False'.
  | 
  | Mis en place dans IMPR_TABLE & IMPR_FONCTION pour les mots-clés GRILLE_X et GRILLE_Y
  | où `val_min=0.0`. On ajoute `val_min_included=False` pour exclure 0.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests modifiés
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32813 DU 24/04/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le bilan d'un test de validation est toujours OK
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans validation, on ne vérifie pas les tests de non régression, valeurs fournis par VALE_CALC,
  | uniquement les tests de référence, VALE_REFE.
  | 
  | Un test doit sortir en erreur NO_TEST_RESU s'il n'y a aucun TEST_xxx.
  | Ce n'est pas le cas aujourd'hui.
  | 
  | 
  | Correction
  | ----------
  | 
  | Avec run_ctest, tout fonctionne correctement (mais les tests sont encore passés avec run_testcases pour les bilans).
  | 
  | run_testcases exécute les cas-tests avec as_run sans passer l'option '--test' d'où le problème.
  | run_testcases/as_run ajoute "service testcase" dans le '.export'.
  | Dans run_aster, on peut donc utiliser cette valeur pour assurer la compatibilité avec as_run.
  | 
  | Avec cette correction, les tests suivants seront en erreur :
  | - ssls13a
  | - ssls14a
  | - ssna124a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : reproduit sur un test court
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32799 DU 19/04/2023
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Plantage dans THER_LINEAIRE en mode DEBUG de calcul
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand on active le debug dans calcul.F90, on plante dans THER_LINEAIRE (en Python) sur un segmentation fault.
  | Par exemple hsnv129a.
  | Bizarrement, ça fonctionne si on compile en DEBUG.
  | 
  | 
  | Correction
  | ----------
  | 
  | 
  | Comment trouver le problème ?
  | 1/ Faire le calcul en NO_DEBUG en activant le flag dbg=.true. dans calcul.F90 => tout va bien
  | 2/ Faire le calcul en DEBUG en activant le flag dbg=.true. dans calcul.F90 => ça plante
  | 
  | Dans le cas 2, on plante en Segmentation Fault dans un appel jeexin de la routine caldbg.F90, un des champs d'entrée de calcul est absent.
  | 
  | Si on WRITE le nom de l'objet, ça répare, évidemment !
  | J'ai donc affiché l'indice de boucle, pour savoir quel champ était concerné, en comparant la version qui marche à celle qui marche pas.
  | 
  | Le bug est là depuis la modification du champ des coordonnées géométriques dans le maillage (issue32604). Ce n'est plus un champ. Les tests exisd dans caldbg
  | échouent donc tous:
  | 
  | call exisd('CARTE', champ, iret)
  | if (iret .gt. 0) ojb = champ//'.VALE'
  | call exisd('CHAM_NO', champ, iret)
  | if (iret .gt. 0) ojb = champ//'.VALE'
  | call exisd('CHAM_ELEM', champ, iret)
  | if (iret .gt. 0) ojb = champ//'.CELV'
  | call exisd('RESUELEM', champ, iret)
  | if (iret .gt. 0) ojb = champ//'.RESL'
  | 
  | Ce qui rend la chaîne ojb non-initialisée ! Le jeexin en dessous échoue donc (parfois ! mais jamais en debug puisque dans ce cas, les variables sont toutes
  | initialisées) sur une chaîne non initialisée.
  | 
  | Je fais deux corrections 
  | 1/ J'ajoute un exisd sur CHAM_GEOM
  | 2/ J'initialise ojb à " "
  | 
  | 
  | NB: quand les champs sont out, ils n'existent pas forcément, on ne peut donc pas s'arrêter si iret.le.0

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsnv124a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32691 DU 14/03/2023
================================================================================
- AUTEUR : ESCOFFIER Florian
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Evolution CALC_STAB_PENTE (ex CALC_SRM) ajout méthode Bishop Fellenius Spencer Morgenstern_Price
- FONCTIONNALITE :
  | Objectif:
  | ========
  | 
  | L'analyse de stabilité des digues et des barrages en remblai est un processus indispensable selon les standards en vigueur dans 
  | l'industrie hydraulique du monde. De ce fait, basée sur la méthode SRM (Strength Reduction Method), la macro-commande CALC_SRM 
  | a été développée et intégrée dans Code_Aster en janvier 2023. Bien que cette méthode soit numériquement simple et s'adapte à la 
  | plupart des circonstances industrielle, ses inconvénients résident dans les 3 aspects suivants :
  | 
  | La pertinence du résultat dépend fortement de la configuration du calcul FEM.
  | Le calcul FEM non-linéaire quasi-statique conduit à un temp de calcul d'autant plus élevé que les paramètres SRM sont mal 
  | configurés. La méthode SRM ne fait pas partie dans les méthodes d'analyse exigées dans certains standards du métier, ce qui 
  | empêche éventuellement son application industrielle.
  | 
  | Il est donc nécessaire de se disposer d'un moyen de vérification et d’une alternative de la méthode SRM dans la macro-commande 
  | CALC_SRM. Ce développement supplémentaire de CALC_SRM consiste à y ajouter les méthodes d'analyse de stabilité basée sur la 
  | théorie d'équilibre limite (LEM). 
  | 
  | Développement:
  | ==============
  | 
  | 4 méthodes (2 catégories) LEM ont été développées :
  | 
  | Méthodes de Fellenius et Bishop simplifié pour les surfaces de rupture circulaire.
  | Méthodes de Spencer et Morgenstern-Price pour les surfaces de rupture non-circulaire.
  | Ces méthodes découpent la pente en plusieurs tranches qui sont considérées comme corps rigides. Etant donnée une surface de 
  | rupture potentielle, le calcul du facteur de stabilité se fait par résoudre le bilan des forces et des moments de l'ensemble 
  | des tranches. Les 4 méthodes de calcul se distinguent par les hypothèses de simplification sur les forces d'interaction entre 
  | les tranches.
  | 
  | Afin de repérer la surface critique minimisant le facteur de sécurité, on a développé les algorithmes d'optimisation adaptés à 
  | la forme des surfaces. On emploie la méthode d'exhaustion pour les surfaces circulaires (Fellenius et Bishop), et l'algorithme 
  | de feu d'artifice amélioré (EFWA) pour les surfaces non-circulaires (Spencer et Morgenstern-Price).
  | 
  | En outre, afin d'éliminer l'influence de la finesse du maillage sur le résultat du facteur de sécurité, on automatise le 
  | raffinement du maillage dans la macro à l'aide de MACR_ADAP_MAIL. Le raffinement est piloté par un champ de proximité afin de 
  | raffiner les mailles uniquement sur le contour des tranches.
  | 
  | Maintenant la macro-commande se disposant à la fois les méthodes SRM et LEM, on a modifié le nom de la macro en CALC_STAB_PENTE 
  | pour éviter la méprise des utilisateurs.
  | 
  | Ainsi on arrive à enrichir les fonctionnalités de l'opérateur CALC_STAB_PENTE de sorte qu'elle remplisse parfaitement les 
  | nécessitées de l'industrie hydraulique et accompagne Yuansuan lors de son exploitation dans la marché chinoise.
  | Basée sur la macro-commande CALC_SRM (CALC_STAB_PENTE), les 4 méthodes LEM (Bishop, Fellenius, Morgenstern-Price, Spencer) ont 
  | été développées. La macro permet de chercher la surface de rupture circulaire ou multilinéaire, et de calculer le facteur de 
  | sécurité par la méthode choisie. Elle raffine automatiquement le maillage sur le contour des tranches afin d’éliminer son 
  | influence sur le résultat. Le calcul LEM prend en entrée le champ des matériaux, les limites des paramètres géométriques de la 
  | surface potentielle de rupture et les paramètres de l’algorithme d’optimisation (EFWA). Toutes les méthodes fournissent une 
  | visualisation de la surface de rupture (résultat evol_noli) sous la demande de l’utilisateur.
  | 
  | Vocabulaire:
  | 
  | On ajoute le vocabulaire suivant:
  | 
  | ♦	METHODE_STAB     =	/’SRM’,			                          [DEFAUT]      
  |                                 /’LEM’
  | Si METHODE_STAB = ‘LEM’
  | 	{
  | 		◊METHODE_LEM        =	/‘BISHOP’	                          [DEFAUT]
  | 					/’FELLENIUS’
  | 					/’SPENCER’
  |   				        /’MORGENSTERN_PRICE’
  | 
  | 		◊	CRITERE            =    /’MOHR_COULOMB’                       [DEFAUT]
  | 						/’DRUCK_PRAGER’
  | 		
  | 		♦	GROUP_MA           =	profil_pente                     [l_gr_maille]
  | 
  | 		◊   NB_TRANCHE         =   /10                                    [DEFAUT]
  | 					   /Nt                                         [I]
  | 		
  | 		RAFF_MAIL = _F(
  | 					◊   NB_RAFF_MAXI     =   /4                           [DEFAUT]
  | 								 /Nraff                            [I]
  | 					◊   RAFF_RESI_MAXI   =   /1e-3                        [DEFAUT]
  | 							         /raff_resi                        [R]
  | 					)
  | 
  | 		♦	X1_MINI        =	x1_min                                         [R]
  | 		♦	X1_MAXI        =	x1_max                                         [R]
  | 		♦	X2_MINI        =	x2_min                                         [R]
  | 		♦	X2_MAXI        =	x2_max                                         [R]
  | 		
  | 		Si METHODE_LEM = ‘BISHOP’ ou ‘FELLENIUS’: 
  | 		{
  | 			◊   NB_POINT_1          =   /5                                [DEFAUT]
  | 						    /nb_gauche                             [I]
  | 			◊   NB_POINT_2          =   /5                                [DEFAUT]
  | 			       			    /nb_droite                             [I]
  | 			◊   Y_MINI              =   y_min                                  [I]
  | 			◊   Y_MAXI              =   y_max                                  [I]
  | 
  | 		}
  | 		Si METHODE_LEM = ‘MORGENSTERN_PRICE’ ou ‘SPENCER’: 
  | 		{
  | 			
  | 			# Paramètres de l’algorithme EFWA
  | 
  | 			ALGO_EFWA = _F(
  | 					◊   ETAT_INIT   =   tabfs                        [table_aster]
  | 
  | 					◊   ITER_MAXI           =      /1e2                                   [DEFAUT]                            
  | 								       /iter_max                              [I]
  | 
  | 					♦   A      =   /efwa_a                                [R]
  | 
  | 					◊   N      =   /5                                [DEFAUT]
  | 						       /efwa_n                                [I]
  | 					◊   M           =   /40                               [DEFAUT]
  | 							    /efwa_m                                [I]
  | 					◊   MG          =   /5                                [DEFAUT]
  | 						            /efwa_mg                               [I]
  | 					◊   SA          =   /0.04                             [DEFAUT]
  | 							    /efwa_sa                               [R]
  | 					◊   SB          =   /0.8                              [DEFAUT]
  | 							    /efwa_sb                               [R]
  | 					◊   NB_STAB_MAXI    =   /10                           [DEFAUT]
  | 								/stab_maxi                         [I]
  | 					◊   CRIT_STAB       =   /1e-3                         [DEFAUT]
  | 							        /efwa_resi                         [R]
  | 					◊   MARGE_PENTE     =   /0.1                          [DEFAUT]
  | 							        /marge                             [R]
  | 			    )
  |                                         
  | 		} 	
  | 	}
  | 
  | 
  | Exemple d’analyse de stabilité avec surface de rupture circulaire :
  | 
  |  
  | __CHMAT = AFFE_MATERIAU(AFFE=_F(TOUT = 'OUI', MATER=(sol, )),
  | 
  |                                                       AFFE_VARC = _F(NOM_VARC = 'PTOT', TOUT = 'OUI', CHAM_GD = CHSEEP),
  | 
  |                                                      MAILLAGE=mesh)
  | 
  |  
  | 
  | TABFS = CALC_STAB_PENTE(CHAM_MATER = __CHMAT,
  | 
  |                         METHODE_STAB = 'LEM',
  | 
  |                         METHODE_LEM = 'BISHOP',
  | 
  |                         GROUP_MA = ('range_pente',),
  | 
  |                         NB_TRANCHE = nb_tran,
  | 
  |                         X1_MINI = 3.,
  | 
  |                         X1_MAXI = 3.,
  | 
  |                         X2_MINI = 13.2,
  | 
  |                         X2_MAXI = 13.2,
  | 
  |                         NB_POINT_1 = 1,
  | 
  |                         NB_POINT_2 = 1,
  | 
  |                         CHAM_DEFO = CO('chamdef'),
  | 
  |                         )
  | 
  |  
  | 
  | Exemple d’analyse de stabilité avec surface de rupture non-circulaire :
  | 
  |  
  | 
  | __CHMAT = AFFE_MATERIAU(AFFE=(_F(GROUP_MA=('slope', ), MATER=(sol_dur, )),
  | 
  |                                                                   _F(GROUP_MA=('weak', ), MATER=(sol_mou, ))),
  | 
  |                                                       AFFE_VARC = _F(NOM_VARC = 'PTOT', CHAM_GD = CHSEEP, TOUT = 'OUI'),
  | 
  |                                                      MAILLAGE=mesh)
  | 
  |  
  | 
  | TABFS = CALC_STAB_PENTE(CHAM_MATER = __CHMAT,
  | 
  |                         METHODE_STAB = 'LEM',
  | 
  |                         METHODE_LEM = 'MORGENSTERN_PRICE',
  | 
  |                         GROUP_MA = ('range_pente',),
  | 
  |                         NB_TRANCHE = nb_tran,
  | 
  |                         RAFF_MAIL = _F(NB_RAFF_MAXI = 4,),
  | 
  |                         X1_MINI = 1.8,
  | 
  |                         X1_MAXI = 2.4,
  | 
  |                         X2_MINI = 15.6,
  | 
  |                         X2_MAXI = 16.2,
  | 
  |                         EFWA = _F(ITER_MAXI = 30, EFWA_A = 50),                                                                                        
  | 
  |                         CHAM_DEFO = CO('chamdef'),
  | 
  |                         )
  | 
  |  
  | 
  | On a développé deux cas de validation et un cas de vérification pour valider les fonctionnalités LEM :
  | 
  | SSNP06 : Analyse de stabilité d’une pente homogène par la méthode Bishop. Ce cas de validation provient du cas test du 
  | logiciel commercial Plaxis2D avec une solution de référence.
  | SSNP07 : Analyse de stabilité d’une pente non-drainée avec couche faible par la méthode Morgenstern-Price. Ce cas de 
  | validation est issu de l’article de D. V. Griffiths avec une solution de référence.  
  | ZZZZ146 : Validation informatique des méthodes LEM dans la macro-commande CALC_STAB_PENTE. Ayant pas de sens scientifique, ce 
  | cas de vérification comprend 2 modélisations validant la programmation informatique.
  |  
  | 
  | Les fichiers python modifiées et renommées :
  | 
  | calc_srm.py à calc_stab_pente.py (catalogue de la macro)
  | calc_srm_ops.py à calc_stab_pente_ops.py (main script)
  | calcsrm.py à calcstabpente.py (catalogue de message UTMESS)
  |  
  | 
  | Autres fichiers modifiés dans SRC :
  | 
  | vocab01a.34  (ajout des nouvelles vocabulaires)
  |  
  | 
  | Les cas tests ajoutés :
  | 
  | Validation
  | SSNP06 : Analyse de stabilité d’une pente homogène par la méthode Bishop.
  | SSNP07 : Analyse de stabilité d’une pente non-drainée avec couche faible par la méthode Morgenstern-Price.
  | SSNP507 : Stabilité d'une pente argileuse non-drainée sur la fondation faible
  | 
  | Vérification
  | ZZZZ146 a b : Validation informatique des méthodes LEM dans la macro-commande CALC_STAB_PENTE.
  |  
  | 
  | Les cas tests modifiés :
  | 
  | ZZZZ135 : Validation de l'utilisation de la macro-commande CALC_SRM
  | 
  | 
  | Les documentations créés :
  | 
  | R7.05.02 : Méthode d’analyse  de stabilité des pentes.
  | V1.01.146 : cas test ZZZZ146 a et b
  | V6.03.006 : cas test SSNP06
  | V6.03.007 : cas test SSNP07
  |  
  | 
  | Les documentations modifies :
  | 
  | U4.84.47 : Macro-commande CALC_STAB_PENTE
  | V6.03.507 : cas test SSNP507
  | V1.01.135 : cas test ZZZZ135
  | 
  | Le travail a été effectué dans le cadre du TSA YuanSuan

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : R7.05.02 U4.84.47 V6.03.507 V1.01.146 V6.03.007 V6.03.006 v1.01.135
- VALIDATION : submit + zzzz146 + ssnp06 + ssnp07
- NB_JOURS_TRAV  : 25.0


================================================================================
                RESTITUTION FICHE 32636 DU 20/02/2023
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.3.8, le cas test ssnl201i est en erreur NO_CONVERGENCE sur CRONOS et Gaia
- FONCTIONNALITE :
  | Le problème :
  | =============
  | En version 16.3.8, le cas test ssnl201i est en erreur NO_CONVERGENCE sur CRONOS et Gaia
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <MECANONLINE9_7>                                                                   ║
  |  ║                                                                                                ║
  |  ║ Arrêt pour cause d'absence de convergence avec le nombre d'itérations requis dans l'algorithme ║
  |  ║ non-linéaire de Newton.                                                                        ║
  | .......                                                                                        
  | .......
  | 
  | 
  | La solution :
  | =============
  | Le chargement passe par "zéro" les résidus sont relativement faibles : 10E-23 et même 0.000
  | 
  | Dans les 3 STAT_NON_LINE, on ajoute :
  | CONVERGENCE=_F(RESI_REFE_RELA=1.0E-05, SIGM_REFE=1.0, EFFORT_REFE=1.0, MOMENT_REFE=1.0,),
  | 
  | Tout est OK, sur cronos, gaia, sci

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage du cas test
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32805 DU 20/04/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.3.15, le cas test perf901b est en ABNORMAL_ABORT
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | On plante dans perf901b.com1 avec l'erreur
  | 
  | Traceback (most recent call last):
  |   File "./perf901b.com1.changed.py", line 256, in <module>
  |     tab_depl = CREA_TABLE(TYPE_TABLE="TABLE_CONTAINER", **motcles)
  |   File "/gpfsgaia/home/ms06427s/dev/codeaster/install/std/lib/aster/code_aster/Supervis/ExecuteCommand.py", line 180, in run
  |     return cmd.run_(**kwargs)
  |   File "/gpfsgaia/home/ms06427s/dev/codeaster/install/std/lib/aster/code_aster/Supervis/ExecuteCommand.py", line 239, in run_
  |     self.post_exec_(keywords)
  |   File "/gpfsgaia/home/ms06427s/dev/codeaster/install/std/lib/aster/code_aster/Supervis/ExecuteCommand.py", line 516, in post_exec_
  |     self.post_exec(keywords)
  |   File "/gpfsgaia/home/ms06427s/dev/codeaster/install/std/lib/aster/code_aster/Commands/crea_table.py", line 100, in post_exec
  |     self._result.build()
  | RuntimeError: missing parameter NOM_SD in TableContainer
  | 
  | Analyse
  | ---
  | 
  | SAV issue29418 qui ajoute des vérifications dans le cas des TableContainer
  | oubli de modifier le perf901b.com1 (sans doute pas vu car le test était déjà cassé)
  | 
  | Solution
  | ---
  | 
  | Dans ce cas il faut faire un CREA_TABLE(TYPE_TABLE="TABLE" et non TYPE_TABLE="TABLE_CONTAINER"

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perf901b

