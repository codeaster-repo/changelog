==================================================================
Version 16.0.11 (révision 157304691e53) du 2021-09-28 22:28 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31473 COURTOIS Mathieu         Exécution directe d'un fichier de commande
 31474 COURTOIS Mathieu         Test ssls146a instable
 26481 LE CREN Matthieu         [RUPT]  – Impression de variable de commande dans la table sortant de POST_K1...
 27723 DEGEILH Robin            [RUPT] Harmonisation des tables de sortie de POST_K1_K2_K3 et CALC_G
 31436 YU Ting                  Le test mfron02i est incorrect
 31438 YU Ting                  COMBINAISON_CHARGE - Elements de membrane
 31475 YU Ting                  Défaut de couverture de test pour la fonctionnalité : CALC_CHAMP/--/CONTRAINT...
 31476 YU Ting                  Problème POST_CHAMP/INDIPLAS sur CRONOS
 31456 PIGNET Nicolas           Changer le pickling de l'objet Result
 31472 PIGNET Nicolas           printMedFile avec un fichier unique
 31325 PIGNET Nicolas           Supression option VAEX
 31455 PIGNET Nicolas           Test de perf en thermo-méca
 31430 COURTOIS Mathieu         Traceback non présent dans le .mess
 30820 YU Ting                  Absence de sortie du ETA lors d'un calcul en PILOTAGE
 31444 YU Ting                  Calcul de la norme de Von Mises de EPMG_ELGA
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31473 DU 04/10/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Exécution directe d'un fichier de commande
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | On souhaite lancer un calcul avec :
  | 
  | run_aster [options] file.py
  | 
  | 
  | Développement
  | -------------
  | 
  | usage:
  | . . run_aster [options] FILE[.export|.py]
  | 
  | positional arguments:
  | . FILE. . . . . . . . . Export file (.export) defining the calculation or
  | . . . . . . . . . . . . Python file (.py|.comm). Without file, it starts an
  | . . . . . . . . . . . . interactive Python session. (default: None)
  | 
  | Pour ne pas ajouter d'option supplémentaire, on utilise l'extension du fichier en argument : .py ou .comm pour un lancement direct, sinon on suppose qu'il
  | s'agit d'un fichier export.
  | 
  | La doc embarquée donne ce type d'exemples :
  | 
  | - zzzz100f n'a pas de fichier de données (depuis install/std) :
  | 
  |   bin/run_aster share/aster/tests/zzzz100f.comm
  | 
  | 
  | - zzzz504v.py est modifié pour l'exemple (depuis install:mpi) :
  | 
  |   bin/run_aster -n 2 --only-proc0 share/aster/tests/zzzz504v.py
  | 
  |   Dans ce test, on charge le maillage à partir du nom de fichier. Il suffit de le prendre en relatif par rapport au '.py'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz100f, zzzz504v
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31474 DU 04/10/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Test ssls146a instable
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | ssls146a plante parfois dans SDVERI en disant que les objets d'un LIGREL n'existent pas.
  | 
  | 
  | Analyse
  | -------
  | 
  | - On passe un mult_elas à COMBINAISON_FERRAILLAGE.
  | - CALC_FERRAILLAGE y ajoute des champs restreints sur un groupe de mailles. Ces champs s'appuient donc sur un nouveau ligrel (préfixé par le nom du mult_elas).
  | - On extrait ces champs.
  | - On les met dans un nouveau résultat de type evol_elas (!?).
  | - On extrait le max de ces champs, on extrait les composantes sur lequelles on évalue des formules pour produire une carte.
  | - On ajoute le max et la carte au mult_elas initial.
  | - En sortant de la macro, on détruit l'evol_elas de travail. Or les champs de celui-ci s'appuient sur le ligrel du mult_elas qui est donc détruit.
  | 
  | 
  | Correction
  | ----------
  | 
  | Le problème vient de CREA_CHAMP/EXTR (point 3).
  | Car le champ extrait donc posséder son propre ligrel.
  | => On modifie dans copich pour que le champ copié ait une copie du ligrel, attaché/préfixé par le nouveau champ.
  | 
  | Le build des champs ne fait pas grand chose et suppose que le ligrel du champ est celui du modèle.
  | Et donc le ligrel "restreint" n'est pas détruit en même temps que le champ...
  | => On modifie donc le build des FieldOnCells pour :
  | - si le ligrel appartient au champ (même préfixe), on définit correction son ligrel/attribut '_dofDescription'.
  | - sinon on pointe vers celui du modèle.
  | 
  | 
  | Notes
  | -----
  | 
  | Le problème pour trouver ce type d'incohérence est, qu'en fortran, le lien est uniquement dans une chaine de caractères (à l'indice 1 du CELK)...
  | Pour déboguer ce type de problème, on peut ajouter ce hook, appelé après chaque commande, à mettre en début de '.comm' :
  | 
  | import gc
  | 
  | from code_aster.Objects import DataStructure
  | from code_aster.Supervis.ExecuteCommand import ExecuteCommand
  | 
  | def clean(cmd, dummy):
  |     gc.collect()
  |     if not isinstance(cmd.result, DataStructure):
  |         return
  |     IMPR_CO(
  |         UNITE=10000 + cmd._counter,
  |         CONCEPT=_F(NOM=cmd.result))
  | 
  | ExecuteCommand.register_hook(clean)
  | 
  | Mettre IMPR_MACRO="OUI" pour le faire aussi dans les commandes filles.
  | 
  | On lance avec :
  | 
  | rm -rf /local00/tmp/dbg/*
  | ../install/std/bin/run_aster --wrkdir=/local00/tmp/dbg --time_limit=7200 astest/test.export > output
  | 
  | Ainsi le répertoire d'exécution n'est pas supprimé et on peut consulter les fort.100NN produits après chaque commande.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssls146a, ssls126e (plus court)
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 26481 DU 18/05/2017
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RUPT]  – Impression de variable de commande dans la table sortant de POST_K1_K2_K3
- FONCTIONNALITE :
  | Objectif:
  | ---
  | 
  | Ajouter la variable de commande dans la table résultat de POST_K1_K2_K3
  | 
  | Travail effectué:
  | ---
  | 
  | La variable de commande TEMP ou NEUT1 est contenue dans le champ matériau présent dans le résultat fourni en entrée de POST_K1_K2_K3.
  | La macro fait appel à la fonction POST_K_VARC pour extraire la variable de commande aux points de fond de fissure,
  | elle en a besoin pour évaluer les propriétés matériaux pour le calcul de K et G.
  | Il suffit d'ajouter ces variables (TEMP ou NEUT1) dans la table de sortie pour chaque nœud du fond de fissure
  | 
  | Validation : on complète les tests informatiques zzzz298a (3D), zzzz298b (xfem) et zzzz298d (2D) avec des tests table sur 'TEMP' et 'NEUT1'
  | 
  | * impact doc V1.01.298

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.298
- VALIDATION : cas test
- NB_JOURS_TRAV  : 2.5


================================================================================
                RESTITUTION FICHE 27723 DU 04/06/2018
================================================================================
- AUTEUR : DEGEILH Robin
- TYPE : evolution concernant code_aster (VERSION 14.3)
- TITRE : [RUPT] Harmonisation des tables de sortie de POST_K1_K2_K3 et CALC_G
- FONCTIONNALITE :
  | Objectif :
  | ---
  | 
  | Harmoniser les sorties de POST_K1_K2_K3 avec celles de CALC_G.
  | Il faut, entre autre, ajouter les coordonnées des nœuds de fond de fissure COOR_X, COOR_Y et COOR_Z
  | 
  | Travail effectué:
  | ---
  | 
  | 1/ POST_K1_K2_K3 à besoin des coordonnées des points de fonds de fissure pour le calcul de K et G (abscisses curvilignes, etc.)
  | Ces valeurs sont déjà extraites. Il suffit de les ajouter dans la table sous les nouvelles colonnes COOR_X, COOR_Y et COOR_Y.
  | On en profite pour uniformiser le code entre les 3 cas possibles : TYPE_MAILLAGE="REGLE" (d_coor) ou "LIBRE" (d_coorf) et XFEM (Coorfo),
  | on utilise dans tous les cas la variable d_coor.
  | 
  | 2/ On renomme la colonne NOEUD_FOND en NOEUD
  | 
  | 3/ On réordonne les colonnes de la tables comme suit:
  | FISSURE, FOND_FISS, NUME_FOND, NUME_ORDRE, INST, FREQ, NOEUD, NUM_PT, COOR_X, COOR_Y, COOR_Z, ABSC_CURV, TEMP, NEUT1
  | 
  | Validation : tests informatiques zzzz298a (3D), zzzz298b (xfem) et zzzz298d (2D), ajout des tests table sur 'COOR_X', 'COOR_Y' et 'COOR_Z'
  | 
  | * impact doc V1.01.298

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V1.01.298
- VALIDATION : zzzz298
- NB_JOURS_TRAV  : 2.5


================================================================================
                RESTITUTION FICHE 31436 DU 23/09/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test mfron02i est incorrect
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le test mfron02i a servi à vérifier l'utilisation de MFRONT en THM.
  | Il a été supprimé dans issue30543 car il était cassé à cause de la loi Mfront 
  | (DruckerPragerEcroLin.mfront - écrouissage linéaire) dont personne ne s'occupe.
  | 
  | Cependant, STAT_NON_LINE/COMPORTEMENT/RELATION_KIT#MFRONT n'a été plus couvert
  | suite à la suppression. Donc il a été ré-introduit par issue30740 en copiant
  | de mfron02c juste pour la couverture.
  |  
  | Or le test est incorrect. On veut faire de la THM mais le AFFE_MODELE est "3D" !
  | (DESOLÉE, JE SUIS BALOURDE)
  | 
  | Correction/Développement
  | ------------------------
  | 
  | J'ai pris la cas test wtnp114a qui est similiare au test mfron02i initial.
  | wtnp114a combine ('ELAS','LIQU_SATU','HYDR_UTIL',),
  | 
  | Je l'ai modifié pour un test en THM (D_PLAN_HM) avec RELATION='KIT_HM',
  |                   RELATION_KIT=('MFRONT','LIQU_SATU','HYDR_UTIL',),
  | dont la loi Mfront est une loi de plasticité
  | 
  | Par contre, si je remplace le modèle de D_PLAN_HM par D_PLAN.
  | ça plante. Mais mfron02i avant la correction se termine normal avec la
  | modélisation 3D alors que 3D_THM doit être affecté, sans détecter 
  | l'incohérence entre le modèle et comportement.
  | J'ai émis une fiche dessus (issue31452)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.03.127
- VALIDATION : mfron02i
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31438 DU 27/09/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : COMBINAISON_CHARGE - Elements de membrane
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | L'utilisation de COMBINAISON CHARGE repose sur l'hypothèse de superposition, 
  | donc élasticité linéaire en petites déformations.
  | 
  | Or, il y a ELAS_MEMBRANE* dans la liste des comportements, ce qui est est un 
  | comportement hyperélastique, en grandes déformations (GROT_GDEP)
  | A mon avis '(Mickaël), il ne faut pas l'autoriser.
  | 
  | Mickaël l'a déjà supprimé de la doc. Il faut changer la macro
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Ma faute pendant la création de la macro à cause du manque des connaissances
  | sur MEMBRANE.
  | 
  | Effectivement, ELAS_MEMBRANE* dans la liste des comportements est un 
  | comportement hyperélastique en grandes déformations (GROT_GDEP)
  | Il faut utiliser ELAS_MEMBRANE dans DEFI_MATERIAU pour la relation ELAS
  | en petite déformation.
  | 
  | C'est corrigé dans la macro et j'ai modifié le test sslx300f (V3.05.300)
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | depuis v15.3.18 (création de la macro) lorsqu'on utilise la macro avec des éléments 
  | MEMBRANE dans le modèle

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 15.3.18
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 16.0.1
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V3.05.300
- VALIDATION : sslx300f
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31475 DU 04/10/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Défaut de couverture de test pour la fonctionnalité : CALC_CHAMP/--/CONTRAINTE#STRX_ELGA (en version 16.0.9)
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Cette fonctionnalité n'est plus testée :
  | CALC_CHAMP/--/CONTRAINTE#STRX_ELGA (ssll102i~0.66s, ssll102j~0.74s, zzzz330a~1.18s, ssll111b~1.24s)
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Le champ SIEF_ELGA/STRX_ELGA (cas des éléments de poutres/tuyaux ou des discrets) est calcalé
  | par défaut dans MECA_STATIQUE / STAT_NON_LINE.
  | 
  | Dans ssll102i, j'ai mis OPTION = 'SANS' dans MECA_STATIQUE, donc SIEF_ELGA/STRX_ELGA n'est pas
  | calculé. Ensuite, j'ai ajouté un CALC_CHAMP pour la couverture.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssll102i
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31476 DU 04/10/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Problème POST_CHAMP/INDIPLAS sur CRONOS
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Erreur lors de l'utilisation  NOM_VARI/MIN_MAX_SP dans POST_CHAMP
  | POST_CHAMP(RESULTAT=dynline,
  |                   GROUP_MA='tuyau',
  |                   MIN_MAX_SP=_F(NOM_CHAM='VARI_ELGA',
  |                                 NOM_VARI='INDIPLAS',    
  |                                 TYPE_MAXI='MAXI',
  |                                 NUME_CHAM_RESU=7),),
  | 
  | Cela marche quand on donne NOM_CMP='V7'. 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Dans le code, les cas NOM_VARI et NOM_CMP sont séparés dans MIN_MAX_SP / POST_CHAMP.
  | 
  | Lorsqu'on donne NOM_VARI, MIN_MAX_SP(w155mx.F90) selectionne des éléments dans la zone 
  | géométrique définie, par exemple, GROUP_MA, TOUT='OUI', par getelem.F90 au 2ème niveau
  | , donc dans MIN_MAX_SP.
  | 
  | Cependant dans la catalogue, GROUP_MA et TOUT sont définis au 1er niveau, directement
  | POST_CHAMP. Donc il ne trouve pas d'élément et cela conduit l'erreur.
  | Donc j'ai corrigé la cherche dans le bon niveau.
  | 
  | De plus, dans getelem.F90 :
  | quand il ne trouve pas d'option GROUP_MA / TOUT / MAILLE => 0 élément
  | Mais souvent dans l'autre sens, cela correspond à TOUT='OUI'.
  | Donc j'ai ajouté un paramètre optionnel pour forcer comme TOUT='OUI'
  | s'il ne trouve aucune option.
  | 
  | J'ai ajouté NOM_VARI/MIN_MAX_SP dans ssnl132a pour tester.
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Non

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnl132a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31456 DU 30/09/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Changer le pickling de l'objet Result
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Il faut changer le pickling de l'objet Result pour éviter les warning
  | 
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Lors du pickling de l'objet Result, on tente de sauvegarde le cara_elem. Quand on le récupère, une exception est émise avec un message s'il est absent
  | 
  | Je rajoute une méthode hasElementaryCharacteristics(rank) pour savoir s'il existe avant de le récupérer. Ainsi plus de problème d'exception
  | 
  | Je vérifie dans zzzz503c

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503c
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31472 DU 01/10/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : printMedFile avec un fichier unique
- FONCTIONNALITE :
  | Problème
  | -----------------
  | Maintenant que l'on peut écrire un seul fichier en hpc, il faut mettre l'api python à jour
  | 
  | 
  | Correction
  | ------------------------
  | 
  | Je rajoute un argument optionel local = true par défaut (pour reproduire l'ancien comportement) En std, cela n'a aucun effet
  | Cela concerne Mesh, FieldOnNodes, FieldOnCells, Result
  | 
  | J'en profite pour corriger un bug de comptage dans iremed_filtre.F90
  | 
  | Je teste ces impressions dans zzzz504a

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz504a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31325 DU 17/08/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 16.1)
- TITRE : Supression option VAEX
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Il faut supprimer VAEX_ELNO et VAEX_ELGA (+ VAEX_NOEU)des catalogues, fortran, tests et doc.
  | 
  | 
  | 
  | Travail effectué :
  | ================
  | 
  | Suppression des sources et partie de code liées à VAEX_XXXX
  | Suppression des appels à ces options dans les tests.
  | Suppression des références à ces options dans les docs :
  | V7.31.123 (pas de références dans les autres docs des tests modifiés)
  | U4.81.04 : CALC_CHAMP

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V7.31.123, U4.81.04
- VALIDATION : lancement des tests modifiés + submit


================================================================================
                RESTITUTION FICHE 31455 DU 30/09/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Test de perf en thermo-méca
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Je propose de créer un test de perf en thermo-méca en se basant sur perf008 (sûrement une nouvelle lettre) car on n'a pas de gros test sur cette partie
  | 
  | 
  | Correction/Développement
  | ------------------------
  | Je modifie le test perf008 pour faire plusieurs pas de temps et faire dépendre le matériau de la température
  | 
  | Je fais 10 pas de temps et deux calculs avec différents solveurs de petsc. Les performances semblent logique. 1M de ddl 
  | 
  | perf008e -> 4 cpu et perf008f -> 4 cpu HPC
  | 
  | On voit un beau gain sur la mémoire en HPC

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.01.252
- VALIDATION : perf008e,f
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 31430 DU 21/09/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 16.1)
- TITRE : Traceback non présent dans le .mess
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Depuis astk local sur gaia en 15.4 ou en unstable, on provoque une erreur :
  | DEBUT()
  | print(blabla)
  | FIN()
  | 
  | Dans le .o du flasheur, il y a bien l'erreur suivante :
  | NameError: name 'blabla' is not defined
  | 
  | Mais dans le .mess rapatrié, rien, comme si tout s'était bien passé.
  | 
  | 
  | Correction
  | ----------
  | 
  | Sur Cronos, le problème n'apparaît pas.
  | En effet, il ne s'agit pas une anomalie dans code_aster mais d'un retard de mise à jour (par sécurité !) du lanceur sur Gaia => je vais le mettre à jour.
  | Cela va s'accompagner d'une évolution dans asterstudy qui appelle as_run en dur quelque soit la version aujourd'hui.
  | 
  | Sans suite (pour code_aster) + évolution dans asterstudy + update sur Gaia de asrun.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30820 DU 25/03/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : Absence de sortie du ETA lors d'un calcul en PILOTAGE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans une étude de détermination de la pression limite admissible en pilotage, le calcul a été repris 
  | sans modification, mais il s'est terminé en échec sur NonConvergenceError.
  | 
  | La demande est de comprendre l'utilisation de pilotage :
  | 
  | - comment obtenir la fonction ETA en sortie  ?
  | - pourquoi ça termine en NonConvergenceError alors que je pensais qu'un pilotage savait gérer 
  |   de sorte à s'arrêter juste avant la limite?
  | - comment obtenir le .rmed sur les instants convergés sur pilotage.
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Des pistes à proposer pour faire les post-traitements ETA des instants qui ont 
  | convergés à l aide d'une poursuite.
  | 
  | Cependant l'utilisation n'a pas de temps de tester pour l'instant. 
  | D'autres activités actuelles font passer ça en second donc il émettra une autre fiche
  | à l'avenir si nécessaire.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : perso
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31444 DU 27/09/2021
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Calcul de la norme de Von Mises de EPMG_ELGA
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Adriana essaye de calculer la norme de Von Mises de la déformation mécanique en grandes 
  | transformations avec EPMG_* dans CALC_CHAMP en donnant une formule.
  | 
  | Elle tombe sur un "floating point exception"
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En vérifiant le calcul, la formule 
  | VALE="""sqrt(3./2. * (EPXX**2 + EPYY**2 + EPZZ**2 + 2*EPXY**2 + 2*EPXZ**2 + 2*EPYZ**2)
  | - 1./2. * (EPXX+EPYY+EPZZ)**2)""")
  | donne des valeurs négatives (~ -1.0e-50) sous sqrt.
  | 
  | En fait, il y a une erreur dans la formule pour calculer la déformation VMIS.
  | Finalement, elle a trouvé l'option CRITERE='INVA_2'ou 'VMIS' dans CALC_CHAMP
  | pour directement obtenir la déformation souhaitée.
  | 
  | Je ferme la fiche sans source

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test perso
- NB_JOURS_TRAV  : 0.5

