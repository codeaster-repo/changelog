==================================================================
Version 16.3.10 (révision a1a75ce9d8a0) du 2023-02-22 15:55 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31546 COURTOIS Mathieu         code_aster ne compile plus avec mumps 5.2.1
 29516 DUTRECH Laurent          [Résorption] Commande POST_T_Q
 32629 LORENTZ Eric             DEFI_LIST_INST / DELTA_GRANDEUR : filtre par GROUP_MA
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31546 DU 02/11/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : code_aster ne compile plus avec mumps 5.2.1
- FONCTIONNALITE :
  | La version de Mumps est vérifiée par le waf configure. Les tests de version du Fortran ont été résorbés dans issue31976
  | Pour gérer 2 versions à l'avenir il suffira d'ajouter un #ifdef sur la variable MUMPS_VERSION
  | 
  | On supprime le action=VERSION, non utilisé, dans amumph.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build


================================================================================
                RESTITUTION FICHE 29516 DU 27/01/2020
================================================================================
- AUTEUR : DUTRECH Laurent
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [Résorption] Commande POST_T_Q
- FONCTIONNALITE :
  | Demande :
  | =======
  | 
  | Résorption de POST_T_Q.
  | Macro - Tests - Docs
  | 
  | Travail effectué :
  | ================
  | 
  | Résorption de POST_T_Q.
  | Macro - Tests - Docs
  | 
  | (Je n'ai pas la main pour supprimer les docs)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.82.40
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 32629 DU 16/02/2023
================================================================================
- AUTEUR : LORENTZ Eric
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : DEFI_LIST_INST / DELTA_GRANDEUR : filtre par GROUP_MA
- FONCTIONNALITE :
  | Usage
  | -----
  | 
  | On peut dorénavant filtrer le champ utilisé dans DELTA_GRANDEUR (en échec ou en adaptation) selon une liste de groupes de mailles (pour SIEF_ELGA ou VARI_ELGA)
  | ou une liste de groupes de noeuds (pour DEPL), en utilisant respectivement les mots-clés GROUP_MA et GROUP_NO dans la définition de l'évènement ou de la règle
  | d'adaptation.
  | 
  | Sans filtrage, c'est l'intégralité du champ qui est prise en compte. Et en cas de filtrage, l'usage du mot-clé MODELE est impératif dans la commande
  | DEFI_LIST_INST.
  | 
  | 
  | Réalisation informatique
  | ------------------------
  | 
  | 1) Dans la commande DEFI_LIST_INST
  | 
  | On donne la possibilité de renseigner des GROUP_MA ou GROUP_NO lorsqu'on définit les paramètres des blocs DELTA_GRANDEUR (en échec et en adaptation). Pour
  | pouvoir construire les listes de mailles et de noeuds concernés, on définit dorénavant le MODELE dans DEFI_LIST_INST (en mot-clé simple, hors de tout bloc). Il
  | est optionel et n'est requis que si on utilise GROUP_MA ou GROUP_NO au moment de définir DELTA_GRANDEUR.
  | 
  | 
  | 2) On enrichit la SD DEFI_LIST_INST
  | 
  | lisins.ECHE.LOCA (V I)-> localisation pour chaque évènement de type échec
  | lisins.ADAP.LOCA (V I)-> localisation pour chaque évènement de type adaptation
  | lisins.MODELE    ( K8)-> modèle auquel fait référence la localisation
  | 
  | Une "localisation" est un vecteur jeveux d'entiers contenant des informations et des listes de numéros de noeuds et/ou de mailles. Elle a la structure suivante
  | :
  | 
  | Une entête qui contient trois informations pour chaque occurrence d'échec ou d'adaptation:
  | a) Un entier qui indique: 0=pas de localisation, 1=liste partielle de mailles ou de noeuds, 2=toutes les mailles ou tous les noeuds (constantes définies dans
  | event_def.h)
  | b) Dans le cas d'une liste partielle, les deux entiers suivants pointent sur le début et la fin de la liste dans le vecteur localisation. Pour les cas sans
  | liste ou liste totale, les deux entiers sont nuls.
  | Après cette entête, on trouve les listes proprement dites (s'il y en a).
  | 
  | 
  | 3) Dans la commande STAT_NON_LINE (op0070 et en-dessous)
  | 
  | a) On reporte les évolutions de la SD DEFI_LIST_INST dans la structure de données interne à op0070.
  | b) On teste si le modèle défini dans DEFI_LIST_INST est identique à celui de STAT_NON_LINE
  | c) On filtre le calcul de DELTA_GRANDEUR selon les listes de noeuds ou de mailles (si requis)
  | 
  | 
  | Validation
  | ----------
  | 
  | ssnp15f  -> on filtre sur un groupe de mailles pour un échec
  | ssnp140g -> on filtre sur un groupe de noeuds pour une adaptation 
  | 
  | 
  | Limitation
  | ----------
  | 
  | Il est probable que la fonctionnalité pose souci en cas de maillage distribué (à vérifier). C'est peut-être d'ailleurs plus généralement le cas du traitement
  | des évènements dans STAT_NON_LINE.
  | 
  | Par ailleurs, il serait sans doute préférable de procéder à une revue de code dans le cas de ce développement : je mets le bundle en PJ.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.34.03 D4.06.17
- VALIDATION : Informatique: ssnp15f, ssnp140g
- NB_JOURS_TRAV  : 3.0

