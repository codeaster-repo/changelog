==================================================================
Version 16.5.11 (révision 4bdb2cf800) du 2024-07-05 12:39:11 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33855 COURTOIS Mathieu         Erreur CALC_CHAMP en version 16.4
 33914 COURTOIS Mathieu         Warning un peu zelé avec GCC 12
 33907 BETTONTE Francesco       Méthode transform sur un champ ELGA
 33588 FERTÉ Guilhem            Mauvaise orientation en IFS : fatal!
 33887 BETTONTE Francesco       Erreur mémoire dans nmisot
 33868 SELLENET Nicolas         POST_RCCM : erreur d'affichage dans la table de sortie pour l'option 'EFAT', ...
 33867 SELLENET Nicolas         POST_RCCM plante si prise en compte du séisme et des effets d'environnement
 33918 COURTOIS Mathieu         Backport incomplet de #33704
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33855 DU 06/06/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 16.4)
- TITRE : Erreur CALC_CHAMP en version 16.4
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'étude jointe échoue dans CALC_CHAMP/SIEF_NOEU.
  | 
  | 
  | Analyse
  | -------
  | 
  | Ce qu'on peut dire à partir des .mess :
  | 
  | - Maillage 150000 noeuds, 35000 hexa20
  | - 4000 pas de temps
  | 
  | 1/. mem=6 Go sur cn:
  | 
  | On ne peut pas écrire la base vola.2. Plus de place sur disque.
  | En réalité, c'est en mémoire car les noeuds cn n'ont pas de disque.
  | 
  | A ce moment là, on a 50 Go de glob + 22 Go de vola.
  | 
  | A la louche, il faut 45 Go pour stocker les champs de déplacements, vitesse et accélération.
  | Effectivement, glob.5 (fichiers de 12 Go chacun) est créé avant la fin de DYNA_VIBRA.
  | 
  | SIEF_NOEU : 2 fois plus gros qu'un champ de déplacement, env. 30 Go.
  | 
  | Normal que ça ne tienne pas.
  | 
  | 
  | 2/. et 3/. Les calculs ont été tués par Slurm (gestionnaire de batch), probablement car ils
  | dépassaient la limite mémoire.
  | 
  | 
  | Solution (de contournement)
  | ---------------------------
  | 
  | On ne sait mesurer de manière fiable que la mémoire Jeveux.
  | Le reste (Python, Mumps, ...) peut déborder à un moment, Slurm stoppe le calcul.
  | 
  | On peut lancer le calcul avec, par exemple, 16 Go (pour que Slurm prenne cette limite en compte
  | -- en pratique, on ajoute systématiquement une marge --) et dans le jeu de commandes,
  | avant DEBUT, dire à code_aster de garder une réserve (4 Go dans l'exemple).
  | 
  | 
  | import code_aster
  | from code_aster.Commands import *
  | from code_aster.Utilities import ExecutionParameter
  | 
  | parse_args_orig = ExecutionParameter.parse_args
  | 
  | 
  | def parse_args_hack(self, argv=None):
  |     parse_args_orig(self, argv)
  |     reserve = 4000.0
  |     self.set_option("memory", self.get_option("memory") - reserve)
  | 
  | 
  | ExecutionParameter.parse_args = parse_args_hack
  | 
  | DEBUT(...)
  | 
  | 
  | Proposition de correction "évolutive"
  | -------------------------------------
  | 
  | De la même manière que RESERVE_CPU, on ajoute dans DEBUT/POURSUITE :
  | 
  | . . RESERVE_MEMOIRE=FACT(
  | . . . . fr=tr("réserve de mémoire pour les bibliothèques externes"),
  | . . . . statut="d",
  | . . . . max=1,
  | . . . . regles=(EXCLUS("VALE", "POURCENTAGE"),),
  | . . . . # if VALE is provided, use it, otherwise take a POURCENTAGE of the initial memory
  | . . . . # Default value is assigned in debut.py (before syntax checkings).
  | . . . . VALE=SIMP(statut="f", typ="I", val_min=0),
  | . . . . POURCENTAGE=SIMP(statut="f", typ="R", val_min=0.0, val_max=1.0, defaut=0.1),
  | . . ),
  | 
  | On retire cette limite de la mémoire maximale allouée à Jeveux.
  | Cela permet de retrouver le mécanisme 'mem_aster' mais directement au niveau de la mise en données.
  | 
  | La prise en compte de cette réserve n'est active que si on demande plus de 1024 Mo, ceci évite de
  | le faire dans les cas-tests.
  | Seul sslv155c dérobe à cette règle. On vérifie visuellement que sa limite est réduite de 10% :
  | 
  | setting '--memory' value to 1382.40 MB (keyword RESERVE_MEMOIRE)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.11.01
- VALIDATION : test
- DEJA RESTITUE DANS : 17.0.23
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33914 DU 28/06/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Warning un peu zelé avec GCC 12
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | In file included from ../../../bibcxx/MemoryManager/JeveuxAllowedTypes.h:31,
  | . . . . . . . .. from ../../../bibcxx/MemoryManager/JeveuxCollection.h:33,
  | . . . . . . . .. from ../../../bibcxx/include/aster_pybind.h:24,
  | . . . . . . . .. from ../../../bibcxx/DataStructures/DataStructure.h:32,
  | . . . . . . . .. from ../../../bibcxx/DataStructures/DataStructure.cxx:24:
  | ../../../bibcxx/MemoryManager/JeveuxString.h: In member function ‘void JeveuxString<lengthT>::safeCopyFromChar(const 
  | char*, int) [with int lengthT = 1]’:
  | ../../../bibcxx/MemoryManager/JeveuxString.h:54:19: warning: ‘void* memcpy(void*, const void*, size_t)’ specified size 
  | between 18446744071562067968 and 18446744073709551615 exceeds maximum object size 9223372036854775807 [-Wstringop-
  | overflow=]
  | .. 54 |. . . . . .. memcpy( &currentValue, chaine, sizeof( char ) * size );
  | . . . |. . . . . .. ~~~~~~^~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  | 
  | 
  | Correction
  | ----------
  | 
  | Il y a plusieurs bugs GCC à ce sujet.
  | 
  | Je ne l'ai vu qu'en debug et je pense que le warning est levé car size est initialisé à 0 dans ce cas,
  | et memstr_dup doit faire un malloc de 'size - 1' (de ce que j'ai cru comprendre) d'où le warning.
  | 
  | C'est un faux positif. Il faut aider l'optimiseur, il ne rencontrera pas le cas size < 1 :
  | 
  |         if ( size < 1 ) {
  |             __builtin_unreachable();
  |         }

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build debug + verification
- DEJA RESTITUE DANS : 17.0.23
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33907 DU 25/06/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Méthode transform sur un champ ELGA
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'utilisation de la méthode transform sur un champ donne l'erreur suivant : 
  | 
  | ValueError: Returned value of type different from ASTERDOUBLE
  | 
  | 
  | Analyse
  | -------
  | La fonction "max" utilisée par l'utilisateur restitue des objets de type différents (int,float) et le type int n'est pas traitée par la méthode transform. Le
  | champ produit n'est pas correct, car malgré le message d'erreur la fonction restitue un champ et celui ci est rempli partiellement.
  | 
  | 
  | Correction
  | ----------
  | 
  | 1 - on force la conversion des entiers en float pour rendre plus flexible la fonction
  | 2 - en cas d'erreurs c'est un AsterError qui est levé
  | 3 - on rends disponible la méthode sur les champs aux nœuds
  | 
  | Le tout est testé dans zzzz503f

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503f
- DEJA RESTITUE DANS : 17.0.23
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33588 DU 19/01/2024
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Mauvaise orientation en IFS : fatal!
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Une mauvaise orientation des normales pour FLUI_STRU peut donner des résultats faux
  | 
  | Correction
  | ----------
  | 
  | Afin de garantir des résultats corrects avec la modélisation FLUI_STRU, les mailles doivent être orientés du fluide vers la structure 
  | 
  | 1/ On remplace l'alarme FLUID1_4 par une erreur
  | et on améliore le message
  | 
  | """
  | Certaines normales entre fluide et structure ne sont pas orientées dans le bon sens,
  | l'orientation des normales doit être sortante du fluide vers la structure."""
  | 
  | 2/ on ajoute MODI_MAILLAGE/ORIE_PEAU pour orienter correctement les mailles dans les cas tests
  | qui émettent FLUID1_4
  | 
  | 17 cas tests dans vérification
  | 
  |     adls102a
  |     adlv100b
  |     adlv100c
  |     adlv100d
  |     adlv100e
  |     adlv100f
  |     adlv100g
  |     adlv100h
  |     adlv100o
  |     adlv100p
  |     fdlv111a
  |     fdlv111b
  |     fdlv112h
  |     fdlv112i
  |     fdlv112j
  |     fdnv100a
  |     zzzz256b
  | 
  | 1 cas test dans validation
  | 
  |     sdlv103b
  | 
  | 3/ bilan
  | ---
  | 
  | 2 tests sont NOOK
  | 
  | * adls102a : les résultats obtenus sont exactement l'inverse de la référence
  | 
  | Après analyse de la doc V, la référence est correcte. Mais il y a deux erreurs qui se compensent
  | - l'orientation des mailles FLUI_STRU en contact avec le "piston paroi" est inversée
  | - un bug dans le te0204 (PRES_REP) : la condition imposée par le "piston excitateur" est inversée 
  | la normale est selon -X, la pression est négative donc la résultante dvrait "tirer" selon -X
  | 
  | On corrige dans le te0204
  | 
  | * fdlv112j : la valeur des modes à légèrement changer suite à la réorientation 
  | (10 mailles mal orientés sur les 16 à l'interface)
  | 
  | Ce sont des résultats faux en NON_REGRESSION, je mets à jour les TEST_RESU
  | 
  | 4/ docs
  | 
  | U3.14.02 : mettre à jour la remarque
  | V8.01.112 : mise à jour des non-regressions

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U3.14.02,V8.01.112
- VALIDATION : src + valid
- DEJA RESTITUE DANS : 17.0.23


================================================================================
                RESTITUTION FICHE 33887 DU 19/06/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Erreur mémoire dans nmisot
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Un certain nombre de tests (ssnp15c entre autres) remontent un dépassement de mémoire dans nmisot.
  | 
  | ==52240== Invalid read of size 8
  | ==52240==    at 0x10B8A4EF: nmisot_ (nmisot.F90:403)
  | ==52240==    by 0x1104A5F3: lc0002_ (lc0002.F90:66)
  | ==52240==    by 0x11045AA2: lc0000_ (lc0000.F90:349)
  | ==52240==    by 0x106D4E63: redece_ (redece.F90:146)
  | ==52240==    by 0x11399F16: nmcomp_ (nmcomp.F90:204)
  | ==52240==    by 0x115788B5: dktnli_ (dktnli.F90:424)
  | ==52240==    by 0x119D0DAE: te0031_ (te0031.F90:269)
  | ==52240==    by 0x1090E0F0: te0000_ (te0000.F90:726)
  | ==52240==    by 0x108F8040: calcul_ (calcul.F90:389)
  | ==52240==    by 0x11350468: merimo_ (merimo.F90:261)
  | ==52240==    by 0x1142265B: nmrigi_ (nmrigi.F90:130)
  | ==52240==    by 0x11414ED8: nmprma_ (nmprma.F90:217)
  | ==52240==    by 0x11415FE9: nmprta_ (nmprta.F90:188)
  | ==52240==    by 0x114143A4: nmpred_ (nmpred.F90:141)
  | ==52240==    by 0x113FE31F: nmnewt_ (nmnewt.F90:260)
  | ==52240==    by 0x114AD48A: op0070_ (op0070.F90:249)
  | ==52240==    by 0x119A703A: ex0000_ (ex0000.F90:295)
  | 
  | Effectivement on cherche à lire un peu trop loin dans le vecteur deps(dimension 4) dans cette boucle
  | 
  |     do k = 1, 3
  |         depsth(k) = deps(k)-coef-(defap(k)-defam(k))
  |         depsth(k+3) = deps(k+3)-(defap(k+3)-defam(k+3))
  |         depsmo = depsmo+depsth(k)
  |     end do
  | 
  | 
  | Correction
  | ----------
  | Réécriture de la boucle en question pour respecter la taille du vecteur.
  | 
  |     depsmo = 0.d0
  |     do k = 1, 3
  |         depsth(k) = deps(k)-coef-(defap(k)-defam(k))
  |         depsmo = depsmo+depsth(k)
  |     end do
  |     depsmo = depsmo/3.d0
  |     do k = 4, ndimsi
  |         depsth(k) = deps(k)-(defap(k)-defam(k))
  |     end do
  |     do k = 1, ndimsi
  |         depsdv(k) = depsth(k)-depsmo*kron(k)*co
  |     end do

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : valgrind
- DEJA RESTITUE DANS : 17.0.22
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33868 DU 11/06/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : POST_RCCM : erreur d'affichage dans la table de sortie pour l'option 'EFAT', colonne FUEN_TOTAL
- FONCTIONNALITE :
  | Contexte
  | ########
  | 
  | ATR (Bureau d’Étude lyonnais) réalise pour le département CMC de la DT une analyse à la fatigue (de type B 3200) de la tubulure d'aspersion PZR PQY/DPY avec
  | POST_RCCM dans le cadre des DRR VD4 1300 MWe.
  | 
  | Les localisations inox en contact avec le fluide primaire doivent faire l'objet d'une analyse intégrant les effets du fluide primaire sur la durée de vie en
  | fatigue -> option 'EFAT'
  | 
  | Problème et analyse
  | ###################
  | 
  | Les valeurs renseignées dans les colonnes FU_TOTAL, FEN_TOTAL, FEN_INTEGRE et FUEN_TOTAL ne reflètent pas les équations de la RPP-3 du RCC-M dans le cas où la
  | quantité FEN_TOTAL est <= FEN_INTEGRE. En effet dans le RCC-M, FUEN_TOTAL est obtenu selon l'équation :  
  | 
  | FUEN_TOTAL = FU_TOTAL * max( 1, FEN_TOTAL / FEN_INTEGRE )
  | 
  | Il y a une erreur dans la routine rc32rs(), si FEN_TOTAL est <= FEN_INTEGRE, on devrait avoir FUEN_TOTAL == FU_TOTAL, mais ce n'est pas le cas.
  | 
  | Correctif
  | #########
  | Il faut modifier le remplissage de la table de sortie de POST_RCCM.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.01.107
- VALIDATION : test unitaire
- DEJA RESTITUE DANS : 17.0.23
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33867 DU 11/06/2024
================================================================================
- AUTEUR : SELLENET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : POST_RCCM plante si prise en compte du séisme et des effets d'environnement
- FONCTIONNALITE :
  | Contexte
  | ########
  | ATR (Bureau d’Étude lyonnais) réalise pour le département CMC de la DT une analyse à la fatigue (de type B 3200) de la tubulure d'aspersion PZR PQY/DPY avec
  | POST_RCCM dans le cadre des DRR VD4 1300 MWe.
  | 
  | Les localisations inox en contact avec le fluide primaire doivent faire l'objet d'une analyse intégrant les effets du fluide primaire sur la durée de vie en
  | fatigue -> option 'EFAT'
  | 
  | 
  | Problème
  | ########
  | ATR se trouve obligé de retirer le chargement sismique sinon POST_RCCM plante en floating point exception. Si on retire la partie "environnementale" (option
  | 'FATIGUE' au lieu de 'EFAT', c.à.d un calcul de fatigue "classique") et que l'on conserve le séisme, on sort alors de POST_RCCM sans encombre. 
  | 
  | 
  | Analyse
  | #######
  | Après une première analyse, et sauf incompréhension de ma part, il s'agît d'une variable initialisée à R8VIDE qui est utilisée dans la routine rc32env(). Le
  | code est écrit de la manière suivante dans la routine (appelante) rc32ac() :
  | 
  | . . debut rc32ac()
  | . . . . 
  | . . . . initialisation à R8VIDE des 13 cmps du vecteur '&&RC3200.MAX_RESU.'//lieu(im)
  | . . . . 
  | . . . . call rc32fact() -> rc32env()
  | . . . . 
  | . . . . écriture de la valeur de 'fuseism' dans la 12ème cmp du vecteur '&&RC3200.MAX_RESU.'//lieu(im) 
  | . . . . 
  | . . fin rc32ac()
  | 
  | rc32fact() appelle rc32env(), et dans rc32env() on cherche à lire en présence d'un chargement sismique la 12ème cmp du vecteur '&&RC3200.MAX_RESU.'//lieu(im)
  | afin de l'utiliser dans une opération. Cela ce situe au niveau du bloc conditionnel "if (zi(jfact+6*k+5) .eq. 2) then" ligne 74. Le problème est qu'à ce stade,
  | cette cmp vaut R8VIDE puisqu'on y écrit la valeur que l'on souhaite lire... à la fin de rc32ac.
  | 
  | 
  | Correctif
  | #########
  | La solution consiste donc à utiliser dans rc32env la valeur qu'on souhaite mettre à la douzième position de '&&RC3200.MAX_RESU.'//lieu(im). C'est à dire (de ce
  | que j'ai compris le facteur d'usage total du séisme car on en a déjà calculé la valeur.
  | 
  | 
  | Validation
  | ##########
  | Test élémentaire de POST_RCCM.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test unitaire
- DEJA RESTITUE DANS : 17.0.23
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33918 DU 01/07/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Backport incomplet de #33704
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Les tests erreu09d/erreu09e sont toujours en erreur sur Cronos.
  | 
  | 
  | Correction
  | ----------
  | 
  | Le backport de issue33704 est incomplet dans v16.
  | 
  | Il manque la fonction `CA.exit()`.
  | erreu09d.comm n'importe pas CA.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : erreu09d/erreu09e avec require_mpiexec=1
- NB_JOURS_TRAV  : 0.5

