==================================================================
Version 16.2.16 (révision 0c362833c821) du 2022-11-16 17:50 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 32395 PIGNET Nicolas           Utilisation de setMaterialField
 32391 PIGNET Nicolas           En version 16.2.14, le cas test verification arcad01a est en erreur sur gaia ...
 32400 PIGNET Nicolas           Tests ttlv100d,e trop longs
 32260 PIGNET Nicolas           En version 16.2.5  - Défaut de couverture -
 32378 BETTONTE Francesco       Sortie table POST_DYNA_MODA_T
 32175 AUDEBERT Sylvie          Macro-commande POST_ROCHE : arrêt intempestif d'un calcul post-transitoire
 32196 ABBAS Mickael            MODI_REPERE et contraintes SIEF_ELGA
 31872 ABBAS Mickael            Amortissement visqueux en harmonique
 31596 ABBAS Mickael            etendre LIAISON_GROUP à PRES ET PHI pour FLuide
 30627 ABBAS Mickael            [prima] Calcul de résilience - Plantage DVP_2
 31094 ABBAS Mickael            SAV  doc fiche 30124
 30520 ABBAS Mickael            Corrections à faire dans [R7.01.30]
 29853 ABBAS Mickael            Doc de comp010 (V6.07.110)
 25924 ABBAS Mickael            [Résorption] Options VAEX_EL* dans CALC_CHAMP
 31860 LE CREN Matthieu         [RUPT] Evolution de DEFI_FOND_FISS pour la macro POST_J
 32134 LE CREN Matthieu         [prima] Ecart sur CALC_G entre 14.8 et 16.1
 31514 LE CREN Matthieu         Erreur DVP_2 dans DEFI_FOND_FISS
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 32395 DU 15/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Utilisation de setMaterialField
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Je voudrais changer le paramètre M de DEFI_MATERIAU/WEIBULL dans le résultat RSSY2D.
  | 
  | J'essaie de changer le matériau avec la méthode setMaterialField.
  | Une fois la commande exécutée (RSSY2D.setMaterialField(chmatwb)), je vérifie le champ matériau que le 
  | résultat contient par l'appel à RSSY2D.getMaterialField().getName().
  | 
  | Malheureusement, je n'ai pas le résultat attendu. Au lieu de :
  | nom du nouveau champ de matériau voulu =  0000000d
  | nom du champ materiau obtenu =  00000004
  | 
  | j'aurais voulu avoir :
  | nom du nouveau champ de matériau voulu =  0000000d
  | nom du champ materiau obtenu =  0000000d
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | En fait si un matériau est déjà défini, la méthode ne fait rien même si le matériau est différent.
  | 
  | Je corrige dans Result. Même problème pour setModel et setElementaryCharacteristic.
  | 
  | Test dans zzzz503d.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz503d
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32391 DU 14/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.14, le cas test verification arcad01a est en erreur sur gaia et cronos
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | En version 16.2.14, le cas test verification arcad01a est en ERROR sur gaia et cronos
  | 
  | RESUSECH = THER_NON_LINE(ARCHIVAGE=_F(CRITERE='RELATIF',
  |                                       PRECISION=1e-06),
  |                          CHAM_MATER=CHMATTH,
  |                          COMPORTEMENT=_F(RELATION='SECH_GRANGER',
  |                                          TOUT='OUI'),
  |                          CONVERGENCE=_F(ITER_GLOB_MAXI=50),
  |                          ETAT_INIT=_F(PRECISION=1e-06,
  |                                       VALE=128.8),
  |                          EVOL_THER_SECH=RESUTHER,
  |                          EXCIT=(_F(CHARGE=SECH_IN),
  |                                 _F(CHARGE=SECH_OUT)),
  |                          INCREMENT=_F(LIST_INST=LINST_SE,
  |                                       PRECISION=1e-06),
  |                          METHODE='NEWTON',
  |                          MODELE=MODELETH,
  |                          NEWTON=_F(ITER_LINE_MAXI=0,
  |                                    REAC_ITER=0,
  |                                    RESI_LINE_RELA=0.001),
  |                          PARM_THETA=0.57,
  |                          SOLVEUR=_F(ACCELERATION='AUTO',
  |                                     ELIM_LAGR='LAGR2',
  |                                     FILTRAGE_MATRICE=-1.0,
  |                                     GESTION_MEMOIRE='AUTO',
  |                                     LOW_RANK_SEUIL=0.0,
  |                                     MATR_DISTRIBUEE='NON',
  |                                     METHODE='MUMPS',
  |                                     MIXER_PRECISION='NON',
  |                                     NPREC=8,
  |                                     PCENT_PIVOT=35,
  |                                     POSTTRAITEMENTS='AUTO',
  |                                     PRETRAITEMENTS='AUTO',
  |                                     REDUCTION_MPI=0,
  |                                     RENUM='AUTO',
  |                                     RESI_RELA=-1.0,
  |                                     STOP_SINGULIER='OUI',
  |                                     TYPE_RESOL='AUTO'))
  | 
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <F> <SUPERVIS_4>                                                                               ║
  |  ║                                                                                                ║
  |  ║ Erreur de syntaxe dans THER_NON_LINE                                                           ║
  |  ║                                                                                                ║
  |  ║ For keyword ETAT_INIT/PRECISION: Unauthorized keyword: 'PRECISION'                             ║
  |  ║                                                                                                ║
  |  ║ Exception détaillée ci-dessous.                                                                ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Cette erreur est fatale. Le code s'arrête.                                                     ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Suite un refactoring du catalogue de THER_NON_LINE, le mot-clé PRECISION n'est plus disponible que pour EVOL_THER.
  | 
  | Il est inutile ici, on le supprime

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : arcad01a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 32400 DU 18/11/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Tests ttlv100d,e trop longs
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Tests ttlv100d,e trop longs
  | 
  | Correction/Développement
  | ------------------------
  | 
  | J'étais reparti de la modélisation b mais comme HHO ne supporte pas prisme j'avais tout découpé en tétra.
  | 
  | J'ai refait un maillage plus petit en HEXA à partir des donnés de la géométrie (un bout de tuyau).
  | - ttlv100d: 41s -> 6s
  | - ttlv100e: 91s -> 14s

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ttlv100d,e
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32260 DU 05/09/2022
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 16.2.5  - Défaut de couverture -
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | Les fonctionnalités suivantes ne sont plus testées alors qu'elles l'étaient précédemment :
  | 
  |  AFFE_CHAR_THER/LIAISON_GROUP/GROUP_MA_1 (hplv107a~9.3s)
  |  AFFE_CHAR_THER/LIAISON_GROUP/GROUP_MA_2 (hplv107a~9.3s)
  |  ME#3D_HHO#1/CHAR_MECA_PRES_F (ssnv187o~31.8s)
  |  ME#3D_HHO#1/CHAR_MECA_PRES_R (ssnv187p~15.8s)
  |  ME#3D_HHO#2/CHAR_MECA_PRES_F (ssnv187t~74.5s)
  |  ME#3D_HHO#2/CHAR_MECA_PRES_R (ssnv187u~31.2s)
  |  ME#D_PLAN_HHO#1/CHAR_MECA_PRES_F (ssnv187n~12.6s)
  |  ME#D_PLAN_HHO#2/CHAR_MECA_PRES_F (ssnv187s~14.7s)
  | 
  | Modifications apportées :
  | =======================
  |  AFFE_CHAR_THER/LIAISON_GROUP/GROUP_MA_1 (hplv107a~9.3s)
  |  AFFE_CHAR_THER/LIAISON_GROUP/GROUP_MA_2 (hplv107a~9.3s)
  | 
  | => on remplace les GROUP_NO_1 et 2 par des GROUP_MA_1 et 2 dans tplv101c.
  | 
  |  ME#3D_HHO#1/CHAR_MECA_PRES_F (ssnv187o~31.8s)
  |  ME#3D_HHO#1/CHAR_MECA_PRES_R (ssnv187p~15.8s)
  |  ME#3D_HHO#2/CHAR_MECA_PRES_F (ssnv187t~74.5s)
  |  ME#3D_HHO#2/CHAR_MECA_PRES_R (ssnv187u~31.2s)
  |  ME#D_PLAN_HHO#1/CHAR_MECA_PRES_F (ssnv187n~12.6s)
  |  ME#D_PLAN_HHO#2/CHAR_MECA_PRES_F (ssnv187s~14.7s)
  | 
  | On réintègre les tests ssnv187n,o, p s, t et u. On remplace la loi MFRONT utilisée par ELAS_HYPER en GREEN_LAGRANGE.
  | Les résultats sont identiques.
  | 
  | Impact doc :
  | ----------
  | 
  | On remet les 6 modélisations dans V6.04.187.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V6.04.187
- VALIDATION : ssnv178n-p


================================================================================
                RESTITUTION FICHE 32378 DU 08/11/2022
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Sortie table POST_DYNA_MODA_T
- FONCTIONNALITE :
  | Problème :
  | ========
  | 
  | POST_DYNA_MODA_T avec OPTION = 'IMPACT' sort une liste des lieux de choc avec les principales valeurs des chocs (instant, effort 
  | max, ...). 
  | La colonne INTITULE sortie par la table est limitée à 8 caractères ce qui coupe les noms des intitulés des lieux de choc définis 
  | dans 
  | DYNA_VIBRA.
  | Il faudrait que la colonne INTITULE ait le même nombre de caractères que INTITULE défini dans COMPORTEMENT de DYNA_VIBRA (K24 au 
  | minimum).
  | 
  | Correction :
  | ==========
  | 
  | On modifie le fortran pour prendre en compte 24 caractères au lieu de 8 pour la colonne INTITULE de la table.
  | Dans sdll107a, on ajoute un INTITULE de 24 caractères dans DYNA_VIBRA.
  | On vérifie ensuite à l'aide d'un TEST_TABLE que le nom est bien complet dans la table.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdll107a
- DEJA RESTITUE DANS : 15.6.7


================================================================================
                RESTITUTION FICHE 32175 DU 06/07/2022
================================================================================
- AUTEUR : AUDEBERT Sylvie
- TYPE : anomalie concernant code_aster (VERSION 15.6)
- TITRE : Macro-commande POST_ROCHE : arrêt intempestif d'un calcul post-transitoire
- FONCTIONNALITE :
  | Pb. :
  | =======
  | Le calcul POST_ROCHE, option  TYPE_CHAR = ‘SISM_INER_ TRAN ’, s'arrête après un certain nombre de pas de temps (calculs indépendants) 
  | selon :
  | 
  | ║ <EXCEPTION> <DVP_2>                                                                            ║
  |  ║                                                                                                ║
  |  ║ Erreur numérique (floating point exception).                                                   ║
  |  ║                                                                                                ║
  |  ║                                                                                                ║
  |  ║ Il y a probablement une erreur dans la programmation.                                          ║
  |  ║ Veuillez contacter votre assistance technique.
  | Analyse :
  | =======
  | 
  | 1/ Le calcul plante lors de l'évaluation de la déformation vraie car la valeur de la contrainte vraie est -1 et on a alors une
  | puissance réelle d'un nombre négatif... 
  | La valeur -1 signifie que l'algo de recherche de zéro n'a pas fonctionné correctement (but non atteint après 30 itérations).
  | 
  | 2/ J'ai regardé en détail pourquoi l'algo ne convergeait pas. En fait on veut une amélioration du résidu de 1e-6, ce qui est parfois
  | impossible quand la fonction évalué est déjà très proche de zéro à l'itération 0.
  | 
  | Correction :
  | ==========
  | 
  | 1/ On blinde la formule de la déformation vraie pour renvoyer -1 si la contrainte vraie est -1 (=> l'alarme POSTROCHE_12 est présente pour
  | indiquer l'échec de la détermination de la contrainte vraie).
  | 
  | 2/ J'ajoute des seuils absolus pour gérer ces cas particulier. Je les mets le plus petits possibles pour ne pas risquer de dégrader les
  | solutions que l'algo est capable de déterminer : l'algo est considéré avoir convergé si l'évaluation de la fonction est inférieure à 1E-20
  | ou bien que son incrément entre deux itérations est inférieure à 1e-20.
  | 
  | On pourra affiner les choses si l'alarme POSTROCHE_12 apparaît à nouveau dans une étude.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : calcul inspiré de sdll157b
- DEJA RESTITUE DANS : 15.6.7
- NB_JOURS_TRAV  : 0.0


================================================================================
                RESTITUTION FICHE 32196 DU 26/07/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : MODI_REPERE et contraintes SIEF_ELGA
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La commande MODI_REPERE ne fonctionne pas pour le champ SIEF_ELGA (TENS_3D). Elle indique que le modèle change au cours du transitoire (ce qui n'est pas le
  | cas).
  | 
  | Cette combinaison n'est pas testée. 
  | 
  | Avec des champs aux noeuds, tout se passe bien.
  | 
  | 
  | Correction
  | ----------
  | 
  | Il y a une erreur dans la routine modirepresu.F90
  | do iord = 1, nbordr
  | . . iordr = zi(jordr-1+iord)
  | . . if (iordr .eq. 1) then
  | . . . modelRefe = model
  | . . else
  | . . . if (modelRefe.ne.model) then
  | . . . . lModelVariable = ASTER_TRUE
  | . . . endif
  | . . endif
  | end do
  | 
  | Alors qu'il faudrait écrire:
  | do iord = 1, nbordr
  | . . iordr = zi(jordr-1+iord) 
  | . . if (iord .eq. 1) then <====> ET NON iordr
  | . . . modelRefe = model
  | . . else
  | . . . if (modelRefe.ne.model) then
  | . . . . lModelVariable = ASTER_TRUE
  | . . . endif
  | . . endif
  | end do
  | 
  | Par ailleurs, la variable modelRefe n'est jamais initialisée.
  | 
  | Dans le cas d'un calcul non-linéaire, on a toujours le premier numéro d'ordre qui vaut zéro, la variable modelRefe n'est donc jamais initialisé
  | 
  | On détecte faussement un modèle variable (drapeau LModelVariable levé)
  | 
  | On modifie le test ssnv113a pour ajouter un changement de repère sur SIEF_ELGA. Plante avant correction, est OK après.
  | 
  | Report en 15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv113a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31872 DU 28/02/2022
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : Amortissement visqueux en harmonique
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La documentation de l'amortissement R5.05.04 fait état concernant l'amortissement modal visqueux (on cite) "en analyse harmonique une demande d'évolution avec
  | l'opérateur DYNA_LINE_HARM [R5.05.03] [U4.54.02] est déposée. Elle n'est pas traitée en version 3.6."
  | 
  | Sachant qu'on est en version 16, il me paraîtrait raisonnable de savoir si ça a été fait.
  | 
  | Correction
  | -----------
  | 
  | C'est possible de le faire maintenant. On modifie la doc en conséquence

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : R5.05.04
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31596 DU 25/11/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : etendre LIAISON_GROUP à PRES ET PHI pour FLuide
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | A ce jour, il n'existe pas de moyen de relier des DDL de PRES et PHI pour les modélisations fluide pour une grand nombre de noeuds : 
  | 
  |  - la commande AFFE_CHAR_MECA  / LIAISON_GROUP : est apparemment limité au ddl de déplacements mécaniques
  | 
  |  - la commande AFFE_CHAR_MECA  /LIAISON_DDL : permet uniquement de relier des ddl de 1 noeud par rapport a un autre, mais pas en grande quantité entre 2 groupes
  | de noeuds par exemple..
  | 
  | 
  | Correction
  | ----------
  | 
  | On peut utiliser n'importe quel DDL dans LIAISON_GROUP.
  | 
  | Sans suite donc

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30627 DU 21/01/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.4)
- TITRE : [prima] Calcul de résilience - Plantage DVP_2
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En tentant un calcul de résilience en version unstable, on tombe sur un plantage DVP_2 dans STAT_NON_LINE.
  | 
  | 7] libbibfor.so       00002ABEBBF422E9  dfdm3d_                    74  dfdm3d.F90
  | [7] libbibfor.so       00002ABEBC4CCF36  nmgeom_                    79  nmgeom.F90
  | [7] libbibfor.so       00002ABEBC4F58C0  nmpl3d_                   142  nmpl3d.F90
  | [7] libbibfor.so       00002ABEBC9CCDEF  te0139_                   205  te0139.F90[4] 
  | 
  | Correction
  | ----------
  | 
  | Sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31094 DU 02/06/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : SAV  doc fiche 30124
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | 
  | La doc R7.01.30 a été modifiée dans issue30124 
  | Une grande partie de la mise en forme a sauté, des équations ont "disparu", les numéros de référence aussi.
  | Y a du surlignage en fluo partout.
  | 
  | Il a été impossible de valider la doc
  | Les modifs liées à la fiche issue30124 sont minimes dans cette doc et qu'il faudrait donc recommencer en partant de la révision 9aaf44f79511 dans l'appli doc 
  | 
  | 
  | Correction
  | ----------
  | 
  | La doc a été entièrement reprise.
  | On peut fermer la fiche

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30520 DU 15/12/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : Corrections à faire dans [R7.01.30]
- FONCTIONNALITE :
  | La fiche  issue31094 a résolu la plupart des problèmes, sauf la figure de l'organigramme et la nuémrotation de ses équations.
  | 
  | On ferme sans suite

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29853 DU 07/05/2020
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : Doc de comp010 (V6.07.110)
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La doc V6.07.10 est incomplète, il manque les modélisations C et de E à I.
  | 
  | Correction
  | ----------
  | 
  | On ajoute les informations concernant ces modélisations sur le modèle des modélisations déjà documentées.
  | 1- Info sur la loi
  | 2- Doc R
  | 3- Valeurs des paramètres matériaux
  | 4- Résultats du dernier calcul

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : V6.07.110
- VALIDATION : rien


================================================================================
                RESTITUTION FICHE 25924 DU 10/01/2017
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.4)
- TITRE : [Résorption] Options VAEX_EL* dans CALC_CHAMP
- FONCTIONNALITE :
  | Déjà fait en 16.0.11 par issue31325
  | Sans suite donc

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31860 DU 23/02/2022
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RUPT] Evolution de DEFI_FOND_FISS pour la macro POST_J
- FONCTIONNALITE :
  | Fiche à classer sans suite
  | 
  | Les développements ont été réalisés dans la fiche associée à l'introduction de post_Jmod issue28296

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 32134 DU 15/06/2022
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : [prima] Ecart sur CALC_G entre 14.8 et 16.1
- FONCTIONNALITE :
  | Fiche à classer sans suite : aide utilisation

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : documentation
- NB_JOURS_TRAV  : 0.0


================================================================================
                RESTITUTION FICHE 31514 DU 21/10/2021
================================================================================
- AUTEUR : LE CREN Matthieu
- TYPE : evolution concernant code_aster (VERSION 15.4)
- TITRE : Erreur DVP_2 dans DEFI_FOND_FISS
- FONCTIONNALITE :
  | Fiche à classer sans suite, la documentation précise le cadre d'utilisation.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : documentation
- NB_JOURS_TRAV  : 0.0

