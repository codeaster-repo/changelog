==================================================================
Version 16.0.14 (révision 0158b8a7b957) du 2021-10-29 14:43 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31180 FLEJOU Jean Luc          Pb dans la sortie au format MED des éléments à SP
 31532 COURTOIS Mathieu         run_aster : lancement manuel
 30125 FLEJOU Jean Luc          [GC] Option DEPL_ELGA pour les éléments à sous-points
 30909 BETTONTE Francesco       [XFEM] wtnv145a est en erreur sur Scibian9
 31522 PIGNET Nicolas           Le test de vérification mpi "zzzz504a" est en erreur en version 16.0.11
 31526 FLEJOU Jean Luc          Documentation sur l'utilisation de MAZARS_GC
 31521 COURTOIS Mathieu         De nombreux tests de validation cassés en version 15.4.7 et 16.0.12
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31180 DU 21/06/2021
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 16.1)
- TITRE : Pb dans la sortie au format MED des éléments à SP
- FONCTIONNALITE :
  | Les DKT multi-couches sont des éléments avec 3 sous-points par couche.
  | 
  | Lors d'un IMPR_RESU au format MED des valeurs aux sous-points, si on mélange des TRI3 et QUA4,
  | le fichier au format MED n'est pas lisible dans ParaVis.
  | Ce type de mélange d'élément n'est pas dans la base de cas test : SP_SHELL TRI3 et QUA4
  | 
  | Dans l'exemple joint :
  | - 2 QUA4          : c'est OK
  | - 4 TRI3          : c'est OK
  | - 1 QUA4 + 2 TRI3 : c'est NOOK
  | 
  | Analyse :
  | =========
  | Vu avec Anthony (fiche pléiade #23738) :
  | - il y a un problème dans code_aster
  | - il y a un problème dans le MedReader
  | 
  | Côté code_aster :
  | -----------------
  | Bien que le mdump3 passe quand on mélange des TRI3-QUA4, le QUA4 se retrouve avec 3 points de Gauss.
  | Code_aster ne fait pas ce qu'il faut.
  | 
  | Côté MedReader :
  | ----------------
  | Anthony n'avais pas prévu l'éventuel changement de support pour les SP_SHELL. Donc même avec un fichier
  | correct cela n'aurait pas fonctionné. La modification est réalisée dans MedReader et disponible en DEV.
  | 
  | Tests :
  | =======
  | Test zzzz413d (dans la suite des a,b,c qui teste l'impression au format MED des sous-points) qui produit des fichier "med"
  | - Seulement des TRI3
  | - Seulement des QUA4
  | - Mélange de TRI3 et QUA4
  | 
  | Ce sont les fichiers "med" produit par code_aster qui sont dans la base de tests de MedReader du côté Salome.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.413
- VALIDATION : passage cas test
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 31532 DU 28/10/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : run_aster : lancement manuel
- FONCTIONNALITE :
  | Depuis issue31473, on peut directement lancé un .comm/.py :
  | 
  | run_aster fichier.comm
  | 
  | 
  | Anomalie : 
  | On ne peut plus faire 'run_aster' sans argument qui lance Python sans jeu de commandes, juste en important code_aster.
  | 
  | Évolution :
  | Si on ne définit pas de fichier export, cela signifie qu'on lit les fichiers de données en indiquant le chemin :
  | 
  | meshfile = "/home/user/.../file.med"
  | 
  | ou mieux, en relatif par rapport au fichier de commandes :
  | 
  | here = os.path.dirname(__file__)
  | mesh.readMedFile( os.path.join(here, "file.med")
  | 
  | Si on n'a pas fait les imports des commandes dans le .comm, un ficher 'xxx.changed.py' est automatiquement créé pour y ajouter ces imports.
  | Maintenant, on redéfinit également la variable "__file__" pour qu'elle contienne l'ancien nom de fichier afin de remonter au répertoire contenant le .comm
  | original.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : lancement interactif
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 30125 DU 21/07/2020
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : [GC] Option DEPL_ELGA pour les éléments à sous-points
- FONCTIONNALITE :
  | Il est possible dans AsterStudy de visualiser un CHAMP_ELGA "déformée". Pour réaliser cela,
  | un champ DEPL_ELGA est calculé à partir du champ DEPL (aux nœuds) à l'aide des fonctions
  | de forme de l'élément.
  | 
  | Pour les éléments à sous-points, la connaissance des fonctions de forme ne suffit pas, il
  | faut également connaître la position des sous-points ainsi que les équations cinématiques
  | reliant les déplacements de l'élément support aux déplacements des sous-points. Ces
  | informations sont seulement disponibles via le CARA_ELEM.
  | 
  | Proposition :
  | -------------
  | Développer l'option DEPL_ELGA pour les éléments à sous-points (grilles et plaques) dans CALC_CHAMP.
  | L'impression de ce champ ELGA aux sous-points est déjà gérée par le format MED (impression aux sous-points).
  | Les grilles et plaques sont celles dont on a besoin pour les post-traitements et vérifications
  | de la transmission de la cinématique dans les liaisons.
  | voiles-plancher lors des études SCS.
  | - Les grilles excentrées   ==> nappes d'acier
  | - Les plaques multicouches ==> tôles des voiles et planchers SCS 
  | 
  | Impact sur le code :
  | --------------------
  | - Pas de nouveau mot clef.
  | - Pas d'impact sur l'impression
  | - Nouvelle option DEPL_ELGA
  | - Nouveau TE, qui traite la nouvelle option
  | - Ajout de l'option dans les catalogues
  | 
  | Tests :
  | =======
  | Test zzzz413e (dans la suite des a,b,c,d qui testent l'impression au format MED des sous-points).
  | Cas test est basé sur le maillage de zzzz419.
  | TEST_RESU sur les composantes DX, DZ de DEPL_ELGA aux sous-points.
  | 
  | 
  | Pour les éléments à sous-points :
  | ---------------------------------
  | La position des sous-points est fonction, pour :
  | - les grilles de l'excentrement.
  | - les plaques de l'épaisseur, du nombre de couches et de l'excentrement.
  | - les poutres des coordonnées des fibres
  | - les tuyaux du rayon, de l'épaisseur ainsi que du nombre de secteur et de couche
  | La cinématique pour :
  | - Grilles excentrées ou pas              : cinématique "simple"
  | - Plaques multicouches excentrées ou pas : cinématique "simple"
  | - Poutres multifibres                    : cinématique avec gauchissement (ou pas), centre de torsion
  | - Tuyaux                                 : cinématique avec ovalisation
  | 
  | La cinématique "simple" c'est : U(sous-point) = U(point-gauss) + W^Xsp
  | 
  | Remarque :
  | ----------
  | Les poutres et tuyaux seront traités si besoin.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.01.413, U4.81.04
- VALIDATION : passage cas test + études
- NB_JOURS_TRAV  : 10.0


================================================================================
                RESTITUTION FICHE 30909 DU 20/04/2021
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [XFEM] wtnv145a est en erreur sur Scibian9
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | wtnv145a est en erreur sur Scibian9. L'erreur se produise seulement avec l'option JEVEUX='OUI'.
  | 
  | Valgrind détecte des fuites de mémoire lors de la conversion entre champs simple et champs longs.
  | 
  | Le problème se produit dans le te0514 car on cherche à écrire systématiquement dans le champ PJONCNO alors qu'il n'est prevue que 
  | pour 
  | les éléments multi-Heaviside HM-XFEM cohésifs (cf. D4.10.02)
  | 
  | Correction
  | ----------
  | 
  | On ajoute une condition pour interdire l'écriture dans PJONCNO si ce n'est pas un élément multi-Heaviside HM-XFEM cohésif.
  | 
  | Avec report en v15.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : wtnv145a,wtnv145b
- NB_JOURS_TRAV  : 8.0


================================================================================
                RESTITUTION FICHE 31522 DU 25/10/2021
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Le test de vérification mpi "zzzz504a" est en erreur en version 16.0.11
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le test est en erreur sur toutes les machines.
  | On peut voir l'erreur suivante dans le fichier ".o" :
  | 
  | Erreur de programmation.
  | Condition non respectée:
  |     .false.
  | Fichier /fscronos/home/G79848/dev/codeaster-default/src/bibfor/utilitai/dismoi.F90, ligne 186
  | 
  | 
  | Un bisect est en cours.
  | 
  | le test fonctionne à la révision a352a94f016b :
  | [#31456] Fix pickling with empty cara_elem
  | 
  | et il est en erreur à la révision 157304691e53 :
  | [#31325] Resorb options VAEX_XXXX
  | 
  | 
  | Correction/Développement
  | ------------------------
  | C'était un problème dans impr_resu en parallèle. C'est corrigé dans issue31500
  | 
  | sans src

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz504a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 31526 DU 26/10/2021
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant Documentation (VERSION 15.*)
- TITRE : Documentation sur l'utilisation de MAZARS_GC
- FONCTIONNALITE :
  | Les documents modifiés
  | 
  | U2.03.07 : Panorama des outils disponibles pour réaliser des calculs de structure de Génie Civil en béton
  | U2.06.10 : Réalisation d'une étude de génie civil sous chargement sismique
  | U4.51.11 : Comportements non linéaires
  | U3.11.07 : Modélisation POU_D_EM
  | 
  | ==> Déposés dans l'application documentaire
  | 
  | 
  | Distinction entre :
  | MAZARS_GC en 1D ==> Modèle unilatéral avec refermeture des fissures ==> PMF
  | MAZARS_GC en C_PLAN ==> Modèle classique
  | 
  | 
  | Remarque :
  | ----------
  | La fiche issue31366 devrait régler le reste des 'questions'.
  | Mais plus long (donc à planifier) car c'est avec restitution de code/tests/doc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U2.03.07,U2.06.10,U4.51.11,U3.11.07
- VALIDATION : humaine
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 31521 DU 25/10/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant devtools (VERSION *)
- TITRE : De nombreux tests de validation cassés en version 15.4.7 et 16.0.12
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | 2/3 des cas-tests de validation ont échoué en CPU_LIMIT sur Cronos et Gaia.
  | 
  | 
  | Correction
  | ----------
  | 
  | Les tests ont été exécutés sur la frontale d'où le CPU_LIMIT et ERROR pour les tests MPI.
  | 
  | Récemment, 18 octobre, un wrapper a été ajouté autour de run_testcases pour que le développeur puisse l'utiliser directement, sans avoir à charger
  | l'environnement.
  | Or ce wrapper devine où est le fichier d'environnement et il essaie dans dev/codeaster/src (env.d/).
  | Il charge alors l'environnement des prérequis de la version courante.
  | Or la configuration de asrun fourni dans les prérequis est basique et ne définit pas les particularités du batch sur les clusters.
  | 
  | On modifie donc la construction des prérequis pour que, sur Cronos et Gaia, l'installation de asrun en tant que prérequis pointe vers la configuration générique
  | du cluster qui contient entre autres le paramétrage de Slurm.
  | (Fait rétroactivement sur les anciennes versions des prérequis déjà installées)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : run_testcases_validation
- NB_JOURS_TRAV  : 0.5

