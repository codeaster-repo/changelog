========================================================================
Version 7.1.11 du : 09/07/2003
========================================================================


-----------------------------------------------------------------------
--- AUTEUR adbhhvv V.CANO   DATE  le 07/07/2003 a 15:18:36

--------------------------------------------------------------------------
REALISATION EL 2000-070
   NB_JOURS_TRAV : 70
   INTERET_UTILISATEUR : OUI
   TITRE "FAIRE DU SIMO_MIEHE AVEC LE MODELE DE ROUSSELIER EN NON LOCAL"
   FONCTIONNALITE
   Implantation du mod�le de Rousselier avec l'option SIMO_MIEHE
   en version non locale (mod�le � gradient de variables internes).
   On introduit 3 variables suppl�mentaires qui repr�sentent les gradients
   dans les trois directions de la d�formation plastique cumul�e.

   RESU_FAUX_VERSION_EXPLOITATION   : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  : NON
   RESTITUTION_VERSION_EXPLOITATION : NON
   RESTITUTION_VERSION_DEVELOPPEMENT: OUI
   IMPACT_DOCUMENTAIRE : OUI
    DOC_U : U4.51.03
      EXPL_ :
        A) rajouter dans le paragraphe 3.3.1 Relation
        sous % Mod�les trait�s en formulation non locale,
        le texte suivant :
         / 'ROUSSELIER'
        B) Rajouter dans le paragraphe 3.3.1.3 Mod�les non locaux
        le texte suivant:
         / 'ROUSSELIER'
           Cf. [R5.04.11] pour plus de d�tail pour la version non locale.
           Mod�lisation non locale support�e : GRAD_VARI
           Nombre de variables internes : 12
           Signification :
           V1 :  d�formation plastique cumul�e,
           V2 � V4 : gradient de la d�formation plastique cumul�e suivant
           les axes x, y, z, respectivement,
           V5 : porosit�,
           V6 � V11 : d�formations �lastiques utilis�es pour SIMO et MIEHE,
           V12 : indicateur de plasticit�
            (0 si l�astique,
             1 si plastique et solution r�guli�re,
             2 si plastique et solution singuli�re).
    DOC_R : R5.04.11
    DOC_V : V6.03.122

   VALIDATION
   1 cas test SSNP122A
   Traction simple qui compare les versions locale et non locale

   DETAILS    			 :
   Le mod�le concern� est ROUSSELIER avec l'option SIMO_MIEHE.
   Modification des deux fortrans suivants:
   edcomp.f (rajout de la loi ROUSSELIER)
   nmcomp.f (rajour de la loi ROUSSELIER sous GRADVARI)
   Nouveaux fortrans :
   edronl.f (calcul des variables internes au point de Gauss)
   lcronl.f (calcul des contraintes � l'instant plus)
   edrofg.f
   edrofs.f
   edroma.f (donn�es mat�riaux)
   edropc.f
   edrotg.f (matrice tangente pour le probl�me primal)
   edroy1.f
   edroy2.f
   edroyi.f
   Remarque: la matrice tangente utilis�e pour la r�solution
   des �quations d'�quilibre par Newton est la matrice �lastique
   en grandes d�formations (celle de SIMO_MIEHE)


-----------------------------------------------------------------------
--- AUTEUR cibhhlv L.VIVAN   DATE  le 07/07/2003 a 14:13:09

--------------------------------------------------------------------------
CORRECTION AL 2003-157
   NB_JOURS_TRAV  : 0.5
   POUR_LE_COMPTE_DE : J.M.PROIX
   INTERET_UTILISATEUR : OUI
   TITRE  POST_K1_K2_K3 et impressions des instants de calcul

   FONCTIONNALITE
   Etat des lieux:
   Le calcul des K1_K2_K3 se r�alise � partir d'un r�sultat ou d'un champ
   de d�placement.
   Dans le cas d'un r�sultat, les instants de calcul sont dans la
   structure de donn�es, r�cup�r�s et imprim�s.
   Dans le cas d'un champ, il n'existe pas d'instant. Le choix a �t�
   d'initialiser un instant fictif � 0.
   correction:
   Dans le cas d'un champ, il n'existe pas de param�tres INST dans la
   table en sortie d'op�rateur.

   DETAIL
   On en profite pour supprimer le message d'Alarme:
   <A> <DEFI_FOND_FISS> ATTENTION : VEILLER A CE QUE LE GROUP_NO ...
                        DEFINISSANT LE FOND DE FISSURE SOIT ORDONNE
   dans le cas ou le GROUP_NO ne contient qu'un ou deux noeuds.

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION
      validation sur un champ: modification de sslv134c
      passage des 7 cas tests validant POST_K1_K2_K3
   IMPACT_DOCUMENTAIRE : NON

--------------------------------------------------------------------------
CORRECTION AL 2003-158
   NB_JOURS_TRAV  : 0.5
   POUR_LE_COMPTE_DE : J.PELLET
   INTERET_UTILISATEUR : OUI
   TITRE  IMPR_RESU/GMSH d'un r�sultat ne contenant pas DEPL

   FONCTIONNALITE
   Etat des lieux:
   on veut imprimer un r�sultat issu d'un ECLA_PG qui ne contient que
   le champ 'SIEF_ELGA_DEPL'.
   Le code s'arrete en fatal:
   <F> <IMPR_RESU> <DISMCP> CHAMP INEXISTANT:TOTO2   .001.000001
   correction:
   Dans la routine irgmsh.f , on r�cup�re le maillage dans un champ.
   Malheureusement on cherchait � partir du premier champ d�fini dans
   le .DESC de la SD R�sultat sans tester l'existence de ce champ.
   La correction consiste � balayer les champs et � tester s'il existe
   ou pas.

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION
      passage de l'�tude.
   IMPACT_DOCUMENTAIRE : NON

--------------------------------------------------------------------------
CORRECTION AL 2003-161
   NB_JOURS_TRAV  : 0.5
   POUR_LE_COMPTE_DE : J.PELLET
   INTERET_UTILISATEUR : NON
   TITRE  CREA_CHAMP/EVAL plantage

   FONCTIONNALITE
   Etat des lieux:
   <F> <CREA_CHAMP> <CESCEL> NOMBRES DE POINTS DIFFERENTS POUR LA
                             MAILLE: MA10  CHAM_ELEM DE: NEUT_R
   le plantage a lieu lorsque le nombre de points varie d'une maille
   � l'autre.
   Correction:
   Dans la routine ceseva.f le champ en sortie est dimensionn� au nombre
   max de points sur une maille. En sortie de routine, on utilise la
   routine CESTAS qui tasse le CHAM_ELEM_S lorsqu'il a �t� allou� trop
   grand. Il n'y a plus de probl�me dans la routine cescel.f qui transforme
   le CHAM_ELEM_S en CHAM_ELEM

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION
      passage de l'etude
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR cibhhpd D.NUNEZ   DATE  le 08/07/2003 a 11:31:31

--------------------------------------------------------------------------
REALISATION EL 2003_104
   NB_JOURS_TRAV  : 7
   POUR_LE_COMPTE_DE :J-M.PROIX
   INTERET_UTILISATEUR : OUI
   TITRE Crea_maillage : extrusion des mailles coques en 3D

   FONCTIONNALITE
   nouvelle fonctionnalit� dans CREA_MAILLAGE : a partir de la donn�e
   d'un groupe de mailles QUAD4 (resp TRIA3), on construit le maillage
   HEXA8 (resp. PENTA6) s'appuyant dessus, par "�paississement" suivant
   la moyenne des normales communes � tous les �l�ments concourrant en
   un noeud.
   Si on souhaite des mailles HEXA20 refaire un CREA_MAILLAGE(LINE_QUAD=_F()).

   Syntaxe :
   A partir du maillage MA, on cr�e le maillage MA2 comme suit :
   MA2 = CREA_MAILLAGE(MAILLAGE = MA,                            [maillage]
                   COQU_VOLU =_F( o NOM= nom,                    [TXM]
                                  o GROUP_MA = gma,              [group_ma]
                                  o EPAIS = ep,                  [R8]
                                  f PREF_MAILLE = / prma         [TXM]
                                                  / 'MS'         [d�faut]
                                  f PREF_NUMA = / ima            [I]
                                                / 1              [d�faut]
                                  f PREF_NOEUD = / prno          [TXM]
                                                 / 'NS'          [d�faut]
                                  f PREF_NUNO = / ino            [I]
                                                / 1              [d�faut]
                                  o  PLAN= ('MOY','SUP','INF'),  [TXM]
                                     si PLAN ='MOY
                                       / SURF_INT='OUI',
                                       / SURF_EXT='OUI',
                                ))
   GROUP_MA est la liste des groupes de mailles du maillage MA pour
            lesquels on souhaite l'extrusion.
   EPAIS est l'�paisseur de la coque
   PREF_MAILLE prefixe des mailles cr��es, par defaut "MS"
   PREF_NUMA   num�ro initial des mailles cr��es, par d�faut 1
   PREF_NOEUD  prefixe des noeuds cr��s, par defaut "NS"
   PREF_NUNO   num�ro initial des noeuds cr��s, par d�faut 1
   PLAN : on dit si la surface d�crite par GROUP_MA constitue
    *  plan_sup de la coque : dans ce cas les noeuds cr�es seront translat�s
                              suivant la direction  -N.epais
    *  plan_inf de la coque : dans ce cas les noeuds cr�es seront translat�s
                              suivant la direction  +N.epais
    *  plan_moyen de la coque : dans ce cas on d�crit SURF_INT et SURF_EXT
                                pour translater la surface initiale d'une
                                demi-epaisseur en -N.e/2 ou +N.e/2
                                puis on cr�e les nouveaux noeuds en(+ ou -)N.e
                                suivant le cas

   Le code �met un message d'Alarme si en un noeud les normales des mailles
   concourrant ont un angle sup�rieur � 90 degr�s :
 <A> <CREA_MAILLAGE> <COQU_VOLU> L'ANGLE AU NOEUD N1       FORME PAR LE VECTEUR
     NORMAL DE LA MAILLE M3       ET LE VECTEUR NORMAL DE LA MAILLE M4
     EST SUPERIEUR A 90 DEGRES ET VAUT 1.800E+02  DEGRES.

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION
     forma01c
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.23.02


-----------------------------------------------------------------------
--- AUTEUR d6bhhjp J.P.LEFEBVRE   DATE  le 07/07/2003 a 11:42:26

--------------------------------------------------------------------------
CORRECTION AL 2002_412
   NB_JOURS_TRAV  : 1
   INTERET_UTILISATEUR : NON
   TITRE METIS ET MED lisent sur l'unit� logique 81
   FONCTIONNALITE
   DETAILS
   Dans la fiche il appara�t la demande suivante :
   "Il faudrait un distributeur de num�ros d'unit�s logiques ..."
   La refonte des utilitaires de gestion des unit�s logiques ouvre cette possibilit�.
   La routine ULNUME renvoie un num�ro d'unit� logique "libre".
   Les routines PREML1 et PREMLE sont modifi�es pour ne plus utiliser obligatoirement les
   unit�s logiques 81 et 85.
   La mise en oeuvre de ce m�canisme a n�cessit� la modification du script de lancement
   de l'utilitaire de renum�rotation METIS. Le script travaille maintenant dans un
   r�pertoire particulier sous le r�pertoire d'ex�cution et admet un param�tre suppl�mentaire
   qui est le nom du fichier r�sultat, par d�faut valant fort.85.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION

-------------------------------------------------------------------------------
REALISATION EL 2003-117
   NB_JOURS_TRAV  : 0.1
   INTERET_UTILISATEUR : NON
   TITRE Commande IMPR_JEVEUX
   FONCTIONNALITE
   DETAILS
   On met � jour le catalogue de la commande .
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.91.21
       EXPL_ : mettre � jour la liste des noms de l'attribut pour les objets syst�mes
               op�rande NOMATR
   VALIDATION


-----------------------------------------------------------------------
--- AUTEUR durand C.DURAND   DATE  le 04/07/2003 a 16:18:48

------------------------------------------------------------------------------
REALISATION EL 2003-123
   NB_JOURS_TRAV  : 2.5
   INTERET_UTILISATEUR : NON
   TITRE detruire et les formules
   FONCTIONNALITE superviseur, detruire, formule, macr_recal
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION zzzz100a
   DETAILS
   Le superviseur avait �t� brid� (ops de detruire) pour ne pas permettre
   la destruction des formules. En effet, pour les d�truire proprement et
   totalement, il faut, en plus de d�truire l'objet python et les objets
   jeveux pr�fix�s par le nom de la formule, supprimer toute r�f�rence
   dans les common fortran '&&SYS FI'

   Nouvelle routine fidetr.f qui fait ce travail.

   Modification de ops007 (detruire) pour appeler fidetr quand sd formule.

   Ca a des cons�quences sur macr_recal. Je fais des destructions plus cibl�es
   dans la m�thode detr_concepts : seuls les concepts du fichier esclave sont
   d�truits d'une it�ration � l'autre.

   Un fichier esclave de MACR_RECAL peut donc contenir d�sormais toute
   commande ASTER sans restriction.

------------------------------------------------------------------------------
CORRECTION AL 2003-162
   NB_JOURS_TRAV  : 2
   INTERET_UTILISATEUR : NON
   TITRE lire_resu au format MED et champ VARI_ELNO
   DETAILS
   pour le cas d'un vari_elno, il faut d�finir les mots cl�s NOM_CMP_MED
   et NOM_CMP_ASTER en pr�cisant V1, V2 ....
   correction d'un bug qui ne se manifeste que sur linux.
   FONCTIONNALITE LIRE_RESU LIRE_CHAMP au format MED
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
     zzzz162a

------------------------------------------------------------------------------
CLASSEMENT SANS SUITE AL 2003-156
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE affe_materiau et usage dans les macros python
   FONCTIONNALITE macro commandes
   DETAILS
   erreur dans profil d'execution de l'utilisateur
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR jmbhh01 J.M.PROIX   DATE  le 04/07/2003 a 15:28:00

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.1
   INTERET_UTILISATEUR : NON
   TITRE  J'ai cass� Stanley en V7
   FONCTIONNALITE
   Histor de la semaine derni�re :
   ...
   - en v7, on a  copi� un exemple de ficher env.py (avec les 4
   configurations en commentaires) dans  bibpyt/Stanley, pour
    founir un exemple aux utilisateurs.

    - mais quand on utilisait Stanley avec la 7.1.10, il ne lisait plus le env.py de
    l'utilisateur mais celui d�pos� dans bibpyt/Stanley, qui n'a que valeur d'exemple.

    - je renomme donc ce fichier exemple_env. Comme dans FORMA03 tous les import stanley sont
    en commentaires, cela ne devrait poser aaucun probleme.

   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
      malheureusement non. IL faudra essayer en interactif


-----------------------------------------------------------------------
--- AUTEUR pabhhhh N.TARDIEU   DATE  le 07/07/2003 a 13:27:23

------------------------------------------------------------------------------
REALISATION EL 2002-135
   NB_JOURS_TRAV  : 5.5
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE Fusion des cas tests ssnv105, ssnv106 et ssnv128
   FONCTIONNALITE
   Ces 3 cas tests mod�lisent le m�me probl�me. On les fusionne en un seul
   cas test intitul� ssnv128. Celui-ci correspond � une analyse quasi-statique
   d'un probl�me de m�canique avec contact et frottement. Une plaque est soumise
   � des forces de pression et est comprim�e sur un plan ind�formable.
   Ce test s'appuie sur des r�sultats 2D propos�s par le GRECO en grandes
   d�formations.
   DETAIL
   VALIDATION :
   On d�finit 15 mod�lisations diff�rentes pour ce cas test
   (mod�lisation a - o)
   ssnv128a : mod�lisation 2D QUAD4 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation automatique et appariement nodal
   ssnv128b : mod�lisation 2D QUAD8 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation control�e et appariement ma�tre-esclave
   ssnv128c : mod�lisation 2D TRIA3 lagrangien et p�nalis� en MF et LDLT
              sans r�actualisation et appariement ma�tre-esclave
   ssnv128d : mod�lisation 2D TRIA6 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation control�e et appariement ma�tre-esclave
   ssnv128e : mod�lisation 3D HEXA8 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation automatique et appariement nodal
   ssnv128f : mod�lisation 3D HEXA20 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation control�e et appariement ma�tre-esclave
   ssnv128g : mod�lisation 3D HEXA27 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation automatique et appariement ma�tre-esclave
   ssnv128h : mod�lisation 3D PENTA6 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation control�e et appariement ma�tre-esclave
   ssnv128i : mod�lisation 3D PENTA15 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation automatique et appariement ma�tre-esclave
   ssnv128j : mod�lisation 3D TETRA4 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation control�e et appariement et normale
              ma�tre-esclave
   ssnv128k : mod�lisation 3D TETRA10 lagrangien et p�nalis� en MF et LDLT
              avec r�actualisation automatique et appariement ma�tre-esclave
   ssnv128l : mod�lisation 3D HEXA8 lagrangien en MF et LDLT
              avec r�actualisation automatique et appariement ma�tre-esclave
              plaque inclin�e � 45�
   ssnv128m : mod�lisation 2D QUAD4 continue en MF et LDLT
              avec appariement ma�tre-esclave
   ssnv128n : mod�lisation 3D HEXA8 continue en MF et LDLT
              avec appariement ma�tre-esclave
   ssnv128o : mod�lisation 2D QUAD4 lagrangien en MF et LDLT
              sans r�actualisation et appariement ma�tre-esclave

   cas tests supprim�s : ssnv105h ssnv105j ssnv105k ssnv105m ssnv105n ssnv105o
                         ssnv105p ssnv105q ssnv106a ssnv128e ssnv128f ssnv128g
                         ssnv128h ssnv128i ssnv128j ssnv128k ssnv128l ssnv128m
   cas tests modifi�s  : ssnv128a ssnv128b ssnv128c ssnv128d ssnv128e ssnv128f
                         ssnv128g ssnv128h ssnv128i ssnv128j ssnv128k ssnv128l
                         ssnv128m ssnv128n ssnv128o

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   IMPACT_DOCUMENTAIRE : OUI
      DOC_V : V6.04.128

------------------------------------------------------------------------
CORRECTION AL 2003-164
   NB_JOURS_TRAV  : 4.
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE Probl�me dans l'ajout de liaisons de frottements adh�rentes
   FONCTIONNALITE
   Ajout de liaisons de frottements adh�rentes par la m�thode lagrangienne
   en 2D et en 3D.
   DETAIL
   En 2d :
   Lorsqu'on ajoute des liaisons de frottements adh�rentes, on ne v�rifiait
   pas si ces liaisons �taient � pivots nuls ou pas. On ajoute un test dans
   la routine fro2gd pour corriger.
   En 3d :
   Lorsqu'on ajoute un ensemble de liaisons adh�rentes ou glissantes
   on ne v�rifie pas si celui-ci contient des liaisons � pivots nuls.
   On cr�e la routine CFELPV qui re�oit en donn�e le num�ro de la liaison
   et le type de la liaison (contact, frottement adh�rent suivant les deux
   directions, ou suivant une seule direction) et qui renvoit en retour un
   logique qui vaut "true" si la liaison est � pivot nul ou "false" sinon.
   De plus deux appels � des routines �taient faits pour v�rifier
   la coh�rence des liaisons adh�rentes. Or s'il n'y a pas de liaisons
   de contact on cr�ait des vecteurs de dimensions nuls. On corrige
   en v�rifiant avant l'appel � ces routines que NBLIAC est diff�rent de 0.
   routine modifi�e : FRO2GD,FROLGD
   routine cr��e    : CFELPV

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION :
      tests de contact
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
CORRECTION AL 2003-165
   NB_JOURS_TRAV  : 2.
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE Probl�me dans l'�limination des liaisons � pivot nul
   FONCTIONNALITE
   V�rification des liaisons � pivots nuls pour les m�thodes lagrangiennes
   en 2D et 3D.
   DETAIL
   Dans la routine d'�limination des pivots nuls pour la m�thode lagrangienne
   toutes les liaisons n'�taient pas prises en compte. Ce qui peut entra�ner
   un plantage en matrice singuli�re.
   routine modifi�e : ELPIV2

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION :
      tests de contact
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
CORRECTION AL 2003-166
   NB_JOURS_TRAV  : 2.
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE  Calcul de A.C-1.AT
   FONCTIONNALITE
   Probl�me dans l'optimisation du calcul de A.C-1.AT pour toutes les m�thodes
   de contact et de frottement.
   DETAIL
   Dans la routine cfaca1, on calcule LGBLOC de la fa�on suivante :
      LGBLOC = (LG-LGBLMA-LGIND-15*NEQ)/ (2* (NEQ+1))
   Or ne v�rifiait pas que cette valeur n'�tait pas n�gative ou nulle
   (ce qui peut n�anmoins arriver). Donc :
   1) apr�s JEDISP(1,LG)
      on remplace LG=LG/2 par LG =(3*LG)/4 (all�gement de la pr�caution)
   2) apr�s le calcul LGBLOC =( LG-LGBLMA -LGIND ....)
      IF (LGBLOC.LE.0) THEN
          LGBLOC = MIN(96,LG/4)
      END IF
   routine modifi�e : CFACA1

   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION :
     tests de contact
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 1.5
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE Probl�me de contact entre segments en 3D
   FONCTIONNALITE
   Contact ma�tre-esclave en 3D entre segments
   DETAIL
   Dans la routine PROJLI (projection d'un noeud esclave sur
   un segment) on utilise le vecteur VECSEG. Celui-ci a �t� cr��,
   � l'origine, pour le mot-cl� VECT_Y. En ce servant de ce vecteur
   pour le contact entre segments, le vecteur VECSEG n'a pas encore
   �t� initialis�. On initialise donc le vecteur VECSEG dans le
   cas de contact entre segments.
   routine modifi�e : PROJLI

   RESU_FAUX_VERSION_EXPLOITATION    : OUI  DEPUIS : 6.3.3
   RESU_FAUX_VERSION_DEVELOPPEMENT   : OUI  DEPUIS : 6.3.3
   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION :
      tests de contact
   IMPACT_DOCUMENTAIRE : NON


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT ssnp122a                      adbhhvv V.CANO             236    236      0
 CASTEST AJOUT ssnv128a                      pabhhhh N.TARDIEU          472    472      0
 CASTEST AJOUT ssnv128b                      pabhhhh N.TARDIEU          440    440      0
 CASTEST AJOUT ssnv128c                      pabhhhh N.TARDIEU          441    441      0
 CASTEST AJOUT ssnv128d                      pabhhhh N.TARDIEU          447    447      0
 CASTEST AJOUT ssnv128n                      pabhhhh N.TARDIEU          265    265      0
 CASTEST AJOUT ssnv128o                      pabhhhh N.TARDIEU          269    269      0
 CASTEST MODIF sslv134c                      cibhhlv L.VIVAN            229     11      5
 CASTEST MODIF ssnv128e                      pabhhhh N.TARDIEU          401    325    140
 CASTEST MODIF ssnv128f                      pabhhhh N.TARDIEU          420    277    368
 CASTEST MODIF ssnv128g                      pabhhhh N.TARDIEU          280    174    377
 CASTEST MODIF ssnv128h                      pabhhhh N.TARDIEU          399    321    171
 CASTEST MODIF ssnv128i                      pabhhhh N.TARDIEU          418    274    342
 CASTEST MODIF ssnv128j                      pabhhhh N.TARDIEU          398    293    218
 CASTEST MODIF ssnv128k                      pabhhhh N.TARDIEU          299    205    223
 CASTEST MODIF ssnv128l                      pabhhhh N.TARDIEU          255    178    158
 CASTEST MODIF ssnv128m                      pabhhhh N.TARDIEU          278    203    171
 CASTEST MODIF zzzz100a                       durand C.DURAND          1727      5      3
 CASTEST MODIF zzzz162a                       durand C.DURAND           295     25      5
 CASTEST SUPPR ssnv105h.comm                 pabhhhh N.TARDIEU          419      0    419
 CASTEST SUPPR ssnv105j.comm                 pabhhhh N.TARDIEU          399      0    399
 CASTEST SUPPR ssnv105k.comm                 pabhhhh N.TARDIEU          283      0    283
 CASTEST SUPPR ssnv105m.comm                 pabhhhh N.TARDIEU          228      0    228
 CASTEST SUPPR ssnv105n.comm                 pabhhhh N.TARDIEU          205      0    205
 CASTEST SUPPR ssnv105o.comm                 pabhhhh N.TARDIEU          230      0    230
 CASTEST SUPPR ssnv105p.comm                 pabhhhh N.TARDIEU          197      0    197
 CASTEST SUPPR ssnv105q.comm                 pabhhhh N.TARDIEU          276      0    276
 CASTEST SUPPR ssnv106a.comm                 pabhhhh N.TARDIEU          160      0    160
CATALOPY MODIF commande/crea_maillage        cibhhpd D.NUNEZ            129     23      1
CATALOPY MODIF commande/impr_jeveux          d6bhhjp J.P.LEFEBVRE        55      3      3
 FORTRAN AJOUT algeline/cmcovo               cibhhpd D.NUNEZ            608    608      0
 FORTRAN AJOUT algorith/cfelpv               pabhhhh N.TARDIEU           98     98      0
 FORTRAN AJOUT algorith/edrofg               adbhhvv V.CANO              66     66      0
 FORTRAN AJOUT algorith/edrofs               adbhhvv V.CANO              58     58      0
 FORTRAN AJOUT algorith/edroma               adbhhvv V.CANO              92     92      0
 FORTRAN AJOUT algorith/edronl               adbhhvv V.CANO             260    260      0
 FORTRAN AJOUT algorith/edropc               adbhhvv V.CANO              61     61      0
 FORTRAN AJOUT algorith/edrotg               adbhhvv V.CANO              85     85      0
 FORTRAN AJOUT algorith/edroy1               adbhhvv V.CANO             111    111      0
 FORTRAN AJOUT algorith/edroy2               adbhhvv V.CANO              80     80      0
 FORTRAN AJOUT algorith/edroyi               adbhhvv V.CANO             102    102      0
 FORTRAN AJOUT algorith/lcronl               adbhhvv V.CANO             161    161      0
 FORTRAN AJOUT supervis/fidetr                durand C.DURAND            89     89      0
 FORTRAN MODIF algeline/op0167               cibhhpd D.NUNEZ            842     39      3
 FORTRAN MODIF algeline/preml1               d6bhhjp J.P.LEFEBVRE       316     27     14
 FORTRAN MODIF algeline/premle               d6bhhjp J.P.LEFEBVRE        36      9      9
 FORTRAN MODIF algorith/cfaca1               pabhhhh N.TARDIEU          334     11      4
 FORTRAN MODIF algorith/edcomp               adbhhvv V.CANO             127      9      1
 FORTRAN MODIF algorith/elpiv2               pabhhhh N.TARDIEU          370      2      2
 FORTRAN MODIF algorith/fro2gd               pabhhhh N.TARDIEU          671      5      4
 FORTRAN MODIF algorith/frolgd               pabhhhh N.TARDIEU         1045    115     64
 FORTRAN MODIF algorith/nmcomp               adbhhvv V.CANO             562     11      1
 FORTRAN MODIF algorith/projli               pabhhhh N.TARDIEU          215     12      3
 FORTRAN MODIF elements/gverif               cibhhlv L.VIVAN            689     11      1
 FORTRAN MODIF prepost/irgmsh                cibhhlv L.VIVAN            235     20     10
 FORTRAN MODIF prepost/lrceme                 durand C.DURAND           197     33      7
 FORTRAN MODIF prepost/lrcnme                 durand C.DURAND           159      1      1
 FORTRAN MODIF prepost/op0188                cibhhlv L.VIVAN            306     43     15
 FORTRAN MODIF supervis/ops007                durand C.DURAND           103     11      1
 FORTRAN MODIF utilitai/ceseva               cibhhlv L.VIVAN            199      2      9
 FORTRAN MODIF utilitai/op0150                durand C.DURAND           819      2      1
  PYTHON AJOUT Stanley/exemple_env           jmbhh01 J.M.PROIX           78     78      0
  PYTHON MODIF Cata/ops                       durand C.DURAND           343      1      4
  PYTHON MODIF Macro/reca_algo                durand C.DURAND           256      4      1
  PYTHON MODIF Macro/recal                    durand C.DURAND           403      3      3
  PYTHON SUPPR Stanley/env                   jmbhh01 J.M.PROIX            0      0      0


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :   21        4519      4519             +4519
 MODIF :   35       13810      2688    2343      +345
 SUPPR :   10        2397              2397     -2397
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   66       20726      7207    4740     +2467 
