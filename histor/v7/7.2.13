========================================================================
Version 7.2.13 du : 04/02/2004
========================================================================


-----------------------------------------------------------------------
--- AUTEUR assire A.ASSIRE   DATE  le 03/02/2004 a 09:32:54

------------------------------------------------------------------------------
REALISATION EL 2004-006
   NB_JOURS_TRAV  : 1.
   INTERET_UTILISATEUR : OUI
   TITRE Recuperation en python d'une unit� logique disponible
   FONCTIONNALITE
     On enrichie la commande INFO_EXEC_ASTER pour qu'elle fournisse en sortie
     dans une table Aster la premiere unit� logique libre : la table contiendra
     la premiere unit� logique disponible en decroissant � partir de 99.
        TAB1 = INFO_EXEC_ASTER(LISTE_INFO='UNITE_LIBRE')
   DETAILS
     Pour exploiter ce developpement, on peut recuperer en python la valeur :
        valul = TAB1['UNITE_LIBRE',1]
     La variable Python valul contiendra la valeur de l'unit� logique et est
     utilisable en argumant d'une commande Aster.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     zzzz121a, Stanley (abondament)
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.13.04
       EXPL_ : Ajouter LISTE_INFO='UNITE_LIBRE'

------------------------------------------------------------------------------
REALISATION EL 2002-173
   NB_JOURS_TRAV  : 8.
   POUR_LE_COMPTE_DE   : A.ASSIRE
   INTERET_UTILISATEUR : OUI
   TITRE Nouveau cas-test pour les plaques excentr�es.
   FONCTIONNALITE
     On restitue un nouveau cas-test pour les plaques excentr�es. Il s'agit
   d'une plaque en flexion 3 point :

                        |
                       \ /
      --------------------------------------
      -------------------                  |
     /_\                 -------------------
                                          /_\
   DETAILS
       Ce castest vient s'ajouter aux autres concernant les plaques excentr�es.
     Son interet reside dans le chargement, qui permet d'avoir une comparaison
     avec une solution analytique en termes fleche et de moment flechissant.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     ssls122 : a (DKT/TRIA3), b (DKT/QUAD4), c (DST/TRIA3), d (DST/QUAD4)
   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V3.03.122


-----------------------------------------------------------------------
--- AUTEUR cibhhlv L.VIVAN   DATE  le 30/01/2004 a 17:48:33

------------------------------------------------------------------------------
CORRECTION AL 2004-029
   NB_JOURS_TRAV : 0.5
   POUR_LE_COMPTE_DE : C.DURAND
   INTERET_UTILISATEUR : NON
   TITRE aspic05a DEFI_GROUP du fond de fissure
   FONCTIONNALITE
     le GROUP_MA 'FONDFISS' n'existe que pour
        FISSURE='DEB_INT' ou 'DEB_EXT' ou 'TRAVERS'
   DETAILS
     modification de macr_aspic_mail_ops.py
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
     passage des 10 tests aspic


-----------------------------------------------------------------------
--- AUTEUR d6bhhjp J.P.LEFEBVRE   DATE  le 29/01/2004 a 10:43:38

------------------------------------------------------------------------------
REALISATION EL 2004-017
   NB_JOURS_TRAV  : 2.0
   INTERET_UTILISATEUR : OUI
   TITRE  DEFI_FICHIER
   FONCTIONNALITE
   DETAIL :
   Il est maintenant possible d'omettre le num�ro d'unit� logique, c'est le code
   qui en choisira une en appelant la fonction ULNUME, lors de la lib�ration
   il faudra pr�ciser le nom syst�me.
   Une nouvelle fonction est cr��e : ULNONF qui renvoie le num�ro d'unit�
   logique associ�e � un nom de fichier, ainsi que type de fichier et le type
   d'acces.
   RESU_FAUX_VERSION_EXPLOITATION    : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.12.03 (commande DEFI_FICHIER)
      EXPL_ : Omission du numero d'unite logique lorsque le nom de fichier
      derri�re NOM_SYTEME est indiqu�.

------------------------------------------------------------------------------
CORRECTION AL 2004-016
   NB_JOURS_TRAV  : 1.5
   INTERET_UTILISATEUR : OUI
   TITRE DEFI_FICHIER et MED
   FONCTIONNALITE
   DETAILS
   Le traitement des r�pertoire REPE_IN et REPE_OUT n'�tait pas pr�vu �
   l'origine pour les fichiers autres que les fichiers de type ASCII : le
   lien entre le nom syst�me et le num�ro d'unit� logique �tant r�alis� lors
   de l'OPEN Fortran. Les fichiers MED (TYPE='AUTRE') ne faisaient pas l'objet
   d'un traitement particulier.
   On effectue maintenant une recopie par un appel syst�me (commande cp/copy
   ou mv/move) depuis ./REPE_IN vers le r�pertoire d'ex�cution ou depuis le
   le r�pertoire d'ex�cution vers le ./REPE_OUT. Le type d'acc�s NEW ou OLD
   doit etre precis� pour enclencher la recopie.
   La fonction C cpfile est modifi�e, on ajoute un argument pour pouvoir utiliser
   soit la commande cp (ou copy), soit la commande mv (ou move).
   Les routines OPS019 et ASCSDO sont restitu�es en tenant compte de la suppression
   de la variable TYPELE inutilis�e dans ASCSDO (chantier erreur/warning Foresys)
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     Test personnel, la prise en compte d'un r�pertoire REPE_xxx n'�tant pas
     possible pour les tests officiels.
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.12.03 (commande DEFI_FICHIER)
      EXPL_ : utilisation des r�pertoires REPE_IN et REPE_OUT pour les fichiers
      de type 'AUTRE'.


-----------------------------------------------------------------------
--- AUTEUR jmbhh01 J.M.PROIX   DATE  le 02/02/2004 a 18:03:58

------------------------------------------------------------------------------
CORRECTION AL 2004-022
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE   El�ments de bord pour les quasi-incompressibles 2D
   FONCTIONNALITE
     Pour les �l�ments quasi-incompressibles, il faut ajouter les �l�ments de bord
     dans les ELREFE au niveau du catalogue. Ceci permet � des options comme les
     estimateurs d'erreur de faire des calculs
   DETAILS
     Modif : gener_mepli2.cata
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     Tests des elements D_PLAN_INCO et AXIS_INCO
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------------
CORRECTION AL 2004-019
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE   FORC_NODA+grilles => resultat faux ?
   FONCTIONNALITE
     Le test SSNS100A se plantait en DEBUG dans la routine DXEFFI, car DH n'�tait
     pas initialis�. J'ai ajout� le calcul de DH, et maintenant cela passe.
     Le calcul �tait faux uniquement pour les moments (excentrement) et seulement
     depuis le 08/12/2003
     De meme pour SIEF_ELNO_ELGA (routine DXEFF2) en DEBUG, cela ne passait pas
     pour les memes raisons.
     Les r�sultats n'�taient faux que pour les cmposantes MXX, MYY, MXY, c'est
     � dire en pr�sence d'excentrement. Le seul test de grilles avec excentrement
     �tant le SSLS109B, on ne testait ni FORC_NODA, ni SIEF_ELNO_ELGA.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI DEPUIS : 7.2.7
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
     Tests des elements GRILLE
     Modif dans SSLS109B pour ajouter FORC_NODA et SIEF_ELNO_ELGA + test non r�gression.
   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V3.03.109

------------------------------------------------------------------------------
CORRECTION AL 2004-013
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE   LE TEST DEMO001A s'arrete en ERREUR_F avec la NEW7.2.10
   FONCTIONNALITE
     Je n'arrive pas � reproduire le plantage. Neanmoins, par mesure de precaution
     je mets en commentaires les appels a xmgrace en batch (format postscript)
     pour voir si cela va mieux.
     Remarque : on a remplace les fihciers .datg par .geo, et .19 par .msh, mais
     les fichiers .datg et 19 sont toujours pr�sents sur astest
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR lebouvie F.LEBOUVIER   DATE  le 03/02/2004 a 09:34:44

-------------------------------------------------------------------------------
REALISATION EL 2003-025
   NB_JOURS_TRAV : 4.0
   POUR LE COMPTE DE : C. DURAND
   INTERET_UTILISATEUR : OUI
   TITRE : Mot-cl� FOND de DEFI_FOND_FISS � changer en FOND_FISS
   FONCTIONNALITE :
     Pour assurer l'homog�n�it� du vocabulaire entre les commandes CALC_G_*,
     post_k1_k2_k3, MACR_ASPIC(ASCOUF) et DEFI_FOND_FISS on remplace le mot cl�
     FOND par FOND_FISS
     Fichiers fortran modifi�s: op0055.f, gverfi.f, fonfiss.f
     fichiers catapy : defi_fond_fiss
     Macro python : macr_aspic_calc_ops.py
     62 fichiers cas-tests
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION :
     passage des cas-tests modifi�s et de la liste restreinte.
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U3.14.06
       EXPL_ : Remplacer le mot-cl� FOND de DEFI_FOND_FISS en FOND_FISS

-------------------------------------------------------------------------------
CORRECTION AL 2003-306
   NB_JOURS_TRAV : 6.5
   POUR LE COMPTE DE : A. M. DONORE
   INTERET_UTILISATEUR : OUI
   TITRE : IMPR_RESU / IDEAS
   FONCTIONNALITE : IMPR_RESU au format IDEAS
   DETAIL : Dans le cas-test ssnv200a on essaie d'imprimer des valeurs aux
   noeuds pour des mailles 3D de type PYRAM5, PYRAM13 et HEXA27. Ces mailles
   qui ne sont pas disponibles sous ideas.
   La solution retenue lors de l'�criture du fichier unv est la suivante:
   - Maillage: les mailles PYRAM5, PYRAM13 ne sont pas transf�r�es dans le
     fichier unv. Les mailles HEXA27 sont converties en mailles HEXA20.
   - R�sultats: aucun r�sultat associ� aux mailles PYRAM5 et PYRAM13 sont
     �crits dans le fichier unv. Pour les mailles HEXA27 qui sont converties
     en HEXA20 ont ne transfert que les r�sultats des 20 premiers noeuds

   Evolution: Il est possible d'imprimer au format ideas des cham_elem de
   type ELEM : ex EPOT_ELEM_DEPL
   - Fichiers fortrans modifies  : irmasu.f, utidea.f, ircers.f, iradhs.f
                                   ecrtes.f
   - Fichier cas-test modifie : ssnv200a
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION : - Modification du cas-test ssnv200a, ce cas-test sera
                  restitu� par Jacques PELLET.
                - Passage de la liste restreinte liste_ct.rest
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U7.05.01
       EXPL_ :


-----------------------------------------------------------------------
--- AUTEUR mabbas M.ABBAS   DATE  le 02/02/2004 a 13:35:39

------------------------------------------------------------------------
CORRECTION AL 2004-028
   NB_JOURS_TRAV  : 1.5
   INTERET_UTILISATEUR : NON
   TITRE    Contact avec option SANS_GROUP_NO
   FONCTIONNALITE
     Ce calcul concerne une partie courante d'enceinte 900Mwe.
     On souhaite traiter avec du contact la liaison entre le
     b�ton et le liner.
     Le calcul plante pour cause de liaisons redondantes m�me
      si on utilise l'option SANS_GROUP_NO. Par ailleurs, en
      faisant des tests pour mieux comprendre ce  qui se passait,
      j'ai pu remarquer que le fait d'inverser les mailles
      ma�tres et esclaves ne changeait  rien au message d'erreur...
   DETAILS
   L'utilisation de l'option SANS_GROUP_NO exclue bien les noeuds
   des relations de contact. Toutefois, l'intervention se fait au niveau
   de l'appariement (dans les iterations de STAT_NON_LINE).
   Or, dans le cas qui pose probleme, on utilise des elements de type
   quadratique. Pour les elements quadratiques en contact, on cree
   une liaison de type lineaire entre le noeud milieu et les noeuds sommets.
   Ceci se fait au tout debut (niveau AFFE_CHAR_MECA) dans la routine
   cacoeq.f.
   Mais cette routine ne tient pas compte des noeuds volontairement exclus
   du contact par l'utilisateur.
    Ce qui provoque des liaisons surabondantes dans certains cas
   particuliers (LIAISON_DDL assez contraignantes, cas de cette etude).
   La correction a consiste a exclure les noeuds precises dans SANS_GROUP_NO
   de l'operation de liaison noeuds sommets/noeuds milieux dans cacoeq.
   Attention ! Bien que SANS_GROUP_NO n'aie d'impact que sur les surfaces
   esclaves (la redondance des liaisons ne se produit que sur les noeuds
   de la surface esclave), dans le cas des elements quadratiques, il peut etre
   necessaire d'exclure AUSSI certains noeuds de la surface maitre.
   Il est preferable de laisser l'utilisateur exclure ces noeuds lui-meme, les
   cas ou la surabondance de liaisons provoquant plantage etant rares (cette
   fiche est le premier cas rencontre, puisqu'on conseille d'eviter les elements
   quadratiques dans les cas avec contact).
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
   Liste complete + etude
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.44.01
       EXPL_ : preciser le probleme des elements quadratiques
        et l'extension
       de l'usage de SANS_GROUP_NO dans ce cas.

------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
     Restitution du fichier generateur gibi pour le cas-test rccm06
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION


-----------------------------------------------------------------------
--- AUTEUR romeo R.FERNANDES   DATE  le 02/02/2004 a 13:38:51

------------------------------------------------------------------------
REALISATION EL 2004-002
   NB_JOURS_TRAV  : 1.0
   INTERET_UTILISATEUR : OUI
   TITRE : Loi de couplage LIQU_GAZ_ATM
   FONCTIONNALITE
      Suite au nettoyage des lois de couplage pour la THM,
      et dans le cadre de la loi LIQU_GAZ_ATM, on d�finit la
      commande THM_GAZ comme facultative puisque celle-ci est
      est en fait inutile.
   DETAILS
     modifications des routines thmlec, thmrcp
     modification du catalogue : defi_materiau
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION :
     tests LIQU_GAZ_ATM de la base Aster + tests perso
   IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U4.43.01
         EXPL_ : Mise � jour du tableau des commandes obligatoires
	         suivant la loi de couplage THM dans DEFI_MATERIAU

------------------------------------------------------------------------
CORRECTION AL 2004-027
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE : Plantage sous linux du test wtnp102a
   FONCTIONNALITE
      Loi de couplage de type LIQU_VAPE_GAZ.
   DETAILS
     Correction d'un mauvais dimensionnement sous THM_DIFFU.
     modifications des routines thmrcp
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION :
     test wtnp102a sous linux
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 0.5
   INTERET_UTILISATEUR : NON
   TITRE Nettoyage routine THMRCP
   FONCTIONNALITE
      Lecture des donn�es mat�riau dans le cadre d'un couplage THM
   DETAILS
      On met � jour des dimensionnements de vecteurs, ou des param�tres
      de lecture. Entre autre :
      - suppression des vecteurs NCRA34, VAL34
      - modification de param�tres de lecture pour la commande
        THM_DIFFU sous couplage de type LIQU_VAPE_GAZ
      - modification de param�tres de lecture pour la commande
        THM_DIFFU sous couplage de type LIQU_SATU
      - modification de param�tres de lecture pour la commande
        THM_GAZ sous couplage de type LIQU_GAZ
	
     modification de la routine thmrcp
   VALIDATION
      tests THM de la base Aster
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 2.0
   INTERET_UTILISATEUR : NON
   TITRE Calcul de la matrice tangente pour la loi de comportement
         Drucker_Prager
   FONCTIONNALITE
      Loi de comportement DRUCKER_PRAGER
   DETAILS
      Correction dans le calcul d'une matrice interm�diaire
      (erreur d�tect� par la non-sym�trisation de la matrice tangente)
    + Simplification du calcul de la matrice tangente
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
      tests DRUCKER_PRAGER de la base Aster + tests perso
   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
RESTITUTION HORS AREX
   NB_JOURS_TRAV  : 1.0
   INTERET_UTILISATEUR : OUI
   TITRE Calcul de la matrice tangente en THM
   FONCTIONNALITE
      Calcul de la matrice tangente en pr�sence de thermique
      de type THER_POLY en THM.
   DETAILS
      Probl�me d'initialisation de variable utilis�e pour le
      calcul de la matrice tangente.
   RESU_FAUX_VERSION_EXPLOITATION    :  NON
   RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
   RESTITUTION_VERSION_EXPLOITATION  :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
   VALIDATION
      tests THER_POLY de la base Aster + tests perso
   IMPACT_DOCUMENTAIRE : NON


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT ssls122a                       assire A.ASSIRE           355    355      0
 CASTEST AJOUT ssls122b                       assire A.ASSIRE           437    437      0
 CASTEST AJOUT ssls122c                       assire A.ASSIRE           363    363      0
 CASTEST AJOUT ssls122d                       assire A.ASSIRE           336    336      0
 CASTEST MODIF ascou16a                     lebouvie F.LEBOUVIER        366      1      1
 CASTEST MODIF ascou19a                     lebouvie F.LEBOUVIER        400      1      1
 CASTEST MODIF aspic01b                     lebouvie F.LEBOUVIER        627      2      2
 CASTEST MODIF aspic02a                     lebouvie F.LEBOUVIER        176      1      1
 CASTEST MODIF demo001a                      jmbhh01 J.M.PROIX          272      9      6
 CASTEST MODIF hpla310a                     lebouvie F.LEBOUVIER        943      2      2
 CASTEST MODIF hpla311a                     lebouvie F.LEBOUVIER        289      2      2
 CASTEST MODIF hpla311b                     lebouvie F.LEBOUVIER        293      2      2
 CASTEST MODIF hplp100a                     lebouvie F.LEBOUVIER        298      2      2
 CASTEST MODIF hplp101a                     lebouvie F.LEBOUVIER        435      2      2
 CASTEST MODIF hplp310a                     lebouvie F.LEBOUVIER       2024      2      2
 CASTEST MODIF hplp311a                     lebouvie F.LEBOUVIER        452      2      2
 CASTEST MODIF hplp311b                     lebouvie F.LEBOUVIER        318      2      2
 CASTEST MODIF hplp311c                     lebouvie F.LEBOUVIER        361      2      2
 CASTEST MODIF hplp311d                     lebouvie F.LEBOUVIER        316      2      2
 CASTEST MODIF hplp311e                     lebouvie F.LEBOUVIER        360      2      2
 CASTEST MODIF hplp311f                     lebouvie F.LEBOUVIER        314      2      2
 CASTEST MODIF hplp311g                     lebouvie F.LEBOUVIER        353      2      2
 CASTEST MODIF hplv103a                     lebouvie F.LEBOUVIER        457      2      2
 CASTEST MODIF ssla310a                     lebouvie F.LEBOUVIER        582      2      2
 CASTEST MODIF ssla311b                     lebouvie F.LEBOUVIER         95      2      2
 CASTEST MODIF sslp101b                     lebouvie F.LEBOUVIER        394      2      2
 CASTEST MODIF sslp101c                     lebouvie F.LEBOUVIER        440      2      2
 CASTEST MODIF sslp101e                     lebouvie F.LEBOUVIER        647      2      2
 CASTEST MODIF sslp103a                     lebouvie F.LEBOUVIER        255      2      2
 CASTEST MODIF sslp310a                     lebouvie F.LEBOUVIER       1267      2      2
 CASTEST MODIF sslp311a                     lebouvie F.LEBOUVIER        465      3      3
 CASTEST MODIF sslp311b                     lebouvie F.LEBOUVIER        468      3      3
 CASTEST MODIF sslp311c                     lebouvie F.LEBOUVIER        462      3      3
 CASTEST MODIF sslp311d                     lebouvie F.LEBOUVIER        471      3      3
 CASTEST MODIF sslp313a                     lebouvie F.LEBOUVIER        528      2      2
 CASTEST MODIF sslp313b                     lebouvie F.LEBOUVIER        510      2      2
 CASTEST MODIF sslp313c                     lebouvie F.LEBOUVIER        252      2      2
 CASTEST MODIF sslp314a                     lebouvie F.LEBOUVIER        295      2      2
 CASTEST MODIF sslp314b                     lebouvie F.LEBOUVIER        311      2      2
 CASTEST MODIF sslp314c                     lebouvie F.LEBOUVIER        330      2      2
 CASTEST MODIF sslp314d                     lebouvie F.LEBOUVIER        158      2      2
 CASTEST MODIF sslp314e                     lebouvie F.LEBOUVIER        158      2      2
 CASTEST MODIF sslp314f                     lebouvie F.LEBOUVIER        157      2      2
 CASTEST MODIF ssls109b                      jmbhh01 J.M.PROIX          429    116     22
 CASTEST MODIF sslv110a                     lebouvie F.LEBOUVIER        490      2      2
 CASTEST MODIF sslv110b                     lebouvie F.LEBOUVIER        274      2      2
 CASTEST MODIF sslv110c                     lebouvie F.LEBOUVIER        269      2      2
 CASTEST MODIF sslv110d                     lebouvie F.LEBOUVIER        287      2      2
 CASTEST MODIF sslv110e                     lebouvie F.LEBOUVIER        381      2      2
 CASTEST MODIF sslv112a                     lebouvie F.LEBOUVIER        321      2      2
 CASTEST MODIF sslv112b                     lebouvie F.LEBOUVIER        312      2      2
 CASTEST MODIF sslv134b                     lebouvie F.LEBOUVIER        517      2      2
 CASTEST MODIF sslv134c                     lebouvie F.LEBOUVIER        229      2      2
 CASTEST MODIF sslv134d                     lebouvie F.LEBOUVIER       1310      2      2
 CASTEST MODIF sslv134e                     lebouvie F.LEBOUVIER        324      2      2
 CASTEST MODIF sslv134f                     lebouvie F.LEBOUVIER        488      2      2
 CASTEST MODIF sslv310a                     lebouvie F.LEBOUVIER        510      2      2
 CASTEST MODIF sslv311a                     lebouvie F.LEBOUVIER        177      2      2
 CASTEST MODIF sslv312a                     lebouvie F.LEBOUVIER        240      2      2
 CASTEST MODIF sslv312b                     lebouvie F.LEBOUVIER        240      2      2
 CASTEST MODIF ssnp110a                     lebouvie F.LEBOUVIER        238      2      2
 CASTEST MODIF ssnp110b                     lebouvie F.LEBOUVIER        241      2      2
 CASTEST MODIF ssnp311a                     lebouvie F.LEBOUVIER        847      2      2
 CASTEST MODIF ssnp312a                     lebouvie F.LEBOUVIER        825      2      2
 CASTEST MODIF ssnp312b                     lebouvie F.LEBOUVIER        826      2      2
 CASTEST MODIF ssnv108a                     lebouvie F.LEBOUVIER        259      3      3
 CASTEST MODIF ssnv166a                     lebouvie F.LEBOUVIER        422      2      2
 CASTEST MODIF ssnv166b                     lebouvie F.LEBOUVIER        441      2      2
 CASTEST MODIF zzzz121a                       assire A.ASSIRE           279     20      3
CATALOGU MODIF typelem/gener_megri2          jmbhh01 J.M.PROIX          189      3      3
CATALOGU MODIF typelem/gener_mepli2          jmbhh01 J.M.PROIX          172      5      1
CATALOPY MODIF commande/defi_fichier         d6bhhjp J.P.LEFEBVRE        54     18      4
CATALOPY MODIF commande/defi_fond_fiss      lebouvie F.LEBOUVIER         76      3      3
CATALOPY MODIF commande/defi_materiau          romeo R.FERNANDES       2573      6     15
CATALOPY MODIF commande/info_exec_aster       assire A.ASSIRE            29      2      2
 FORTRAN AJOUT utilitai/ulnomf               d6bhhjp J.P.LEFEBVRE        55     55      0
 FORTRAN MODIF algorith/dplitg                 romeo R.FERNANDES         48      3      3
 FORTRAN MODIF algorith/dpmata                 romeo R.FERNANDES        124     38     65
 FORTRAN MODIF algorith/dppatg                 romeo R.FERNANDES         53      4      4
 FORTRAN MODIF algorith/resdp1                 romeo R.FERNANDES        100     12      4
 FORTRAN MODIF algorith/resdp2                 romeo R.FERNANDES        117     11      4
 FORTRAN MODIF algorith/thmlec                 romeo R.FERNANDES        154      9      9
 FORTRAN MODIF algorith/thmrcp                 romeo R.FERNANDES       1497     10     22
 FORTRAN MODIF calculel/op0055              lebouvie F.LEBOUVIER        195      3      3
 FORTRAN MODIF elements/dxbsig               jmbhh01 J.M.PROIX          151      1      1
 FORTRAN MODIF elements/dxeff2               jmbhh01 J.M.PROIX          173      5      3
 FORTRAN MODIF elements/dxeffi               jmbhh01 J.M.PROIX          173      5      3
 FORTRAN MODIF elements/fonfis              lebouvie F.LEBOUVIER        464      1      1
 FORTRAN MODIF elements/gverif              lebouvie F.LEBOUVIER        715      3      3
 FORTRAN MODIF jeveux/jxcopy                 d6bhhjp J.P.LEFEBVRE       129      2      2
 FORTRAN MODIF modelisa/cacoeq                mabbas M.ABBAS            154     31      1
 FORTRAN MODIF prepost/ascsdo                d6bhhjp J.P.LEFEBVRE       209      2      4
 FORTRAN MODIF prepost/ascthe               lebouvie F.LEBOUVIER        206      2      2
 FORTRAN MODIF prepost/ecrtes               lebouvie F.LEBOUVIER        265      4      1
 FORTRAN MODIF prepost/iradhs               lebouvie F.LEBOUVIER        194      2      1
 FORTRAN MODIF prepost/ircers               lebouvie F.LEBOUVIER        711     25      6
 FORTRAN MODIF prepost/irmasu               lebouvie F.LEBOUVIER        342     17      4
 FORTRAN MODIF prepost/stbast                d6bhhjp J.P.LEFEBVRE        47      3      3
 FORTRAN MODIF supervis/ib0mai               d6bhhjp J.P.LEFEBVRE       100      3      3
 FORTRAN MODIF supervis/ibcata               d6bhhjp J.P.LEFEBVRE       122      2      2
 FORTRAN MODIF supervis/ibcode               d6bhhjp J.P.LEFEBVRE       130      8      8
 FORTRAN MODIF supervis/ibimpr               d6bhhjp J.P.LEFEBVRE       110      3      3
 FORTRAN MODIF supervis/ops019               d6bhhjp J.P.LEFEBVRE      1243      4      4
 FORTRAN MODIF supervis/ops022               d6bhhjp J.P.LEFEBVRE       301      3      3
 FORTRAN MODIF utilifor/lxunit               d6bhhjp J.P.LEFEBVRE       167      2      2
 FORTRAN MODIF utilitai/op0010               d6bhhjp J.P.LEFEBVRE        61      2      2
 FORTRAN MODIF utilitai/op0016               d6bhhjp J.P.LEFEBVRE       210      2      2
 FORTRAN MODIF utilitai/op0026               d6bhhjp J.P.LEFEBVRE        80     27      7
 FORTRAN MODIF utilitai/op0035                assire A.ASSIRE            93     14      4
 FORTRAN MODIF utilitai/uldefi               d6bhhjp J.P.LEFEBVRE       191     33      8
 FORTRAN MODIF utilitai/ulimpr               d6bhhjp J.P.LEFEBVRE        70      5      2
 FORTRAN MODIF utilitai/utidea              lebouvie F.LEBOUVIER         81      7      1
 FORTRAN SUPPR supervis/ops017              lebouvie F.LEBOUVIER       1789      0   1789
  PYTHON MODIF Macro/macr_aspic_calc_ops    lebouvie F.LEBOUVIER        832      2      2
  PYTHON MODIF Macro/macr_aspic_mail_ops     cibhhlv L.VIVAN            622      2      1


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    5        1546      1546             +1546
 MODIF :  109       42202       620     388      +232
 SUPPR :    1        1789              1789     -1789
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :  115       45537      2166    2177       -11 
