========================================================================
Version 6.2.12 du : 10/01/2002
========================================================================


-----------------------------------------------------------------------
--- AUTEUR d6bhhjp J.P.LEFEBVRE   DATE  le 07/01/2002 a 08:53:38

------------------------------------------------------------------------------
REALISATION EL 2001-213
   INTERET_UTILISATEUR : NON
   TITRE Definition de bloc dans les commandes CREA_XXXX
   FONCTIONNALITE
   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.44.12
   VALIDATION
   DETAILS
   Introduction de blocs dans le catalogue de la commande CREA_RESU.
   A cette fin, un nouveau mot cl� est introduit : OPERATION qui peut
   prendre les valeurs suivantes :
   	AFFE      :  construction d'une sd r�sultat
   	ECLA_PG   :  construction d'un r�sultat sur un maillage �clat�
   	PERM_CHAM :  permutation pour les assemblages combustibles
   	PROL_RTZ  :  projection d'un transitoire 1D sur un maillage axi.
	
   Ce mot cl� n'est utilis� que dans le catalogue de la commande et rend
   obligatoire sous eficas le renseignement des mots cl�s.

   Le fortran de la macro MACRO_ELAS_MULT est modifi� pour introduire ce
   nouveau mot cl� (ssll101e) .

   Les tests suivants sont modifi�s :

	forma02d hpla100c hpla100d hpla100e hpla100f hpla100g hpla100h
        hpla100i hpla100j hplp310a hplv100a hsnl100a hsns101a hsns101b
        hsns101c hsns101d hsnv100c hsnv100d hsnv120a hsnv120b hsnv121a
        hsnv121b hsnv121c hsnv121d hsnv123a hsnv124a hsnv124b hsnv124c
        hsnv124d hsnv125a hsnv125b hsnv125c hsnv125d hsnv126a hsnv126b
        mtlp101a mtlp102a sdld02b  ssll100a ssll101e ssll102a ssll102b
        ssll102c ssll501a ssls101a ssls101e ssls101f ssls101g ssls101h
        sslv109a ssna100a ssna100b ssna106a ssnl109a ssnl109b ssnl110a
        ssnl111a ssnl111b ssnl112a ssnl112b ssnl112c ssnl112d ssnl112g
        ssnl112h ssnl112i ssnl112j ssnl112m ssnl114a ssnl114b ssnl121a
        ssnl121b ssnp116a ssnp116b ssnp116c ssns100a ssns100b ssns100c
        ssns100f ssns100g ssnv115a ssnv123a ssnv126a ssnv126b ssnv126c
        ssnv126d ssnv143a ssnv143b ssnv148a ssnv150a ssnv151a tpll01b
        ttnl03a  yyyy106a yyyy106b zzzz107a zzzz113a zzzz113b zzzz113c
        zzzz128a zzzz131a zzzz131b zzzz132a

------------------------------------------------------------------------------
CORRECTION AL 2002-010
   INTERET_UTILISATEUR : NON
   TITRE
   FONCTIONNALITE
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
   DETAILS
   Correction du fichier de commande du test yyyy106a.


-----------------------------------------------------------------------
--- AUTEUR f1bhhaj J.ANGLES   DATE  le 20/12/2001 a 17:04:36

CORRECTION AL 2001-430
   INTERET_UTILISATEUR : OUI
   TITRE : ASCOUF : STA5/ ASCOUF - Transition d'�paisseur et fissure
                    axisym�trique : plantage gibi

   FONCTIONNALITE : MACR_ASCOUF_MAIL. Le maillage produit avec GIBI98 ou
   GIBI2000 plante lorsque l'on demande de mailler une fissure axisym�trique et
   une transition d'�paisseur. GIBI plante car un test est incorrect pour
   le cas d'une fissure axisym�trique dans la proc�dure de r�cup�ration
   des zones de transition d'�paisseur. Correction de la proc�dure gibi :
   ascouf_fiss_v3.datg.

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   IMPACT_DOCUMENTAIRE : NON

   VALIDATION : ASCOU18A

   DETAILS : Modification d'un test dans la partie cr�ation d'une fissure
   axisym�trique de la proc�dure ascouf_fiss_v3.datg.

--------------------------------------------------------------------------------------
REALISATION EL 2001-248
   INTERET_UTILISATEUR : OUI
   TITRE : Compl�ment de post-traitement pour ASPIC

   FONCTIONNALITE : MACR_ASPIC_CALC. La r�alisation de cette EL solde �galement
   l'EL 2000-092.

   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON

   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   IMPACT_DOCUMENTAIRE : OUI
   DOC_U : U0.00.00 (U4.PC.20)

   VALIDATION : ASPIC01A ASPIC02A ASPIC03A ASPIC07A ASPIC08A ASPIC09A

   DETAILS : On ajoute les post-traitements suivant :

   1/ Maillage fissur�

   - Impression dans le fichier RESULTAT du champ de temp�rature en fond de
     fissure pour chaque pas temps de calcul ;

   - Impression au format CASTEM ou IDEAS du maillage et des champs suivants :

        * DEPL
        * EQUI_ELNO_SIGM
        * TEMP

   2/ Maillage sain

   - Impression dans le fichier RESULTAT des champs de contrainte principales
     SI, SII, SIII (EQUI_ELNO_SIGM) pour tous les pas de temps et toutes les
     lignes de d�pouillement demand�es par l'utilisateur ;

   - Impression dans le fichier RESULTAT du champ de temp�rature (si il a �t�
     calcul�) pour tous les pas de temps et toutes les lignes de d�pouillement
     demand�es par l'utilisateur ;

   - Impression dans le fichier RESULTAT des champs de contrainte Pm et Pm+Pb
     (POST_RCCM) pour toutes les lignes de d�pouillement demand�es par
     l'utilisateur ;

   - Impression dans le fichier RESULTAT des param�tres caract�risant la
     distribution de temp�rature (si elle a �t� calcul�e) dans l'�paisseur du
     ligament pour tous les pas de temps et toutes les lignes de d�pouillement
     demand�es par l'utilisateur (POST_RELEVE_T, OPERATION : MOYENNE).

     Pour ce faire nous modifions l'ops017.f et le catalogue python
     macr_aspic_calc.capy.

--------------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE : MACR_ASCOUF_MAIL et MACR_ASCOUF_CALC
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U0.00.00 (U4.CF.10 et U4.CF.20)
   VALIDATION
   DETAILS : mise � niveau des catalogues (capy) et des fortran (ops019.f et
   ops020.f) correspondant. Par exemple : mise � niveau du traitement de la
   commande INCREMENT.

--------------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE : MACR_ASPIC_MAIL et MACR_ASPIC_CALC
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U0.00.00 (U4.PC.10 et U4.PC.20)
   VALIDATION
   DETAILS : mise � niveau des catalogues (capy) et des fortran (ops016.f et
   ops019.f) correspondant. Par exemple : mise � niveau du traitement de la
   commande INCREMENT.


-----------------------------------------------------------------------
--- AUTEUR jfbhhuc C.ROSE   DATE  le 07/01/2002 a 16:33:51


------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
   Je me note RESPONSABLE des routines de la multi-frontale.

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
   DETAILS
   Liste des routines :
     amdapt.f  col21j.f  mlncmj.f  mltalc.f  mltdca.f  mltpas.f  preml0.f  renent.f
     amdbar.f  colni1.f  mlnflm.f  mltasa.f  mltdra.f  mltpos.f  preml1.f  rlfc16.f
     caladj.f  colni2.f  mlnfmg.f  mltasc.f  mltf21.f  mltpre.f  preml2.f  rltfr8.f
     calajt.f  facsmb.f  mlnfmj.f  mltblc.f  mltfc1.f  mmdelm.f  premla.f
     ccl11j.f  genmmd.f  mlnmin.f  mltc21.f  mltfcb.f  mmdint.f  premlc.f
     ccl21j.f  inschn.f  mltacf.f  mltcc1.f  mltfld.f  mmdnum.f  premld.f
     cclni1.f  mlfc16.f  mltacp.f  mltccb.f  mltflj.f  mmdpar.f  premle.f
     cclni2.f  mlnclm.f  mltaff.f  mltclm.f  mltflm.f  mmdupd.f  renen2.f
     col11j.f  mlncmg.f  mltafp.f  mltcmj.f  mltfmj.f  mulfr8.f  renen3.f

-------------------------------------------------------------------------------
CORRECTION AL 2001-339
   INTERET_UTILISATEUR : NON
  POUR_LE_COMPTE_DE : N. TARDIEU
  TITRE  SSNV129B : PLANTAGE PAR MANQUE DE PLACE M�MOIRE

   FONCTIONNALITE
   Il y avait dans la routine PREML0.F une adh�rence au caract�re
   64 bits de CLASTER. En effet, pour surestimer le nombre de
   relations lin�aires, on faisait l'op�ration:
     LT=MIN(LT**2,LT*10)
   Ceci permet, pour les gros cas-tests, de ne pas "trop" surestimer
   LT.
   Or, quand LT est trop grand, le fait de l'�lever au carr� cause
   l'�crasement de son signe et il devient n�gatif. C'est donc la
   valeur au carr� (�norme) qui est retenue. On cherche alors
   � affecter un espace m�moire monstrueux (et n�gatif ce qui n'a
   pas l'air de gesner WKVECT...).
   Or, un nombre est beaucoup plus vite "trop grand" en 32 bits
   qu'en 64 bits, ce qui cause l'apparition de ce bogue.
   On remplace cette op�ration par l'�quivalent :
      IF (LT.LE.10) THEN
         LT=LT**2
      ELSE
         LT=LT*10
      ENDIF

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON

   RESTITUTION_VERSION_EXPLOITATION  :   OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   IMPACT_DOCUMENTAIRE : NON

   VALIDATION
   Passage de l'�tude


-----------------------------------------------------------------------
--- AUTEUR ufbhhll C.CHAVANT   DATE  le 09/01/2002 a 14:16:47


REALISATION EL 2002-006
   INTERET_UTILISATEUR : OUI	
   TITRE				 ELEMENTS PENTA TETRA ET PYRAM EN THM

   FONCTIONNALITE		
   On ecrit les catalogues de elements THM_FACE6 ,THM_PENTA15
   THM_PYRAM13 THM_TETRA10

   On modifie les routines
   Pour elements proprement dits
      assthm.f
      caethm.f
      cabthm.f
      fnothm.f
      te0466.f
      te0600.f
      te0472.f
   Pour initialisation des �l�ments
      ini089.f
      ini010.f
      ini099.f
   Pour passage gauss noeuds
      ppgano.f
      inimat.f

   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :  NON
   RESTITUTION_VERSION_EXPLOITATION :  NON
   RESTITUTION_VERSION_DEVELOPPEMENT:  OUI
   IMPACT_DOCUMENTAIRE :  NON
   VALIDATION Cas test 	WTNV109D : nouvelle mod�lisation de WTNV109
              On en profite pour modifier les autres cas test de WTNV109
              qui n etaient OK que grace a une grande tolerance et ceci
              depuis le passage a P2P1


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT ascou18a                      f1bhhaj J.ANGLES            79     79      0
 CASTEST AJOUT aspic08a                      f1bhhaj J.ANGLES           152    152      0
 CASTEST AJOUT aspic09a                      f1bhhaj J.ANGLES           178    178      0
 CASTEST AJOUT wtnv109d                      ufbhhll C.CHAVANT          280    280      0
 CASTEST MODIF aspic01a                      f1bhhaj J.ANGLES           185      2      1
 CASTEST MODIF aspic02a                      f1bhhaj J.ANGLES           165      2      1
 CASTEST MODIF aspic03a                      f1bhhaj J.ANGLES           264      3      3
 CASTEST MODIF aspic07a                      f1bhhaj J.ANGLES           195      2      1
 CASTEST MODIF forma02d                      d6bhhjp J.P.LEFEBVRE       393      2      2
 CASTEST MODIF hpla100c                      d6bhhjp J.P.LEFEBVRE       486      2      2
 CASTEST MODIF hpla100d                      d6bhhjp J.P.LEFEBVRE       395      2      2
 CASTEST MODIF hpla100e                      d6bhhjp J.P.LEFEBVRE       351      2      2
 CASTEST MODIF hpla100f                      d6bhhjp J.P.LEFEBVRE       349      2      2
 CASTEST MODIF hpla100g                      d6bhhjp J.P.LEFEBVRE       263      2      2
 CASTEST MODIF hpla100h                      d6bhhjp J.P.LEFEBVRE       259      2      2
 CASTEST MODIF hpla100i                      d6bhhjp J.P.LEFEBVRE       377      2      2
 CASTEST MODIF hpla100j                      d6bhhjp J.P.LEFEBVRE       376      2      2
 CASTEST MODIF hplp310a                      d6bhhjp J.P.LEFEBVRE      2023      2      2
 CASTEST MODIF hplv100a                      d6bhhjp J.P.LEFEBVRE       315      2      2
 CASTEST MODIF hsnl100a                      d6bhhjp J.P.LEFEBVRE       297      2      2
 CASTEST MODIF hsns101a                      d6bhhjp J.P.LEFEBVRE       557      2      2
 CASTEST MODIF hsns101b                      d6bhhjp J.P.LEFEBVRE       424      2      2
 CASTEST MODIF hsns101c                      d6bhhjp J.P.LEFEBVRE       457      2      2
 CASTEST MODIF hsns101d                      d6bhhjp J.P.LEFEBVRE       426      2      2
 CASTEST MODIF hsnv100c                      d6bhhjp J.P.LEFEBVRE       216      2      2
 CASTEST MODIF hsnv100d                      d6bhhjp J.P.LEFEBVRE       191      2      2
 CASTEST MODIF hsnv120a                      d6bhhjp J.P.LEFEBVRE       453      2      2
 CASTEST MODIF hsnv120b                      d6bhhjp J.P.LEFEBVRE       343      2      2
 CASTEST MODIF hsnv121a                      d6bhhjp J.P.LEFEBVRE       208      2      2
 CASTEST MODIF hsnv121b                      d6bhhjp J.P.LEFEBVRE       170      2      2
 CASTEST MODIF hsnv121c                      d6bhhjp J.P.LEFEBVRE       222      2      2
 CASTEST MODIF hsnv121d                      d6bhhjp J.P.LEFEBVRE       220      2      2
 CASTEST MODIF hsnv123a                      d6bhhjp J.P.LEFEBVRE       336      2      2
 CASTEST MODIF hsnv124a                      d6bhhjp J.P.LEFEBVRE       512      2      2
 CASTEST MODIF hsnv124b                      d6bhhjp J.P.LEFEBVRE       413      2      2
 CASTEST MODIF hsnv124c                      d6bhhjp J.P.LEFEBVRE       376      2      2
 CASTEST MODIF hsnv124d                      d6bhhjp J.P.LEFEBVRE       363      2      2
 CASTEST MODIF hsnv125a                      d6bhhjp J.P.LEFEBVRE       527      3      3
 CASTEST MODIF hsnv125b                      d6bhhjp J.P.LEFEBVRE       488      3      3
 CASTEST MODIF hsnv125c                      d6bhhjp J.P.LEFEBVRE       623      3      3
 CASTEST MODIF hsnv125d                      d6bhhjp J.P.LEFEBVRE       491      3      3
 CASTEST MODIF hsnv126a                      d6bhhjp J.P.LEFEBVRE       373      2      2
 CASTEST MODIF hsnv126b                      d6bhhjp J.P.LEFEBVRE       373      2      2
 CASTEST MODIF mtlp101a                      d6bhhjp J.P.LEFEBVRE       106      2      2
 CASTEST MODIF mtlp102a                      d6bhhjp J.P.LEFEBVRE       226      2      2
 CASTEST MODIF sdld02b                       d6bhhjp J.P.LEFEBVRE       608      2      2
 CASTEST MODIF ssll100a                      d6bhhjp J.P.LEFEBVRE       366      2      2
 CASTEST MODIF ssll101e                      d6bhhjp J.P.LEFEBVRE       389      2      2
 CASTEST MODIF ssll102a                      d6bhhjp J.P.LEFEBVRE      1080      2      2
 CASTEST MODIF ssll102b                      d6bhhjp J.P.LEFEBVRE       936      2      2
 CASTEST MODIF ssll102c                      d6bhhjp J.P.LEFEBVRE       520      2      2
 CASTEST MODIF ssll501a                      d6bhhjp J.P.LEFEBVRE       410      2      2
 CASTEST MODIF ssls101a                      d6bhhjp J.P.LEFEBVRE       562      2      2
 CASTEST MODIF ssls101e                      d6bhhjp J.P.LEFEBVRE       456      2      2
 CASTEST MODIF ssls101f                      d6bhhjp J.P.LEFEBVRE       465      2      2
 CASTEST MODIF ssls101g                      d6bhhjp J.P.LEFEBVRE       465      2      2
 CASTEST MODIF ssls101h                      d6bhhjp J.P.LEFEBVRE       465      2      2
 CASTEST MODIF sslv109a                      d6bhhjp J.P.LEFEBVRE       578      2      2
 CASTEST MODIF ssna100a                      d6bhhjp J.P.LEFEBVRE       395      3      3
 CASTEST MODIF ssna100b                      d6bhhjp J.P.LEFEBVRE       424      3      3
 CASTEST MODIF ssna106a                      d6bhhjp J.P.LEFEBVRE       147      2      2
 CASTEST MODIF ssnl109a                      d6bhhjp J.P.LEFEBVRE       632      3      3
 CASTEST MODIF ssnl109b                      d6bhhjp J.P.LEFEBVRE       701      4      4
 CASTEST MODIF ssnl110a                      d6bhhjp J.P.LEFEBVRE       296      2      2
 CASTEST MODIF ssnl111a                      d6bhhjp J.P.LEFEBVRE       619      2      2
 CASTEST MODIF ssnl111b                      d6bhhjp J.P.LEFEBVRE       629      2      2
 CASTEST MODIF ssnl112a                      d6bhhjp J.P.LEFEBVRE       369      2      2
 CASTEST MODIF ssnl112b                      d6bhhjp J.P.LEFEBVRE       348      2      2
 CASTEST MODIF ssnl112c                      d6bhhjp J.P.LEFEBVRE       337      2      2
 CASTEST MODIF ssnl112d                      d6bhhjp J.P.LEFEBVRE       359      2      2
 CASTEST MODIF ssnl112g                      d6bhhjp J.P.LEFEBVRE       380      2      2
 CASTEST MODIF ssnl112h                      d6bhhjp J.P.LEFEBVRE       346      2      2
 CASTEST MODIF ssnl112i                      d6bhhjp J.P.LEFEBVRE       336      2      2
 CASTEST MODIF ssnl112j                      d6bhhjp J.P.LEFEBVRE       336      2      2
 CASTEST MODIF ssnl112m                      d6bhhjp J.P.LEFEBVRE       397      2      2
 CASTEST MODIF ssnl114a                      d6bhhjp J.P.LEFEBVRE       112      2      2
 CASTEST MODIF ssnl114b                      d6bhhjp J.P.LEFEBVRE       126      2      2
 CASTEST MODIF ssnl121a                      d6bhhjp J.P.LEFEBVRE       588      3      3
 CASTEST MODIF ssnl121b                      d6bhhjp J.P.LEFEBVRE       581      3      3
 CASTEST MODIF ssnp116a                      d6bhhjp J.P.LEFEBVRE       880      5      5
 CASTEST MODIF ssnp116b                      d6bhhjp J.P.LEFEBVRE       836      5      5
 CASTEST MODIF ssnp116c                      d6bhhjp J.P.LEFEBVRE       647      3      3
 CASTEST MODIF ssns100a                      d6bhhjp J.P.LEFEBVRE       473      2      2
 CASTEST MODIF ssns100b                      d6bhhjp J.P.LEFEBVRE       474      2      2
 CASTEST MODIF ssns100c                      d6bhhjp J.P.LEFEBVRE       375      2      2
 CASTEST MODIF ssns100f                      d6bhhjp J.P.LEFEBVRE       474      2      2
 CASTEST MODIF ssns100g                      d6bhhjp J.P.LEFEBVRE       474      2      2
 CASTEST MODIF ssnv115a                      d6bhhjp J.P.LEFEBVRE       198      2      2
 CASTEST MODIF ssnv123a                      d6bhhjp J.P.LEFEBVRE       165      2      2
 CASTEST MODIF ssnv126a                      d6bhhjp J.P.LEFEBVRE       368      2      2
 CASTEST MODIF ssnv126b                      d6bhhjp J.P.LEFEBVRE       312      2      2
 CASTEST MODIF ssnv126c                      d6bhhjp J.P.LEFEBVRE       484      2      2
 CASTEST MODIF ssnv126d                      d6bhhjp J.P.LEFEBVRE       377      2      2
 CASTEST MODIF ssnv143a                      d6bhhjp J.P.LEFEBVRE       311      3      3
 CASTEST MODIF ssnv143b                      d6bhhjp J.P.LEFEBVRE       313      3      3
 CASTEST MODIF ssnv148a                      d6bhhjp J.P.LEFEBVRE       185      2      2
 CASTEST MODIF ssnv150a                      d6bhhjp J.P.LEFEBVRE       270      3      3
 CASTEST MODIF ssnv151a                      d6bhhjp J.P.LEFEBVRE       386      3      3
 CASTEST MODIF tpll01b                       d6bhhjp J.P.LEFEBVRE       168      2      2
 CASTEST MODIF ttnl03a                       d6bhhjp J.P.LEFEBVRE       455      3      3
 CASTEST MODIF wtnv109a                      ufbhhll C.CHAVANT          231     23     19
 CASTEST MODIF wtnv109b                      ufbhhll C.CHAVANT          195     13     11
 CASTEST MODIF wtnv109c                      ufbhhll C.CHAVANT          206     17     32
 CASTEST MODIF yyyy106a                      d6bhhjp J.P.LEFEBVRE      1224      7     26
 CASTEST MODIF yyyy106b                      d6bhhjp J.P.LEFEBVRE       987      9      9
 CASTEST MODIF zzzz107a                      d6bhhjp J.P.LEFEBVRE       170      2      2
 CASTEST MODIF zzzz113a                      d6bhhjp J.P.LEFEBVRE      4755      2      2
 CASTEST MODIF zzzz113b                      d6bhhjp J.P.LEFEBVRE      1247      2      2
 CASTEST MODIF zzzz113c                      d6bhhjp J.P.LEFEBVRE      4767      2      2
 CASTEST MODIF zzzz128a                      d6bhhjp J.P.LEFEBVRE       244      2      2
 CASTEST MODIF zzzz131a                      d6bhhjp J.P.LEFEBVRE       534      2      2
 CASTEST MODIF zzzz131b                      d6bhhjp J.P.LEFEBVRE       207      3      3
 CASTEST MODIF zzzz132a                      d6bhhjp J.P.LEFEBVRE       313      2      2
CATALOGU MODIF compelem/phenomene_modelisation__    ufbhhll C.CHAVANT          548      5      1
CATALOGU MODIF typelem/gener_me3h52          ufbhhll C.CHAVANT           54      3      1
CATALOGU MODIF typelem/gener_me3h53          ufbhhll C.CHAVANT          109      5      1
CATALOPY MODIF commande/crea_resu            d6bhhjp J.P.LEFEBVRE       104     35     21
CATALOPY MODIF commande/macr_ascouf_calc     f1bhhaj J.ANGLES           189     30      2
CATALOPY MODIF commande/macr_ascouf_mail     f1bhhaj J.ANGLES           130     17      3
CATALOPY MODIF commande/macr_aspic_calc      f1bhhaj J.ANGLES           206     53      6
CATALOPY MODIF commande/macr_aspic_mail      f1bhhaj J.ANGLES            82     21      5
    DATG MODIF ascouf_fiss_v3.datg          J.ANGLES 10943               28     19      0
 FORTRAN MODIF algeline/amdapt               jfbhhuc C.ROSE             173      2      1
 FORTRAN MODIF algeline/amdbar               jfbhhuc C.ROSE            1307      2      1
 FORTRAN MODIF algeline/caladj               jfbhhuc C.ROSE             145      2      1
 FORTRAN MODIF algeline/calajt               jfbhhuc C.ROSE              58      2      1
 FORTRAN MODIF algeline/ccl11j               jfbhhuc C.ROSE              29      2      1
 FORTRAN MODIF algeline/ccl21j               jfbhhuc C.ROSE              59      2      1
 FORTRAN MODIF algeline/cclni1               jfbhhuc C.ROSE              25      2      1
 FORTRAN MODIF algeline/cclni2               jfbhhuc C.ROSE              27      2      1
 FORTRAN MODIF algeline/col11j               jfbhhuc C.ROSE              28      2      1
 FORTRAN MODIF algeline/col21j               jfbhhuc C.ROSE              58      2      1
 FORTRAN MODIF algeline/colni1               jfbhhuc C.ROSE              23      2      1
 FORTRAN MODIF algeline/colni2               jfbhhuc C.ROSE              25      2      1
 FORTRAN MODIF algeline/facsmb               jfbhhuc C.ROSE             220      2      1
 FORTRAN MODIF algeline/genmmd               jfbhhuc C.ROSE             229      2      1
 FORTRAN MODIF algeline/inschn               jfbhhuc C.ROSE              36      2      1
 FORTRAN MODIF algeline/mlfc16               jfbhhuc C.ROSE             283      2      1
 FORTRAN MODIF algeline/mlnclm               jfbhhuc C.ROSE              71      2      1
 FORTRAN MODIF algeline/mlncmg               jfbhhuc C.ROSE              42      2      1
 FORTRAN MODIF algeline/mlncmj               jfbhhuc C.ROSE              56      2      1
 FORTRAN MODIF algeline/mlnflm               jfbhhuc C.ROSE              68      2      1
 FORTRAN MODIF algeline/mlnfmg               jfbhhuc C.ROSE              41      2      1
 FORTRAN MODIF algeline/mlnfmj               jfbhhuc C.ROSE              56      2      1
 FORTRAN MODIF algeline/mlnmin               jfbhhuc C.ROSE              39      2      1
 FORTRAN MODIF algeline/mltacf               jfbhhuc C.ROSE              63      2      1
 FORTRAN MODIF algeline/mltacp               jfbhhuc C.ROSE              68      2      1
 FORTRAN MODIF algeline/mltaff               jfbhhuc C.ROSE              33      2      1
 FORTRAN MODIF algeline/mltafp               jfbhhuc C.ROSE              33      2      1
 FORTRAN MODIF algeline/mltalc               jfbhhuc C.ROSE              33      2      1
 FORTRAN MODIF algeline/mltasa               jfbhhuc C.ROSE             156      2      1
 FORTRAN MODIF algeline/mltasc               jfbhhuc C.ROSE             158      2      1
 FORTRAN MODIF algeline/mltblc               jfbhhuc C.ROSE              67      2      1
 FORTRAN MODIF algeline/mltc21               jfbhhuc C.ROSE              59      2      1
 FORTRAN MODIF algeline/mltcc1               jfbhhuc C.ROSE             169      2      1
 FORTRAN MODIF algeline/mltccb               jfbhhuc C.ROSE             214      2      1
 FORTRAN MODIF algeline/mltclm               jfbhhuc C.ROSE              55      2      1
 FORTRAN MODIF algeline/mltcmj               jfbhhuc C.ROSE              45      2      1
 FORTRAN MODIF algeline/mltdca               jfbhhuc C.ROSE             205      2      1
 FORTRAN MODIF algeline/mltdra               jfbhhuc C.ROSE             203      2      1
 FORTRAN MODIF algeline/mltf21               jfbhhuc C.ROSE              62      2      1
 FORTRAN MODIF algeline/mltfc1               jfbhhuc C.ROSE             158      2      1
 FORTRAN MODIF algeline/mltfcb               jfbhhuc C.ROSE             210      2      1
 FORTRAN MODIF algeline/mltfld               jfbhhuc C.ROSE              49      2      1
 FORTRAN MODIF algeline/mltflj               jfbhhuc C.ROSE             215      2      1
 FORTRAN MODIF algeline/mltflm               jfbhhuc C.ROSE              92      2      1
 FORTRAN MODIF algeline/mltfmj               jfbhhuc C.ROSE             126      2      1
 FORTRAN MODIF algeline/mltpas               jfbhhuc C.ROSE              95      2      1
 FORTRAN MODIF algeline/mltpre               jfbhhuc C.ROSE             462      2      1
 FORTRAN MODIF algeline/mmdelm               jfbhhuc C.ROSE             192      2      1
 FORTRAN MODIF algeline/mmdint               jfbhhuc C.ROSE              64      2      1
 FORTRAN MODIF algeline/mmdnum               jfbhhuc C.ROSE              95      2      1
 FORTRAN MODIF algeline/mmdpar               jfbhhuc C.ROSE              36      2      1
 FORTRAN MODIF algeline/mmdupd               jfbhhuc C.ROSE             256      2      1
 FORTRAN MODIF algeline/mulfr8               jfbhhuc C.ROSE             303      2      1
 FORTRAN MODIF algeline/preml0               jfbhhuc C.ROSE             175      8      2
 FORTRAN MODIF algeline/preml1               jfbhhuc C.ROSE             215      2      1
 FORTRAN MODIF algeline/preml2               jfbhhuc C.ROSE             146      2      1
 FORTRAN MODIF algeline/premla               jfbhhuc C.ROSE              44      2      1
 FORTRAN MODIF algeline/premlc               jfbhhuc C.ROSE             239      2      1
 FORTRAN MODIF algeline/premld               jfbhhuc C.ROSE              37      2      1
 FORTRAN MODIF algeline/premle               jfbhhuc C.ROSE              25      2      1
 FORTRAN MODIF algeline/renen2               jfbhhuc C.ROSE             149      2      1
 FORTRAN MODIF algeline/renen3               jfbhhuc C.ROSE              81      2      1
 FORTRAN MODIF algeline/renent               jfbhhuc C.ROSE             115      2      1
 FORTRAN MODIF algeline/rlfc16               jfbhhuc C.ROSE             136      2      1
 FORTRAN MODIF algeline/rltfr8               jfbhhuc C.ROSE             135      2      1
 FORTRAN MODIF algorith/assthm               ufbhhll C.CHAVANT          273      6      5
 FORTRAN MODIF algorith/cabthm               ufbhhll C.CHAVANT          270      9      7
 FORTRAN MODIF algorith/fnothm               ufbhhll C.CHAVANT          313      7      5
 FORTRAN MODIF elements/caethm               ufbhhll C.CHAVANT          762    297     16
 FORTRAN MODIF elements/ini010               ufbhhll C.CHAVANT          913     18      2
 FORTRAN MODIF elements/ini089               ufbhhll C.CHAVANT          384     18      8
 FORTRAN MODIF elements/ini099               ufbhhll C.CHAVANT          426     21     10
 FORTRAN MODIF elements/inimat               ufbhhll C.CHAVANT          766     19      8
 FORTRAN MODIF elements/ppgano               ufbhhll C.CHAVANT          249     17     11
 FORTRAN MODIF elements/te0466               ufbhhll C.CHAVANT          517     15     16
 FORTRAN MODIF elements/te0472               ufbhhll C.CHAVANT          444     13     17
 FORTRAN MODIF elements/te0600               ufbhhll C.CHAVANT          933     25     13
 FORTRAN MODIF supervis/ops010               d6bhhjp J.P.LEFEBVRE       639      3      1
 FORTRAN MODIF supervis/ops016               f1bhhaj J.ANGLES           847      5      1
 FORTRAN MODIF supervis/ops017               f1bhhaj J.ANGLES          1754    532     27
 FORTRAN MODIF supervis/ops019               f1bhhaj J.ANGLES          1081      3      2
 FORTRAN MODIF supervis/ops020               f1bhhaj J.ANGLES           896     34      3
 FORTRAN MODIF utilifor/mltpos               jfbhhuc C.ROSE             163      2      1


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :    4         689       689              +689
 MODIF :  201       76901      1670     586     +1084
 SUPPR :    0           0                 0        +0
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :  205       77590      2359     586     +1773 
