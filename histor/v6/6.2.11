========================================================================
Version 6.2.11 du : 20/12/2001
========================================================================


-----------------------------------------------------------------------
--- AUTEUR assire A.ASSIRE   DATE  le 19/12/2001 a 15:04:35

------------------------------------------------------------------------------
CORRECTION AL 2001-519
   INTERET_UTILISATEUR : OUI
   TITRE Erreur d'affichage de la distance d'un noeud � un plan
   FONCTIONNALITE �l�ments de plaque QUAD4
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
   DETAILS
   Lors du test de la plan�it� des �l�ments QUA4, on affiche la valeur de la
   distance au plan � titre informatif (le test en lui meme est angulaire).
   Dans le calcul il manquait une norme.
---------------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR cibhhab N.RAHNI   DATE  le 19/12/2001 a 19:14:27

CORRECTION AL 2001-431
   POUR_LE_COMPTE_DE : G.DEVESA
   INTERET_UTILISATEUR : OUI
      TITRE : PROJ_MESU_MODAL: objet inexistant lorsque l'on raffine le maillage

   FONCTIONNALITE
   Le probl�me rencontr� venait d'une mauvaise lecture du nom du mod�le de
   mesure associ� au CARA_ELEM (le nom �tait donn� en argument de la commande
   PROJ_MESU_MODAL/MESURE/CARA_ELEM, la lecture se faisait dans le .NOLI).

   On introduit alors un nouveau mot cl� simple MODELE sous le mot cl� facteur
   MESURE de la commande PROJ_MESU_MODAL pour passer le nom du mod�le de mesure:

     resu = PROJ_MESU_MODAL ( ...
                              _F( MESURE = (  o MAILLAGE  =
                                              o MODELE    =
                                              o CARA_ELEM =
                                              ...
                                            )
                             )

   RESU_FAUX_VERSION_EXPLOITATION : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON

   RESTITUTION_VERSION_EXPLOITATION : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   VALIDATION
     Etude associ�e
     SDLD104A, SDLD104B, SDLV122A, SDLV122B

   IMPACT_DOCUMENTAIRE :   OUI
   DOC_U : U4.73.01


-----------------------------------------------------------------------
--- AUTEUR cibhhbc R.FERNANDES   DATE  le 17/12/2001 a 17:48:52

CORRECTION AL 2001-466
   POUR_LE_COMPTE_DE   : N. TARDIEU
   INTERET_UTILISATEUR : NON
   TITRE : Contact : pas de convergence avec la m�thode lagrangienne

   FONCTIONNALITE
   DETAIL
    Mauvaise initialisation du vecteur ATMU lorsque les liaisons de
    contact sont d�sactiv�es.
    routines modifi�es : algocl en V6, algocp en V5.

   VALIDATION
    Cas tests de contact.
    ssnv129a (modifications des valeurs de r�f�rence)

   RESTITUTION_VERSION_EXPLOITATION  : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   RESU_FAUX_VERSION_EXPLOITATION  : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON

   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V6.04.129



-----------------------------------------------------------------------
--- AUTEUR cibhhlv L.VIVAN   DATE  le 17/12/2001 a 19:44:56

CORRECTION AL 2001-526
   POUR_LE_COMPTE_DE : N.TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE  NEW6.2.10, les tests forma01f et g terminent en ERREUR_<F>

   FONCTIONNALITE
   Suite du traitement de l'AL 2001-492, les noms des groupes de
   mailles ASTER issus de GMSH commencent par GM et non pas GMA.

   VALIDATION

   RESTITUTION_VERSION_EXPLOITATION : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON

   IMPACT_DOCUMENTAIRE : NON

------------------------------------------------------------------------
CORRECTION AL 2001-499
   POUR_LE_COMPTE_DE : J.M.PROIX
   INTERET_UTILISATEUR : OUI
   TITRE  Alarme �mis par DXMATE

   FONCTIONNALITE
   Lorsqu'il y a variation de temp�rature dans l'�paisseur d'une coque
   ou d'une plaque, on conseille � l'utilisateur de faire un
   STAT_NON_LINE au lieu d'un MECA_STATIQUE.
   Ce message d'Alarme �tait toujours �mis, meme pour un calcul non
   lin�aire et meme en post-traitement.

   Maintenant, on v�rifie l'origine du calcul. Si ce n'est pas un post
   ou un calcul non lin�aire, on �met le message d'Alarme.

   VALIDATION

   RESTITUTION_VERSION_EXPLOITATION : OUI
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON

   IMPACT_DOCUMENTAIRE : NON
------------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR cibhhpd D.NUNEZ   DATE  le 19/12/2001 a 09:57:45

 REALISATION EL 2001-158
   INTERET_UTILISATEUR : OUI
   POUR_LE_COMPTE_DE : G.DEVESA
   TITRE CALCULER POUR UNE PARTIE DE MAILLAGE DONNEE SES CARACTERISTIQUES MODALES

   FONCTIONNALITE
1- On cr�e un nouvelle option dans DEFI_BASE_MODALE nomm�e DIAG_MASS
   permettant de cr�er une base modale contenant les modes statiques
   et les modes dynamiques.

   Les modes dynamiques sont ceux du probl�me initial.

   Les modes statiques de la base modale sont recalcul�s � partir des
   modes statiques initiaux du probl�me et des modes dynamiques de
   telle sorte que la matrice de masse projet�e sur cette base modale
   soit diagonale.
   Ce calcul de modes statiques est bas� sur l'�limination de la contribution
   dynamique et une orthogonalisation de Graam-Schmidt.

   La syntaxe est la suivante :
    bamo =  DEFI_BASE_MODALE( DIAG_MASS :  o  MODE_MECA = modes,
                                           o  MODE_STAT = stat   )
                            )

2- Dans MACR_ELEM_DYNA, on projette donc les matrices de masse et de rigidit�,
   la base modale cr��e permet donc d'avoir une matrice de masse diagonale
   n�cessaire pour coupler ce calcul � EUROPLEXUS.

    La syntaxe est la suivante :

     mael = MACR_ELEM_DYNA ( o    OPTION      = 'DIAG_MASS',
                             o    BASE_MODALE = bamo ,)


3-  Dans IMPR_MACR_ELEM, on rajoute le format PLEXUS:

    La syntaxe est la suivante :
    IMPR_MACR_ELEM(   o    FORMAT         ='PLEXUS',
                      o    MACR_ELEM_DYNA = mael ,
                  )

   Le tout est imprim� au format IDEAS versions 5 :
   -  les matrices de masse et de rigidit� g�n�ralis�es par bloc
      ( partie statique, partie dynamique et couplage STAT/DYNA),
   -  les modes propres dynamiques
   -  les modes statiques de la base modale.

   DETAILS
   On modifie le accas.capy pour d�clarer le concept base_modale comme
   �tant de type r�sultat ceci permettant de faire des TEST_RESU et
   IMPR_RESU sur les valeurs de modes statiques � la sortie de
   DEFI_BASE_MODALE.

   VALIDATION
   Liste compl�te
   sdls110a, les solutions de r�f�rence sont calcul�es analytiquement.

   RESU_FAUX_VERSION_EXPLOITATION : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
	
   IMPACT_DOCUMENTAIRE : OUI
   DOC_U : U4.64.02  (DEFI_BASE_MODALE)
           U4.65.01  (MACR_ELEM_DYNA)
           U7.04.33  (IMPR_MACR_ELEM)
   DOC_V : V2.03.110 (SDLS110A)

----------------------------------------------------------------------------------------
 REALISATION EL 2001-183
   INTERET_UTILISATEUR : OUI
   POUR_LE_COMPTE_DE : G.DEVESA
   TITRE RESTITUTION DE 3 CAS TESTS DE VALIDATION DU CHAINAGE MISS3D-ASTER

   FONCTIONNALITE
   On restitue les cas tests :
     miss01a : Calcul MISS_3D-Code_Aster dans le cas de l'ilot EPR
     miss02a : Calcul MISS_3D-Code_Aster pour l'interaction stucture-sol-structure sur l'ilot
                nucl�aire du site de Civaux
     miss03a : Calcul MISS_3D-Code_Aster dans le cas du batiment combustible de l'ilot nul�aire
                PWR 1300 P'4

   VALIDATION
   RESU_FAUX_VERSION_EXPLOITATION : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
	
   IMPACT_DOCUMENTAIRE : OUI
   DOC_V : V1.10.120
           V1.10.121
           V1.10.122


-----------------------------------------------------------------------
--- AUTEUR durand C.DURAND   DATE  le 18/12/2001 a 10:11:25

------------------------------------------------------------------------------
CORRECTION AL 2001-525
   INTERET_UTILISATEUR : NON
   TITRE ELEMENTS verif syntaxique dans ACCAS / cas tests listes vali et perf
   FONCTIONNALITE superviseur ACCAS
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
   DETAILS
   Suite � la restitution de la v�rif syntaxique : correction des cas tests
   des listes de validation et de performance + SSLL106E
------------------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR gnicolas G.NICOLAS   DATE  le 18/12/2001 a 09:19:56

RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   TITRE  "Modification de num�rotation de cas-test"

   FONCTIONNALITE
Les cas-tests pour la sensibilit� ne portaient pas le bon num�ro : 138 alors qu'il faut 143. On supprime donc les tests zzzz138a et zzzz138a et on les recr�e sous les noms zzzz143a et zzzz143b.

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V1.01.143
   VALIDATION Nouveaux cas-test zzzz143a, zzzz143b
------------------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR jmbhh01 J.M.PROIX   DATE  le 17/12/2001 a 16:28:10

REALISATION EL 2001-191
  INTERET_UTILISATEUR : NON
     TITRE
      amelioration SSNL502
   FONCTIONNALITE
Je voulais am�liorer le test SSNL502 :
- la mod�lisation C (POU_D_T_G) : modif des valeurs de r�f�rence,
en coh�rence avec la doc V pour les moments
(correction d'une erreur sur ces valeurs de r�f�rence)
OK je restitue en V6 uniquement car la mod�lisation n'existe pas en V5.

- les mod�lisations A et B : si possible am�liorer la convergence,
et utiliser ETA_PILO_MAX, en V5 et V6 : cela ne marche pas bien.
Je laisse tomber

      IMPACT_DOCUMENTAIRE : OUI
      DOC_V : V6.02.502
     RESU_FAUX_VERSION_EXPLOITATION   :   NON
     RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
     RESTITUTION_VERSION_EXPLOITATION  :   NON
     RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   VALIDATION

--------------------------------------------------------------------------------------
RESTITUTION HORS AREX
  INTERET_UTILISATEUR : OUI
     TITRE
       Utilisation de COMP1D dans PFMCOM
   FONCTIONNALITE
Suite a l'introduction d'une routine COMP1D permettant de traiter des comportements
3D en 1D pour les barres (EL2001-147), j'en profite pour utiliser cette routine
dans les PMF, et ainsi permettre d'utiliser les comportements 3D dans ces elements.
La possibilit� existe, reste � voir si les performances ne sont pas trop d�grad�es
par rapport � un comportement int�gr� directement en 1D.
VALIDATION
nouvelle modelisation B du test ssnl119, avec comportement VMIS_ECMI_LINE pour
l'acier.
      IMPACT_DOCUMENTAIRE : OUI
      DOC_U : U3.00.00 : La future doc U3 des poutres multifibres...
     RESU_FAUX_VERSION_EXPLOITATION   :   NON
     RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
     RESTITUTION_VERSION_EXPLOITATION  :   NON
     RESTITUTION_VERSION_DEVELOPPEMENT :   OUI


-----------------------------------------------------------------------
--- AUTEUR pabhhhh N.TARDIEU   DATE  le 17/12/2001 a 15:54:53

RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   TITRE  "Sensibilite au chargement en force dans MECA_STATIQUE"

   FONCTIONNALITE
   Ce travail est une contribution � la r�alisation de EL 2001-146.
   Il permet d�sormais le calcul de sensibilit� au chargement en
   force en m�canique lin�aire (Voir le rapport HI-72/01/009 (G. Nicolas & J. Pellet)
   pour les sp�cifications).
   Ceci n'est faisable que pour les chargements m�caniques couverts par AFFE_CHAR_MECA_F
   et parmis ceux-ci seulement pour :
       .forces nodales
       .force r�partie volumique en 3D
       .force r�partie surfacique en 3D
       .force r�partie lin�ique en 3D
       .force r�partie surfacique en 2D
       .force r�partie lin�ique en 2D
       .force r�partie lin�ique en 1D
       .force r�partie pour les coques "2D"
       .force r�partie pour les coques "3D"
       .pression r�partie

   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.51.01  Insertion du mot-cl� SENSIBILITE dans MECA_STATIQUE
     DOC_V : V1.01.144 Sensibilit� � la pression impos�e en m�canique lin�aire
           : V1.01.145 Sensibilit� aux forces nodales impos�es en m�canique lin�aire

   VALIDATION
   Nouveaux cas-tests SENSI01 et SENSI02

   DETAILS
1. Insertion dans les programmes de m�canique statique
======================================================
La principale routine impact�e est VECHME qui rep�re si une charge
d�pend d'un param�tre sensible ou non et, en fonction de cela, lance
ou pas le calcul du chargement d�riv�.
C'est la routine NMDOME qui contient le DATA indiquant si on sait ou pas
calculer la charge d�riv�e. Ceci est � l'heure actuelle possible pour les
types :  .FORNO .F3D3D .F2D3D .F1D3D .F2D2D
         .F1D2D .F1D1D .FCO2D .FCO3D .PRESS

2. Cas-tests
============
Cr�ation de deux nouveaux cas tests
   SENSI01 : Sensibilit� � la pression impos�e en m�canique lin�aire
   SENSI02 : Sensibilit� aux forces nodales impos�es en m�canique lin�aire


-----------------------------------------------------------------------
--- AUTEUR pbbhhpb P.BADEL   DATE  le 19/12/2001 a 20:09:06

------------------------------------------------------------------------------
REALISATION EL 2001-243
   INTERET_UTILISATEUR : NON
   TITRE : RESORBTION PRED_ELAS_INCR, AMELIORATION PRED_ELAS
   FONCTIONNALITE : PILOTAGE (STAT_NON_LINE)
   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
            DOC_U : U4.51.03
            DOC_R : R5.03.01
            DOC_R : R5.03.80
   VALIDATION : cas test de pilotage (serie des SSNV137) avec suppression
                des cas tests associes a PRED_ELAS_INCR SSNV137G et SSNV137H)
   DETAILS
     La resolution de l'equation de pilotage (pour les pilotages PRED_ELAS et
     PRED_ELAS_INCR) F(ETA_PILOTAGE)=DELTA_TAU etait menee de maniere
     approchee par linearisation au point de Gauss des criteres de
     reversibilite. La difference entre PRED_ELAS et PRED_ELAS_INCR venait
     du choix du point autour duquel on linearisait les criteres.
     En fait, on peut resoudre exactement l'equation de pilotage en se servant
     de la convexite des criteres : cela revient a lineariser de la meme facon
     que pour PRED_ELAS en remplacant le seuil du critere de reversibilite par
     (seuil+delta_tau). delta_tau est la quantite dont on veut sortir d'au
     moins un critere quand on ecrit F(ETA_PILOTAGE)=DELTA_TAU.
     Cela clot la discussion sur le choix du point autour duquel on linearise,
     on modifie (legerement) PRED_ELAS et on resorbe PRED_ELAS_INCR.
     On en profite pour rendre plus robuste tout ce qui touche a la loi
     BETON_ENDO_LOCAL concernant le pilotage.
     On en profite pour corriger l'AL concernant les criteres de convergence
     dans la recherche lineaire et pilotage

     Dans le detail, on touche a :
       critet.f : calcul du critere de BETON_ENDO_LOCAL (nouveau : donne aussi
                  la derivee du critere)
       pipeds.f : module BETON_ENDO_LOCAL de PRED_ELAS
       pipeef.f : module ENDO_LOCAL de PRED_ELAS
       pipedp.f : module BETON_DOUBLE_DP de PRED_ELAS
       pipepe.f : | passage d'une carte ou l'on a stocke
       te0543.f : | delta_tau (cree dans nmpipe.f)
       nmpilo.f
       nmpipe.f : on doit maintenant etre capable de traiter deux droites
                  par point de Gauss
       op0070.f : | correction du bug concernant les codes retours (mauvais
       nmrepl.f : | dialogue entre op0070 et nmrepl)

       pour les catalogues : modif des elements qui supportent PRED_ELAS
                             modif des grandeurs simples pour passer 4
                                  parametres definissant les 2 droites
                                  fournies par les points de Gauss
                             modif de PILO_PRED_ELAS.cata
       pour ce qui concerne la resorbtion de PRED_ELAS_INCR
       modif de
         nmdopi.f
         nmpilo.f
         pipepe.f
       suppresion de
         pipids.f
         pipief.f

       modif de stat_non_line.capy impossible (note prioritairement par
       ailleurs) : il faudra le faire par la suite (PRED_ELAS_INCR sera
       syntaxiquement correct mais on se prendra une erreur fatale durant
       l'execution)
------------------------------------------------------------------------------
CORRECTION AL 2001-036
   INTERET_UTILISATEUR : NON
   TITRE : NMREPL DOIT RETOURNER DEUX CODES RETOUR
   FONCTIONNALITE : PILOTAGE (STAT_NON_LINE)
   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION : Cf l'EL CI-DESSUS
   DETAILS
------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION NEANT
   DETAILS

   Calcul de l'energie pour les lois a gradients
     On somme dorenanvant les differences pour des raisons de precision
     numerique.

     DELAGR
     DELALO
     DELANR   (nouveau)
     DELAPR
------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.71.02
   VALIDATION SSNV147E
   DETAILS
   Lecture des param�tres dans une SD

     OP0174
     RECU_TABLE

   fonctionnement :

     TABLE = RECU_TABLE(CO=EVOL, NOM_PARA='ETA_PILOTAGE')

   La table contient deux colonnes : les numeros d'ordre et la valeur du
   parametre ETA_PILOTAGE correspondant. Pour l'instant, on ne traite que
   les parametres reels. De la meme maniere, on peut avoir acces aux
   instants archives, NOM_PARA='INST'.

   Les mots-cles NOM_PARA et NOM_TABLE (autre fonction de RECU_TABLE)
   s'excluent.
------------------------------------------------------------------------------


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

 CASTEST AJOUT miss01a                       cibhhpd D.NUNEZ           2147   2147      0
 CASTEST AJOUT miss02a                       cibhhpd D.NUNEZ           1114   1114      0
 CASTEST AJOUT miss03a                       cibhhpd D.NUNEZ           1271   1271      0
 CASTEST AJOUT sdls110a                      cibhhpd D.NUNEZ            218    218      0
 CASTEST AJOUT sensi01a                      pabhhhh N.TARDIEU          158    158      0
 CASTEST AJOUT sensi02a                      pabhhhh N.TARDIEU          158    158      0
 CASTEST AJOUT ssnl119b                      jmbhh01 J.M.PROIX          149    149      0
 CASTEST AJOUT zzzz143a                     gnicolas G.NICOLAS          109    109      0
 CASTEST AJOUT zzzz143b                     gnicolas G.NICOLAS          142    142      0
 CASTEST MODIF ascou01a                       durand C.DURAND           278      1      2
 CASTEST MODIF ascou04a                       durand C.DURAND           375      1      2
 CASTEST MODIF elsa01a                        durand C.DURAND           213      1      1
 CASTEST MODIF forma01f                      cibhhlv L.VIVAN            324     22     22
 CASTEST MODIF forma01g                      cibhhlv L.VIVAN            337     26     26
 CASTEST MODIF hpla311a                       durand C.DURAND           278      1      2
 CASTEST MODIF hpla311b                       durand C.DURAND           282      1      2
 CASTEST MODIF hplp300a                       durand C.DURAND           487      1      3
 CASTEST MODIF hplp311a                       durand C.DURAND           441      1      2
 CASTEST MODIF hplp311b                       durand C.DURAND           307      1      2
 CASTEST MODIF hplp311c                       durand C.DURAND           350      1      2
 CASTEST MODIF hplp311d                       durand C.DURAND           305      1      2
 CASTEST MODIF hplp311e                       durand C.DURAND           349      1      2
 CASTEST MODIF hplp311f                       durand C.DURAND           303      1      2
 CASTEST MODIF hplp311g                       durand C.DURAND           342      1      2
 CASTEST MODIF sdld104a                      cibhhab N.RAHNI            298      3      2
 CASTEST MODIF sdld104b                      cibhhab N.RAHNI            309      3      2
 CASTEST MODIF sdld400a                       durand C.DURAND           462      3      5
 CASTEST MODIF sdld400b                       durand C.DURAND           431      3      5
 CASTEST MODIF sdll403a                       durand C.DURAND           237      1      3
 CASTEST MODIF sdls501b                       durand C.DURAND           121      2      3
 CASTEST MODIF sdlv122a                      cibhhab N.RAHNI            372      3      2
 CASTEST MODIF sdlv122b                      cibhhab N.RAHNI            259      3      2
 CASTEST MODIF sdlv302b                       durand C.DURAND           372      1      2
 CASTEST MODIF sdlx400a                       durand C.DURAND           362      4      7
 CASTEST MODIF ssll106e                       durand C.DURAND           665      5      5
 CASTEST MODIF ssls502c                       durand C.DURAND           222      2      4
 CASTEST MODIF ssls502d                       durand C.DURAND           221      2      4
 CASTEST MODIF sslv306a                       durand C.DURAND           183      1      2
 CASTEST MODIF ssnl119a                      jmbhh01 J.M.PROIX          136      1      1
 CASTEST MODIF ssnl501b                       durand C.DURAND           218      1      3
 CASTEST MODIF ssnl502a                       durand C.DURAND           361      3      5
 CASTEST MODIF ssnl502b                       durand C.DURAND           378      3      5
 CASTEST MODIF ssnl502c                      jmbhh01 J.M.PROIX          833     19     19
 CASTEST MODIF ssnv129a                      cibhhbc R.FERNANDES        593     20     18
 CASTEST MODIF ssnv147e                      pbbhhpb P.BADEL            209     24      4
 CASTEST MODIF tplp01b                        durand C.DURAND           134      1      1
 CASTEST MODIF yyyy103c                       durand C.DURAND           267      3      3
 CASTEST MODIF yyyy106b                       durand C.DURAND           987      3     14
 CASTEST MODIF yyyy110a                       durand C.DURAND           177      2      3
 CASTEST MODIF yyyy110b                       durand C.DURAND           176      2      3
 CASTEST SUPPR ssnv147g.comm                 pbbhhpb P.BADEL            186      0    186
 CASTEST SUPPR ssnv147h.comm                 pbbhhpb P.BADEL            204      0    204
 CASTEST SUPPR zzzz138a.comm                gnicolas G.NICOLAS          109      0    109
 CASTEST SUPPR zzzz138b.comm                gnicolas G.NICOLAS          143      0    143
CATALOGU MODIF compelem/grandeur_simple__    pbbhhpb P.BADEL            234      2      2
CATALOGU MODIF options/pilo_pred_elas        pbbhhpb P.BADEL             25      2      1
CATALOGU MODIF typelem/gener_me3d_3          pbbhhpb P.BADEL            389      6      5
CATALOGU MODIF typelem/gener_meax_2          pbbhhpb P.BADEL            464      6      5
CATALOGU MODIF typelem/gener_meaxs2          pbbhhpb P.BADEL            467      6      5
CATALOGU MODIF typelem/gener_mecpl2          pbbhhpb P.BADEL            462      6      5
CATALOGU MODIF typelem/gener_medpl2          pbbhhpb P.BADEL            468      6      5
CATALOGU MODIF typelem/mecptr3               pbbhhpb P.BADEL            457      6      5
CATALOGU MODIF typelem/mecptr6               pbbhhpb P.BADEL            453      6      5
CATALOGU MODIF typelem/medpqs8               pbbhhpb P.BADEL            456      6      5
CATALOGU MODIF typelem/medptr6               pbbhhpb P.BADEL            460      6      5
CATALOPY MODIF commande/accas                cibhhpd D.NUNEZ            319      2      2
CATALOPY MODIF commande/defi_base_modale     cibhhpd D.NUNEZ             36      6      2
CATALOPY MODIF commande/impr_macr_elem       cibhhpd D.NUNEZ             41      6      3
CATALOPY MODIF commande/macr_elem_dyna       cibhhpd D.NUNEZ             19      3      2
CATALOPY MODIF commande/proj_mesu_modal      cibhhab N.RAHNI             46      2      1
CATALOPY MODIF commande/recu_table           pbbhhpb P.BADEL             16      4      2
 FORTRAN AJOUT algorith/delanr               pbbhhpb P.BADEL            110    110      0
 FORTRAN AJOUT algorith/diag99               cibhhpd D.NUNEZ            236    236      0
 FORTRAN AJOUT algorith/zerod2               pbbhhpb P.BADEL             52     52      0
 FORTRAN AJOUT algorith/zerog2               pbbhhpb P.BADEL             75     75      0
 FORTRAN AJOUT elements/dxtema               cibhhlv L.VIVAN             97     97      0
 FORTRAN AJOUT utilitai/iredpl               cibhhpd D.NUNEZ            342    342      0
 FORTRAN MODIF algorith/algocl               cibhhbc R.FERNANDES        691      2      5
 FORTRAN MODIF algorith/bmnbmd               cibhhpd D.NUNEZ            131      3      3
 FORTRAN MODIF algorith/chstnl               pabhhhh N.TARDIEU          116      2      2
 FORTRAN MODIF algorith/comp1d               jmbhh01 J.M.PROIX          239      2      8
 FORTRAN MODIF algorith/critet               pbbhhpb P.BADEL            118     63     11
 FORTRAN MODIF algorith/delagr               pbbhhpb P.BADEL            237     12     60
 FORTRAN MODIF algorith/delalo               pbbhhpb P.BADEL            202     29     88
 FORTRAN MODIF algorith/delapr               pbbhhpb P.BADEL            171     35     17
 FORTRAN MODIF algorith/dlfext               pabhhhh N.TARDIEU          123      4      3
 FORTRAN MODIF algorith/meacmv               pabhhhh N.TARDIEU          496     17      5
 FORTRAN MODIF algorith/mpmail               cibhhab N.RAHNI            137     11     14
 FORTRAN MODIF algorith/nmchar               pabhhhh N.TARDIEU          286      2      2
 FORTRAN MODIF algorith/nmdome               pabhhhh N.TARDIEU          630      5      5
 FORTRAN MODIF algorith/nmdopi               pbbhhpb P.BADEL            278      2      2
 FORTRAN MODIF algorith/nmpilo               pbbhhpb P.BADEL            200     11      6
 FORTRAN MODIF algorith/nmpipe               pbbhhpb P.BADEL            280     67     20
 FORTRAN MODIF algorith/nmrepl               pbbhhpb P.BADEL            313      7     11
 FORTRAN MODIF algorith/op0070               pbbhhpb P.BADEL            835      2      2
 FORTRAN MODIF algorith/op0081               cibhhpd D.NUNEZ            103     23     28
 FORTRAN MODIF algorith/op0099               cibhhpd D.NUNEZ            105     10      2
 FORTRAN MODIF algorith/pipedp               pbbhhpb P.BADEL            316      1      1
 FORTRAN MODIF algorith/pipeds               pbbhhpb P.BADEL            494    333    250
 FORTRAN MODIF algorith/pipeef               pbbhhpb P.BADEL            144     28     33
 FORTRAN MODIF algorith/pipepe               pbbhhpb P.BADEL            179     18     34
 FORTRAN MODIF algorith/refe81               cibhhpd D.NUNEZ            266      2      2
 FORTRAN MODIF algorith/refe99               cibhhpd D.NUNEZ            134     26      7
 FORTRAN MODIF algorith/vechme               pabhhhh N.TARDIEU          405    112     69
 FORTRAN MODIF elements/dxmate               cibhhlv L.VIVAN            553     15     57
 FORTRAN MODIF elements/dxqpgl                assire A.ASSIRE           144      5      4
 FORTRAN MODIF elements/matrc                cibhhlv L.VIVAN             77     27     74
 FORTRAN MODIF elements/pmfcom               jmbhh01 J.M.PROIX          168     34      7
 FORTRAN MODIF elements/te0221               cibhhlv L.VIVAN            308      9      9
 FORTRAN MODIF elements/te0543               pbbhhpb P.BADEL            147      4      3
 FORTRAN MODIF prepost/irmac2                cibhhlv L.VIVAN             87      8      8
 FORTRAN MODIF prepost/op0106                pabhhhh N.TARDIEU          642      2      2
 FORTRAN MODIF utilitai/metyse               pabhhhh N.TARDIEU           93      4      9
 FORTRAN MODIF utilitai/op0160               cibhhpd D.NUNEZ             47      3      1
 FORTRAN MODIF utilitai/op0174               pbbhhpb P.BADEL            109     66     13
 FORTRAN MODIF utilitai/pstyse               pabhhhh N.TARDIEU          132      4      1
 FORTRAN SUPPR algorith/pipids               pbbhhpb P.BADEL            111      0    111
 FORTRAN SUPPR algorith/pipief               pbbhhpb P.BADEL             54      0     54


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :   15        6378      6378             +6378
 MODIF :   97       28902      1270    1139      +131
 SUPPR :    6         807               807      -807
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :  118       36087      7648    1946     +5702 
