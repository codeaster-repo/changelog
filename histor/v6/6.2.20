========================================================================
Version 6.2.20 du : 21/02/2002
========================================================================


-----------------------------------------------------------------------
--- AUTEUR boyere E.BOYERE   DATE  le 19/02/2002 a 09:39:27

CORRECTION AL 2002-024
   INTERET_UTILISATEUR : OUI

   TITRE Le mot-clef NB_VECT n'est pas traite dans PROJ_MATR_BASE

   FONCTIONNALITE
   Jusqu'ici on demandait a l'utilisateur de specifier par NB_VECT
   dans l'operateur NUM_DDl_GENE (qui gere la numerotation
   des concepts projetes) le nombre de vecteurs de la base modale
   que l'on doit utiliser. Ce qui est naturel. En revanche
   ce qui etait potentiellement source d'erreur de la part de l'utilisateur
   est qu'on demandait a nouveau le nombre de vecteurs a employer pour
   la projection dans PROJ_MATR_BASE et PROJ_VECT_BASE. Il va de soit
   que ce nombre doit etre le meme que celui indique lors de la demande
   de numerotation. Or cette information est disponible a Code_Aster.
   Les operateurs de projection ont donc ete modifies pour recuperer
   eux-memes l'information et eviter a l'utilisateur d'entrer des
   commandes incoherentes. Les mots-clefs NB_VECT disparaissent donc
   de PROJ_MATR_BASE et et PROJ_VECT_BASE.
   En revanche il n'y avait aucun risque de se tromper avec la macro
   MACRO_PROJ_BASE (methode utilisee en pratique) puisque toutes
   les donnees de la projection sont entrees en une seule commande.

   RESU_FAUX_VERSION_EXPLOITATION   :   NON (mais risque d'erreur de la part de l'utilisateur)
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON

   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.63.12, U4.63.13

   VALIDATION
   passage d'astout des cas tests qui utilisent les operateurs de projection
   PROJ_MATR_BASE et PROJ_VECt_BASE et la macro MACRO_PROJ_BASE

   DETAILS
   elimination du mot_clef NB_VECT dans proj_matr_base.capy et proj_vect_base.capy

   recuperation du nombre de vecteurs modaux reduit dans le fortran par :
      CALL JEVEUO(NUMGEN//'.SLCS.DESC','L',LLDESC)
      NBMODE   = ZI(LLDESC)

   modification de macro_proj_base.capy pour tenir compte
   de la disparition des mots-clefs NB_VECT dans PROJ_MATR_BASE et PROJ_VECT_BASE

   modification des cas tests sdnl112b et sdnl112c en conformite
   avec les nouveaux catalogues.


-----------------------------------------------------------------------
--- AUTEUR durand C.DURAND   DATE  le 14/02/2002 a 09:28:57

-----------------------------------------------------------------------
CORRECTION AL 2002-087
   INTERET_UTILISATEUR : NON
   TITRE : zzzz108a plante sous linux
   FONCTIONNALITE
     macro_miss_3d
   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI
   RESU_FAUX_VERSION_EXPLOITATION  : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION
    cas test zzzz108a
   DETAIL
   probleme lors de l'initialisation de la chaine de caracteres 'nom'
   dans la fonction repout de astermodule.c
-----------------------------------------------------------------------


-----------------------------------------------------------------------
--- AUTEUR f6bhhbo P.DEBONNIERES   DATE  le 15/02/2002 a 17:07:56

REALISATION EL 2000-146

   INTERET_UTILISATEUR : NON

   TITRE Ajout carte responsable dans les tests de viscoplasticite

   FONCTIONNALITE  STAT_NON_LINE

   VALIDATION
   Ajout de la carte dans les cas tests suivants :
   SSNA01A       SSNL109A
   SSNP05A       SSNL109B
   SSNP101A      SSNL121A
   SSNP107A      SSNL121B
   SSNV109A      SSNL121C
   SSNV111A      SSNA104A
   ZZZZ113A      SSNA105A
   ZZZZ113B      SSNA106A
   ZZZZ113C      SSNA107A

   RESU_FAUX_VERSION_EXPLOITATION : NON

   RESU_FAUX_VERSION_DEVELOPPEMENT : NON

   RESTITUTION_VERSION_EXPLOITATION  : NON

   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   IMPACT_DOCUMENTAIRE : NON


-----------------------------------------------------------------------
--- AUTEUR gjbhhel E.LORENTZ   DATE  le 20/02/2002 a 10:50:19

RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   FONCTIONNALITE      : PLASTICITE A GRADIENT
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : NON
   VALIDATION : NOUVEAUS TESTS
       3D     : SSNV156A (Liste complete, 1 heure)
       D_PLAN : SSNV156B (Liste restreinte)
       AXIS   : SSNV156C (Liste complete)

   DETAILS

     Evolution de la formulation de la plasticite a gradient :
     (PLAS_GRAD_LINE et PLAS_GRAD_TRAC)

     Les lois d'evolution sont dorenavant locales, c'est-a-dire
     qu'il n'y a pas regularisation du potentiel de dissipation.

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   FONCTIONNALITE : RECU_TABLE
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.71.02
   VALIDATION : MODIF SSNV147A

   DETAILS

     On autorise dorenavant la saisie d'une liste de parametres.
     Leurs types peuvent etre reel, entier ou complexe.

     Exemple :

     T = RECU_TABLE(CO = EVOL, NOM_PARA = ('INST','ETA_PILOTAGE'))

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   FONCTIONNALITE : POST_ELEM
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.81.22
   VALIDATION : MODIF SSNV147A

   DETAILS

    Calcul du travail des efforts exterieurs

     On remplace le vocable ENER_EXT par TRAV_EXT. Les parametres
     de la table gardent leurs noms (TRAV_ELAS et TRAV_REEL).
     Quant au mot-cle TOUT = 'OUI', il disparait egalement (on ne
     calcule pas le travail sur une partie de la structure).

------------------------------------------------------------------------------
RESTITUTION HORS AREX
   INTERET_UTILISATEUR : OUI
   FONCTIONNALITE : DEFI_MATERIAU ( NORTON_HOFF )
   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.43.01
   VALIDATION : MODIF SSNV124A, SSNV124B, SSNV124C
                      SSNV146A
   DETAILS

     Resorption du materiau NORTON_HOFF

     On va chercher la limite d'elasticite sous ECRO_LINE, en
     coherence avec l'approche statique pour le calcul de la
     charge limite.


-----------------------------------------------------------------------
--- AUTEUR godard V.GODARD   DATE  le 19/02/2002 a 18:10:15

------------------------------------------------------------------------------
REALISATION EL 2001-238
   INTERET_UTILISATEUR : OUI
   TITRE : LOIS NON LOCALES A DEFORMATIONS REGULARISEES
   FONCTIONNALITE
     Introduction de lois non locales regularisees sur la deformation : on
     definit un champ de deformation regularisee (appele parfois deformation
     generalisee), liee a la deformation "locale" classique par un operateur
     regularisant (ici de type moindre carre avec penalisation du gradient)
     qui a pour objectif de limiter les concentrations de deformations (i.e.
     les forts gradients coutent cher dans l'operateur de regularisation, donc
     sont limites).
     Perimetre : elements isoparametriques 3D, 2D contraintes planes et 2D
     deformations planes ; deux lois de comportement endommageantes implantees:
       - ENDO_GRAD_EPSI (s'appuie sur le modele local ENDO_LOCAL)
       - BETON_GRAD_EPSI (s'appuie sur BETON_ENDO_LOCAL)
     Les deformations generalisees servent a evaluer les variables internes,
     en revanche les contraintes sont calculees a partir des variables internes
     et des deformations locales (prendre les deformations generalisees pour le
     calcul des contraintes revient a "trop" regulariser le probleme, i.e. un
     probleme local bien pose (exemple : elasticite lineaire) peut n'avoir
     aucune solution apres regularisation).
     Cette formulation possede les avantages suivants :
       - l'equation differentielle liant deformation locale et deformation
         generalisee est resolue classiquement par elements finis : il suffit
         donc d'introduire de nouvelles variables nodales representant ces
         deformations generalisees. On se retrouve donc simplement avec des
         inconnues nodales supplementaires. De plus cette equation est du type
         elasticite (resolution en une iteration).
       - peu de developpement a effectuer sur les lois de comportement : seule
         la partie calcul des variables internes est a (legerement) modifier
         pour s'appuyer sur les deformations generalisees. Evidemment la
         matrice tangente est a retoucher egalement.
       - existence d'une matrice tangente (de largeur de bande raisonnable
         - par rapport a une formulation integrale par exemple -),
         facilitant la vitesse de convergence. En revanche, cette matrice
         tangente est non-symetrique (on ne peut pas gagner tous les jours !)
      De plus, les techniques de pilotage s'adaptent tres facilement a cette
      formulation (on offre donc la possibilite de pilotage par sortie de
      critere ou par deformation disponible)
      D'un point de vue utilisateur, pour utiliser cette formulation, il suffit
      de mettre dans son fichier de commande les infos suivantes :
        - dans DEFI_MATERIAU, NON_LOCAL=_F(LONG_CARA=XXX,)
             -> pour definir la longueur caracteristique du modele
        - dans AFFE_MODELE, MODELISATION='3D_GRAD_EPSI' ou 'D_PLAN_GRAD_EPSI' ou
          'C_PLAN_GRAD_EPSI'
           -> utilisation de la formulation pour les groupes de mailles voulues
        - dans STAT_NON_LINE, COMP_INCR avec une loi non locale developpee
          (pour le moment ENDO_GRAD_EPSI ou BETON_GRAD_EPSI)
        - pilotage eventuel (mots cles habituels)


   RESU_FAUX_VERSION_EXPLOITATION  :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT :   NON
   RESTITUTION_VERSION_EXPLOITATION  :   NON
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI
   IMPACT_DOCUMENTAIRE : OUI
     DOC_U : U4.41.01-g2 (AFFE_MODELE)
             U4.51.03-f3 (STAT_NON_LINE)
             U3.13.06 (elements 2D)
             U3.14.11 (elements 3D)
     DOC_R : R5.03.80 (PILOTAGE)
             R5.04.15 (pour la methode elle-meme)
   VALIDATION
     cas-tests de non regression sur barreau (modelisation 3D_GRAD_EPSI,
     D_PLAN_GRAD_EPSI et C_PLAN_GRAD_EPSI) + test du pilotage associe
     ssnv157a : 3D (liste complete)
     ssnv157b : CPLAN (liste complete)
     ssnv157c : DPLAN (liste restreinte)
   DETAILS
     On cree de nouveaux elements finis qui portent, outre les deplacements
     classiques, les deformations generalisees. Pour une question de dimension
     on utilise des elements P2 pour le deplacement et P1 pour ces deformations
     generalisees. D'ou les elements "geometriques" utilisables : TETRA10,
     HEXA20, PENTA15, PYRAM13, TRI6, QUAD8.
     Les matrices elementaires sont non symetriques.
     Remarque : dans ces formulations mixtes P2/P1, il n'est pas superflu de
     preciser que les noeuds milieux DOIVENT etre au milieu des aretes...
     Nouveaux catalogues : gener_me3dg.cata
                           gener_mecpg.cata
                           gener_medpg.cata
     Catalogues modifies : phenomenes_modelisation__.cata
                           grandeurs_premieres__.cata

     On ecrit de nouveaux te pour calculer les options mecaniques (FORC_NODA,
     RIGI_MECA_TANG, RAPH_MECA, FULL_MECA). Nombre d'options n'utilisant que
     le deplacement peuvent etre reutilises sans modifications (en
     definissant un mode local purement deplacement et en utilisant le
     mecanisme de filtre offert par CALCUL/CATALOGUES).
     Nouveaux fortrans :
       te0007.f
       te0113.f
       te0121.f
       te0438.f : calcul des options mecaniques
       nmgeob.f : analogues de nmgeom mais calcul des deformations generalisees
                  et de leurs gradients aux points de Gauss (en lieu et place
                  des deformations)
       nmpl2g.f :
       nmpl3g.f : analogues des nmpl2d et nmpl3d
     Fortran modifie:
       nmcomp.f : ajout des nouvelles lois de comportement

     Nouvelles lois de comportement : ENDO_GRAD_EPSI et BETON_GRAD_EPSI
     S'appuient sur ENDO_LOCAL et BETON_ENDO_LOCAL, utilisent les deformations
     generalisees au niveau du calcul des variables internes et les
     deformations locales pour le calcul des contraintes.
     Adaptation de la matrice tangente en consequence.
     Nouveaux fortrans :
       lclocc.f : ENDO_GRAD_EPSI
       lcdsbe.f : BETON_GRAD_EPSI

     Pilotage adapte a cette formulation. Pour le pilotage en deformation, il
     n'y a rien a faire (juste passer les deplacements sans les deformations
     generalisees). Pour le pilotage par critere, il faut un nouveau te, qui
     va chercher les bonnes fonctions de forme (pour avoir les deformations
     generalisees au point de Gauss). On ajoute une option (pour differencier
     le pilotage par deformation ou par critere : pour les elements locaux,
     c'est une simple dupplication), pour les nouveaux elements dans une
     on passe les deplacements et dans l'autre les deformations generalisees.
     Modif de catalogues : gener_me3d_3.cata
                           gener_meax_2.cata
                           gener_meaxs2.cata
                           gener_mecpl2.cata
                           gener_medpl2.cata
                           mecptr3.cata
                           mecptr6.cata
                           medpqs8.cata
                           medptr6.cata
     Ajout de catalogues : pilo_pred_defo.cata
     Modif de fortrans :   nmpilo.f
                           nmpipe.f
                           pipepe.f

------------------------------------------------------------------------------    Restitution de R.Fernandes

CORRECTION AL 2002-101
   POUR_LE_COMPTE_DE   : N. TARDIEU
   INTERET_UTILISATEUR : OUI
   TITRE : Mise � jour de la nomenclature de VALE_CONT.

   FONCTIONNALITE
     Uniformisation des composantes de VALE_CONT et de INFO = 2
     pour le CONTACT.

   DETAIL
     Les composantes associ�es au champ VALE_CONT sont :
     - CONTACT : Indicateur de contact :
                    Vaut 1 si les liaisons sont actives
                    Vaut 0 sinon;
     - JEU     : correspondant � la valeur du jeu entre le noeud
                 esclave et le (la) noeud (maille) ma�tre associ�(e);
     - RNN     : multiplicateur de lagrange du contact :
                    RNN > 0 si contact
                    RNN = 0 sinon;
     - DUGT1   : correspondant � la valeur du d�placement tangentiel
                 suivant X;
     - DUGT2   : correspondant � la valeur du d�placement tangentiel
                 suivant Y;
     - DUGT    : correspondant � la norme des d�placements tangentiels;
     - RN      : correspondant � la r�action normale;
     - RT1     : correspondant � la r�action tangentielle suivant X;
     - RT2     : correspondant � la r�action tangentielle suivant Y;
     - RT      : correspondant � la norme des r�actions tangentielles;
     - MU      : correspondant au rapport SQRT(RT1^2+RT2^2)/ABS(RN).

     Modification du catalogue  : grandeur_simple__.cata
     Modification de la routine : resuco

   VALIDATION
    Cas tests de contact.
    Modifications des cas tests : sdnv103b, sdnv103c, sdnv103d, ssnv105h

   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   RESU_FAUX_VERSION_EXPLOITATION  : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON

   IMPACT_DOCUMENTAIRE : OUI
      DOC_D : D4.04.02
      DOC_V : V6.04.105
      DOC_V : V5.03.103

---------------------------------------------------------------------------
CORRECTION AL 2002-058
   POUR_LE_COMPTE_DE   : N. TARDIEU
   INTERET_UTILISATEUR : NON
   TITRE : Le test sdnv103c termine en NOOK

   FONCTIONNALITE
     Diff�rence de r�sultats suite � la modification du calcul des fonctions
     de formes pour les mailles quadratiques.

   DETAIL
     Mise � jour du cas test sdnv103c avec les nouvelles valeurs de
     non-r�gression. La diff�rence (2% d'�cart sur le jeu) est due
     � la modification du calcul des fonctions de formes pour les mailles
     quadratiques.

   VALIDATION
    Cas tests de contact.
    Modifications des cas tests : sdnv103c

   RESTITUTION_VERSION_EXPLOITATION  : NON
   RESTITUTION_VERSION_DEVELOPPEMENT : OUI

   RESU_FAUX_VERSION_EXPLOITATION  : NON
   RESU_FAUX_VERSION_DEVELOPPEMENT : NON

   IMPACT_DOCUMENTAIRE : OUI
      DOC_V : V5.03.103


-----------------------------------------------------------------------
--- AUTEUR sellali N.SELLALI   DATE  le 15/02/2002 a 11:10:32

RESTITUTION HORS AREX
   INTERET_UTILISATEUR : NON
   FONCTIONNALITE : NON

   OBJET
   ======

   On renomme les cas tests SSNV401 et SSNV402 en SSNV154 et SSNV155
   car ils n'ont pas ete restitues dans le cadre d'une validation
   independante de la version 4 ni d'aucune autre d'ailleurs.


   RESU_FAUX_VERSION_EXPLOITATION   :   NON
   RESU_FAUX_VERSION_DEVELOPPEMENT  :   NON

   RESTITUTION
   ===========

   RESTITUTION_VERSION_EXPLOITATION  :   OUI
   RESTITUTION_VERSION_DEVELOPPEMENT :   OUI

   Restitution des cas tests SSNV154 et SSNV155

   SSNV154A/B --> Anciens cas tests SSNV401A/B
   SSNV155A   --> Ancien  cas test  SSNV402A

   SSNV401A/B --> Supprimes via le .UNIG.
   SSNV402A   --> Supprime  via le .UNIG.

   VALIDATION
   ==========
   aucune

   IMPACT_DOCUMENTAIRE : OUI
     DOC_V : V6.04.154 au lieu de V6.04.401
             V6.04.155 au lieu de V6.04.402


========================================================================
=== Recapitulation des operations demandees pour toutes les restitutions
========================================================================


    TYPE Action    unite                      user      Auteur         nblg  ajout suppr.

       C MODIF supervis/astermodule           durand C.DURAND          3813      3      2
 CASTEST AJOUT ssnv154a                      sellali N.SELLALI          357    357      0
 CASTEST AJOUT ssnv154b                      sellali N.SELLALI          370    370      0
 CASTEST AJOUT ssnv155a                      sellali N.SELLALI          371    371      0
 CASTEST AJOUT ssnv156a                      gjbhhel E.LORENTZ          450    450      0
 CASTEST AJOUT ssnv156b                      gjbhhel E.LORENTZ          444    444      0
 CASTEST AJOUT ssnv156c                      gjbhhel E.LORENTZ          439    439      0
 CASTEST AJOUT ssnv157a                       godard V.GODARD           147    147      0
 CASTEST AJOUT ssnv157b                       godard V.GODARD           148    148      0
 CASTEST AJOUT ssnv157c                       godard V.GODARD           148    148      0
 CASTEST MODIF sdnl112b                       boyere E.BOYERE           476      1      3
 CASTEST MODIF sdnl112c                       boyere E.BOYERE           471      1      3
 CASTEST MODIF sdnv103b                       godard V.GODARD           404     10     10
 CASTEST MODIF sdnv103c                       godard V.GODARD           287     13     13
 CASTEST MODIF sdnv103d                       godard V.GODARD           277      4      4
 CASTEST MODIF ssna01a                       f6bhhbo P.DEBONNIERES      381      2      1
 CASTEST MODIF ssna104a                      f6bhhbo P.DEBONNIERES      137      2      1
 CASTEST MODIF ssna105a                      f6bhhbo P.DEBONNIERES      233      2      1
 CASTEST MODIF ssna106a                      f6bhhbo P.DEBONNIERES      148      2      1
 CASTEST MODIF ssna107a                      f6bhhbo P.DEBONNIERES      143      2      1
 CASTEST MODIF ssnl109a                      f6bhhbo P.DEBONNIERES      633      2      1
 CASTEST MODIF ssnl109b                      f6bhhbo P.DEBONNIERES      702      2      1
 CASTEST MODIF ssnl121a                      f6bhhbo P.DEBONNIERES      583      2      7
 CASTEST MODIF ssnl121b                      f6bhhbo P.DEBONNIERES      576      2      7
 CASTEST MODIF ssnl121c                      f6bhhbo P.DEBONNIERES      440      4      9
 CASTEST MODIF ssnp05a                       f6bhhbo P.DEBONNIERES      263      2      1
 CASTEST MODIF ssnp101a                      f6bhhbo P.DEBONNIERES      137      2      1
 CASTEST MODIF ssnp107a                      f6bhhbo P.DEBONNIERES      275      2      1
 CASTEST MODIF ssnv105h                       godard V.GODARD           400     14      2
 CASTEST MODIF ssnv109a                      f6bhhbo P.DEBONNIERES      196      2      1
 CASTEST MODIF ssnv111a                      f6bhhbo P.DEBONNIERES      153      2      1
 CASTEST MODIF ssnv124a                      gjbhhel E.LORENTZ          220      2      2
 CASTEST MODIF ssnv124b                      gjbhhel E.LORENTZ          137      2      2
 CASTEST MODIF ssnv124c                      gjbhhel E.LORENTZ          126      2      2
 CASTEST MODIF ssnv146a                      gjbhhel E.LORENTZ          135      3      2
 CASTEST MODIF ssnv147a                      gjbhhel E.LORENTZ          210     57      4
 CASTEST MODIF zzzz113a                      f6bhhbo P.DEBONNIERES     4756      2      1
 CASTEST MODIF zzzz113b                      f6bhhbo P.DEBONNIERES     1248      2      1
 CASTEST MODIF zzzz113c                      f6bhhbo P.DEBONNIERES     4768      2      1
 CASTEST SUPPR ssnv401a.comm                 sellali N.SELLALI          338      0    338
 CASTEST SUPPR ssnv401b.comm                 sellali N.SELLALI          349      0    349
 CASTEST SUPPR ssnv402a.comm                 sellali N.SELLALI          354      0    354
CATALOGU AJOUT options/pilo_pred_defo         godard V.GODARD            31     31      0
CATALOGU AJOUT options/rigi_meca_elas         godard V.GODARD            68     68      0
CATALOGU AJOUT typelem/gener_me3dg_3          godard V.GODARD           338    338      0
CATALOGU AJOUT typelem/gener_mecpg2           godard V.GODARD           358    358      0
CATALOGU AJOUT typelem/gener_medpg2           godard V.GODARD           359    359      0
CATALOGU MODIF compelem/grandeur_simple__     godard V.GODARD           270     15     12
CATALOGU MODIF compelem/phenomene_modelisation__     godard V.GODARD           560     15      1
CATALOGU MODIF typelem/gener_me3d_3           godard V.GODARD           371      5      1
CATALOGU MODIF typelem/gener_meax_2           godard V.GODARD           445      5      1
CATALOGU MODIF typelem/gener_meaxs2           godard V.GODARD           448      5      1
CATALOGU MODIF typelem/gener_mecpl2           godard V.GODARD           443      5      1
CATALOGU MODIF typelem/gener_medpl2           godard V.GODARD           448      5      1
CATALOGU MODIF typelem/mecptr3                godard V.GODARD           438      5      1
CATALOGU MODIF typelem/mecptr6                godard V.GODARD           434      5      1
CATALOGU MODIF typelem/medpqs8                godard V.GODARD           437      5      1
CATALOGU MODIF typelem/medptr6                godard V.GODARD           440      5      1
CATALOPY MODIF commande/accas                gjbhhel E.LORENTZ          321      2      2
CATALOPY MODIF commande/affe_modele           godard V.GODARD           140      4      1
CATALOPY MODIF commande/defi_materiau        gjbhhel E.LORENTZ         1294      1      4
CATALOPY MODIF commande/macro_proj_base       boyere E.BOYERE            86      3      3
CATALOPY MODIF commande/post_elem            gjbhhel E.LORENTZ          393      6     13
CATALOPY MODIF commande/proj_matr_base        boyere E.BOYERE            24      1      2
CATALOPY MODIF commande/proj_vect_base        boyere E.BOYERE            17      1      2
CATALOPY MODIF commande/recu_table           gjbhhel E.LORENTZ           16      4      4
CATALOPY MODIF commande/stat_non_line         godard V.GODARD           440      5      1
 FORTRAN AJOUT algorith/lcdsbe                godard V.GODARD           429    429      0
 FORTRAN AJOUT algorith/lclocc                godard V.GODARD           263    263      0
 FORTRAN AJOUT algorith/nmpl2g                godard V.GODARD           565    565      0
 FORTRAN AJOUT algorith/nmpl3g                godard V.GODARD           559    559      0
 FORTRAN AJOUT elements/nmgeob                godard V.GODARD            85     85      0
 FORTRAN AJOUT elements/te0007                godard V.GODARD           141    141      0
 FORTRAN AJOUT elements/te0113                godard V.GODARD           204    204      0
 FORTRAN AJOUT elements/te0121                godard V.GODARD           224    224      0
 FORTRAN AJOUT elements/te0437                godard V.GODARD           155    155      0
 FORTRAN AJOUT elements/te0544                godard V.GODARD           163    163      0
 FORTRAN MODIF algorith/lcplgr               gjbhhel E.LORENTZ          336     86    280
 FORTRAN MODIF algorith/nmcomp                godard V.GODARD           466     13      2
 FORTRAN MODIF algorith/nmhoff               gjbhhel E.LORENTZ           97      2      2
 FORTRAN MODIF algorith/nmholi               gjbhhel E.LORENTZ           81      2      2
 FORTRAN MODIF algorith/nmpilo                godard V.GODARD           224      3      3
 FORTRAN MODIF algorith/nmpipe                godard V.GODARD           281     13     12
 FORTRAN MODIF algorith/op0071                boyere E.BOYERE           127     10      3
 FORTRAN MODIF algorith/op0072                boyere E.BOYERE           234      7     11
 FORTRAN MODIF algorith/pipepe                godard V.GODARD           197     23      5
 FORTRAN MODIF algorith/resuco                godard V.GODARD           496     13     13
 FORTRAN MODIF elements/te0543                godard V.GODARD           153      7      1
 FORTRAN MODIF utilitai/op0107               gjbhhel E.LORENTZ          263      2      2
 FORTRAN MODIF utilitai/op0174               gjbhhel E.LORENTZ          169    103     42


        nb unites  nb lignes  ajouts  suppr.  difference
 AJOUT :   24        6816      6816             +6816
 MODIF :   63       33317       536     519       +17
 SUPPR :    3        1041              1041     -1041
 DEPLA :    0           0 
         ----      ------     ------  ------   ------
 TOTAL :   90       41174      7352    1560     +5792 
