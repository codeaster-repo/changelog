==================================================================
Version 13.1.20 (révision eb4a7666060d) du 2016-05-24 14:27 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 25185 DU 06/05/2016
AUTEUR : BADEL Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    CALC_MAC3COEUR / LAME : non convergence
FONCTIONNALITE
   Problème :
   ========
   Un calcul de lame d'eau ne converge pas malgr la mise en place de la stratégie d'adaptation du coefficient de pénalisation. Il doit
   y avoir une erreur quelque part, vu que ce calcul a déjà tourné avec une version privée de cette adaptation.
   
   Correction :
   ==========
   Petite erreur dans mac3coeur_calcul : le pas de temps 0 (ajouté pour créer un état d'irradiation non nul) se faisait avec le contact
   activé et pas avec le contact relaché. sur le cas test ca réussissait quand meme à converger, mais pas sur un cas plus compliqué
   comme celui de la fiche.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sur calcul de la fiche
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25211 DU 16/05/2016
AUTEUR : BADEL Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En 13.1.18-053cdce446f5, le cas test mac3c01a est NOOK sur AthosDev et Aster5
FONCTIONNALITE
   suite aux restitutions en 13.1.15 les valeurs de référence ont très légèrement 
   changé (modifications de la discretisation des nappes matériaux en temperature 
   et irradiation). 
   
   le probleme etait masqué sur calibre9 par le TOLE_MACHINE qui me semble pas 
   indispensable donc je l'enleve
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    mac3c01a
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25234 DU 20/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Utiliser l'exécutable hg pour faciliter la construction avec Yamm... et la vie d'Isabelle
FONCTIONNALITE
   Problème
   --------
   
   Il y a des difficultés dans Yamm pour la construction de Salome-Meca.
   Sur Calibre9, la version de Python n'est pas celle du système. Ainsi, on ne
   peut pas utiliser Mercurial du système en tant que module Python.
   
   On utilise l'API Python de Mercurial lors de `waf configure` de code_aster.
   
   Il est alors nécessaire d'installer Mercurial.
   
   
   Correction
   ----------
   
   Cette utilisation dans `waf configure` sert à récupérer des informations sur
   l'état courant du répertoire de travail.
   On veut distinguer une version stable ou testing d'une version de développement
   en cours de modification.
   C'est pour cela qu'on récupère :
   - la révision parente,
   - la branch courante et la branche officielle d'origine,
   - le dernier tag connu dans cette branche,
   - le nombre de révisions postérieures au tag,
   - les changements en cours non versionnés.
   
   Maintenant, on ne passe plus par l'API Python de Mercurial mais en appelant
   directement l'exécutable `hg`.
   On peut se limiter à deux appels (un `log` et un `status` pour les changements
   non versionnés).
   
   On commence par chercher les informations sur les 25 dernières révisions.
   Si cela ne permet pas de remonter à une révision de référence, on interroge
   le dépôt depuis la dernière testing/stable.
   
   
   Bonus
   -----
   
   Pour `salome test`, on définie une nouvelle liste de tests étiquetés SMECA_INTEGR
   dans laquelle on met uniquement forma02a pour le moment.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    configure
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25224 DU 18/05/2016
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Arrêt brutal dans FIN lors de l'impression de la liste des SD produites
FONCTIONNALITE
   La correction est aisée, il suffit de tester le nombre de numéro d'ordre dans rsinfo et de sauter l'impression de la SD incomplète.
   
   
   Je propose d'imprimer un message du type suivant pour indique que la SD est vide :
   
   
    ======>
   
    STRUCTURE DU CONCEPT BASEAP   VIDE, NE CONTENANT AUCUN NUMERO D'ORDRE
   diff -b bibfor/resu_util/rsinfo.F90 ~/rsinfo.F90
   116a117,120
   >     if (nbordt .eq. 0) then
   >        write (ifi,103) nomd2(1:8)
   >        goto 999
   >     endif
   127a132
   > 103 format(/,1x,'STRUCTURE DU CONCEPT ',a,' VIDE, NE CONTENANT AUCUN NUMERO D''ORDRE')
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    etude fournie dans issue25188
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25233 DU 20/05/2016
AUTEUR : ANDRIAMBOLOLONA Hari
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    En version 13.1.19-70c3ec0e4889, le cas test sdnv108c est NOOK sur clap0f0q
FONCTIONNALITE
   Contexte
   ========
   En version 13.1.19-70c3ec0e4889, le cas test sdnv108c est NOOK sur clap0f0q.
   Lorsque le test est relancé, il est NOOK ou OK de façon aléatoire.
   
   Il a été 'corrigé' en version 13.1.18 par issue25138 pour le même soucis.
   
   Analyse
   =======
   Il s'agit d'un calcul de modes complexes d'une structure cylindrique en 3D_SI avec des éléments quadratiques.
   On signale un pivot presque nul sur un ddl de la structure.
   Le numéro de ce ddl change en fonction de la configuration de lancement du test.
   Il se trouve que, sur clap0f0q, en fonction de la configuration de lancement, on considère à tord un couple de modes comme étant des
   modes réels (i.e. pas de fréquence propre associée).
   Les TEST_RESU sont faits sur les fréquences propres.
   On effectue alors les tests sur les mauvais numéros de fréquence propre.
   La solution proposée dans issue25138, qui consiste à réduire SEUIL_FREQ à 5e-3, semble être insuffisante sur clap0f0q.
   
   Solution proposée
   =================
   Faire passer SEUIL_FREQ à 1e-4
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnv108c
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25106 DU 18/04/2016
AUTEUR : BOITEAU Olivier
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.1.14-faf2e4504821, le cas test fdlv111a est NOOK sur clap0f0q
FONCTIONNALITE
   PROBLEME
   ========
      NOOK sévère sur la valeur du champ DISS_SCH sur clap0f0q.
   
   ANALYSE
   =======
     Ce TEST_RESU concerne une erreur numérique d'un schéma (celui de Newmark). Donc sa valeur 
      change dès que l'on fait quoi que ce soit. Le principale est qu'elle reste faible. C'est
     toujours le cas ici.
     J'ai déjà évoqué ce pb avec les responsables du cas-tests (G.Devesa/L/Idoux). Ils tenaient à
     le garder en l'état. Je l'ai tracé ds le fichier .comm du cas-test et ds l'histor de
     l'issue17384.
   
   PROPOSITION
   ===========
      On supprime les TEST_RESU sur DISS_SCH dans ce test. La dissipation du schéma (DISS_SCH) est testée par ailleurs de manière plus
   pertinente dans le test SDNV100A.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    non-régression
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25191 DU 09/05/2016
AUTEUR : BOITEAU Olivier
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    En version 13.1.14-faf2e4504821, le test sdls504a s'arrête en erreur sur Calibre 9
FONCTIONNALITE
   PROBLEME
   ========
   Le seuil d'erreur sur le quatrième mode propre est légèrement supérieur au
   seuil fixé par défaut: 1.04 au lieu de 1.0 * 10-6. Ce pb n'apparait que sur
   C9 et depuis l'upgrade METIS4/METIS5.
   
   ANALYSE
   =======
   En changeant les paramètres solveurs linéaires on influe effectivement un peu
   sur cette norme d'erreur, et ce, quelque soit la machine.
   En jouant sur les paramètres du solveur modal (ici SORENSEN), on ne peut pas
   améliorer cette norme.
   
   Par contre, en activant la nouvelle option AMELIORATION de CALC_MODES, le
   calcul de post-traitement de MODE_ITER_INV, améliore grandement cette norme
   d'erreur: elle passe à 3.8 10-8.
   
   Petit bémol, on est quand même arrêté car le test est basé sur la norme
   d'erreur initiale et non sur celle améliorée !
   
   PROPOSITION
   ===========
     * Proviroirement transformer le test d'arrêt en alarme (VERIF_MODE/STOP_ERREUR='NON') et
    activer AMELIORER='OUI'.
     * Emettre une fiche pour corriger la relation VERI_MODE/SEUIL et AMELIORATION afin de ne tester que le
      résidu final (Issue25241).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    non-régression
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25235 DU 20/05/2016
AUTEUR : DEVESA Georges
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Restitution des champs DEPL et VITE après DYNA_ISS_VARI
FONCTIONNALITE
   Pb :
   ----
   Dans un calcul avec DYNA_ISS_VARI, on ne calcule effectivement en transitoire que 
   le champ ACCE par REST_SPEC_TEMP. Il s'agit bien d'un bug car on restitue aussi des 
   champs DEPL et VITE, mais ils demeurent nuls sans que ce soit précisé explicitement 
   par l'opérateur.
   Rq:
   ---
   Il y a bien un risque de résultats faux non seulement sur les champs DEPL et VITE 
   qui ne sont généralement pas nuls en transitoire, mais aussi et surtout pour le 
   calcul des efforts en transitoire après DYNA_ISS_VARI. C'est d'ailleurs comme cela 
   qu'on a découvert le problème.
   
   Réponse:
   --------
   Dans la boucle fréquentielle de calcul de DYNA_ISS_VARI par DYNA_VIBRA, il faut 
   également évaluer les champs de déplacement et vitesse à partir du champ 
   accélération en réponse. Pour cela, il faut traiter le cas particulier de la borne 
   de fréquence nulle par prolongement afin d'éviter l'inversion par une valeur nulle 
   de fréquence.
   On restitue également ces champs DEPL et VITE en temporel dans REST_SPEC_TEMP. 
   Pour tester la correction, on enrichit le test SDLS118B de réponse transitoire avec 
   DYNA_ISS_VARI sur une valeur de déplacement maximum en non régression.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 11.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 11.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : v2.03.118
VALIDATION
    tests SDLS118* DYNA_ISS_VARI
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24135 DU 19/08/2015
AUTEUR : CORUS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TMA : Necs
TITRE
    Blindage de DEFI_MODELE_GENE
FONCTIONNALITE
   Travail effectué en TMA :
   =========================
   
   Développement :
   
   Dans rotlir, on émet une alarme si des ddls autres que DX, DY, DZ, DRX, DRY, DRZ ont été activés sur les interfaces car ils ne sont
   pas pris en compte. 
   
   On vérifie ensuite pour chaque mode, que le PROF_CHNO du mode correspond à celui contenu dans le NUME_DDL de la base modale. Ceci
   n'étant pas prévu, on émet une erreur car la projection du mode sur la trace de l'interface est fausse dans ce cas.
   
   Vérification :
   
   La vérification est faite sur un test où la base modale est construite délibérément en assemblant des modes dynamiques et statiques
   qui ne s'appuient pas sur la même numérotation.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test perso

--------------------------------------------------------------------------------
RESTITUTION FICHE 24134 DU 19/08/2015
AUTEUR : CORUS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TMA : Necs
TITRE
    Incompatibilité des modélisations IFS avec MODE_ITER_CYCL
FONCTIONNALITE
   Travail effectué en TMA :
   =========================
   
   Dans ctetgd, la vérification de la cohérence des interfaces est faite sur les ddls de 1 à 6 au lieu de 1 à 10. Pour les ddl > 6, on
   émet une erreur (voir algorith15) si la composante est active.
   La liste typeddl dans ctetgd (écrite en dur) est fausse, on en profite pour corriger en récupérant la liste par le catalogue NOMCMP.
   Ainsi l'impression du message est correcte et on peux généraliser la vérification pour tous les ddls du catalogue > 6 (de 7 à nbcmp
   = taille de NOMCMP) plutôt que les 4 suivants (de 7 à 10).
   
   La vérification est faite par un test où on active délibérément les ddls PRES et PHI sur l'interface.
   
   Le bundle est en PJ
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test perso

--------------------------------------------------------------------------------
RESTITUTION FICHE 24133 DU 19/08/2015
AUTEUR : CORUS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Concaténation des groupes de noeuds dans DEFI_INTERF_DYNA/INTERFACE
FONCTIONNALITE
   Travail effectué en TMA :
   =========================
   
   Dans le cas où l'utilisateur définit plusieurs groupes de nœuds pour une interface dans DEFI_INTERF_DYNA, les groupes de nœuds sont
   fusionnés plutôt que concaténés pour éviter d'avoir des nœuds doubles qui conduisent à des incohérences dans les autres opérateurs.
   On fait appel à utlisi (option 'UNION'), la routine recuno est résorbée.
   
   On vérifie les tests de la base utilisant DEFI_INTERF_DYNA + un test où des groupes possédant des nœuds en communs sont passés au
   GROUP_NO lors de la définition d'une interface.
   
   Impact documentaire :
   
   Avec cette correction dans la doc U4.64.01 DEFI_INTERF_DYNA $3.2.3 : 
   
   ==========================================================================================
   GROUP_NO = ’lgno’
   
   Liste ordonnée des groupes de nœuds du maillage composant l'interface. La liste finale des
   nœuds est obtenue par concaténation des groupes de nœuds dans l'ordre donné par
   l'utilisateur, lors de la définition des groupes.
   ==========================================================================================
   
   Il faut remplacer concaténation par union.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.64.01
VALIDATION
    perso

--------------------------------------------------------------------------------
RESTITUTION FICHE 22832 DU 08/07/2014
AUTEUR : BOTTONI Marina
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    La loi GRANGER_FP_V n'est pas validée
FONCTIONNALITE
   Résumé : il existent trois lois "Granger" : GRANGER_FP_INDT, GRANGER_FP, GRANGER_FP_V :
   - GRANGER_FP_INDT : loi viscoélastique linéaire de type Kelvin généralisé (loi classique)
   - GRANGER_FP_     : loi pour le fluage du béton : GRANGER_FP_INDT + effet de la température et de l'humidité
   - GRANGER_FP_V    : loi pour le fluage du béton : GRANGER_FP + effet du vieillissement et de la température sur le vieillissement
   
   Actuellement, seule la loi GRANGER_FP_INDT est vérifiée informatiquement.
   Il faut donc vérifier GRANGER_FP et GRANGER_FP_V, avec les cas test suivants : 
   - Prise en compte de l'humidité relative : loi GRANGER_FP
   - Prise en compte de la température sans vieillissement : loi GRANGER_FP
   - Prise en compte du vieillissement (à la température de référence) : loi GRANGER_FP_V
   - Prise en compte de la température avec vieillissement : loi GRANGER_FP_V
   
   Le vieillissement et l'humidité marchent correctement, comme prévu par la formulation décrite dans la docR.
   Pour la température (déjà sans vieillissement), les choses ne semblent pas marcher correctement. On rappelle qu'une température
   différente de la température de réference a deux effets :
   1) elle modifie la contrainte (contrainte équivalente)
   2) elle modifie l'échelle temporelle, en la dilatant ou contractant.
   
   Le point 1) est correctement implémenté. Pour le 2) :
   a) les routines ne correspond pas à la doc, car le temps équivalent y est défini non sur sa valeur absolue (comme marqué sur la
   docR) mais sur le dt. Cela peut être mieux, car sinon on introduit une dépendence à l'instant initial, mais il faudrait que ce choix
   soit clarifié dans la doc et validé expérimentalement.
   b) les routines ne correspondent pas à la doc car le temps équivalent y est calculé seulement pour des températures constantes sur
   le dt (ce qui introduit des approximations grossières). dans la doc, il est défini par le biais d'un intégral sur l'historique des
   températures.
   c) il y a clairement un bug : pour une température > de la température de référence on obtient des valeurs négatives de la
   déformations initiale, alors que le test est en traction (voir pièce jointe).
   
   Bref, je suggère de resorber la dépendence à la température et de faire de deux lois (GRANGER_FP et GRANGER_FP_V) une seule :
   GRANGER_FP_V.
   Cela signifie:
   - éliminer l'opérande QSR_K du mot-clé GRANGER_FP de DEFI_MATERIAU 
   - éliminer l'opérande QSR_VEIL du mot-clé V_GRANGER_FP de DEFI_MATERIAU
   - éliminer le catalogue de loi GRANGER_FP : granger_fp.py
   
   Les fortrans restera inchangée, car il y a les mêmes routines pour les trois lois.
   Cela permettra de reintroduire très rapidement la dépendance à la température, si cela intéresse quelque projet (Ciwap2, Stockage,
   autre)
   
   Je restitue donc deux cas-test (SSNV105a et b), clé doc associée V6.04.105:
   - Prise en compte de l'humidité relative, avec allure linéaire et décroissante à partir de l'instant initial (test séchant);
   contrainte constante.
   - Prise en compte du vieillissement ; contrainte constante.
   
   Impact doc : 
   - V6.04.105 (nouveaux cas test)
   - doc R Granger R7.01.01
   - doc U de DEFI_MATERIAU  U4.43.01
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 12.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 13.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V6.04.105, R7.01.01, U4.43.01
VALIDATION
    SSNV105a
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25041 DU 06/04/2016
AUTEUR : FERTE Guilhem
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Couverture de dclhoa
FONCTIONNALITE
   Anomalie:
   ---------
   
   Le routine dclhoa n'est plus couverte
   
   Solution:
   ---------
   
   Lors de résolutions avec GLRC, on est amené à trouver les racines d'une fonction, puis à les trier suivant un certain critère. S'il
   y a moins de 8 racines, on met en oeuvre un algorithme de tri basique, s'il y en a plus, on utilise un algorithme plus évolué. C'est
   cet algorithme plus évolué qui est codé par la routine dclhoa. 
   
   Depuis les développement de l'an dernier, il y a au maximum 4 racines, la routine dclhoa n'est plus appelée, et peut donc être
   supprimée.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas tests GLRC_DAMAGE
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 25125 DU 20/04/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    [robinetterie] Plantage LIRE_CHAMP/ELEM_COEH_R
FONCTIONNALITE
   Problème :
   ----------
   Un utilisateur essaye de relire un champ de coefficients d'échange thermique contenu dans un fichier MED par LIRE_CHAMP. Cela plante
   avec le message :
   """
   il n y a pas de paramètre  INOUT  associe a la grandeur: COEH_R  dans l option: TOU_INI_ELEM
   """
   
   
   Solution :
   ----------
   Il faut modifier tou_ini_elem.py et ther_face3.py pour que l'initialisation du cham_elem soit possible dans LIRE_CHAMP.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    etude jointe
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25063 DU 08/04/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Plantage dans Calc_Champ
FONCTIONNALITE
   Problème :
   ----------
   Cette fiche est liée à issue24882. Après un calcul avec STAT_NON_LINE (9min), on plante dans CALC_CHAMP.
   
   
   Solution :
   ----------
   Le problème vient de la structure de données charge. Grosso modo, on a 4 charges stockées dans la sd_resu mais quand on regarde la
   liste, on obtient 3 noms et une chaîne vide.
   
   Du coup quand on fait le dismoi('TYPE_CHARGE') avec la chaîne vide, on plante.
   
   Je propose d'ignorer cette chaîne vide. Ainsi le dismoi se passera correctement.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test unitaire
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25239 DU 23/05/2016
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    En version 13.1.19-bc7c294adc01, le cas test sdnv142a est NO_TEST_RESU sur AthosDevValid
FONCTIONNALITE
   Problème
   --------
   
   En version 13.1.19-bc7c294adc01, le cas test sdnv142a est NO_TEST_RESU sur AthosDevValid.
   
   
   Correction
   ----------
   
   Il faut appliquer le correctif de issue25190 à la modélisation A.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnv142a
NB_JOURS_TRAV  : 0.25

--------------------------------------------------------------------------------
RESTITUTION FICHE 25021 DU 05/04/2016
AUTEUR : BOYERE Emmanuel
TYPE aide utilisation concernant Code_Aster (VERSION 12.5)
TITRE
    Aide sur la realisation de calcul modal d'ailette (Partie 2)
FONCTIONNALITE
   Contexte 
   --------
   
   Il s'agit d'une AOM concernant un calcul de roue de turbine, réalisé par sous-structuration cyclique.
   Une première pré-étude avait montré que l'étude était possible.
   Pour accroître la précision du calcul l'utilisateur a modifié le maillage pour le faire passer de linéaire à quadratique.
   
   
   Point de blocage
   -----------------
   
   Alors qu'avec le maillage linéaire le calcul passe, le même maillage avec des noeuds milieux pour le rendre quadratique provoque
   l'erreur suivante dans MODE_ITER_CYCL :
   
      !-------------------------------------------!
      ! <EXCEPTION> <ALGORITH16_52>               !
      !                                           !
      !  axe de symétrie cyclique différent de Oz !
      !  numéro du couple de noeuds :  2267       !
      !  noeud droite -->  N11211                 !
      !  noeud gauche -->  N11285                 !
      !-------------------------------------------!
   
   
   Diagnostic
   ----------
   
   C'est en effet une bonne idée que de passer le maillage 3D d'éléments linéaires en éléments quadratiques car il est bien connu que
   les éléments linéaires ne convergent que lentement.
   Dans le cas de l'étude d'ailettes en question, on le constate bien : sur un secteur de roue, le calcul en éléments linéaires donne
   pour fréquence fondamentale 158 Hz alors que les éléments quadratiques basés sur le même maillage conduisent à une estimation de 117
   Hz. Le maillage linéaire est bien trop "raide" par rapport à la réalité.
   
   En revanche le maillage de l'ailette n'est pas assez propre pour conduire à des éléments quadratiques de qualité.
   De mémoire, il me semble qu'il a été produit par scan laser. Il faudrait nettoyer la géométrie produite par ce relevé avant de
   construire le maillage éléments finis. 
   En effet on observe déjà sur le maillage en éléments linéaires des éléments de très mauvaise qualité (rapport de forme 3D supérieur
   à 1000 !!!). Cela correspond à des éléments 3D complètement écrasés.
   Sur les résultats globaux, comme les fréquences propres, ce n'est pas forcément très grave, à condition que les éléments en question
   ne se situent pas à des endroits critiques (comme les conditions aux limites).
   Par contre ces éléments aplatis mènent dans l'opération de construction du maillage quadratique à des éléments tordus, qui n'ont pas
   une forme saine. C'est pourquoi Code_Aster râle lors de l'opération de raccord de maillages dans le calcul par sous-structuration
   cyclique : les normales des éléments distordus ne peuvent pas être bien orientées.
   
   Je joins quelques illustrations pour montrer le problème.
   Enfin je me permets un dernier conseil pour la création du maillage quadratique à partir du scan de l'ailette.
   Il faudrait, si possible, construire directement le maillage quadratique, sans passer par une facettisation linéaire.
   En effet, il est important de conserver les courbures, en particulier au niveau des zones de contact comme les trous des broches.
   
   
   Conclusion
   ----------
   
   Il est louable de chercher à faire converger le calcul modal en raffinant le maillage mais la première opération est de partir d'une
   géométrie saine. La description actuelle ne permet qu'une première approche grossière du comportement vibratoire de la roue.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25122 DU 20/04/2016
AUTEUR : DEVESA Georges
TYPE anomalie concernant Code_Aster (VERSION 10.*)
TITRE
    DEFI_SOL_EQUI : liste de référence d'abcisses pour interpolation des courbes G-gamma
FONCTIONNALITE
   Pb :
   ---
   "Suite à un retour du CNEPE, on constate que, quand il y a plusieurs listes 
   d’abscisses de déformation différentes par matériau dans les courbes G-gamma, 
   DEFI_SOL_EQUI va utiliser comme liste de référence pour interpolation celle donnée 
   pour la dernière couche correspondant au bedrock. A l’avenir, il faudrait au moins 
   le préciser dans la doc de l’opérateur ou donner la possibilité de donner une liste 
   de référence d’abscisses pour interpolation."
   
   Réponse:
   --------
   La réponse est dans la question. Compte tenu des délais et de la prévision 
   d'introduire des développements conséquents dans le cadre de la fiche issue24600, il 
   est plus raisonnable actuellement de compléter la doc de DEFI_SOL_EQUI en précisant 
   comme remarque supplémentaire concernant les listes d’abscisses de déformation (il y 
   en a déjà 2) ce qui est dit ci-avant.
   Dans le cadre de la fiche d'évolution issue24600, il est bien prévu de donner la 
   possibilité d'introduire une liste de référence d’abscisses pour interpolation 
   commune à tous les matériaux.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.84.31
VALIDATION
    doc DEFI_SOL_EQUI
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 25111 DU 18/04/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.1.14-faf2e4504821, le cas test ssnp140c est NOOK sur Calibre9 et clap0f0q
FONCTIONNALITE
   Problème
   ========
   
   En version 13.1.14-faf2e4504821, le cas test ssnp140c est NOOK sur Calibre9 et clap0f0q
   apparemment suite à [#17384] Upgrade of METIS: from 4.0 to 5.1.0
   
   Solution
   ========
   
   Ce test vérifie ENDO_FRAGILE en mode IMPLEX. Le premier SNL fait du ENDO_FRAGILE en mode standard (Newton-Raphson), puis un SNL en
   mode IMPLEX.
   
   ENDO_FRAGILE est peu robuste (il doit être remplacé par ENDO_SCALAIRE, voir issue14675). En plus, il s'agit du mode local (D_PLAN)
   et pas de ses versions régularisées (GRAD_EPSI, GRAD_VARI). Le premier SNL est donc instable: quand on change la numérotation, le
   solveur, on a des différences entre machines.
   Ce n'est pas la peine de s'acharner: on propose de récupérer les résultats (des fonctions) du premier SNL pour faire la comparaison
   avec IMPLEX
   mais de ne pas refaire le calcul systématique pour comparer (on ne garde que le calcul IMPLEX)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnp140c
NB_JOURS_TRAV  : 0.5

