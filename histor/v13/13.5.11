==================================================================
Version 13.5.11 (révision a352ffe99f26) du 2018-05-22 13:05 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 27684 DU 23/05/2018
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant code_aster (VERSION 11.8)
TITRE
    DEFI_GROUP / CREA_GROUP_xxx : le mot-clé NOM attend un texte, pas un grma
FONCTIONNALITE
   Problème
   --------
   
   Dans DEFI_GROUP, les mots-clés NOM sont de type grma ou grno. Or on attend un nom pour un nouveau groupe de mailles/noeuds à créer.
   
   Dans MACR_ADAP_MAIL, les mots-clés NOM_CMP sont de type grma. Or on attend une liste de noms de composantes.
   
   
   Correction
   ----------
   
   On change pour le type 'TXM'.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit
DEJA RESTITUE DANS : 14.1.18
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27603 DU 18/04/2018
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant code_aster (VERSION 14.1)
TMA : Necs
TITRE
    Restitution correction Conditional jump
FONCTIONNALITE
   Problème
   --------
   
   Dans le cadre de la vérification informatique de code_aster (cf. issue27600), on corrige plusieurs variables non initialisées ou
   branchements mal effectués.
   
   Correction
   ----------
   
   On corrige les "conditionnal jump" suivants détectés par Valgrind :
   
   - orilgm.F90 --> initialisation de la variable vect(:)=0.d0 --> 1 cas-test
   
   - xcface.F90--> initialisation de la variable minlst = 1*r8maem() --> 4 cas-tests
   
   - conori.F90 --> initialisation de la variable loreo0=.false. --> 19 cas-tests
   
   - dfllsv.F90 --> ajout d'une condition --> 17 cas-tests
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage des tests incriminés
DEJA RESTITUE DANS : 14.1.18

--------------------------------------------------------------------------------
RESTITUTION FICHE 27636 DU 27/04/2018
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant code_aster (VERSION 11.8)
TITRE
    PERF - fdlv112a sur Eole vs Aster5
FONCTIONNALITE
   Problème
   --------
   
   Le test fdlv112a est beaucoup plus lent sur Eole que sur Aster5.
   
   
   Correction
   ----------
   
   Analyse faite par l'exploitation en référence dans l'historique de la fiche.
   
   La différence (au moins sur le test produit par Thomas) vient de l'adaptation automatique de Openblas à l'architecture du
   processeur. En forçant l'architecture à SandyBridge, on retrouve les temps corrects :
   
   $ export OPENBLAS_CORETYPE=SANDYBRIDGE
   $ python ~/dev/python/test_complex_op.py 
   47.7957818508
   
   contre 115 secondes par défaut.
   
   On ajoute donc cette variable dans la configuration de Eole.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    fdlv112a
DEJA RESTITUE DANS : 14.1.17
NB_JOURS_TRAV  : 1.0

