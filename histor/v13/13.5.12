==================================================================
Version 13.5.12 (révision 735c25cb4277) du 2018-06-04 10:37 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 27370 DU 01/02/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.2)
TITRE
    Erreur de compilation avec Intel2018
FONCTIONNALITE
   Problème :
   ========
   Les routines lub_module.F90, yacs_module.F90 et yacsnl_module.F90 ne compilent pas avec la version 2018 du compilateur Intel ifort :
   l'affectation de la valeur du pointeur dans la déclaration est refusée. 
   Correction :
   ==========
   On sépare en deux instructions la déclaration et l'affectation du pointeur.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    compilation des routines lub_module.F90, yacs_module.F90 et yacsnl_module.F90
DEJA RESTITUE DANS : 14.1.19
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27492 DU 22/03/2018
AUTEUR : ABBAS Mickael
TYPE anomalie concernant code_aster (VERSION 11.8)
TITRE
    FAUX - Blindage LIAISON_UNIL et CONTACT discret contre les matrices non-symétriques
FONCTIONNALITE
   Problème
   --------
   
   Avec la mise en place ALGO_CONT='PENALISATION' pour LIAISON_UNIL, on souhaite interdire l’utilisation de matrices non-symétriques
   avec l’algorithme des contraintes actives, qu’il soit utilisé en formulation DISCRETE ou LIAISON_UNIL.
   
   
   Correction
   ----------
   
   En V14, on passe les messages en erreur fatale et on change les conseils:
   1/ Pour LIAISON_UNILATER: des algorithmes de type PENALISATION 
   2/ Pour le contact: la formulation CONTINUE ou la pénalisation.
   
   En V13, Pour LIAISON_UNILATER, on invite l'utilisateur à se mettre dans une version plus récente.
   
   Résultat faux
   -------------
   
   L'utilisation conjointe de LIAISON_UNILATER et de matrices non-symétriques peut conduire à des résultats faux.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 13.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 14.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    src
DEJA RESTITUE DANS : 14.1.19
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26753 DU 24/07/2017
AUTEUR : DEGEILH Robin
TYPE evolution concernant code_aster (VERSION 14.1)
TITRE
    [RUPT] Modification maillages des tests perfe02a et ssnv108a issus du plugin CT
FONCTIONNALITE
   Problème:
   =========
   Suite à la fiche issue26697 sur des erreurs dans les tests unitaires du plugin CT (suite à évolution des mailleurs), il faut changer
   les maillages des tests code_aster perfe02a et ssnv108a.
   
   Solution:
   =========
   J'ai re-généré les maillages des cas tests aster ssnv108a et perfe02a en utilisant les tests du même nom dans smeca (avec la version
   V8_INTEGR du 24/05/2018, donc a priori basé sur Salome 8.5 RC2). Les maillages sont visuellement très proches des maillages
   originaux. J'ai mis à jour les valeurs des tests qui étaient en NOOK (écart max de 1.3%).
   
   Impact doc:
   =========
   Mise à jour des valeurs de non-régression dans les doc des cas tests
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : V6.04.108, V1.02.012
VALIDATION
    ssnv108a, perfe02a
DEJA RESTITUE DANS : 14.1.19
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27632 DU 26/04/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.4)
TMA : Necs
TITRE
    Erreur <JEVEUX_26> dans CREA_RESU
FONCTIONNALITE
   Problème :
   ========
   Lors de l'appel à CREA_RESU dans une boucle python for, on obtient un message d'erreur du type
   
      !-------------------------------------------------------------!
      ! <EXCEPTION> <JEVEUX_26>                                     !
      !                                                             !
      ! Objet inexistant dans les bases ouvertes : .MODELE    .LGRF !
      ! l'objet n'a pas été créé ou il a été détruit                !
      ! Ce message est un message d'erreur développeur.             !
      ! Contactez le support technique.                             !
      !-------------------------------------------------------------!
   
   Analyse :
   =======
   Le problème survient car CREA_RESU, en mode "reuse" et quand un pas de temps a déjà été stocké, fait quelques vérifications
   complémentaires qui ne sont pas blindées. Il faut protéger l'appel à dismoi dans lrcomm en évitant de lui passer un nom de modèle
   vide (c'est le cas dans crtype si nb_modele > 1) Dans ce cas il faut émettre un message d'erreur en suggérant d'ajouter le nom du
   modèle dans CREA_RESU.
   
   Correction effectuée :
   ====================
   On ajoute une vérification que le nom du modèle n'est pas vide avant l'appel à dismoi. S'il est vide on émet un message d'erreur
   pour prévenir que le modèle est absent et qu'il faut renseigner le mot-clé MODELE. 
   
   Validation : bonne émission du message sur le test fourni.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test fourni
DEJA RESTITUE DANS : 14.1.19

--------------------------------------------------------------------------------
RESTITUTION FICHE 27513 DU 29/03/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 14.1)
TMA : Necs
TITRE
    Erreur de programmation dans PROJ_CHAMP
FONCTIONNALITE
   Problème:
   ========
   Lorsqu'on utilise PROJ_CHAMP en version unstable sur aster5 pour post-traiter une 
   étude Vercors, on obtient le message d'erreur suivant :
   
   Description de l'erreur :
      !-------------------------------------------------------!
      ! <EXCEPTION> <DVP_1>                                   !
      !                                                       !
      ! Erreur de programmation.                              !
      !                                                       !
      ! Condition non respectée:                              !
      !     .false.                                           !
      ! Fichier pjefca.F90, ligne 163                         !
      !                                                       !
      !                                                       !
      !                                                       !
      ! Il y a probablement une erreur dans la programmation. !
      ! Veuillez contacter votre assistance technique.        !
      !-------------------------------------------------------!
   
   Analyse :
   =======
   
   Dans le cadre de issue27226, j'ai modifié le traitement des mots-clés TOUT_2, GROUP_MA_2 et MAILLE_2 de VIS_A_VIS. Précédemment en
   construisait directement une liste de nœuds à partir de ces données (plus celles issus de GROUP_NO_2 et NOEUD_2). Maintenant, on
   récupère d'abord les mailles afin de supprimer les mailles qui ne sont pas de la même dimension topologique que la maille de la plus
   grande dimension topologique. Pour trouver cette dimension, j'utilise la routine pjefca comme cela est fait dans les cas TOUT_1,
   GROUP_MA_1 ...
   
   Or dans le cas présent, le maillage sur lequel on souhaite projeter contient seulement des éléments POI1 et la routine pjefca ne
   prend pas en compte la dimension topologique 0, d'où le ASSERT.
   
   Le problème se corrige facilement en intégrant le cas de la dimension 0 au cas de la dimension 1.
   
   Point sur lequel faire attention :
   --------------------------------
   Si un utilisateur cherche à projeter un champ sur un groupe de maille ne contenant que des POI1 (avant ou après correction)(mot-clé
   GROUP_MA_1), il va tomber sur le même ASSERT que celui du présent problème. Avec la modification que je propose pour corriger ce
   problème, le calcul va aller un peu plus loin et l'utilisateur obtiendra le message suivant au lieu du ASSERT :
   
   
      !--------------------------------------!
      ! <EXCEPTION> <CALCULEL4_57>           !
      !                                      !
      !  Il n'y a pas de mailles a projeter. !
      !--------------------------------------!
   
   Ce message ne me semble pas très clair. Pour clarifier les choses je propose le message suivant :
   
      !------------------------------------------------------------------------------!
      ! <EXCEPTION> <CALCULEL4_57>                                                   !
      !                                                                              !
      !  Aucune des mailles du maillage 1 fournies ne permet d'effectuer la          !
      !  projection souhaitée.                                                       !
      !                                                                              !
      !  Conseil :                                                                   !
      !     Vérifiez que les mailles fournies ne sont pas toutes ponctuelles (POI1). !
      !------------------------------------------------------------------------------!
   
   Travail effectué :
   ================
   
   1- Modification de pjefca pour que les mailles POI1 soient considérées de dimension topologique 1D. (ce qui règle le problème de
   cette fiche)
   2- Modification du message CALCULEL4_57 pour qu'il soit plus clair pour l'utilisateur comme proposé ci-dessus.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test perso
DEJA RESTITUE DANS : 14.1.19

--------------------------------------------------------------------------------
RESTITUTION FICHE 27544 DU 04/04/2018
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant code_aster (VERSION 11.8)
TMA : Necs
TITRE
    Affichages intempestifs dans MED
FONCTIONNALITE
   Problème :
   ========
   la fiche issue26834 réactive certains messages intempestifs de MED à l'ouverture d'un fichier.
     IMPR_RESU(FORMAT='MED',
               RESU=_F(RESULTAT=RESUNL,
                       INFO_MAILLAGE='NON',
                       IMPR_NOM_VARI='OUI',),
               UNITE=80,
               INFO=1,
               PROC0='OUI',)
   
   /home/I27518/TRAV2018/Y2/V8_PRE_default_codeaster_prerequisites_calibre_9_mpi/tools/src/Medfichier-331-hdf51814/src/ci/MEDfileVersionOpen.c
   [71] : Erreur à l'ouverture du fichier 
   /home/I27518/TRAV2018/Y2/V8_PRE_default_codeaster_prerequisites_calibre_9_mpi/tools/src/Medfichier-331-hdf51814/src/ci/MEDfileVersionOpen.c
   [71] : fort.80
   
   Analyse :
   =======
   Le message med est écrit car on tente d'ouvrir un fichier med en mode lecture alors que le fichier MED n'a pas été créé. Cependant,
   ce qui est embêtant est que les fichiers fort.80, ou par exemple fort.97, si on choisit l'unité 97, existent déjà sans avoir été
   créés par MED.
   
   La routine as_mfiope, renvoie un code retour négatif en plus du message. Mais dès que l'appel est fait en mode création (3) (dans
   irmhdf), le fichier fort.80 devient un fichier med et le problème disparaît. 
   
   J'ai tenté d'utiliser la routine mfiexi, qui teste l'existence du fichier, mais elle teste justement seulement l'existence du
   fichier, elle ne regarde pas si c'est une fichier med. L'autre seule routine de MEDFile*** que l'on peut appeler sans avoir ouvert
   le fichier est justement MEDFileCompatibility, mais elle ressort avec un code retour négatif et émet elle aussi un message.
   
   Il faut donc voir pourquoi le fichier est déjà existant. Cela est du à l'appel à ulopen dans op0039. Cet utilitaire ouvre le fichier
   tout en gérant la table des unités logiques. Avec l'aide de Jean-Pierre, nous avons constaté que dans DEFI_FICHIER, ce n'est pas
   ulopen mais uldefi qui est utilisé pour les fichiers binaires. Cette routine, contrairement à ulopen, ne crée pas le fichier,
   cependant elle n'est pas tout à fait équivalente car elle ne permet pas de modifier les informations sur le fichier MED (F_MED), ce
   qui pose problème si on souhaite écrire des résultats MED dans des fichiers différents.
   
   Correction :
   ==========
   On duplique ulopen en ulaffe en supprimant le open (et les close). J'appelle ulaffe dans
   op0039 dans le cas med. Les tests utilisant IMPR_RESU fonctionnent en v13 et v14.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests utilisant impr_resu
DEJA RESTITUE DANS : 14.1.19

