==================================================================
Version 13.3.10 (révision 164ae89f2592) du 2017-03-15 15:02 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 25995 DU 25/01/2017
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D111.17 - Remplacer le passage de température en argument par les variables de commande
FONCTIONNALITE
   Demande
   =======
   
   Le but de la fiche est de mettre fin au passage en argument de la température pour la THM.
   
   En effet, en THM, la température est une inconnue. Donc, pour utiliser les LdC, on a ajouté la température en argument (optionnel),
   ce qui oblige à modifier les LdC mécanique dès qu'on les branche en THM.
   
   Cette fiche a pour but de remplacer ce mécanisme peu élégant par le mécanisme standard, c'est-à-dire par les variables de commande.
   
   Réalisation
   ===========
   
   L'idée principale est donc de passer par le mécanisme des variables de commande pour que les routines de LdC qui récupèrent
   l'information sur la température ne soient pas modifiées quand on les utilise en THM.
   
   Pour cela, on ajoute trois valeurs dans le common CALCUL:
   
       integer :: ca_ctempl_
       real(kind=8) :: ca_ctempr_, ca_ctempm_, ca_ctempp_ 
   !     ca_ctempl_ : 1 if temperature is coupled variable (non external state variable)
   !     ca_ctempr_ : for reference temperature when coupled variable (non external state variable)
   !     ca_ctempm_ : for previous temperature when coupled variable (non external state variable)
   !     ca_ctempp_ : for end temperature when coupled variable (non external state variable)
   
   
   Au moment de la création du matériau codé (routine rcmfmc), on prépare également les informations pour les variables de commande. On
   ajoute donc l’initialisation de ce common (ca_ctempl_ = 1 en THM).
   
   NB: on interdit l'usage simultanée de la THM et de la variable de commande TEMP mais les autres variables de commande restent
   autorisées.
   
   On a modifié rcvarc pour interroger ce common quand on est en THM.
   Les valeurs de température sont remplies dans le TE de la THM (routine calcva.F90).
   
   Validation: tous les tests THM sont OK
   
   Dans la fiche issue25879, on remplacera les appels actuels loi par loi, par un appel direct unique à nmcomp.
   
   NB1: l’utilisation de la routine nmcomp permet (en théorie) de faire des contraintes planes de DeBorst en THM.
   NB2: la programmation spéciale de LKR oblige à faire une glute dans plasti.F90. En effet, cette loi stocke la température dans les
   paramètres matériaux, ce qui n'est pas conforme au schéma général de plasti
   NB3: je me suis mis responsable sur un certain nombre de routines liées aux variables de commande et j'ai fait un peu de refactoring.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25879 DU 04/01/2017
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    D111.17 - Utilisation de nmcomp en THM
FONCTIONNALITE
   Proposition
   ===========
   
   La THM n'utilise pas le schéma standard des lois de comportement pour la mécanique.
   En effet, elle n'appelle pas la routine générale nmcomp.F90 qui gère les appels aux comportements. Or, par conception (notions de
   contrainte et de déformation généralisées), il est possible de le faire.
   De fait, en pratique, quand on veut utiliser une LdC mécanique qui n'a pas été prévue initialement en THM, il est nécessaire de la
   brancher dans la routine calcme.F90 (et donc faire un développement)
   
   Développement
   =============
   
   Pour utiliser la mécanique en THM sans modification des lois, il est nécessaire de faire un travail préparatoire:
   1/ Gérer la température THM comme une variable de commande. C'est fait par issue25995
   2/ Utiliser un format d'entrée-sorties standardisée pour nmcomp. C'est l'objet de cette fiche
   
   Pour le format générique de nmcomp, l'idée est d'interdire d’utiliser des comportements spécifiques:
   * liés à une modélisation particulière (GRAD_EPSI, JOINT ...)
   * qui utilisent des données non standard (en particulier, ce qui est passé dans wkin: longueur caractéristique, repère de rotation)
   
   
   Modélisations particulières
   ===========================
   
   En fait, en pratique, il faudrait identifier par les catalogues la cohérence entre la LdC et la modélisation. Or nous avons
   découvert que ce travail n'était pas vraiment fait (voir issue26245) contrairement à ce que semblait indiquer le contenu des
   catalogues des LdC.
   C'est un gros travail et nous proposons dans cette fiche de régler le problème partiellement, en l'occurrence, déplacer toutes les
   LdC identifiées comme spécifiques au-delà du numéro 100. Et ainsi, s'arrêter en erreur fatale dans calcme si l'utilisateur en
   utilise une.
   
   A titre d'exemple, nous avons mis en place la future architecture des catalogues LdC pour identifier Ldc/comportements spécifiques
   pour plusieurs cas: GRAD_EPSI, GRAD_VARI, GDVARINO, GRAD_SIGM, SIMO_MIEHE et IMPLEX
   L'idée est d'utiliser le même principe que le compte des variables internes
   Une LdC est constituée par une série de comportements élémentaires identifiant les différentes composantes (par exemple, type de
   déformation, De Borst, type de matrice). Ensuite, le mécanisme en Python additionne le nombre total de variables internes.
   Par exemple: SIMO_MIEHE ajoute systématiquement 6 variables internes, quelque soit la relation de comportement.
   On étend ce mécanisme au numéro de la LdC: c'est aussi la somme des numéros élémentaires.
   Par exemple, SIMO_MIEHE a pour numero 1000 + VMIS_* a pour numéro 2 => VMIS_* avec SIMO_MIEHE a pour numéro 1002
   GRAD_EPSI : 5000
   GRAD_VARI : 6000
   GDVARINO  : 3000
   GRAD_SIGM : 4000
   IMPLEX    : 2000
   
   Développement:
   - on identifie l'attribut TYPMOD2 dans le catalogue des éléments
   - on a interdit l'usage de deux modélisations différentes sur la même occurrence de COMPORTEMENT pour simplifier (ce qui est
   réaliste en pratique)
   - on a crée les nouvelles lcxxxx.F90 nécessaire en déplaçant les routines qui étaient dans le même lcxxxx à l'origine
   
   Dans un second temps, on utilisera TYPMOD2 dans le catalogue des LdC pour vérifier de manière explicite la cohérence. Actuellement,
   l’utilisateur qui utiliserait une LdC non développée pour une modélisation donnée tombera sur un message d'erreur dans nmcomp.
   
   Données non standard
   ====================
   
   Ces données concernent essentiellement des informations données dans WKIN/WKOUT. On ne sait pas remplir ces informations (par
   exemple longueur caractéristique) quand on est en THM.
   On a donc soit déplacé toutes les LdC spécifiques au delà du numéro 100, soit supprimer cet argument dans les routines lcxxxx.F90
   parce qu'elles ne servaient pas.
   Les VRAIES lois utilisant ces paramètres sont:
   - BETON_DOUBLE_DP
   - META_LEMA_ANI
   - MONOCRISTAL (utilisant l’environnement plasti, ce dernier a du être modifié pour rendre la donnée optionnelle en entrée)
   - CZM_*
   - JOINT_*
   - tout ce qui concerne KIT_RGI
   - CABLE_GAINE
   
   Branchement de nmcomp
   =====================
   
   On stocke dans compor(16) le numéro de la LdC mécanique, puis on branche nmcomp dans calcme.
   Il reste des lois très spécifiques: HOEK_BROWN, BARCELONE et ELAS_GONF qui ne sont pas intégrables directement par le mécanisme
   nmcomp. D'ailleurs ces lois ne sont pas utilisables en mécanique pure.
   On verra ce qu'on en fait plus tard
   
   
   Validation: tous les tests
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    submit
NB_JOURS_TRAV  : 7.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26108 DU 23/02/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    A203.xx - Suppression des mots-clés globaux
FONCTIONNALITE
   Dans les catalogues de commandes, le statut global d'un mot-clé lui permet d'être accessible de l'ensemble des mot-clés facteurs ou
   des blocs, dans le cas contraire, il n'est visible que du mot-clé facteur ou du bloc courant. Pour AsterStudy, on est conduit à
   supprimer cet attribut qui ne permet pas de pas de présenter facilement un enchaînement déterminé de boîtes de dialogue.
   La suppression de l'attribut "global" dans les catalogues de commandes entraîne la réécriture des catalogues suivants pour ajouter
   des blocs et modifier les conditions logiques.  
   Listes des commandes impactées :
   
   calc_mac3coeur.capy
   calc_modes.capy
   calc_spectre_ipm.capy
   comb_sism_modal.capy
   crea_resu.capy
   defi_contact.capy
   defi_fiss_xfem.capy
   defi_list_inst.capy
   dyna_vibra.capy
   impr_fonction.capy
   impr_resu.capy
   macr_spectre.capy
   modi_repere.capy
   post_k1_k2_k3.capy
   
   Seule la modification du catalogue de la commande DEFI_LIST_INST a un impact sur les fichiers de commandes : on est obligé déplacer
   le mot clé simple METHODE qui était auparavant sous un mot clé facteur DEFI_LIST.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.34.03
VALIDATION
    liste vérification
NB_JOURS_TRAV  : 6.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26139 DU 01/03/2017
AUTEUR : BOITEAU Olivier
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    A301.xx - Montée de version de MUMPS 5.1
FONCTIONNALITE
   OBJET
   =====
     Upgrade de MUMPS5.0.2(consortium) à MUMPS5.1.0(consortium).
   
   IMPACT FONCTIONNEL
   ==================
     * Code_Aster compatible avec MUMPS: v5.0.2(consortium)/v5.1.0(consortium)
     au lieu de v5.0.1(consortium)/v5.0.2(consortium). On garde la compatibilité informatique version de
      MUMPS N/N-1 (consortium). On a toutes les fonctionnalités avec la version Nconsortium.
     * Dans le bloc SOLVEUR des commandes impliquant la résolution de systèmes linéaires, nouveau mot-clé
       ACCELERATION à la place de LOW_RANK_TAILLE.
     
      * Correction d'un bug lors du cumul low_rank + threads (Issue25663)
   
      * Gains en temps variables: en moyenne 20% (avec FR+/LR+) jusqu'à 50% sur certains cas très favorables.
   
   CHANGEMENTS SYNTAXE
   ===================
     * LOW_RANK_TAILLE disparait laissant la place à ACCELERATION. LOW_RANK_SEUIL subsiste.
     Fonctionnalité disponible avec 5.1.0(consortium).
   
   -   _BlocMU['LOW_RANK_TAILLE'] =SIMP(statut='f', typ='R', defaut=-1.0,)
   +   _BlocMU['ACCELERATION'] =SIMP(statut='f', typ='TXM', defaut='AUTO',into=('AUTO','FR','FR+','LR','LR+'))
       _BlocMU['LOW_RANK_SEUIL']=SIMP(statut='f', typ='R', defaut=0.0, )
   
     * On désactive toutes les optimisations via ACCELERATION='FR' (calcul classique dit 'full rank').
       C'est la situation qui a prévalue jusqu'à présent.
   
     * On introduit la valeur 'AUTO' (pour homogéneiser l'API )
            - qui vaut 'FR' pour l'instant qqes soient les modèles/versions de MUMPS (heuristique à partir
              de Code_Aster v14).
            - valeur par défaut.
     * On active les compressions low-rank std via ACCELERATION='LR' au lieu de LOW_RANK_TAILLE>0 précédemment. Le seuil de
       compression est toujours fixé par LOW_RANK_SEUIL.
       => destinés à progressivement être mis en mode 'AUTO' (heuristique).
   
     * D'autres variantes à l'essai (seulement avec 5.1.0consortium):
        - 'FR+' une version accélérée de 'FR'
        - 'LR+' une version accélérée de 'LR' (à donc besoin de LOW_RANK_SEUIL aussi).
       Ces dernières donnent souvent de meilleures perfs (en temps) que leurs pendants, 'FR' et 'LR' mais des
       bugs et des pbs de consistance de comportement (test de sturm, détection de singularité...) ne sont
       pas à exclure. Donc pour l'instant prudence. Usage avancé.
       => destinés à progressivement être mis en mode 'AUTO' (heuristique) et surtout pourrait être utilisés
       avec moins de risque avec PETSc (moins de pb de détection de singularité, de sturm ou de consistance)
       en usage préconditionneur.
   
      RQ1. Suivant les versions de MUMPS seules certaines accélérations sont disponibles via Code_Aster.
      Lorsque cela n'est pas le cas, on emet une alarme (FACTOR_48) et on passe en 'FR'.
   
       48: _(u"""
   Solveur MUMPS :
     Une option d'accélération non disponible avec cette version de MUMPS a été activée.
     Pour continuer malgré tout le calcul, on lui a substitué l'option %(k1)s.
     Votre calcul risque juste d'être ralenti.
   """),
   
       Régle de compatibilité:
       MUMPS5.1.0consortium  : toutes les options d'accélération
       MUMPS5.1.0            : seulement 'FR','LR'
       MUMPS5.0.2(consortium): seulement 'FR'
   
   RAJOUT DE NOUVEAUX CALCULS DANS LES CAS-TESTS
   =============================================
      * Dans mumps03a (arithmétique complexe double précision) et mumps05a (idem réel double précision),
      rajout de nouveaux calculs pour tester explicitement les nouvelles valeurs du mot-clés ACCELERATION.
      * Ils génèrent des alarmes normales (FACTOR_48) si Code_Aster construit avec MUMPS 5.0.2(consortium)
       et 5.0.1. Noté dans le cartouche des .comm.
   
   VERIFICATION
   ============
     * Astout des 473 cas-tests appelant MUMPS sur athosdev et clap0f0q (avec 5.1.0consortium).
     * Astout OK sur athosdev linké avec MUMPS5.1.0 et MUMPS5.0.2consortium.
   
     5 pbs détectés:
     ==> message parasite dans MUMPS qui gâche les tableaux de cv de STAT/DYNA_NON_LINE.
         pb signalé à l'équipe MUMPS. Il le corrige pour le patch 5.1.1 à venir (avec qqes autres bugs).
         En attendant, versions MUMPS5.1.0consortium (légèrement) surchargée: 8 lignes write(6,*** enlevées !
     
   ==> cas-tests habituellement sensibles au changement de paramètres numériques (solveur linéaire,
         mpi, blas...):
         SSNP118T/U: non-convergence de STAT_NON_LINE. Rajout de quelques itérations d'IR dans MUMPS
                    (via POSTTRAITEMENTS='AUTO' + RESI_RELA=1.D-6 au lieu de -1.0) pour retrouver la cv du
                     Newton.
         SSLS124f/G: NOOK sur 2 valeurs RESU33, valeur de référence OK, valeur de non-régression très variable
                     car conditionnement en 1.D+13. On change la valeur de non-régression et on
                     augmente le TOLE_MACHINE (pour clap0f0q).
   
     * Reste à faire: astout avec Code_Aster linké avec 5.1.0 et 5.0.2(consortium).
   
   VERIFICATIONS SUPPLEMENTAIRES
   =============================
     * Astout OK sur Athosdev avec les valeurs par défaut d'ACCELERATION='AUTO','FR','FR+','LR+'.
     * Nombreux calculs OK sur EOLE avec METIS/SCOTCH_entier_long.
   
   VALIDATION DES GAINS DE PERFORMANCE
   =================================== 
      Temps elapsed des 3 étapes de MUMPS (si MECA_STATIQUE) ou de tout le transitoire (si STAT_NON_LINE)
      sur athosdev/EOLE.
      _____________________FR__________FR+____________LR______________LR+
      PERF01D (mousqueton creux 2Mddls)
      1_noeud :1X4X6________91s________63s____________53s____________53s
      2_noeuds:2X4X6________47s________40s____________43s____________40s
   
      PERF08C (diabolo plein 1Mddls)
      1_noeud :1X4X7________90s________91s____________69s____________74s
      3_noeuds:3X4X7________60s________48s____________45s____________42s
   
      PERF08D (diabolo plein 2Mddls)
      1_noeud :1X4X6_______541s_______506s___________320s___________308s
   Rappel Code_aster 13.3__887s______________________629s
   Rappel Code_aster 13.0_1773s
      4_noeuds:4X4X6_______257s_______184s___________121s___________105s
   Rappel Code_aster 13.3__259s______________________131s
   Rappel Code_aster 13.0__739s
   
     PERF08D_RAFF (diabolo plein 8Mddls)
      3_noeuds:3X4X7________33min______24.5min_______24.5min________15min
     12_noeuds:12X4X7_____21.5min_______9.5min________7.5min_______6.5min
   
     PERF09A_RAFF (pompe_ris, 5Mddls)
      2_noeuds:2X4X6______165s_________160s___________166s_________161s
   Rappel Code_aster 13.3_167s________________________176s
      4_noeuds:4X4X6______125s_________119s___________133s_________128s
   Rappel Code_aster 13.3_125s________________________149s
   
     PERF09A_RAFF2 (pompe_ris, 39Mddls, parmetis)
   6_noeuds:6X2X14______27.5min______20.5min__________31min_______25min
   24_noeuds:24x2x14____13.5min_______8min____________18min_______12min
     
     calcul d'agrégat (1.6Mddls)
      1_noeud:1X4X6_______1099s________956s___________615s________586s
      4_noeuds:4x4x6_______390s________217s___________279s________219s
   
     Trés gros calcul d'agrégat (10Mddls, Issue24317)
   24_noeuds:24X2X14______31min________33.5min________27min_______24min
   
    calcul de THM (excavation, 108 pas de temps, 5.2Mddls non symétrique)
      6 noeuds:6x4x7_____32h57min___22h57min________31h9min___23h50min
   RQ: PETSc+MUMPS________8h20min_____bientôt 7h (avec aster v14, ACCELERATION='AUTO' ds PETSC+LDT_SP ?)
   
   IMPACTS DOCUMENTAIRES
   ======================
   R6.02.03,U4.50.01/U4.55.01,U2.08.03,D4.06.11,V1.04.113,V1.04.115
   
   OPTIONS DE COMPIL
   =================
     * MUMPS5.1.0: plus de -DALLOW_NON_INIT
     * MUMPS5.1.0consortium: plus de -DALLOW_NON_INIT, plus de -DLRMUMPS/Dthr_all, ces
       derniers sont remplacés par -DBLR_MT.
     * MUMPS5.0.2consortium: plus besoin de -DLRMUMPS/Dthr_all, car de toute façon on
       active plus le low-rank de cette version.
   
   NBRE DE JOURS
   ==============
   20 (OB) + 5 (JPL)
   
   IMPACTS SOURCES
   ===============
   M astest/mumps03a.comm
   M astest/mumps03a.export
   M astest/mumps05a.comm
   M bibfor/algeline/crsint.F90
   M bibfor/algeline/crsmsp.F90
   M bibfor/algorith/conint.F90
   M bibfor/algorith/crsolv.F90
   M bibfor/algorith/crsvmu.F90
   M bibfor/include/asterfort/crsolv.h
   M bibfor/mumps/amumph.F90
   M bibfor/mumps/amumpi.F90
   M bibfor/mumps/amumpu.F90
   M bibfor/op/op0014.F90
   M bibfor/op/op0069.F90
   M bibpyt/Messages/factor.py
   M bibpyt/SD/sd_solveur.py
   M catapy/commande/factoriser.capy
   M catapy/commun/c_solveur.capy
   M waftools/mumps.py
   M wafcfg/athosdev.py
   M wafcfg/athosdev_mpi.py
   M wafcfg/calibre9.py
   M wafcfg/calibre9_mpi.py
   M wafcfg/clap0f0q.py
   M wafcfg/eole.py
   M wafcfg/eole_mpi.py
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : R6.02.03,U4.50.01,U4.55.01,U2.08.03,D4.06.11,V1.04.113,V1.04.115,FORMA7.03
VALIDATION
    informatique,fonctionnelle,non-régression,performance
NB_JOURS_TRAV  : 25.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 21614 DU 17/10/2013
AUTEUR : MICHEL-PONNELLE Sylvie
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Validation de la loi BETON_REGLE_PR à compléter
FONCTIONNALITE
   Contenu
   ========
   La loi BETON_REGLE_PR n'était pas suffisamment vérifiée. L'objectif du travail était de compléter cette validation
   
   
   Travail effectué
   ================ 
   Deux nouveaux cas tests ont été intégrés:
   
   a) SSNS114
   clé doc : V6.05.114
   test : SSNS114
   titre : Dégradation d'une plaque en béton armé sous sollicitations variées avec BETON_REGLE_PR
   qui reprend les modélisations du cas test ssns106 qui valide GLRC_DM et DHRC:
   modélisation A : traction – compression - traction pure
   modélisation B : flexion pure alternée ( modèle multi-fibre)
   modélisation C : couplage de traction - compression et flexion
   modélisation D : Distorsion et cisaillement pur dans le plan.
   modélisation E : traction – compression pure, sollicitations élevées 
   modélisation F : flexion pure alternée, sollicitations élevées 
   modélisation G : couplage traction/compression et flexion, sollicitations élevées 
   modélisation H : sollicitations chargement thermique 
   Les résultats sont comparés à la modélisation avec la loi ENDO_ISOT_BETON
   
   b) SSNP172 (Cas test de validation "concrete")
   clé doc : V6.03.172
   test : SSNP172
   titre : Flexion d'une dalle en béton armé avec BETON_REGLE_PR
   Cet exemple est extrait de la documentation R7.01.27: le jeu de paramètre (maillage, matériau) le plus pertinent 
   est identifié. Les déplacements et déformations sont comparés aux valeurs expérimentales.
   
   Des corrections mineures ont par ailleurs été apportées :
   - R7.01.27: Ajout d'un § sur les limitations de la loi et la formule pour avec une pente initiale (eps=0) 
   identique en traction et compression
   - U4.51.11: Suppression de la référence à une variable interne dans §4.3.7.18 
   - Correction de la référence dans le fichier beton_regle_pr.py
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.51.11, R7.01.27,V6.03.172, V6.05.114
VALIDATION
    ssns114, ssnp172

--------------------------------------------------------------------------------
RESTITUTION FICHE 26127 DU 24/02/2017
AUTEUR : ZENTNER Irmela
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    INIT_ALEA dans GENE_ACCE_SEISME
FONCTIONNALITE
   Restitution du point 1) de la fiche 25860
   
   Problème
   ---------
   INIT_ALEA : SEPTEN souhaite pouvoir récupérer la valeur du germe quand 
   INIT_ALEA n'est pas renseigné par l'utilisateur. Chaque génération étant 
   nouvelle, si on cherche a regénérer des accélérogrammes identiques 
   alors qu'INIT ALEA n'a pas été renseigné par l'utilisateur, cela est 
   impossible.Or cela est essentiel pour des questions d'archives
   
   
   
   Solutions
   ---------
   
   1) Le générateur aléatoire de python ne permet pas de récupérer le germe après-
   coup (on peut uniquement récupérer l'état via getstate mais il s'agit d'un 
   ensemble de variables d'état plus compliqué et pas directement utilisable). Je 
   propose donc de générer un germe dans l'opérateur GENE_ACCE_SEISME s'il n'est 
   pas renseigné via INIT_ALEA. En faisant ainsi, on connaît toujours la valeur du 
   germe, qui sera affiché dans le .mess. On peut donc récupérer le germe et 
   relancer le même calcul si besoin.
   
   Cas test:
   ---------
   Il faut modifier les cas test où on lance GENE_ACCE_SEISME en boucle. Afin d'obtenir des résultats 
   répétables (même tirages) il faut
   fournir une liste de germes pour les cas tests zzzz317a et zzzz317c.
   
   Documentation: U4.36.04
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : u4.36.04, v1.01.317
VALIDATION
    zzzz317a, zzzz317c
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25763 DU 15/11/2016
AUTEUR : SELLENET Nicolas
TYPE evolution concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Suppression de LIRE_MAILLAGE RENOMME
FONCTIONNALITE
   Travail effectué :
   ================
   
   - Suppression du code source lié à la fonctionnalité LIRE_MAILLAGE/RENOMME
   - Suppression de l'utilisation de la fonctionnalité dans le test zzzz162a
   - Mise à jour de la doc de LIRE_MAILLAGE(FORMAT='MED') U7.01.21.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U7.01.21
VALIDATION
    aucune

--------------------------------------------------------------------------------
RESTITUTION FICHE 24998 DU 01/04/2016
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    Interdire variables de commande dans CALC_VECT_ELEM
FONCTIONNALITE
   Proposition
   ===========
   
   Suite à la fiche issue24918, on propose de ne pas traiter les variables de commandes dans CALC_VECT_ELEM mais dans la commande CALCUL.
   Initialement, la proposition vient du fait que la programmation venait du temps où les variables de commande étaient introduites par
   AFFE_CHAR_MECA. Toutes les autres variables de commande étaient ignorées.
   
   Résolution
   ==========
   
   1/ On duplique provisoirement me2mme.F90
   - La version actuelle est utilisée dans sschge.F90
   - On crée une nouvelle version (me2mme_2) sans variables de commandes appelée par op0008. 
   
   Cependant comme on ne peut pas supprimer le CHAM_MATER (nécessaire pour les chargements de PESANTEUR), l'utilisateur ne peut pas
   savoir que les variables de commande ne sont plus prises en compte. On ajoute donc un message d'alarme dans m2emme_2 lorsque l'on
   détecte la présence de variable de commande.
   
   2/ On modifie les cas-tests pour calculer la contribution des variables de commande via la commande CALCUL (comme
   attendu dans issue24918)
   - Une contribution qui vient de CALC_VECT_ELEM
   - Une contribution qui vient des variables de commande via la commande CALCUL
   
   Suite aux modifications sur l'opérateur CALCUL apportées par issue26040, la suppression des opérandes DEPL,
   INCR_DEPL SIGM et VARI de CALCUL font planter le test hpla311a qui s'est alors arrêté sur un assert dans dismoi(). On a donc
   complété le test du if à la ligne 153 de bibfor/algorith/calcPrepDataMeca.F90 :
   on a 
   """
   if (iret .gt. 0) then
   """
   Par
   """
   if (iret .gt. 0 .and. vari_prev .ne. ' ') then
   """
   afin que tout ce passe bien si VARI n'est pas renseigné.
   Avec cela, on peut supprimer tout les DEPL, INCR_DEPL SIGM et VARI dans les tests.
   
   3/ Adaptation de MACRO_ELAS_MULT et CALCUL:
   - on rend facultatif EXCIT dans CALCUL afin que MACRO_ELAS_MULT ne plante pas en l'absence de CHAR_MECA_GLOBAL
   - on fait le même travail que dans les cas tests pour le calcul du chargement induit par les VARC (appel à CALCUL)
   - un fois cela fait le test hslv304a ne fonctionnait pas encore, car impossibilité d'accéder au champ PHARMON. Pour que tout
   fonctionne comme avec CALC_VECT_ELEM, il a fallu ajouter MODE_FOURIER à CALCUL et faire tous les branchements nécessaires.
   
   4/ Pour l'option CHAR_THER, le CHAM_MATER n'est pas disponible dans le catalogue => on le supprime des sources (me2mth).
   
   Impact doc :
   U4.61.02 : on supprime "dilatation" de la liste est chargement ayant besoin de CHAM_MATER et on précise bien que les variables de
   commande se sont pas prises en compte.
   U4.51.10 : Opérateur CALCUL => changement du statut de certaines variables (passage en facultatif) + mot-clef MODE_FOURIER
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.61.02, U4.51.10
VALIDATION
    zzzz104b

--------------------------------------------------------------------------------
RESTITUTION FICHE 24993 DU 31/03/2016
AUTEUR : SELLENET Nicolas
TYPE evolution concernant Code_Aster (VERSION 11.4)
TMA : Necs
TITRE
    A203.16 - Supprimer GENE_MATR_ALEA
FONCTIONNALITE
   Travail effectué :
   ================
   
   - Suppression des opérateurs GENE_MATR_ALEA et GENE_VARI_ALEA
   - Suppression du code source concerné
   - Suppression des 4 tests utilisant ces fonctionnalités (sdnl105d, sdns01a et b, shls200a)
   
   Impact doc :
   
   - Suppression de U4.36.06, U4.36.07
   - Suppression de V5.06.001 (sdns01)
   - Suppression de V2.06.200 (shls200)
   Pas de modification de la doc de sdnl105 : Seules les modélisations A et B sont documentées.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U4.36.06, U4.36.07, V5.06.001, V2.06.200
VALIDATION
    aucune

--------------------------------------------------------------------------------
RESTITUTION FICHE 23081 DU 22/09/2014
AUTEUR : JULAN Emricka
TYPE evolution concernant Documentation (VERSION )
TITRE
    RUPT : REX formation rupture 2014-2015
FONCTIONNALITE
   Objectif de la fiche
   --------------------
   Mettre à jour les supports de formation rupture
   
   Travail réalisé
   ---------------
   TPs :
   -Forma05a : dans le texte, dire qu’il faut mettre SYME=OUI sous DEFI_FOND_FISS
   -Forma05a : dans le texte, dire qu’il faut mettre DIRECTION (direction du champ theta)
   sous CALC_G
   -Forma06a : dire que l’apparition de l’alarme dans CALC_CHAMP / CRITERE=SIEQ_XXXX qui dit qu’il faut rajouter EXCIT est normale et
   que ce n’est pas nécessaire de rajouter EXCIT.
   
   Exposé Rupture :
   -T13 : il manque un ‘-‘ devant ‘2 gamma’
   -T18 : transparent qui explique la syntaxe de DEFI_FOND_FISS : parler de SYME=OUI/NON
   -T23 : transparent qui explique la méthode 2 de POST_K1_K2_K3, la pente de la droite est K² (K² max et K² min donc)
   -T18 : vérifier syntaxe DEFI_FOND_FISS
   -T20 : vérifier syntaxe POST_K1_K2_K3
   -T29 : ajout NB_POINT_FOND dans la syntaxe CALC_G et mise à jour de la syntaxe
   -T30 : mise à jour du slide
   -T33 : refaire courbe pour supprimer lissage LAGRANGE_REGU te ajout lissage LAGRANGE avec NB_POINT_FOND (ajout d'une référence pour
   le graphique)
   
   Exposé X-FEM :
   -Mentionner l’existence du Wizard, voire faire une mini démo
   -T25 : rajouter dans l'IMPR_RESU : FORMAT='MED'
   -Vérifier la syntaxe de DEFI_FISS_XFEM
   
   TPs :
   -FORMA08 : Suppression du TP FORMA08 sur les CZM lors de la formation.
   -Refonte des différents TPs de rupture 05, 06 et 07
   -Intégration de slides de conclusion suite à chaque TP dans les polys de cours en annexe
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V3.02.111, V3.02.112, V3.04.156
VALIDATION
    aucun
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25809 DU 25/11/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.2.15, certains tests s'arretent dans LIRE_RESU format MED sur calibre9 et athosdev mpi avec np=3
FONCTIONNALITE
   Problème :
   ----------
   Les tests plexu02b plexu02a plexu10e plexu10d plexu10f ne fonctionnent pas en parallèle avec np = 3.
   
   
   Analyse :
   ---------
   Le fichier de commande produit par CALC_EUROPLEXUS ne peut pas fonctionner en parallèle. De ce fait, à l'issue de l’exécution
   d'Europlexus, aucun des fichiers de sortie ne sont trouvés et les tests s'arrêtent donc.
   
   Il faut envisager des évolutions de CALC_EUROPLEXUS pour que les tests fonctionnent.
   
   Serguei proposait de les faire réaliser en TMA.
   
   La question est : est-ce qu'on le fait dans le cadre de cette fiche ou est-ce qu'on ouvre une autre fiche ?
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    aucune
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25901 DU 05/01/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.2.15, le test plexu07a est NOOK sur athosdev mpi avec np=3
FONCTIONNALITE
   Les exécutables EUROPLEXUS lancés dépendent du nombre de processeurs utilisés : si on lance la version parallèle de code_aster sur
   un processeur, c'est la version séquentielle d'Europlexus qui est lancée par CALC_EUROPLEXUS, si on utilise plus d'un
   processeur,c'est la version parallèle d'Europlexus qui est lancée. Or les versions parallèle et séquentielle d'Europlexus sont
   connues pour donner des résultats légèrement différents, y compris pour la version parallèle si elle est utilisée sur un nombre de
   processeur différent. 
   
   Ce qui explique les différences obtenues sur les valeurs de non-régression.
   
   On note le problème lié à Europlexus et on clôt la fiche sans suite.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25903 DU 05/01/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.2.15, le test plexu05a est NOOK sur athosdev mpi avec np=3
FONCTIONNALITE
   Problème lié à Europlexus, la fiche est mise sans suite (cf issue25901).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 25918 DU 09/01/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.3.1, les tests plexu04a et plexu06a s'arretent anormalement dans EXEC_LOGICIEL sur calibre9 mpi avec np=1 ou np=3
FONCTIONNALITE
   Europlexus n'est pas disponible sur station de travail Calibre9, on ne dispose que d'une version sur cluster (ATHOSDEV, ASTER5). La
   fiche est sans suite.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 25902 DU 05/01/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : en version 13.2.15, le test plexu03c est NOOK sur athosdev mpi avec np=3
FONCTIONNALITE
   Problème lié à Europlexus, la fiche est mise sans suite (cf issue25901), la version parallèle lancée sur 2 ou 3 processeurs donne
   les mêmes résultats, mais ils sont différents de ceux produits par la version séquentielle.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 25595 DU 19/09/2016
AUTEUR : DROUET Guillaume
TYPE anomalie concernant Documentation (VERSION 10.*)
TITRE
    Mise à jour des répertoires des filtres dans la doc v3.02.326
FONCTIONNALITE
   # Problématique #
   #################
   
   L’enchaînement des filtres dans la section Post traitement/Paravis de la doc v3.02.326 n'est pas à jour.
   
   # Solution #
   ############
   
   La documentation V3.02.326 a été mis à jour. La documentation amendée est soumise dans l'application documentaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V3.02.326
VALIDATION
    submit appli doc
NB_JOURS_TRAV  : 0.1

