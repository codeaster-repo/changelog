==================================================================
Version 13.3.20 (révision a73791e65a24) du 2017-05-23 13:59 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26437 DU 09/05/2017
AUTEUR : GUILLOUX Adrien
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    Interpolation log-log dans CALC_FONCTION/LISS_ENVELOP
FONCTIONNALITE
   Problème
   ========
   
   Les fonctions enveloppe_spectres et enveloppe_nappe appliquent des filtres pour passer de lin-lin à log-log (et vice-versa) sur des
   objets de type spectre : spec.filtre(filterLogLog).
   
   Sur un objet de type spectre, cette méthode telle qu'elle est définie ne modifie pas le spectre mais en renvoie un nouveau.
   
   Il en résulte que l'interpolation lorsqu'on fait une enveloppe dans CALC_FONCTION/LISS_ENVELOP est dans le domaine linéaire alors
   qu'elle devrait être dans le domaine log.
   
   Travail effectué
   ================
   Sur les fonctions enveloppe_nappe et enveloppe_spectres, j'ai fait en sorte que les spectres produits écrasent les originaux.
   
   Le cas test zzzz408b utilise l'option VERIFICATION. Seule une valeur pour le NB_FREQ_LISS est fournie dans la macro au lieu de deux,
   le lissage n'est pas bien réalisé dans ce cas ==> on corrige.
   
   Vérification
   ============
   
   Le test zzzz408b s'exécute correctement avec ces modifications.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz100e,zzzz408b
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26439 DU 09/05/2017
AUTEUR : GUILLOUX Adrien
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    Fréquences de spectres élargis avec CALC_FONCTION/LISS_ENVELOP
FONCTIONNALITE
   Problème
   ========
   
   Lors d'un élargissement de spectre(s) avec CALC_FONCTION/LISS_ENVELOP, les différentes étapes effectuées sont :
   1. Création de spectre décalés à droite et à gauche
   2. Enveloppe des trois spectre (celui d'origine, celui décalé à gauche, celui décalé à droite) avec calculs aux fréquences de 
   définition de chacun des trois spectres. On a donc trois fois plus de points
   3. Retour aux points de définition du spectre initial
   
   La troisième étape occasionne une perte de points et dans le cas d'un nombre de points relativement restreint, les pics ne 
   sont plus élargis de manière satisfaisante. Comme rien n'imposait dans le cahier des charges cette étape, on devrait la 
   supprimer.
   
   Travail effectué
   ================
   J'ai supprimé les lignes correspondant à la suppression des points dont la fréquence n'appartient pas à la 
   liste des fréquences des spectres d'entrée et je n'ai gardé que la notion de fmin et fmax des spectres 
   d'entrée.
   
   La modification conduit à une augmentation du nombre de points en sortie de l'élargissement. Or, pour la macro LISS_SPECTRE, pour
   les courbes _verif, on utilise le nombre maximal de fréquences des signaux d'entrée pour le plot _verif (NB_FREQ_LISS) de façon à ne
   faire de lissage. Ce nombre n'est plus correct après l'élargissement donc je l'ai augmenté de façon à ce que le lissage ne se fasse
   pas dans ce cas.
   
   Vérification
   ============
   
   J'ai mis à jour le cas-test zzzz100e pour tenir compte de ces modifications.
   
   Dans zzzz408b, on obtient un graphe avec beaucoup de points (mémoire du cas test 1024Mo mais temps < 300s).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz100e,zzzz408b
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26450 DU 11/05/2017
AUTEUR : GUILLOUX Adrien
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TMA : Necs
TITRE
    Sortie png de vérification de LISS_SPECTRE
FONCTIONNALITE
   La macro LISS_SPECTRE génère 4 sorties par plancher/direction :
   -une sortie au format tableau de la nappe lissée
   -une sortie en PNG (format LISS_ENVELOP) de la nappe lissée
   -une sortie au format tableau de la nappe de vérification
   -une sortie en PNG (format LISS_ENVELOP) de la nappe de vérification
   
   Comme indiqué dans le CCTP, la sortie PNG de vérification devrait contenir la nappe lissée (mot-clé 
   NAPPE_LISSEE) et la nappe de vérification (mot-clé NAPPE) :
   IMPR_FONCTION(FORMAT=’LISS_ENVELOP’, COURBE=_F(NAPPE_LISSEE= Nappe_liss, NAPPE=Nappe_verif), UNITE=tmp)
   
   Cela permet graphiquement de contrôler que le spectre obtenu par lissage respecte la condition 
   d'être enveloppe.
   
   Travail effectué :
   ================
   On remplace la ligne :
   
   IMPR_FONCTION(FORMAT=’LISS_ENVELOP’, COURBE=_F(NAPPE_LISSEE= Nappe_verif,), UNITE=tmp)
   
   par 
   
   IMPR_FONCTION(FORMAT=’LISS_ENVELOP’, COURBE=_F(NAPPE_LISSEE= Nappe_liss, NAPPE=Nappe_verif), UNITE=tmp)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas test

--------------------------------------------------------------------------------
RESTITUTION FICHE 26452 DU 11/05/2017
AUTEUR : GUILLOUX Adrien
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TMA : Necs
TITRE
    Nappes et table du cas-tests zzzz408
FONCTIONNALITE
   Pour les cas-tests zzzz408a et zzz408b, on utilise des spectres au format nappes (unité 18 et 37) ou tables 
   (unité 38 et 39). Dans les deux cas, la deuxième "nappe" de spectres est obtenue par permutation circulaire des 
   amortissements. Cela aboutit à des valeurs de spectres plus élevées pour la valeur d'amortissements la plus 
   forte. On teste donc la macro dans une situation qui ne doit pas se produire en pratique : pour respecter le non 
   croisement des courbes, on aligne toutes les spectres sur les valeurs du plus fort amortissement. Donc cela ne 
   permet pas d'évaluer véritablement la macro.
   
   Travail effectué :
   ================
   
   On reconstruit les fichiers zzzz408a.37 et zzzz408a.39 pour que le deuxième spectre fourni à LISS_SPECTRE 
   soit une copie des
   spectres de zzzz100e.18 et de zzzz408a.38 avec un décalage de fréquence de 5%.
   
   On met à jour des valeurs des TEST_RESU en fonction dans zzzz408a et zzzz408b.
   
   Pas d'impact doc.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    théorique

--------------------------------------------------------------------------------
RESTITUTION FICHE 26415 DU 01/05/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.16-274df259cba8, le cas test ssll117h échoue sur Aster5_mpi et Calibre9_mpi
FONCTIONNALITE
   Ce test ne fonctionne pas parce qu'on ne détecte pas de liens entre les deux mailles du maillage. Les deux mailles sont
   géométriquement confondus.
   
   Dans ce cas, il faut passer en DISTRIBUTION='CENTRALISE' comme l'indique le message d'erreur <F> <PARTITION_4>.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26148 DU 02/03/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE aide utilisation concernant Code_Aster (VERSION 11.4)
TITRE
    LIAISON_SOLIDE : bug eventuel
FONCTIONNALITE
   Objet de la fiche : LIAISON_SOLIDE
   ==================================
   On  a le problème suivant :
   - un modèle DIS_T à 4 nœuds N1,N2,N3, N4 alignés suivant l'axe x;
   - On utilise LIAISON_SOLIDE pour former deux blocs solides (N1-N2) d'une part et (N3-N4) de l'autre;
   - Les deux blocs solides entrent en contact grâce à DIS_CHOC/DYNA_NON_LINE. 
   - Le noeud N1 du premier bloc solide est fixé tandis que le noeud N2 est libre de se déplacer à condition de respecter la liaison solide
   - Le noeud N4 du deuxème bloc est soit bloqué suivant (Y,Z) ou suivant (X,Z); le noeud N3 est libre de se déplacer à condition de
   respecter la liaison solide.
   
   On constate que si on impose une pesanteur suivant X tout se passe bien mais si on impose une pesanteur suivant Y. La condition de
   LIAISON_SOLIDE n'est pas respectée pour le premier bloc solide (voir animation_a). 
   
   Explication :
   ============
   
   D'après la doc R3.03.02 sur les liaisons solides, on se place dans le cas 3DB cad que dans la liste des noeuds rigidifée il n'y a
   aucun noeud qui ne porte un ddl de rotation. Dans ce cas, la documentation précise que dans la liste des noeuds à rigidifier il faut
   au moins 3 noeuds non alignés. Or dans la liste des noeuds du fichier de commande il y en a que 2. 
   
   Dans le cas d'une sollicitation suivant X, il n'y a pas de problème. Dans le cas suivant Y, l'utilisation de LIAISON_SOLIDE devient
   dangeureuse à cause de l'effet rotation.
   
   Une solution aurait été d'utiliser TYPE_CHARGE=SUIV pour imposer explicitement le fait que la distance entre les noeuds est bien
   constante mais ce type de chargment n'est pas autorisé en dynamique non linéaire. 
   
   Il ne reste que deux solutions :
   - La première est de remplacer DIS_T par DIS_TR; dans ce cas on retombe sur un cas où on peut calculer le vecteur rotation et donc
   contraindre la rigidification du bloc N1-N2 : u(N2) = u(N1)+OMEGA ^ N1N2 (animation_b)
   - La deuxième c'est de rajouter un troisième noeud fictif dans la liste des noeuds à rigidifier de sorte qu'ils soient non alignés.
   Dans ce cas le noeud N2 reste bien sur un cercle centré sur N1 (animation_c) 
   
   Restitution :
   =============
   
   - Aucune action dans le source fortran
   - Se servir de ce cas-test pour enrichir la doc U4.44.01 (la partie liaison solide)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.44.01
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23380 DU 19/12/2014
AUTEUR : DE SOZA Thomas
TYPE anomalie concernant Documentation (VERSION 10.*)
TITRE
    Doc. U4.81.04 opérateur CALC_CHAMP : définition de l'excitation sous EXCIT
FONCTIONNALITE
   Cette fiche documentaire avait pour objectif de mieux documenter les précautions d'usage de CALC_CHAMP avec des structures de
   données dynamiques, c'est-à-dire issues de l'opérateur DYNA_VIBRA.
   En attendant le traitement de issue19049, l'utilisateur doit en effet communiquer à l'identique à CALC_CHAMP les mots-clés utilisés
   pour le calcul (MODELE, CHAM_MATER, CARA_ELEM, EXCIT).
   
   Un ajout documentaire a été fait dans la documentation de CALC_CHAMP.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : u4.81.04
VALIDATION
    néant
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26456 DU 12/05/2017
AUTEUR : DEGEILH Robin
TYPE aide utilisation concernant Code_Aster (VERSION 11.8)
TITRE
    [RUPT] Différence POST_RCCM entre v11 et v12
FONCTIONNALITE
   **************
     CONTEXTE 
   **************
   
   Il s'agit d'une demande provenant de l'UTO. Le calcul concerne :
   "[...] Une justification a été faite en 2015. Elle consistait à déterminer l'épaisseur minimale (uniforme) [...], avec la version 11
   d'Aster, à partir d'un calcul EF sur modèle 2D axisymétrique du tampon (calcul des contraintes linéarisées Pm et Pm+Pb). Elle a été
   mise à jour en 2017. A cette
   occasion, on s'est rendu compte qu'on obtenait des résultats très différents avec la version v12 et suivantes. [...] on aimerait
   connaître l'origine des différences."
    
   Le demande de l'UTO est :
   "D'où viennent les différences de résultats entre la version v11 et les suivantes : problème d'utilisation de notre part, ou
   évolution du code (apparition d'une erreur, ou au contraire correction d'une erreur) ?
   (maillage et fichier de commande en PJ)"
   
   *********************
     ETUDE DU PROBLEME 
   *********************
   
   Le calcul réalisé est relativement simple : élastique, isotherme, axisymétrique, maillage cartésien rectangulaire.
   Un MECA_STATIQUE est réalisé, puis un MACR_LIGN_COUPE pour extraire le tenseur des contraintes le long de deux lignes (une seule
   pose problème), puis un POST_RCCM.
   
   L'analyse des résultats de MACR_LIGN_COUPE entre les versions v11 et v12, montre une grosse différence entre les deux profils de
   contrainte en un point (SIGXY=+735.25MPa en v12 et SIGXY=-13.MPa en v11). 
   Cet écart peut expliquer les différences de résultats de POST_RCCM entre les deux versions. A noter que dans POST_RCCM, le mot clé
   TYPE_RESU_MECA='EVOLUTION' et que (selon Sarah P.) cette branche n'a pas évolué récemment. Seul la différence de profil de
   contrainte en entrée semble donc incriminée.
   
   En regardant le champ utilisé pour MACR_LIGN_COUPE (SIEF_ELNO) dans Paravis, on constate une très forte discontinuité de contrainte
   (sur SIGXY) entre deux éléments. Or, le MACR_LIGNE_COUPE passe tout pile entre ces deux éléments. Or, suite à issue20925,
   MACR_LIGN_COUPE a potentiellement changé de comportement en ne lissant plus les champs ELNO depuis la v12 (évolution consécutive à
   une demande dans issue20776).
   
   L'origine du problème est donc la discontinuité du champ de contrainte dans cette zone. Cette discontinuité provient d'une mauvaise
   application des conditions limites, notamment l'appui selon DY bloqué en un seul nœud (N_JOINT), donc un effort potentiellement
   infini (8.E+5N selon FORC_NODA dans ce cas). 
      
   *********************
     SOLUTION PROPOSEE 
   *********************
   
   La solution la plus propre, serait de revoir l'application du blocage DY au nœud N_JOINT en raffinant localement et en étalant le
   blocage sur plusieurs nœuds.
   
   La solution pragmatique mais pas propre serait de réaliser le MACR_LIGN_COUPE sur le champ SIEF_NOEU. Cela permet de lisser le champ
   et de ne plus être dépendant de la version d'aster. Cela cache mais ne règle pas le problème de condition limite.
   
   
   *****************
       IMPACT
   *****************
   
   Pas d'impact sur le code ou la doc.
   
   Il est difficile de dire lequel des résultats entre la v11 et v12 est conservatif. Pour info, j'ai réalisé le POST_RCCM sur une
   ligne décalée de +1e-3 et -1e-3 selon X et je trouve des résultats totalement différents sur une même version d'aster. De manière
   générale, il faut s'assurer de la non dépendance à la ligne de coupe dans POST_RCCM.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    aucune
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26471 DU 16/05/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 12.8)
TITRE
    plantage stable-updates_mpi
FONCTIONNALITE
   En lançant depuis ASTK une mise ne donnée très simple en parallèle MPI:
   - stable-updates_mpi -> NOOK
   - stable_mpi -> OK
   - unstable_mpi -> OK
   
   ==> dans le fichier de configuration de la version stable-updates_mpi pour Aster (aster5_mpi.py en branche v12), on avait par erreur
   laissé un "import athosdev" au lieu de "import aster5".
   
   En corrigeant et en recompilant, tout rentre dans l'ordre.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26350 DU 04/04/2017
AUTEUR : BOITEAU Olivier
TYPE aide utilisation concernant Code_Aster (VERSION 13.4)
TITRE
    analyse modale NORM_MODE et IMPRESSION : plantage
FONCTIONNALITE
   Bonjour,
   en espérant que cela répondra à votre attente, voici mon analyse du problème soumis.
   Bon courage, cordialement
   
   PROBLEME
   ========
      Calcul modal frontière sur l'îlot nucléaire de Cruas. Calcul effectué par une équipe d'AREVA pour le compte de DIPDE Marseille.
      Suivant le paramètrage, le calcul modal, via l'opérateur CALC_MODES, plante ou aboutit au résultat escompté.
      Calcul effectué avec Code_Aster v12 + SORENSEN + MUMPS 4.10.0.
   
   ANALYSE
   =======
     Le calcul modal effectué ici est gigantesque. A ma connaissance, c'est le plus gros jamais tenté avec Code_Aster!
     On cherche les 3481 modes propres dans la bande [0Hz,18Hz] d'un modèle EF de taille N=1525014 ddls. Heureusement celui-ci
     peut s'effectuer en parallèle en découpant la bande de calcul en plusieurs sous-bandes indépendantes. Chacune comportant au
     maximum 200 modes. Cela permet:
          - de rendre le calcul réalisable avec les solveurs modaux usuellement utilisés en mécanique des structures,
          - d'améliorer la qualité des modes (résidu modal en 10-5 ou en 10-6),
          - d'accélérer grandement le calcul. 
     Cependant, ce scénario de calcul ne permet pas encore de distribuer les besoins mémoire entre tous les processeurs. Pour l'instant
     ils doivent tous allouer le même objet global permettant de stocker tous les vecteurs propres. Cette redondance d'informations,
     habituellement négligeable ou supportable sur les problèmes de taille moyenne, devient ici très prégnante.
     Il faut réserver des dizaines voire une centaine de Go par processus MPI pour faire aboutir le calcul. Sinon il plante sur des erreurs hératiques dues au fait que
     certains processeurs ont planté (par faute d'assez de mémoire) et qu'on manipule des structures de données alors incomplètes.
   
     Idéalement, pour résoudre plus facilement ce pb, il faudrait:
     - soit réduire la taille du modèle,
     - soit effectuer le calcul modal en plus petits paquets et en mode POURSUITE (par exemple: 800
       modes par run sur 12 processus MPI), puis les réunir via un DEFI_BASE_MODALE.
   
     Si aucun des scénarios précédents n'est envisageable, on peut faire le plus efficacemment possible le calcul en:
       1/ Jaugeant au préalable les sous-bandes via un INFO_MODE (Il me semble que cela a été fait ici, c'est très bien) et en
          construisant des sous-bandes réunissant entre 50 et 200 modes maxi (on conseille généralement plutôt moins, mais vu
          l'étendu du spectre couvert et les ressources machines disponibles, il faut faire ici de gros paquets !).
   
       2/ Changer le solveur linéaire par défaut de CALC_MODES de SOLVEUR=_F(METHODE='MULT_FRONT') à SOLVEUR=_F(METHODE='MUMPS'),
          On peut gagner un facteur x2 en temps voire plus.
   
       3/ Réserver au moins 140Go par processus MPI (case Total memory d'Astk). Car chaque processus parallèle a besoin
          de stocker la grosse matrice regroupant tous les vecteurs propres. Pour l'instant cette structure n'est pas distribuée en
          parallèle. Ici chaque processus doit donc préallouer (rien que pour stocker les données):
                           1525014 ddls x 3481 modes x 8octects x 2 (taille espace de projection paramétré par DIM_COEF_SOUS_ESPACE)
                           =81 Go.
      
       4/ Dans ce type de calcul, on peut exploiter en v12 juste 2 niveaux de parallélisme (contre 3 en v13):
          - celui de sous-bandes de calcul, parallélisme principal par défaut,
          - celui des systèmes linéaires au sein de chaque sous-bande (si MUMPS).
          Compte-tenu des besoins mémoire ici, on ne peut pas se permettre d'activer ce deuxième niveau. On doit donc garder:
         nbre de sous_bandes (cf. CALC_FREQ=_F(FREQ))  = nbre de processus MPI (cf. paramètre mpi_nbcpu d'Astk).
   
   RESULTATS
   =========
      En réservant suffisamment de mémoire on peut effectuer en une seule ou en plusieurs passes, le calcul.
   
      Calcul partiel sur la machine Aster5 (3 noeuds à 256Go)
      ====================================
         En allouant 12 processus MPI pour les 12 premières sous_bandes fréquentielles
                         FREQ=(0.0, 1.5, 3.0, 3.5, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0),
         Le calcul modal aboutit en une dizaine de minutes après avoir consommé jusqu'à 54Go par processus MPI (dont 39 rien que pour les structures
         de données Aster)
                # Mémoire (Mo) : 53885.84 / 13642.29 / 38721.77 / 38721.75 (VmPeak / VmSize / Optimum / Minimum) 
         et les modes sont de bonne qualité:
   
   numéro    fréquence (HZ)     norme d'erreur
       1       2.91662E-01        3.44475E-06
       2       3.07495E-01        2.44059E-06
       3       3.32933E-01        6.28952E-07
       4       3.87070E-01        1.37030E-06
       ...... 
    1107       7.99511E+00        1.19468E-07
    1108       7.99529E+00        7.43187E-08
   
     Norme d'erreur moyenne   :  3.01095E-06
   
      Calcul complet sur la machine EOLE (40 noeuds à 256Go)
      ==================================== 
      Idem sur 40 processus MPI avec le découpage en sous-bandes fréquentielles suivant 
   FREQ=(0.0, 3.0, 4.0, 4.5, 5.0, 5.5, 6.0, 6.5, 7.0, 7.5, 8.0, 8.5, 9.0, 9.25, 9.5, 9.75, 10.0, 10.3, 10.6, 11.0, 11.3, 11.6, 12.0, 12.3, 12.6, 13.0, 13.3, 13.6, 14.0, 14.3, 14.6, 
   15.0, 15.3, 15.6, 16.0, 16.3, 16.6, 17.0, 17.3, 17.6, 18.0),
         Le calcul modal aboutit en moins d'une demi-heure après avoir consommé jusqu'à 137Go par processus MPI (dont 122 rien que pour les structures
         de données Aster) 
                # MÃ©moire (Mo) : 136985.72 / 42845.75 / 136247.08 / 121572.30 (VmPeak / VmSize / Optimum / Minimum)
         et les modes sont de bonne qualité:
    numÃ©ro    frÃ©quence (HZ)     norme d'erreur
       1       2.91662E-01        2.05923E-05
       2       3.07495E-01        9.61889E-06
       ....
    3480       1.79942E+01        4.18083E-08
    3481       1.79997E+01        1.62665E-07
   
     Norme d'erreur moyenne   :  2.05849E-06
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    informatique, fonctionnelle
NB_JOURS_TRAV  : 4.0

