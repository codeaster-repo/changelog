==================================================================
Version 13.3.23 (révision ea46e0172f95) du 2017-06-15 09:20 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26488 DU 22/05/2017
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Mise à jour du texte de la licence GPL dans les sources
FONCTIONNALITE
   Mise à jour du texte de la licence pour corriger l'adresse en nettoyage pour clarifier le passage en GPL version 3.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    *
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26525 DU 01/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version : version 13.3.21-88f6cd1ead31 le cas test zzzz216b échoue sur Eole
FONCTIONNALITE
   Ce test utilise Gmsh pour créer un maillage.
   
   Sur eole, on crée le répertoire partagé /projets/simumeca/outils.
   
   Gmsh ne peut tourner que sur les frontales car les bibliothèques nécessaires ne sont pas disponibles sur les noeuds de calcul.
   
   On identifie ce type de test avec le tag 'verif_interact' dan testlist.
   
   Développement dans devtools pour que run_testcases lance séparément les tests qui ont besoin des bibliothèques interactives. La
   liste des tests à exécuter est coupée en deux :
   - ceux qui n'ont pas 'verif_interact' sont lancés en batch,
   - ceux contenant 'verif_interact' sont lancés en interactif.
   
   Ceci corrige les tests zzzz216, zzzz120a et zzzz120b.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    run_testcases
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26569 DU 09/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version  13.3.21-5e1b0617213e le test mfron03j est NOOK sur Athosdev, EOLE, Calibre 9 et clap0f0q
FONCTIONNALITE
   On ajuste les valeurs de non régression dans le test mfron03j suite au changement de version de MFRONT sur Athosdev.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    mfron03j
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26582 DU 11/06/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.3.22 le cas test ttnl02a échoue sur Aster5_mpi
FONCTIONNALITE
   Problème
   ========
   
   En version 13.3.22 le cas test ttnl02a échoue sur Aster5_mpi
   
   Solution
   ========
   
   Conséquence de  issue26377.
   On corrige le AFFE_CHAR_THER litigieux
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ttnl02a
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 26484 DU 22/05/2017
AUTEUR : ABBAS Mickael
TYPE evolution concernant Code_Aster (VERSION 11.4)
TITRE
    Résorption deprecated
FONCTIONNALITE
   Proposition
   ===========
   
   Supprimer les warning suivants:
   - IMPR_RESU/CASTEM
   - MODELISATON='POU_C_T'
   Ajouter les warnings suivants:
   - MODELISATION='*INCO_UPGB' (issue24022) - V14
   - LIRE_RESU/ENSIGHT (issue25817) - V14
   - Options VAEX_EL* (issue25924) - V14
   
   NB: on garde deprecated_material en mode "anonymisé"
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26462 DU 14/05/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.18-bf89094912f0, le cas test ssls09d échoue sur Aster5_mpi, Athosdev_mpi, Calibre9_mpi et Eole_mpi
FONCTIONNALITE
   Le test ssls09d est la réplique de ssls09c sur deux processeurs.
   
   Suite à issue26034, CALC_ERREUR est bloqué en parallèle.
   On supprime la modélisation "d".
   
   Pas d'impact doc nécessaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    suppression
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26520 DU 01/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version 13.3.21-88f6cd1ead31, les cas tests sdll125a et sdll125b sont nook sur Eole
FONCTIONNALITE
   Problème
   --------
   
   Les tests sdll125a et sdll125b sont légèrement NOOK sur Eole.
   
   
   Correction
   ----------
   
   On actualise les valeurs calculées avec celles obtenues aujourd'hui sur Athosdev.
   
   
   Validation
   ----------
   
   On vérifie que les tests sont OK sur Aster5, Eole, Calibre9 et clap0f0q.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdll125a, sdll125b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26597 DU 13/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Problème du choix de mpirun sur Athosdev/Eole
FONCTIONNALITE
   Sur ces serveurs, il y a plusieurs script mpirun (OpenMPI, Intel MPI...).
   
   Comme on utilise python de /usr/bin, la configuration de code_aster fait que /usr/bin est inséré dans la variable d'environnement
   $PATH au début et après avoir chargé l'environnement Intel MPI (via OPT_ENV).
   
   Les environnements optionnels sont chargés en premier.
   
   On ajoute dans la possibilité d'ajouter un environnement optionnel de clôture: OPT_ENV_FOOTER.
   
   Pour ces clusters, on peut donc "corriger" l'environnement pour remettre Intel MPI en premier.
   
   On augmente également ADDMEM.
   
   Impact : wafcfg/{athosdev,eole}_mpi.py
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test mpi sur eole
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26579 DU 11/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 12.7.10-f3be860c2484 et version 13.3.21-88f6cd1ead31 le cas test sdnx100d échouent sur Eole_mpi
FONCTIONNALITE
   Problème
   --------
   
   Le test sdnx100d échoue sur Eole, en parallèle MPI sur 2 processeurs.
   
   
   Correction
   ----------
   
   Le processeur #0 calcule les fréquences impaires.
   Le processeur #1 calcule les fréquences paires. Lorsque ce processeur essaie de copier en scp ses fichiers dans le répertoire du
   processeur #0, la copie échoue car le répertoire n'est trouvé.
   
   C'est aussi lié au fait qu'en batch sur cette machine, le chemin du répertoire temporaire courant est de la forme
   /tmp/slurm_xxxxxx/tmp/batch_xxxxx.
   
   Comme pour MACR_RECAL, on utilise un répertoire temporaire créé dans 'shared_tmp'.
   
   Pour cela, on crée un nouvel utilitaire `get_shared_tmpdir(prefix, default_dir)` que l'on utilise dans CALC_MISS pour corriger ce
   test, ainsi que dans MACR_RECAL et CALC_EUROPLEXUS.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnx100d
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26581 DU 11/06/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.3.22, les cas-tests perf009defgh, perf010abcd, rccm11a, sdnl131ab, sdnv105b échouent sur Athosdev_mpi et Athosdev_valid
FONCTIONNALITE
   Suite issue26249 (suppression du mot cle DEBUT/CODE/VISU_EFICAS), la syntaxe de certains fichiers de commandes était incorrecte. On
   supprime la "," en trop.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    perf009defgh
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26388 DU 14/04/2017
AUTEUR : DE SOZA Thomas
TYPE evolution concernant Documentation (VERSION 11.4)
TITRE
    Stanley : impression des commandes dans un .comm
FONCTIONNALITE
   '''
   Lors de l'utilisation de STANLEY(); en fin de fichier, je calcule plusieurs champs et je veux les imprimer dans un .med afin de les
   conserver.
   
   Est il possible de récupérer les lignes de commandes  CALC_CHAMP et IMPR_RESU sous jaçantes à l'utilisation de Stanley? Je ne trouve
   pas cette fonction.
   '''
   
   Réponse
   =======
   
   La fonctionnalité IMPR_MACRO='OUI' dans DEBUT permet de déclencher l'affichage dans le fichier de messages des commandes générées
   par STANLEY.
   
   On enrichit la documentation pour faire la publicité de ce point.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : u4.81.31
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26518 DU 01/06/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version version 13.3.21-88f6cd1ead31, les cas tests zzzz120a et zzzz120b échouent sur Eole
FONCTIONNALITE
   résolue par issue26525.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz120a/b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26131 DU 27/02/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    Temps CPU cas tests SHB
FONCTIONNALITE
   Objet de la fiche :
   ==================
   Suite au refactoring des modèles de SHB issue25195, les temps CPU ont beaucoup augmenté notamment en non-linéaire avec grands
   déplacements grandes rotations.
   
   Analyse du source :
   ==================
   Des modifications ont été apportées au calcul des matrices de rigidité te0477
   (https://bitbucket.org/code_aster/codeaster-src/issues/42/shb-refactor-te0477-full_meca) pour les rendre plus robustes et plus
   précises. 
   
   Ces modifications sont : 
   - correction de la matrice B des déformations.
   - correction des termes de stabilisation 
   - corrections des orientations des points de gauss qui étaient faux dans la version précédents. 
   - On utilise des routines communes avec le 3D : nmgrt3 par exemple pour GROT_GDEP. Avant le modèle SHB avait ses routines
   spécifiques (en doublon) quasiment avec le 3D. Le choix de PJ est de mutualiser l'utilisation de certaines routines afin de gagner
   en robustesse-fiabilité. Ce dernier explique notamment les differences de temps CPU. 
   
   On gagne en qualité-robustesse mais on perd en temps CPU. 
   
   Restitution :
   =============
   
   Sans suite. La thèse solide-coque est censé remplacé les modèles SHB à long termes.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

