==================================================================
Version 13.3.15 (révision 356245b3bdc1) du 2017-04-20 17:00 +0200
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 26318 DU 24/03/2017
AUTEUR : BOITEAU Olivier
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    A301.xx - Montée de version MUMPS 5.1.1
FONCTIONNALITE
   OBJET
   =====
     Upgrade de MUMPS5.1.0(consortium) à MUMPS5.1.1(consortium). Version de MUMPS corrigeant des dysfonctionnements
     potentiels. Fiche complémentaire de la fiche Issue26139.
   
   ANALYSE
   =======
   Avec la fiche Issue26139, Code_Aster bénéficie de la v5.1.0consortium) de MUMPS. Malheureusement celle-ci 
   comporte 
   quelques bugs potentiellement importants. Heureusement l'équipe MUMPS les a identifiés et les a rapidement 
   corrigés. 
   Il s'agit:
      - des bugs identifiés depuis plusieurs versions,
      - de fuites mémoires,
      - de potentiels résultats faux,
      - des pbs numériques sur des cas pathologiques.
   
   Cela pourrait concerner la version EDF d'Aster plutôt en BLR, BLR+, multithreading et Out_Of_Core.
   On ne peut pas faire l'impasse dessus (cela aurait été par exemple les RHS creux ou le calcul des 
   termes de l'inverse on aurait pu).
   L'équipe MUMPS a donc rapidement produit une version 5.1.1 (et son pendant consortium) qui corrige 
   uniquement ces bugs et quelques légers dysfonctionnements (tels les write intempestifs qu'on avait 
   fait remonter).
   Les v5.1.0(consortium) ne sont plus disponibles.
   Ils pensent avoir fait le tour des pbs (ils n'ont plus de bug non résolus) et ne prévoit pas de 5.1.2. 
   L'ensemble des pbs corrigés est détaillé et joint à la fiche. 
   
   IMPACT FONCTIONNEL
   ==================
      On substitue à la 5.1.0 la 5.1.1 en terme de compatibilité et de fonctionnement des mots-clés.
   
   VERIFICATION
   ============
     Pour Athosdev: MUMPS installé par JPL et
     MUMPS5.1.1consortium=compilé sous mon $HOME avec mêmes options de compil que pour l'install std de Code_Aster.
     Pour Clap0f0q: uniquement version perso.
   
     Astouts OK des 473 cas-tests appelant MUMPS sur athosdev et clap0f0q.
   
     1 pb détecté sur clap0f0q:
     ==> WTNV142A: cas-test THM NOOK sur un TNR déjà objet de 3 fiches. Vu avec MA. On ne fait rien.
   
   IMPACTS SUR LES PERFORMANCES
   ============================
     Peu d'impact noté dans la passage 5.1.0/5.1.1.
     Machine aster5- 4 noeuds_256Go: 16MPIx6threads, temps analyse+factorisation, compil en -0 + MKL.
     PERF008D (2M dof)
     version________________FR_______LR________FR+________LR+______Vmpeak
     5.1.1_________________231s______104s______182s_______86s______52.8Go
     5.1.0_________________229s______104s______169s_______103s_____52.8Go
     5.0.2_(aster_v13.3)___241s______109s______non_dispo___________54.9Go
     5.0.1_(aster_v13.2)___232s______138s______non_dispo___________54.9Go
     4.10.0(aster_v12.4)___808s________________non_dispo___________54.5Go
   
     PERF009D_RAFF (5.5M dof)
     version________________FR_______LR________FR+________LR+______Vmpeak
     5.1.1_________________128s______137s______120s_______134s_____30.6Go
     5.1.0_________________130s______137s______120s_______134s_____30.6Go
     5.0.2_(aster_v13.3)___130s______155s______non_dispo___________27.7Go
     5.0.1_(aster_v13.2)___144s______149s______non_dispo___________27.5Go
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U4.50.01,U2.08.03
VALIDATION
    informatique,non-régression
NB_JOURS_TRAV  : 4.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26325 DU 27/03/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.11-108e419153d0, le cas test ssnv514a est NOOK sur AthosDevValid
FONCTIONNALITE
   Objet de la fiche :
   ==================
   
   ssnv514a : un cas-test d'emboutissage.
   Les valeurs calculées à partir de la version 13.3.9 par aster ont changé (écart de 0.01% à 1% par rapport aux valeurs de réferences). 
   
   Analyse :
   ========
   Le NOOK s'explique par le fait que désormais la méthode par défaut dans DEFI_CONTACT est ADAPTATION='CYCLAGE'. En rebasculant, le
   test dans son état initial :  ADAPTATION='NON', on retrouve les résultats initiaux. 
   
   Ce test utilise une réactualisation géométrique contrôlée NB_ITER_GEOM=2. En supprimant, la réactualisation contrôlée, la méthode
   avec et sans cyclage donnent les mêmes résultats à la troisième virgule près. On retrouve donc le résultat initialement annoncé :
   pas ou peu de différences du résultat entre la méthode ADAPTATION=CYCLAGE et ADAPTATION=NON. 
   
   Avec ADAPTATION=CYCLAGE et réactualisation géométrique controlée, on gagne 200 itérations par rapport au cas sans adaptation. Par
   contre, on observe des valeurs légèrement différentes.
   
   Action :
   =======
   - Correction de ssnv514a (ADAPTATION='NON').
   - Action doc : on recommande dans la doc U2.04.04 d'utiliser la méthode de CYCLAGE avec une réactualisation géométrique automatique.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U2.04.04
VALIDATION
    ssnv514
NB_JOURS_TRAV  : 1.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26258 DU 13/03/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.9, Temps CPU des cas tests ssnp167h, ssnp167b et sdnv104c a augmenté
FONCTIONNALITE
   L'explication du temps cpu vient d'un blindage apporté dans mmalgo.F90 --> convergence sur les statuts de contact.
   
   Analyse de l'algorithme de convergence sur les statuts :
   =======================================================
   Pour la compréhension :
   IPTC   = point courant de contact
   NPTC   = nombre total de points de contact
   MMCVCA = convergence sur le statut de contact du point courant
   CTCSCA = nombre de points de contact n'ayant pas convergé
   
   
   AVANT BLINDAGE  :
   """
   !ON FAIT BCLE SUR LES POINTS DE CONTACT
   DO IPTC=1, NPTC
   !INITIALISATION DE MMCVCA DANS LA ROUTINE MMMBCA 
       mmcvca=.true.
   
   !
   !APPEL A MMALGO : Convergence ?
   !
       if (indi_cont_init .ne. indi_cont_curr) then
           ctcsta = ctcsta+1
           mmcvca = .false.
       endif
   ENDDO
   """
   
   L'inconvénient de cet algorithme est qu'il suffit que le dernier point de contact ait convergé pour continuer le calcul. On ne
   s'assure pas qu'avant il pouvait avoir des points de contact qui ne converge pas. L'influence sur les résultats est cependant
   limité. En effet, la boucle de NEWTON rattrape la boucle de contact. 
   
   BLINDAGE  :
   """
   !ON FAIT BCLE SUR LES POINTS DE CONTACT
   DO IPTC=1, NPTC
   !INITIALISATION DE MMCVCA DANS LA ROUTINE MMMBCA 
       mmcvca=.true.
   
   !
   !APPEL A MMALGO : Convergence ?
   !
       if (indi_cont_init .ne. indi_cont_curr) then
           ctcsta = ctcsta+1
           mmcvca = .false. .and. (ctcsta .eq. 0) 
       endif
   ENDDO
   """
   
   Avec ce blindage, on voit bien que sur certains tests, on doit faire une itération supplémentaire pour s'assurer que tous les points
   de contact ont bien convergé. D'où l'augmentation du temps CPU. 
   
   Action :
   =======
   Rien
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26326 DU 28/03/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE aide utilisation concernant Code_Aster (VERSION 11.4)
TITRE
    echec convergence : contact + corps mal bloqué
FONCTIONNALITE
   Objet de la demande : 
   ====================
   
   Etude d’une galerie enterrée dans un sol de type sable. Au-dessus, des ouvrages béton remblayés par de la terre sont présents.
   
   L'étude ne converge pas :
   - Loi de comportement : MOHR_COULOM
   - contact défini avec DEFI_CONTACT
   
   Analyse du problème de convergence :
   ==================================
   
   La galerie n'est pas bloquée et ne tient que par le contact. Ce n'est pas une bonne façon de proceder. Il y a deux solutions pour y
   remédier : 
   
   - Choisir quelques noeuds de la surface interne de la galerie  puis imposer des CLs DX,DY=0.
   
   - Attacher un ressort de faible raideur à un noeud de la surface interne de la galerie.
   
   Les deux solutions fonctionnent très bien sur le problème posé avec un matériau élastique. 
   
   Solution :
   =========
   
   Fichiers solutions avec cara_elem ou blocage joints. J'ai arrêté le calcul après le premier pas de temps.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26130 DU 27/02/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.3.7-573555676e5b, les cas tests ssls124b et ssls124e échouent sur clap0f0q
FONCTIONNALITE
   Objet de la fiche : 
   ==================
   
   Les cas-tests ssls124* sont soient sensibles à la machine soient instables. 
   
   La fiche issue26275 trace et résout le problème. 
   
   Action :
   =======
   Rien
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25779 DU 21/11/2016
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    calcul modal coquille spherique DKT
FONCTIONNALITE
   Problème avec les DKT :
   ======================
   
   L'étude concerne les modes propres d'une coquille sphérique très minces (épaisseur = 1mm, diamètre = 1m soit un rapport
   ep/diametre=1/1000) avec les DKT. 
   
   Les 6 premiers modes sont corrects. Au delà des 6 premiers modes, il apparaît beaucoup de modes parasites.
   
   Quelques tests :
   ===============
   - Comparaison à une solution analytique : sdls07 
   On peut exhiber une solution analytique à ce problème(voir document issue25779.odt) inspirée de sdls07. Les modèles de DKT ne
   donnent pas de bon résultats. Avec ce rapport epaisseur/rayon, on se rapproche plus d'un comportement de membrane. 
    
   - Les résultats sont très dépendants des maillages. Par exemple, le modèle COQUE_3D donne de meilleurs résultats sur le maillage
   libre mais pas sur le maillage réglé. 
   
   Les résultats sont bruités pour tous les types de maillages pour les modèles de DKT. Dans l'ensemble les modèles COQUE_3D, SHB, 3D
   donnent de meilleurs résultats que le modèle DKT. 
   
   En symétrisant le problème càd qu'on prend un quart de la coquille sphérique (comme dans sdls07), l'analyse modale se comporte peu
   mieux. 
   
   - Influence du rapport epaisseur/rayon : verrouillage en cisaillement. 
   En deça  d'un rapport 1/500, il apparaît les modes parasite. Au delà, je ne constate pas de mode parasite même lorsque la déformée
   est amplifiée par 100. 
   
   Batoz, Vol 2 :  page 293 donne effectivement un indicateur de verrouillage en cisaillement.
   
   Ind = Nbre_equations / rang [K_c]; K_c liée au cisaillement.
   
   Cet indicateur n'est pas implanté dans aster et demanderait une évolution. 
   
   Ce qu'on sait (en s'appuyant sur Batoz), c'est que les DKT verrouillent en cisaillement pour des rapport
   epaisseur/Longueur_caratéristique << 1. Ca semble être le cas dans cette fiche.
   
   Action :
   =======
   
   Conseils d'utilisation dans U2.02.01 dans l'introduction :
   
   """
   Les éléments de coques et de plaques sont particulièrement utilisés pour modéliser des structures minces où les rapports entre les
   dimensions (épaisseur/longueur caractéristique) sont très inférieurs à 1/10 (coques minces) ou de l’ordre de 1/10
   (coques épaisses). 
   """
   
   Je rajoute,
   """
   Pour les coques très minces, on conseille de ne pas dépasser un rapport 1/500.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : U2.02.01
VALIDATION
    sans objet
NB_JOURS_TRAV  : 3.5

