==================================================================
Version 13.2.22 (révision 3ae09e3c74de) du 2016-12-09 12:03 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 25836 DU 09/12/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.2.20-7aa239b800e1, le cas test sdnd107e est NOOK sur clap0f0q
FONCTIONNALITE
   En version 13.2.20-7aa239b800e1, le cas test sdnd107e est NOOK sur clap0f0q.
   
   Je corrige les valeurs de non-régression et je teste sur clap0f0q, athosdev et calibre9 (la mienne).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnd107e
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24103 DU 05/08/2015
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    ELIM_LAGR  : Segmentation violation
FONCTIONNALITE
   Problème 
   ========
   ELIM_LAGR échoue (erreur mémoire après élimination d'une partie des contraintes) sur une
   étude de génie civil. 
   Analyse
   =======
   L'erreur se déclenche suite à une mauvaise estimation de la structure creuse de la matrice destinée à stocker la matrice des
   contraintes. Cette structure est sur-dimensionnée. Ce n'est pas grave pour un petit calcul. Ici (158000 contraintes) c'est
   rédhibitoire.  
   Correction
   ===========
   On corrige l'estimation en sous évaluant et en autorisant l'insertion ultérieure de termes non prévus dans la matrice.
   Cette correction fonctionne. On passe le point précédent.
   En revanche le code résultant est très lent : pour l'étude attachée on élimine 3486 contraintes en 24h de calculs (sur environ 150000)
   Il se produit durant cette élimination 1433 mallocs de taille comprise entre 242 et 936 : ces opérations sont probablement
   responsables (au moins en partie) de la lenteur du code. 
   Pour la suite
   =============
   On peut utiliser la librairie MUMPS (issue25550) : cette étude va jusqu'au bout  en appelant la fonctionnalité de calcul du noyau de
   MUMPS, même si le temps mis pour calculer la base est long. 
   On peut reprendre la méthode native d'aster et la réécrire en améliorant la prévision du remplissage.
   Ou encore reprendre le problème général de l'élimination des Lagrange avec un autre type de méthode (Krylov projeté)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas-tests elim_lagr et etude jointe
NB_JOURS_TRAV  : 7.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25767 DU 17/11/2016
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    B105.16 : en version 13.2.15, les tests gcpc002a et gcpc002b s'arretent dans FACTORISER par manque de memoire GCPC sur athosdev mpi avec np=3
FONCTIONNALITE
   Problème:
   =========
   Avec np = 3, les cas-tests gcpc002a et gcpc002b échouent dans la commande factoriser (LDLT_SP)  avec le message
   <F> <ALGELINE5_76>                                                               !
       !                                                                                  !
       !    Solveur GCPC :                                                                !
       !    la création du préconditionneur 'LDLT_SP' a échoué car on manque de mémoire.  !
       !                                                                                  !
       !    Conseil :                                                                     !
       !    augmenter la valeur du paramètre PCENT_PIVOT sous le mot-clé facteur SOLVEUR. !
       !                                                                                  !
       ! 
   Correction
   ==========
   La valeur PCENT_PIVOT=30 (au lieu de la valeur par défaut 20) corrige le problème sur calibre9 et athosdev.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    gcpc002a et gcpc002b
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25768 DU 17/11/2016
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    B105.16 : en version 13.2.15, le test ssnv506g s'arrete dans CALC_MODES par manque de memoire GCPC sur calibre9 et athosdev mpi avec np=3
FONCTIONNALITE
   Problème:
   =========
   Avec np = 3, le cas-test ssnv506g échoue avec le message
   <F> <ALGELINE5_76>                                                               !
       !                                                                                  !
       !    Solveur GCPC :                                                                !
       !    la création du préconditionneur 'LDLT_SP' a échoué car on manque de mémoire.  !
       !                                                                                  !
       !    Conseil :                                                                     !
       !    augmenter la valeur du paramètre PCENT_PIVOT sous le mot-clé facteur SOLVEUR. !
       !                                                                                  !
       ! 
   Correction
   ==========
   La valeur PCENT_PIVOT=30 (au lieu de la valeur par défaut 20) corrige le problème sur calibre9 et athosdev.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnv506g
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25450 DU 26/07/2016
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 12.6)
TITRE
    Erreur numérique DYNA_SPEC_MODAL
FONCTIONNALITE
   Problème
   =========
   
   Erreur FPE lors d'une étude de calcul spectral DYNA_SPEC_MODAL en interaction fluide-structure.
   
   
   Analyse et proposition
   ======================
   
   Il s'agit d'une erreur d’enchaînement des opérateurs de dynamique et d'interaction fluide-structure.
   
   La base modale "en air" est calculée pour les 8 premiers modes (CALC_MODES). Les effets de couplage fluide-élastiques sont en
   revanche uniquement calculés pour 2 premiers modes de cette base par l'utilisation du mot-clé de filtrage NUME_ORDRE = (1,2).
   
   A ce stade, il n'y a pas encore défaut.
   
   Néanmoins, on poursuit l’enchaînement par une définition d'un spectre d'excitation turbulente (DEFI_SPEC_TURB) qu'on projette
   ensuite sur la base "en air" (8 modes)
   
   Lors de l'appel final à DYNA_SPEC_MODAL qui permet de déterminer les réponses spectrales, on dispose alors de deux concepts
   incohérents :
   1) Base modale mouillée à 2 modes
   2) Spectre défini sur 8 modes
   
   Le calcul alors plante puisqu'on va chercher des coefficients de couplage au delà des 8 modes disponibles dans la base.
   
   Je propose de blinder ce cas d'utilisation avec un message d'erreur fatale avec des conseils à l'utilisateur.
   
   
   <F> <MODELISA2_76>                                                                                                
   
   Incohérence détectée entre le nombre de modes de la base "mouillée", calculée par CALC_FLUI_STRU, et ceux de la base modale de la
   structure "en air", utilisée pour la projection du spectre de turbulence (PROJ_SPEC_BASE).
   
   - Nombre de modes de la base mouillée : 2  
   - Nombre de modes de la base en air . : 8  
                                              
   Conseil : si vous avez filtré des modes lors du calcul des coefficients de couplage (CALC_FLUI_STRU), il faut obligatoirement les
   omettre de la base de projection du spectre (PROJ_SPEC_BASE).
   
   En pratique, si un filtrage de modes est nécessaire pour l'étape de calcul IFS, un simple appel à EXTR_MODE sur la base initiale
   permet d'extraire ces modes. Cette étape réalisée en amont des calculs couplés Fluide-Structure permet d'assurer la cohérence de
   l’enchaînement.
   
   Cette erreur est fatale. Le code s'arrête.
   
   A rapporter en 12, pas de risque de résultat faux, plantage du code.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas joint
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 25835 DU 09/12/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.3)
TITRE
    En version 13.2.20-7aa239b800e1, le cas test perf009h est NOOK sur AthosDev_mpi et Aster5_mpi
FONCTIONNALITE
   En version 13.2.20-7aa239b800e1, le cas test perf009h est NOOK sur AthosDev_mpi et Aster5_mpi. Apparemment les valeurs de non
   régression non pas été mises à jour par l'issue25812.
   
   Je corrige ses valeurs de non régression.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    perf009h
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 25229 DU 19/05/2016
AUTEUR : HAELEWYN Jessica
TYPE aide utilisation concernant Code_Aster (VERSION 12.1)
TITRE
    ther_hydr
FONCTIONNALITE
   AOM
   ===
   
   Cette demande faisait suite aux difficultés de convergence rencontrées dans issue23948 sur un calcul d'hydratation en particulier
   concernant l'obtention de la convergence du calcul et les performances en cas de convergence.
   
   Analyse (par Jessica H.)
   ========================
   
   '''
   
   1. Proposition de modification de la mise en données :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   - paramètres matériaux pour les mettre en SI soit kg/m/s. J’ai pris ceux que l’on utilise pour la maquette Vercors qui sont validés.
   Les paramètres initiaux sont en kg/m/h. RCP et LAMBDA sont donc en 1e+13 ce qui est compliqué à gérer numériquement. La fonction
   d’affinité nécessite d’être correctement identifiée sur un essai adiabatique.
   - des paramètres de convergence (RESI_GLOB_RELA plutôt que RESI_GLOB_MAXI). 
   - passage à un maillage linéaire, meilleur en thermique. 
   
   Avec ces modifications, pas de problème de convergence ni de dépassement des bornes de la fonction d’affinité (qui relie hydratation
   et température).
   
   2. Résultats sur une couche :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Par contre, les résultats sont très mauvais pour des maillages trop grossiers. Celui de issue23948 est trop grossier avec une seule
   maille dans la hauteur. J’ai testé plusieurs maillages de la 1ère couche (46m x 3m x 16m). On observe des températures max qui
   dépassent 300°C très localement près des bords.
   En adaptant la taille de maille au problème : plus fin près des bords (voir image jointe). On obtient un résultat acceptable (voir
   courbe jointe) et réaliste partout.
   
   Pour le maillage le plus grossier (en jaune sur la courbe), un pas de temps de 6h sur 30 jours, le calcul  tourne en 3min sur 10
   proc/4 nœuds sur Aster5 (8min pour le plus fin). Pour une couche, cela reste raisonnable. 
   
   3. Transposition au barrage :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   
   Le passage au barrage n’est pas une transposition simple. En effet, les couches suivantes sont de plus en plus grandes mais n’auront
   besoin d’un raffinement plus fin que sur les côtés.
   Je pense que le calcul du barrage entier reste envisageable si on repense le maillage pour qu’il soit adapté au problème. 
   
   Si le remaillage proposé n’est pas suffisant ou le calcul trop coûteux, une stratégie couches par couches peut être mise en place.
   Elle consisterait à calculer au maximum deux couches du barrage :
   -	De t0 à t1, calcul de la première couche
   -	De t1 à t2, calcul de la première et deuxième couche
   -	De t2 à t3, calcul de la deuxième et troisième couche avec en CL une projection de la température entre les couches 1 et 2
   -	Et ainsi de suite
   En effet, lorsque la couche 3 est ajoutée, la couche 1 a une température stabilisée et elle n’est pas impactée par l’ajout de la
   couche 3. On peut donc « l’oublier ».
   
   4. Conclusion :
   ~~~~~~~~~~~~~~~
   
   Les deux propositions pour mener le calcul sur un barrage complet nécessitent chacune un travail non négligeable (notamment sur le
   maillage).
   C’est un problème fortement non-linéaire avec un transitoire important qui nécessite de toutes façons un raffinement important en
   espace (et en temps).
   
   Retour utilisateur
   ==================
   
   Les méthodes proposées seront testées en 2017, notamment dans le cadre d'un stage.
   
   Avec l'accord du chef de projet ADELAHYD, on clôt la fiche en l'état. Une nouvelle fiche sera ouverte en 2017 pour accompagner le
   cas échéant la mise en place des propositions de la présente fiche.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    étude jointe
NB_JOURS_TRAV  : 2.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25723 DU 04/11/2016
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Documentation (VERSION 10.*)
TITRE
    La doc R0.00.01 n'est pas à jour
FONCTIONNALITE
   Problème
   ========
   
   La doc R0.00.01 n'est pas à jour
   Cette doc rassemble les doc R par thématique. Or plusieurs nouvelles docs R ont été introduites sans impact sur celle_là
   
   Solution
   ========
   
   Il manquait 18 documentations. 
   
   r3.03.05	Élimination des conditions aux limites dualisées
   r3.03.09	Raccord Arlequin 3D – Poutre
   r3.06.09	Éléments finis de joint mécaniques et éléments finis de joint couplés hydromécaniques
   r3.08.10	Élément CABLE_GAINE
   r4.05.06	Méthode linéaire équivalent pour la propagation des ondes en 1D
   r4.10.07	Calcul de l'erreur en relation de comportement en dynamique sous une formulation fréquentielle
   r5.03.28	Loi d’endommagement à gradient ENDO_FISS_EXP
   r5.05.09	Calcul de signaux reconstitués et de la matrice fonction de transferts
   r5.05.10	Dynamic analysis of structures with viscoelastic materials having frequency dependent properties
   r5.07.01	Calcul de modes non-linéaires
   r7.01.29	Loi de comportement ENDO_HETEROGENE
   r7.01.36	Relation de comportement KIT_RGI
   r7.01.37	Dissipative Homogenised Reinforced Concrete (DHRC) constitutive model devoted to reinforced concrete plates
   r7.01.38	Loi d'Iwan pour le comportement cyclique de matériaux granulaires
   r7.01.40	Modèle de comportement LKR 
   r7.02.18	Élément hydro-mécanique couplé avec XFEM
   r7.02.19	Éléments cohésifs avec X-FEM
   r7.06.01	Modélisation de la déformation des assemblages combustibles avec l'outil MAC3COEUR
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : R0.00.01
VALIDATION
    rien
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25758 DU 15/11/2016
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : En version 13.2.15, certains tests échouent cause problème de chargement de biliothèques sur athosdev et calibre9 mpi avec np=1
FONCTIONNALITE
   Problème :
   ----------
   Les tests umat002a ssnl117b mfron06c mfron02c mfron02i zzzz387a zzzz387b umat001b umat001c et umat001a plantent car la lib MFront
   n'est pas trouvée.
   
   
   Analyse :
   ---------
   Alors effectivement, lorsque je fais un "waf_mpi test -n mfron02c", le test plante en me disant qu'il ne trouve pas la lib MFront.
   
   Par contre lorsque sous debugger, je fais la même chose, ça se passe très bien.
   
   Je vois bien le fichier BurgerAgeing.so mais en "batch" il ne le trouve pas. Il manque "." dans LD_LIBRARY_PATH.
   
   Pour moi, ce n'est pas vraiment un bug.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    tests MFront
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 25755 DU 15/11/2016
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    B105.16 : En version 13.2.15, le test ttlv300a est NOOK sur athosdev mpi avec np=1
FONCTIONNALITE
   Problème :
   ==========
   En 13.3.15, le cas test ttlv300a est NOOK sur athosdev_mpi avec np=1
   Solution :
   =========
   Après vérification le cas test ttlv300a est OK sur calibre9_mpi et athosdev_mpi, avec np=1 et 2
   => sans suite
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ttlv300a
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25756 DU 15/11/2016
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.2)
TITRE
    B105.16 : En version 13.2.15, le test ssls139c manque de mémoire sur calibre9 mpi avec np=1
FONCTIONNALITE
   Problème:
   =========
   Le test ssls139c échoue sur calibre9 mpi avec np =1 
   <E> <JEVEUX_62>                                                           !
      !                                                                           !
      !  Erreur lors de l'allocation dynamique. Il n'a pas été possible d'allouer !
      !  une zone mémoire de longueur 6 Mo, on dépasse la limite maximum          !
      !  fixée à 47 Mo et on occupe déjà 44 Mo.                                   !
      !  La dernière opération de libération mémoire a permis de récupérer 0 Mo.  !
      !                                                                           !
      !                                                                           !
      ! Cette erreur sera suivie d'une erreur fatale.
   
    <F> <JEVEUX_62>                                                           !
      !                                                                           !
      !  Erreur lors de l'allocation dynamique. Il n'a pas été possible d'allouer !
      !  une zone mémoire de longueur 6 Mo, on dépasse la limite maximum          !
      !  fixée à 47 Mo et on occupe déjà 44 Mo.                                   !
      !  La dernière opération de libération mémoire a permis de récupérer 0 Mo.  !
      !                                                                           !
      !                                                                           !
      ! Cette erreur est fatale. Le code s'arr
   Solution
   ========
   Après vérification, le cas passe sans difficulté sur calibre9_mpi avec np=1 (Version 13.2.20).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssls139c
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25757 DU 15/11/2016
AUTEUR : BEREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    B105.16 : En version 13.2.15, le test petsc01e manque de mémoire pour PETSC dans STAT_NON_LINE sur calibre9 mpi avec np=1
FONCTIONNALITE
   Problème
   ========
   Le cas test petsc01e échoue sur calibre9_mpi avec np = 1 pour manque de mémoire dans LDLT_SP 
   F> <PETSC_15>                                                                 !
      !                                                                                !
      ! Solveur PETSc :                                                                !
      !   La création du préconditionneur 'LDLT_SP' a échoué car on manque de mémoire. !
      !                                                                                !
      !   Conseil : augmentez la valeur du mot clé SOLVEUR/PCENT_PIVOT.                !
      !                                                                                !
      !                                                                                !
   Solution
   ========
   PCENT_PIVOT a déjà été augmenté à 30 par la fiche issue25676. 
   Après vérification, petsc01e passe sans problème sur calibre9_mpi en version 13.2.20.
   => sans suite
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    petsc01e
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 23227 DU 24/10/2014
AUTEUR : GENIAUT Samuel
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    X-FEM : bug dans PROPA_FISS : Apparition soudaine de fissures non-désirees
FONCTIONNALITE
   Problème :
   ----------
   
   Lors de calculs de propagation de fissures non-planes en 3D sur des structures industrielles, on note parfois l'apparition de
   fissures à des endroits où les level sets ne sont en principe pas
   modifiables. Les conséquences se manifestent de façon diverses (matrice singulière dans la fiche 23227, échec de création de la
   fissure dans DEFI_FISS_XFEM dans la fiche 23224).
   
   
   Réponse :
   ---------
   
   Il a été décidé de clôturer la fiche, sans suite.
   Comme le mentionne les docs U2 et U4 associées à PROPA_FISS, les algorithmes implémentés à l'heure actuelle ne garantissent pas la
   robustesse totale pour les configurations de propagation en 3d non-plan.
   Seules des modifications majeures des algorithmes permettraient de résoudre les problèmes évoquées. C'est l'objet de la thèse de M.
   Le Cren. Les études associées aux fiches 23224 et 23227 sont conservées à des fins de cas-test.
   
   Comme solution de contournement, il est proposé d'utiliser MoFEM.
   
   Rq : il n'y pas de risque de résultats faux
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    neant
NB_JOURS_TRAV  : 0.01

