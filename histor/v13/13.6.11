==================================================================
Version 13.6.11 (révision d2ecfa1e132b) du 2018-12-04 17:04 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28324 COURTOIS Mathieu         MACR_SPECTRE : 'DEPL' et 'ABSOLU'
 28250 ABBAS Mickael            [BB132] Cannot compute REAC_NODA when no loads from DYNA_NON_LINE
 27980 ABBAS Mickael            [BB128] Cannot use MATRICE='ELASTIQUE' with COQUE_3D
 28152 ABBAS Mickael            Bug dans la multplication matrice vecteur avec AFFE_CHAR_CINE
 27972 MICHEL-PONNELLE Sylvie   DEFI_MATERIAU : avec CABLE_GAINE
 28218 COURTOIS Mathieu         Différence de convergence lié à DEFI_FONCTION ?
 28205 GRANET Sylvie            bug dans ccfnrn.F90
 28071 COURTOIS Mathieu         En version 14.2.10-1f27ce5c5d4e, le cas test icare04a échoue sur Eole_Valid ...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28324 DU 06/12/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : MACR_SPECTRE : 'DEPL' et 'ABSOLU'
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | MACR_SPECTRE échoue si on choisit CALCUL='ABSOLU' et NOM_CHAM='DEPL'.
  | 
  | 
  | Correction
  | ----------
  | 
  | MULT_APPUI n'a de sens que pour un RESU_GENE avec NOM_CHAM='ACCE'.
  | Il ne faut donc pas chercher ce mot-clé si NOM_CHAM='DEPL'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aucun
- DEJA RESTITUE DANS : 14.2.21
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28250 DU 22/11/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : [BB132] Cannot compute REAC_NODA when no loads from DYNA_NON_LINE
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand il n'y a pas de chargements dans DYNA_NON_LINE, CALC_CHAMP plante en erreur fatale.
  | 
  | 
  | Correction
  | ----------
  | 
  | C'est un cas particulier. Sans chargements, on doit écrire néanmoins dans la SD liste_charges qu'on a un(=1) chargement (dans l'objet
  | list_load(1:19)//'.INFC', on met la première valeur à 1) afin que cette SD soit correcte (les objets existent).
  | Or, dans CALC_CHAMP, quand on récupère le nombre de charges, on a 0, le WKVECT échoue donc.
  | 
  | En fait, le souci est dans DYNA_NON_LINE, dans la révision b15118d6c302 (version 13.0.6) un refactoring dans nmdoch.F90 a supprimé
  | la notion de charge vide en enregistrant nb_load=0 dans la SD liste_charges au lieu de 1.
  | En corrigeant ce problème, tout se passe bien.
  | 
  | On ajoute le calcul de REAC_NODA dans sdnv103e pour valider

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnv103e
- DEJA RESTITUE DANS : 14.2.21
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27980 DU 29/08/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [BB128] Cannot use MATRICE='ELASTIQUE' with COQUE_3D
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'utilisation de COQUE_3D avec une matrice élastique en correction provoque une erreur dans le TE (paramètre inconnu)
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans le catalogue de l'élément COQUE_3D, il manque les deux paramètres PCACOQU et PNBSP_I. Le deuxième ne sert pas dans notre cas.
  | Par contre le premier permet de récupérer l'épaisseur de la coque.
  | On les ajoute.
  | 
  | Néanmoins, le bug ne se déclenche que lorsqu'on réactualise la matrice élastique. Sinon on garde la matrice élastique obtenue en
  | prédiction.
  | Dans le cas de l'utilisateur, c'est parce qu'il essaie de faire du contact CONTINUE avec ces éléments. Mais il peut y avoir d'autres
  | cas:
  | - amortissement de Rayleigh (seulement en utilisant la "matrice tangente")
  | - utilisation d’éléments de contact: contact CONTINU et XFEM
  | - présence d'éléments DIS_CHOC => mais dans ce cas on rebascule en matrice TANGENTE
  | - utilisation du contact DISCRET en pénalisation
  | - variables de commande dépendant des variables de commande
  | 
  | Remarque: normalement, il n'est pas recommandé d'utiliser le contact avec les COQUE_3D, je me demande même s'il ne faudrait pas
  | l'interdire, voire alarmer l'utilisateur, voir en particulier issue21030

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test fourni
- DEJA RESTITUE DANS : 14.2.21
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28152 DU 17/10/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Bug dans la multplication matrice vecteur avec AFFE_CHAR_CINE
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Notre doctorant LK a trouvé un bug.
  | 
  | Le produit matrice-vecteur avec AFFE_CHAR_CINE est faux.
  | 
  | 
  | Correction
  | ----------
  | 
  | Il y a un bug dans la routine mrmult
  | 
  | En effet, on utilise la variable neql  ligne 172 dans la routine mrmmvr qui correspond au nombre d'équations sur le domaine en
  | matrice distribuée.
  | Quand on est en matrice pas distribuée, il vaut zéro.
  | Ce qui veut dire qu'on ignore les conditions limites  dans ce cas.
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Le bug se déclenche dans un cas très particulier: le produit matrice-vecteur. Or les contributions de ces termes se font souvent au
  | second membre, il est donc fort peu probable que ce bug ait un impact en terme de résultat sur les calculs, car l'effet des
  | chargements cinématiques est surtout important dans la résolution des systèmes linéaires K.U = F. AFFE_CHAR_CINE travaille alors au
  | niveau du système linéaire et c'est OK.
  | 
  | Après la correction, l'intégralité des tests de la base sont OK, ce qui confirme que le problème n'a probablement pas de
  | conséquences graves.
  | 
  | Le cas détecté par LK concerne l'algo glouton utilisé en ROM, c'est vraiment un usage très particulier

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tout src
- DEJA RESTITUE DANS : 14.2.21
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 27972 DU 24/08/2018
================================================================================
- AUTEUR : MICHEL-PONNELLE Sylvie
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_MATERIAU : avec CABLE_GAINE
- FONCTIONNALITE :
  | Problème :
  | ----------
  | 1- la doc de DEFI_MATERIAU ne contient pas la description du comportement CABLE_GAINE_FROT, 
  | 2- le catalogue de la commande DEFI_MATERIAU n'est pas très propre pour le comportement 
  | CABLE_GAINE_FROT (mot-clé FROT_LINE et FROT_COURB définissable quelque soit le modèle de frottement).
  | 
  | Corrections:
  | --------------
  | 
  | 
  | 1 - Complément dans la doc DEFI_MATERIAU partie comportements particuliers 
  | 2 - Mise en œuvre de la méthode suggérée par JL Fléjou:
  | 
  |  a) op0005 ==> rcstoc (à partir de ligne 246) !  On traite les TX qu'on convertit en R8
  |    Dans le cas CABLE_GAINE, on regarde la chaîne TYPE ==> (1.0, 2.0, 3.0 ) pour 
  | ("FROTTANT","GLISSANT","ADHERENT")
  | 
  | b) On supprime les b_glissant et b_adherent du catalogue DEFI_MATERIAU
  | 
  | c) pour les rcvalb( ... 'CABLE_GAINE_FROT' ... ) 
  |    on va chercher 'TYPE'   
  |    on fait un test sur NINT(dutype) c'est soit 1, 2, 3 ==> FROTTANT GLISSANT ADHERENT
  |    on va chercher les valeurs qu'il faut dans le cas à traiter :
  |       'PENA_LAGR' dans tous les cas
  |       'FROT_LINE','FROT_COURB' dans le cas FROTTANT
  | 
  | Tous les tests utilisant les éléments CABLE_GAINE passent.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.43.01
- VALIDATION : tests utilisant les éléments CABLE_GAINE
- DEJA RESTITUE DANS : 14.2.21


================================================================================
                RESTITUTION FICHE 28218 DU 09/11/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : Différence de convergence lié à DEFI_FONCTION ?
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | On fait un calcul thermique non linéaire. On définit les paramètres matériaux RHO_CP et LAMBDA.
  | 
  | On utilise un prolongement constant à gauche.
  | Dans le 1er cas, la courbe RHO_CP est définie en 0, 20, 50, ..., 300.
  | Dans le second cas, la courbe RHO_CP est définie en 20, 50, ..., 300.
  | 
  | Le second calcul ne converge pas au pas de temps numéro 676.
  | 
  | 
  | Analyse
  | -------
  | 
  | En thermique non linéaire (mot-clé THER_NL de DEFI_MATERIAU), on peut définir RHO_CP ou BETA.
  | Si BETA (enthalpie) est fourni, on l'utilise directement, sinon on le calcule en intégrant RHO_CP (cf. r5.02.02). Le prolongement de
  | BETA est obligatoirement linéaire (cf. u4.43.01).
  | 
  | Si on compare les fonctions BETA calculées par DEFI_MATERIAU, on a dans le 1er cas:
  | 
  |       1 -  0.00000D+00  2.00000D+01  5.00000D+01  1.00000D+02  1.50000D+02
  |       6 -  2.00000D+02  2.50000D+02  3.00000D+02  3.50000D+02  0.00000D+00
  |      11 -  7.20600D-02  1.82265D-01  3.73390D-01  5.73490D-01  7.80190D-01
  |      16 -  9.90940D-01  1.20497D+00  1.42104D+00
  | 
  | et dans le second :
  | 
  |       1 -  2.00000D+01  5.00000D+01  1.00000D+02  1.50000D+02  2.00000D+02
  |       6 -  2.50000D+02  3.00000D+02  3.50000D+02  0.00000D+00  1.10205D-01
  |      11 -  3.01330D-01  5.01430D-01  7.08130D-01  9.18880D-01  1.13291D+00
  |      16 -  1.34898D+00
  | 
  | Les deux fonctions sont bien identiques à une constante près... là où elles sont définies.
  | 
  | Or à partir de l'instant de calcul numéro 666, l'enthalpie est évaluée en dessous de 20°C.
  | Donc à partir de cet instant, on a des valeurs différentes.
  | 
  | 
  | Correction
  | ----------
  | 
  | Quand on intègre l'enthalpie BETA à partir de RHO_CP, on y ajoute une constante de sorte que l'enthalpie soit toujours positive (on
  | fait un prolongement linéaire et on calcule cette constante pour que l'enthalpie soit nulle à T=-273.15°C).
  | Cette constante est sans impact sur les résultats car elle disparaît lors de la résolution (cf. r5.02.02).
  | 
  | On modifie une valeur de non régression qui varie de 1.2e-4% sur ort001a (toutes les valeurs testées en non régression sont
  | identiques sur 12 chiffres significatifs entre Aster5 et Calibre9).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : u4.43.01
- VALIDATION : étude jointe
- DEJA RESTITUE DANS : 14.2.20
- NB_JOURS_TRAV  : 2.5


================================================================================
                RESTITUTION FICHE 28205 DU 07/11/2018
================================================================================
- AUTEUR : GRANET Sylvie
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : bug dans ccfnrn.F90
- FONCTIONNALITE :
  | Nicolas s'est aperçu que l'initialisation du temps était incomplète dans ccfnrn.F90.
  | ! Initialisation
  |         partps(1) = time
  |         partps(1) = time    
  |         partps(1) = 0.D0
  | 
  | Alors qu'il faudrait
  |         partps(1) = time
  |         partps(2) = time    
  |         partps(3) = 0.D0
  | 
  | Cela peut avoir un impact potentiel sur des calculs temporaires utilisant des réactions nodales (problème non rencontré pour
  | l'instant). En THM seul partps(1) est visiblement utilisé donc a priori pas d'impact de ce coté là. Cela pourrait avoir un impact
  | pour dyna_harmo
  | On corrige.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage des cas tests
- DEJA RESTITUE DANS : 14.2.20
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28071 DU 24/09/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 14.1)
- TITRE : En version 14.2.10-1f27ce5c5d4e, le cas test icare04a échoue sur Eole_Valid et Aster5_valid
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Ce test est en erreur fatale, puis NOOK depuis 14.0.5 (issue26883, issue27219).
  | 
  | 
  | Correction
  | ----------
  | 
  | Les deux fiches précédentes n'ont pas corrigé le test.
  | 
  | Depuis issue27388 (et issue28041), PETSc est construit avec le support de SuperLU.
  | Celui-ci entre en conflit avec scipy, utilisé dans les fichiers des tests icareXX.
  | 
  | Il n'y a plus de projet pour maintenir ce test.
  | On propose de le supprimer.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v1.03.136
- VALIDATION : icare04a
- DEJA RESTITUE DANS : 14.2.21
- NB_JOURS_TRAV  : 0.5

