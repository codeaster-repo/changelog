==================================================================
Version 13.0.13 (révision b7aa0d877d31) du 2015-11-19 16:38 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 24382 DU 16/10/2015
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.0.9-4ec12113c343, les cas-test fdlv112h et fdlv113a s'arrêtent en erreur fatale sur Calibre9
FONCTIONNALITE
   En version 13, le test fdlv112h s'arrête par manque de mémoire.
   On ajuste la taille mémoire utilisée lors de l'initialisation, il fonctionne alors sans problème.

   Le test fdlv113a lui, s'arrête brutalement dans MISS (STOP RESOL, PIVOT NUL, ...), on émet une fiche spécifique.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    fdlv112h
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24494 DU 13/11/2015
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Stocker l'environnement pour profile.sh au moment du configure
FONCTIONNALITE
   Pour remplir le fichier profile.sh utilisé lors l'exécution de Code_Aster, il faut récupérer l'environnement (principalement
   LD_LIBRARY_PATH et PYTHONPATH).

   Aujourd'hui, on récupère l'environnement au moment où on produit ce fichier.
   Il est plus naturel de récupérer l'environnement au moment où on vérifie les différentes bibliothèques et modules Python, donc lors
   du configure.
   On stocke donc l'environnement à ce moment et on l'utilise plus tard pour remplir profile.sh.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    waf configure & install
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24329 DU 09/10/2015
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    fichier maillage GIBI vide dans zzzz120a et zzzz120b sur Calibre7
FONCTIONNALITE
   Problème
   --------

   Sur Calibre7, dans ces deux tests (zzzz120a et zzzz120b), le fichier maillage est vide, ce qui engendre une erreur fatale.


   Correction
   ----------

   Lors de la précédente correction, je n'avais pas vu que le traitement du fichier de données Gibi était fait deux fois.
   Donc il se trouve que sur Calibre7, le chemin du répertoire de travail est un peu plus long et comme Gibi ne supporte pas des
   chemins de plus de 72 caractères, le test échoue.

   On fait comme pour le premier fichier, on utilise un lien symbolique pour masquer le chemin réel.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    zzz120a, zzzz120b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24430 DU 30/10/2015
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Erreur dans la methode generique get_mc_simp
FONCTIONNALITE
   Problème
   --------

   La méthode get_mc_simp(niv) des objets "mots-clefs" retourne la liste des mots-clefs simples présents.
   Avec niv=1, elle retourne uniquement les mots-clefs présent dans un mot-clef facteur.
   Avec niv=2, elle descend récursivement sous les blocs mais pas sous les autres mots-clefs facteurs (c'est sa spécification).

   Le problème que l'argument niv n'était pas transmis lors des appels récursifs.


   Correction
   ----------

   On transmet niv récursivement.
   Pour les blocs, cela a déjà été fait dans issue22290.
   De plus, on descend aussi dans les mots-clefs facteurs. La spécif devient plus claire : on retourne tous les mots-clefs simples
   existants.
   Cette méthode n'est utilisée que pour la vérification du vocabulaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    ras
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24482 DU 10/11/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    comp_meca_vari : variables non initialisées
FONCTIONNALITE
   Problème :
   ----------
   En faisant passer le test ssnv209n en mode debug sur calibre9, on se plante dans la routine imvari.F90 (dans un "read" sur la carte
   de comportement).

   Avec l'aide de valgrind, on s'aperçoit que la carte de comportement contient des valeurs non initialisées qui proviennent de la
   recopie des tableaux nume_comp et nb_vari_comp (pas toujours initialisés) de la routine comp_meca_vari.

   Correction :
   ------------
   On initialise à 0 les deux tableaux incriminés.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    ssnv209n
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24447 DU 02/11/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Y-a-t-il d'autres erreurs comme celle de issue24068 ?
FONCTIONNALITE
   Problème :
   ----------

   Suite à issue24068, je me pose la question :
    Y-a-t-il d'autres mots clés facteurs répétables pour lesquels on fait, par erreur, un 'call getvxx(MFAC, MSIMP, iocc=1, ...)' ?

   Réponse :
   =========

   Déjà traité :
   ~~~~~~~~~~~~

   * issue24451 :
   /postrele/rc32r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rc32r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rcZ2r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)
   /postrele/rcZ2r1env.F90: call getvr8('ENVIRONNEMENT','FEN_INTEGRE',iocc=1,scal=fenint,nbret=n5)

   * issue22290 :
   /algorith/mdveri.F90: call getvtx('CHOC','SOUS_STRUC_1',iocc=1,nbval=0,nbret=n1)
   /algorith/mdveri.F90: call getvtx('CHOC','NOEUD_2',iocc=1,nbval=0,nbret=n1)
   /algorith/mdveri.F90: call getvtx('CHOC','SOUS_STRUC_2',iocc=1,nbval=0,nbret=n1)

   /algorith/mdtr74.F90: call getvis('PALIER_EDYOS','UNITE',iocc=1,scal=unitpa,nbret=n1)


   * issue24068 :
   /algorith/chrpel.F90: call getvr8('AFFE','VECT_X',iocc=1,nbval=3,vect=vectx,&
   /algorith/chrpel.F90: call getvr8('AFFE','VECT_Y',iocc=1,nbval=3,vect=vecty,&
   ...
   /algorith/chrpno.F90: call getvr8('AFFE','VECT_X',iocc=1,nbval=3,vect=vectx,&
   /algorith/chrpno.F90: call getvr8('AFFE','VECT_Y',iocc=1,nbval=3,vect=vecty,&
   ...


   Mots clés facteurs que l'on interdit de répéter :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   Dans le catalogue de la commande, on supprime : "max='**'"

   >>>>> MASSIF
   /commande/simu_point_mat.capy
   /commande/test_compor.capy

   >>>>> CONVECTION
   /commande/affe_char_ther.capy
   /commande/affe_char_ther_f.capy

   >>>>> CYCLIQUE
   /commande/defi_squelette.capy
   /commande/rest_sous_struc.capy

   >>>>> PROJ_MODAL
   /commande/dyna_non_line.capy

   >>>>> DIAG_MASS
   /commande/defi_base_modale.capy

   >>>>> CARA_POUTRE
   /commande/post_elem.capy

   >>>>> REPERE
   /commande/crea_maillage.capy

   >>>>> MODE_INTERF
   /commande/mode_statique.capy

   >>>>> ORTHO_BASE
   /commande/defi_base_modale.capy


   Appels justifiés avec iocc=1 :
   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
   J'ai regardé les routines suivantes et je me suis convaincu que le "iocc=1" n'était pas écrit par erreur.

   /utilitai/x195cb.F90: call getvid('COMB','CHAM_GD',iocc=1,scal=ch1,nbret=ib)

   /modelisa/acearp.F90: call getvtx('RIGI_PARASOL','EUROPLEXUS',iocc=1,scal=k8bid,nbret=ibid)
   /modelisa/acearp.F90: call getvtx('RIGI_PARASOL','EUROPLEXUS',iocc=1,scal=k8bid,nbret=ibid)

   /algorith/asefen.F90: call getvtx('DEPL_MULT_APPUI','NOM_CAS',iocc=1,nbval=0,nbret=ns)
   /algorith/asveri.F90: call getvtx('DEPL_MULT_APPUI','NOM_CAS',iocc=1,nbval=0,nbret=ns)
   /op/op0109.F90: call getvid('DEPL_MULT_APPUI','MODE_STAT',iocc=1,scal=stat,nbret=ns)

   /op/op0153.F90: call getvr8('SECTEUR','ANGL_INIT',iocc=1,scal=zr(idangt),nbret=na)

   /utilitai/pecage.F90: call getvtx('CARA_GEOM','SYME_X',iocc=1,scal=symex,nbret=ns1)
   /utilitai/pecage.F90: call getvtx('CARA_GEOM','SYME_Y',iocc=1,scal=symey,nbret=ns2)
   /utilitai/pecage.F90: call getvr8('CARA_GEOM','ORIG_INER',iocc=1,nbval=2,vect=xyp,&
   /utilitai/pecapo.F90: call getvid('CARA_POUTRE','CARA_GEOM',iocc=1,nbval=0,nbret=ntab)
   /utilitai/pecapo.F90: call getvid('CARA_POUTRE','CARA_GEOM',iocc=1,scal=nomtab,nbret=ntab)

   /algorith/crtype.F90: call getvid('AFFE','CHAM_GD',iocc=1,scal=champ,nbret=ier)
   /op/op0018.F90: call getvtx('AFFE','PHENOMENE',iocc=1,scal=phenom)

   /algorith/refe99.F90: call getvid('RITZ','MODE_MECA',iocc=1,nbval=0,nbret=nbg)
   /algorith/refe99.F90: call getvid('RITZ','BASE_MODALE',iocc=1,scal=resul1,nbret=ibmo)
   ...
   /algorith/ritz99.F90: call getvid('RITZ','BASE_MODALE',iocc=1,scal=resul1,nbret=ibmo)
   /algorith/ritz99.F90: call getvid('RITZ','MODE_MECA',iocc=1,nbval=0,nbret=nbgl)
   ...

   /calculel/crpcvg.F90: call getvtx('PERM_CHAM','GROUP_MA_FINAL',iocc=1,nbval=1,vect=nom_gr2,nbret=ibid)
   /calculel/crpcvg.F90: call getvtx('PERM_CHAM','GROUP_MA_INIT',iocc=1,nbval=1,vect=nom_gr1,nbret=ibid)

   /utilitai/peritr.F90: call getvtx('RICE_TRACEY', 'OPTION', iocc=1, scal=optcal(1), nbret=np)
   /utilitai/peritr.F90: call getvtx('RICE_TRACEY', 'LOCAL', iocc=1, scal=optcal(2), nbret=nq)

   /algorith/rec110.F90: call getvtx('RECO_GLOBAL', 'GROUP_NO_1', iocc=1, nbval=0, nbret=igr)
   /algorith/rec110.F90: call getvr8('RECO_GLOBAL', 'PRECISION', iocc=1, scal=prec, nbret=ibid)
   /algorith/rec110.F90: call getvr8('RECO_GLOBAL', 'DIST_REFE', iocc=1, scal=dist, nbret=ndist)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.4

--------------------------------------------------------------------------------
RESTITUTION FICHE 24451 DU 03/11/2015
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Couverture routines de POST_RCCM
FONCTIONNALITE
   Enrichissement des cas tests pour couvrir les nouvelles routines :
   -rccm01c pour rcZ2r1 (option FATIGUE) et rcZ2s0 (SEISME)
   -rccm01b pour rc32env (option EFAT avec UNITAIRE dans rccm01b.com3)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V1.01.107
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24291 DU 30/09/2015
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Erreur à l'émission des messages
FONCTIONNALITE
   Problème
   --------

   Si, par exemple, on ne définit pas les propriétés matériaux avec le
   mot-clé MFRONT de DEFI_MATERIAU, un message d'erreur est émis plus tard
   quand on essaie de récupérer les valeurs.

   Dans un premier temps, on obtient ça :
    . !--------------------------------------------------------------!
    . ! <EXCEPTION> <CALCUL_46> . . . . . . . . . . . . . . . . . . .!
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
    . ! .comportement : MFRONT non trouve . . . . . . . . . . . . . .!
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
    . ! .pour la maille .M9 . . . . . . . . . . . . . . . . . . . . .!
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .!
    . ! -------------------------------------------- . . . . . . . . !
    . ! Contexte du message : . . . . . . . . . . . . . . . . . . . .!
    . ! . .Option . . . . : RAPH_MECA . . . . . . . . . . . . . . . .!
    . ! . .Type d'élément : MEDPQU4 . . . . . . . . . . . . . . . . .!
    . ! . .Maillage . . . : Mesh . . . . . . . . . . . . . . . . . . !
    . ! . .Maille . . . . : M9 . . . . . . . . . . . . . . . . . . . !
    . ! . .Type de maille : QUAD4 . . . . . . . . . . . . . . . . . .!
    . ! . .Cette maille appartient aux groupes de mailles suivants : !
    . ! . . . Malha . . . . . . . . . . . . . . . . . . . . . . . . .!
    . ! . .Position du centre de gravité de la maille : . . . . . . .!
    . ! . . . x=1.500000 y=0.250000 z=0.000000 . . . . . . . . . . . !
    . !--------------------------------------------------------------!

   Ensuite, à la fin du .mess, on obtient :
    . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . ! <S> Exception utilisateur levee mais pas interceptee. !
    . ! Les bases sont fermees. . . . . . . . . . . . . . . . !
    . ! Type de l'exception : error . . . . . . . . . . . . . !
    . ! . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . ! .comportement : RAPH_MECA non trouve . . . . . . . . .!
    . !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

   Le message n'est pas le même. Dans un cas, on a : "comportement : MFRONT non trouve" et dans un autre on lit : "comportement :
   RAPH_MECA non trouve".


   Correction
   ----------

   L'exception est correctement émise avec le bon message (le premier).
   En revanche, les arguments utilisés pour formatter le message sont ceux du
   dernier appel à utmess, c'est-à-dire ici le message de suite qui donne des
   informations de contexte.

   En effet, aujourd'hui, on ne stocke que l'id du premier message.

   On corrige en conservant le premier message complet.
   Pour cela, on crée un type Message et le module associé pour initialiser et
   libérer un Message (message_module.F90).

   Le message est maintenant correct:
    . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . .! <S> Exception utilisateur levee mais pas interceptee. !
    . .! Les bases sont fermees. . . . . . . . . . . . . . . . !
    . .! Type de l'exception : error . . . . . . . . . . . . . !
    . .! . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . .! .Comportement : MFRONT non trouvé . . . . . . . . . . !
    . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    mfron02i avec erreur
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24211 DU 08/09/2015
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Interpréteur de commande : appel a sd_prod après la vérification de la syntaxe mais avant l'appel à OPS.
FONCTIONNALITE
   Problème
   --------

   Lorsque l'on passe l'étude jointe, ca plante avec le message :
     . ! Etape  AFFE_CARA_ELEM ligne :  146 fichier :  fort.1 impossible d affecter un
     . ! type au resultat: Erreur construction de la commande

   La syntaxe de AFFE_CARA_ELEM n'est pas bonne, il y a :
    CARA=('E1','R1','E2','R2') affecté à un GROUP_MA/SECTION=CERCLE,
    VARI_SECT=HOMOTHETIQUE
   alors que cela devrait être
    CARA=('EP_DEBUT','R_DEBUT','EP_FIN','R_FIN')

   Le problème est que la "sd_prod" est appelée avant de vérifier la syntaxe de la
   commande. Comme la syntaxe n'est pas bonne et que la "sd_prod" va chercher EP_DEBUT
   qui n'existe pas ça plante avec une message qui ne devrait jamais être émis en
   utilisation normale.

   Si on remplace dans la "sd_prod" les "raise" par des "print" (le temps de tester),
   cela plante dans l'analyse de la commande, comme il faut :
    POUTRE
    . .b_cercle
    . . . b_homothetique
    . . . . .b_grma
    . . . . . . Mot-clé simple : CARA
    . . . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . . . .! La valeur : 'R1' .ne fait pas partie des choix possibles (u'R_DEBUT', u'R_FIN', !
    . . . . . . . .! u'EP_DEBUT', u'EP_FIN') . . . . . . . . . . . . . . . . . . . . . . . . . . . . !
    . . . . . . . .!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    . . . . . . Fin Mot-clé simple : CARA
    . . . . .Fin b_grma
    . . . Fin b_homothetique
    . .Fin b_cercle
    Fin POUTRE


   Analyse & Correction
   --------------------

   Le superviseur fait deux appels à ``get_sd_prod()`` et donc à la fonction ``sd_prod()``
   de la commande.
   Le premier appel doit permettre de typer le résultat pour construire le JDC complet
   dans le cas PAR_LOT='OUI', ou bien juste l'étape avant son exécution en PAR_LOT='NON'.
   Le second appel intervient plus tard au moment de la validation de la commande
   (vérification syntaxique).

   On ajoute un argument ``__only_type__`` lors de l'appel à ``sd_prod()`` qui
   correspond au premier appel. ``sd_prod()`` peut alors ensuite prendre la responsabilité
   de retourner un type sans savoir si la syntaxe est correcte.
   Lors de la validation de la commande, cet argument n'est pas ajouté. En cas d'erreur,
   il y aura un rapport standard concernant la syntaxe.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    erreur de syntaxe AFFE_CARA_ELEM
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24109 DU 10/08/2015
AUTEUR : BOYERE Emmanuel
TYPE anomalie concernant Code_Aster (VERSION 12.4)
TITRE
    correction statique dans COMB_SISM_MODAL : intitulé "valeurs correction statique" dans .resu à modifier ou supprimer
FONCTIONNALITE
   Lors de l'utilisation de COMB_SISM_MODAL, le message d'information sur le niveau lu pour la correction statique n'est pas assez
   clair et a perturbé un utilisateur.
   Je propose donc de modifier le message de

   < "valeurs correction statique"

   > "valeurs lues sur le spectre pour la correction statique"
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24491 DU 12/11/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Message CONTACT_22 en formulation CONTINUE
FONCTIONNALITE
   Anomalie :
   Cette alarme n'a de sens que si la pénalisation est activée. Or actuellement elle se déclenche même pour la méthode standard.

   Correction :
   1. Désormais cette alarme ne se déclenchera que lorsque la méthode pénalisée est activée.
   2. Améliorer le conseil fourni
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    correction du source TE0364,TE0365
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24440 DU 02/11/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    Défaut couverture de code CALC_PRESSION/GEOMETRIE/DEFORMEE/
FONCTIONNALITE
   je modifie ssnp154 pour prendre en compte ce mot-clef.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE :
VALIDATION
    ssnp154
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24452 DU 03/11/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Couverture de drz12d
FONCTIONNALITE
   drz12d teste LIAISON_SOLIDE en 2D avec au moins un noeud ayant un DRZ.

   Dans sslv114b, il y avait un calcul MECA_STATIQUE dédié à un modèle 3d+poutre et un autre dédié à un modèle
   d_plan+coque_c_plan. Ces deux calculs utilisaient la LIAISON_SOLIDE.

   On propose de rétablir la modélisation sslv114b. La modélisation initiale COQUE_C_PLAN sera remplacée par un
   2D discret portant une rotation (2D_DIS_TR).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE :
VALIDATION
    sslv114b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24464 DU 06/11/2015
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    En version 13.0.11-acccfa1a057f , le cas tests sdld22b échoue sur clap0f0q
FONCTIONNALITE
   . . . . . .**********
   . . . . . * Problème *
   . . . . . .**********

   Erreur FPE sur machine 32-bit / clap0f0q.

   . . . . . .*********
   . . . . . * Origine *
   . . . . . .*********

   Dans la routine dtminfo, on imprime le nombre maximal de pas à calculer, qui est un
   nombre entier mais calculé par la division de la durée du calcul (en secondes) par le pas
   de temps minimal.

   Dans le cas-test sdld22b, ce pas_mini est de 1.E-10 s et la durée du calcul est de 1.5 s.

   Sur une machine 32-bit un entier long ne peut pas contenir une valeur de 1.5E10.


   . . . . . .************
   . . . . . * Correction *
   . . . . . .************

   Plafonner le nombre maximal de pas à calculer dans les impression de dtminfo.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    sdld22b
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24424 DU 28/10/2015
AUTEUR : BOYERE Emmanuel
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Test validation sdls126a
FONCTIONNALITE
   Irmela a repéré une erreur dans le cas test de validation sdls126 lors du calcul de la correction statique:


   on s'est trompé dans la direction et l'amplitude de la correction statique pour les directions X et Y du séisme.

   Au lieu de

   chX=AFFE_CHAR_MECA(MODELE=modele,
                      PESANTEUR=_F(GRAVITE=9.81,
                  DIRECTION=(0.,0.,-1.,),),);

   chY=AFFE_CHAR_MECA(MODELE=modele,
                       PESANTEUR=_F(GRAVITE=9.81,
                  DIRECTION=(0.,0.,-1.,),),);

   il faut écrire

   chX=AFFE_CHAR_MECA(MODELE=modele,
                      PESANTEUR=_F(GRAVITE=1.,
                  DIRECTION=(1.,0.,0.,),),);

   chY=AFFE_CHAR_MECA(MODELE=modele,
                       PESANTEUR=_F(GRAVITE=1.,
                  DIRECTION=(0.,1.,0.,),),);

   La modification n'entraîne pas de conséquences sur les résultats.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    sdls126a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24006 DU 06/07/2015
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.0.1 modifiée, revision c27349df47aa, les cas-tests perf001b, perf002b, perf003d, perf003d, perf004a, perf007a et sdls128a s'arretent par manque de temps CPU sur AthosDev_valid
FONCTIONNALITE
   En version 13, les tests perf001b, perf002b, perf003d, perf003d, perf004a, perf007a et sdls128a s'arrêtent par manque de temps CPU.
   Concernant les tests de performance perf00, c'est le temps système qui explose sur athosdev qui est la cause de l'erreur. Sur
   ATHOSDEV, les tests sont lancés sur un même noeud et sont mal placés : ils occupent un même "socket" sur le noeud qui en possède 2.
   Il y a donc des conflits d'accès à la mémoire et aux I/O. C'est la configuration actuelle du gestionnaire de tâches SLURM qui
   fonctionne mal. Une demande est en cours auprès de l'exploitant du cluster pour corriger ce problème.
   (https://si-forge.edf.fr/mantis/view.php?id=36394). En attendant il nous a été suggéré de lancer chaque exécution en mode exclusif.
   Le plugin d'as_run a été modifié pour repérer les tests de performance et les soumettre en exclusif.
   Les tests perf00 s'exécutent maintenant normalement.

   Concernant le test sdls128a, il suffit d'augmenter le temps CPU (700 -> 750), mais il faut aussi corriger les valeurs utilisées dans
   les tests de non-régression.

                 VALE_CALC=0.0829681   --> 0.0829652
                 VALE_CALC=0.558522    --> 0.55853
                 VALE_CALC=0.0360246   --> 0.0360245
                 VALE_CALC=0.0099933   --> 0.00999361
                 VALE_CALC=0.000433781 --> 0.000433672

   Ce qui corrige les NOOK sur athosdev, mais pas sur les autres plates-formes (les tests sont réalisés avec une tolérance de 5%).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    perf001b, perf002b, perf003d, perf003d, perf004a, perf007a et sdls128a
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 24315 DU 05/10/2015
AUTEUR : BOYERE Emmanuel
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    En 13.0.7-60630ed6e790, le cas-test sdnv111a est NOOK_TEST_RESU sur AthosDev_valid
FONCTIONNALITE
   Lors de la restitution de la fiche 23999 pour ajuster les valeurs de non régression de SDNV111, j'ai probablement mal entré les valeurs.
   Je les corrige à nouveau.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    sdnv111
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24161 DU 27/08/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Documentation (VERSION 10.*)
TITRE
    MACR_ECLA_PG / TAILLE_MIN ne fonctionne que pour les QUAD4
FONCTIONNALITE
   Problème :
   ----------
   La doc U4.44.14 ne dit pas que le mot clé TAILLE_MIN n'est implémenté que pour les mailles de type quadrilatère.

   Correction :
   ------------
   On précise la doc.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : u4.44.14
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 24121 DU 13/08/2015
AUTEUR : PELLET Jacques
TYPE anomalie concernant Documentation (VERSION 10.*)
TITRE
    Documentation sd maillage D4.06.01 : objets .MAOR, .CRMA et .CRNO
FONCTIONNALITE
   Problème :
   ----------
   Les objets  .CRNO, .CRMA, et .MAOR  de ls sd_maillage ne sont pas documentés dans D4.06.01.

   Réponse :
   ---------
   Ce sont 3 objets qui n'existent que dans les maillages créés par la commande CREA_MAILLAGE / RESTREINT.
   Ils servent à retenir les liens entre les 2 maillages : le maillage "restreint" et son maillage "origine".

   Ces informations sont utilisées par la commande EXTR_RESU / RESTREINT.

   On documente ces 3 objets dans d4.06.01
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : d4.06.01
VALIDATION
    rien de particulier
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24410 DU 23/10/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    D114 - Enrichir les messages d'erreur et d'alarme <A> <CALCULEL2_89>  et <F> <CALCUL_37>
FONCTIONNALITE
   classé sans suite.

   Par ailleurs, la doc U2.02.01 montre clairement les options de post-traitement possible avec ce type de
   modélisation.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE :
VALIDATION
    CALCULEL2_89
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24356 DU 13/10/2015
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Documentation (VERSION 10.*)
TMA : DeltaCad
TITRE
    D114 - [BB0061] Incohérences dans la documentation du cas-test SSNS101
FONCTIONNALITE
   La documentation [V6.05.101] SSNS101 - Calaquage d'un panneau cylindrique sous force ponctuelle a été corrigé.

   L'analyse du fichier med du cas test SSNS101 a permis de confirmer les points suivants:
      - valeur de l'angle alpha: "alpha = 0.1 rad"
      - valeur du rayon du panneau cylindrique: "R=2540mm"
      - et l'inversion des lignes L31290 et L4590 dans le deuxième schéma du §1.1.

   La mise en forme des éléments du premier schéma au §1.1 a également été améliorée.


   En pièce jointe, la documentation révisée est mise à disposition.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE :
VALIDATION
    doc SSNS101

--------------------------------------------------------------------------------
RESTITUTION FICHE 24475 DU 09/11/2015
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    En version 13.0.11-acccfa1a057f, le temps CPU DYNA_VIBRA dégradé
FONCTIONNALITE
   Constat : dégradation des temps CPU d'un nombre de cas-tests utilisant DYNA_VIBRA
   . . . . . .**********
   . . . . . * Problème *
   . . . . . .**********

   Dégradation du temps CPU d'un nombre de cas-tests utilisant DYNA_VIBRA

   . . . . . .*********************
   . . . . . * Analyse approfondie *
   . . . . . .*********************

   Il a bien été prévu de constater une dégradation globale du temps CPU des calculs
   DYNA_VIBRA//TRAN/GENE suite aux travaux de restructuration de l'opérateur et la mise en
   place des structures de données calcul (sd_dtm) et d'intégration (sd_int).

   Néanmoins,
   les dégradations visibles sont pour la plupart dues aux améliorations/modifications aux
   fichiers de commandes pour :
   - améliorer la précision des calculs (suppression TOLE_MACHINE);
   - comparer entre les schémas d'intégration.


   Dans la suite, je détaille les modifications aux cas-tests en tête de liste pour donner
   exemple :

   - sdnd104a : le pas de temps du calcul a été réduit par un facteur de 10 pour le schéma
   -désormais- adaptatif DEVOGE. D'autres ajustements ont été effectués pour les autres s
   chémas afin d'obtenir une précision comparable multi-schémas. (par ailleurs, quelques
   appels à DYNA_VIBRA ont gagné en performance dans ce test)

   - sdnl112b : l'appel à GENE_FONC_ALEA prend plus de temps parce que le pas de temps a été
   réduit afin de respecter la raideur effectif du système avec choc pour le calcul transitoire
   IFS avec ITMI reprogrammé (avant, le schéma ITMI se permettait d'ignorer les données de
   l'utilisateur et changer le pas de temps en fonction de la raideur du système).

   - sdld321b : Application de l'archivage aux instants prédéfinis des schémas à pas adaptatifs.
   Les pertes en perfomance sont surement dues à ce nouveau mode de fonctionnement.
   A savoir qu'avec un tel archivage, on a pu supprimer toutes les TOLE_MACHINE du cas-test!

   - sdld321c : réduction du pas d'intégration par un facteur de 10.

   Le seul test montrant fidèlement les pertes de performance est sdns01a. C'est un test où
   l'on emploie le schéma le plus rapide (EULER explicite) pour calculer et stocker 80000 pas,
   pour un tout petit système dynamique (masse-ressort). DYNA_VIBRA effectue ce calcul en
   1.30s au lieu de 0.18s. Pour moi, la performance reste bonne compte tenu les vérifications
   supplémentaires effectuées / les impressions qui ont un "prix presque forfaitaire" en
   terme de temps CPU.

   Pour un système dynamique qui est plus gros/complexe, les écarts en performance sont
   moindres et pourront être généralement compensées par l'utilisation d'un schéma adaptatif.

   Dans le cas-test sdns01a, employer un DEVOGE adaptatif permet par exemple de retomber aux
   chiffres de l'ordre de 0.15s. L'autre avantage des schémas adaptatifs est par rapport au
   post-traitement (généralement l'étape qui prend du temps dans un calcul sur base modale) :

   . . . moins de pas calculés = moins de pas à restituer en physique

   Ici donc d’énormes gains sont à prévoir...


   Compte tenu ces arguments je propose de classer cette fiche sans-suite
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE :
VALIDATION
    sdnd104a, sdnl112b, sdld321b, sdld321c, sdns01a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 24474 DU 09/11/2015
AUTEUR : BERRO Hassan
TYPE anomalie concernant Code_Aster (VERSION 13.1)
TITRE
    En version 13.0.11-acccfa1a057f, le cas test sdnl112b échoue sur calibre9
FONCTIONNALITE
   . . . . . .**********
   . . . . . * Problème *
   . . . . . .**********

   Manque de mémoire pour le cas-test sdnl112b (IFS-ITMI) sur calibre9.

   . . . . . .*********
   . . . . . * Origine *
   . . . . . .*********

   Après la refonte d'ITMI, le cas-test sdnl112b a été raffiné en terme de pas d'intégration
   afin d'assurer une précision suffisante multi-machines. Le paramètre concernant la
   mémoire était vraiment juste (512) pour passer le calcul (sans écriture sur disque)..


   . . . . . .************
   . . . . . * Correction *
   . . . . . .************

   Je change le paramètre memory_limit à 1024Mo au lieu de 512Mo.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    sdnl112b
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 24472 DU 07/11/2015
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.4)
TITRE
    Affichage IMPR_RESU
FONCTIONNALITE
   Problème : présence de messages dans le fichier output lors de l'utilisation de MED.
   Lors de la construction des librairies de pré-requis par YAMM le script python utilisé pour Medfichier a été modifié du côté du
   projet Carbone en utilisant la directive enable-mesgerr qui occasionne de nombreux messages dans le fichier output lors des appels
   aux divers fonctions MED depuis Code_Aster.
   On rétablit la construction avec disable-mesgerr et on modifie src/yamm/project/salome/tools/medfichier.py dans la branche AMA_DEV
   de YAMM.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE :
VALIDATION
    cas test hsla304c
NB_JOURS_TRAV  : 0.5
