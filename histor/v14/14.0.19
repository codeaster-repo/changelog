==================================================================
Version 14.0.19 (révision 7f1615baf83c) du 2017-12-06 14:24 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 27165 DU 21/11/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    COEF_MULT dans AFFE_CHAR_MECA/CHAMNO_IMPO
FONCTIONNALITE
   Problème
   --------
   
   AFFE_CHAR_MECA/CHAMNO_IMPO permet d'imposer un champ comme second membre (bêta_i).
   COEF_MULT est le coefficient multiplicatif du champ d'après la documentation contrairement à LIAISON_CHAMNO où les valeurs du champ
   sont les coefficients de la relation linéaire (alpha_i) et COEF_IMPO la valeur imposée au second membre (bêta).
   
   Or sur le test ajouté, avec COEF_MULT égal à 2., le déplacement obtenu n'est pas le double mais la moitié.
   
   
   Correction
   ----------
   
   Dans caimch, on traitait COEF_MULT comme "alpha" (ce qui est cohérent avec le nom COEF_MULT mais pas ce qui est dit dans la
   documentation) alors qu'il faut mettre :
   - 1. pour le coefficient (alpha) de la relation linéaire,
   - `coef_mult * vale` pour le second membre.
   
   COEF_MULT est donc un coefficient sur bêta et devrait donc s'appeler COEF_IMPO pour être cohérent avec les autres conditions de
   AFFE_CHAR_MECA, LIAISON_DDL par exemple.
   
   On le renomme.
   Le fait de renommer évitera aussi d'éventuel résultats faux si des utilisateurs avaient sciemment inversé la valeur du coefficient.
   
   
   Validation
   ----------
   
   On ajoute la modélisation B 'sslv306b' dans laquelle :
   - après avoir appliqué FORCE_FACE à l'extrémité d'une poutre, 
   - on extrait le champ de déplacement sur la face supérieure de la poutre,
   - on applique ce champ de déplacement avec COEF_IMPO=2,
   - on vérifie qu'on obtient un déplacement deux fois plus grand à l'extrémité.
   
   
   Resultats faux
   --------------
   
   Depuis l'introduction de CHAMNO_IMPO en 8.0.8, si on renseignait COEF_MULT différent de 1., les résultats obtenus était faux car on
   appliquait 1/COEF_MULT fois le champ fourni.
   CHAMNO_IMPO a été introduit pour faire du zoom structural et donc souvent avec un coefficient de 1.
   Pour éviter la confusion, le mot-clé s'appelle dorénavant COEF_IMPO.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 8.0.8
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 8.0.8
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.44.01, v3.04.306
VALIDATION
    sslv306b
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27175 DU 29/11/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    PROJ_CHAMP avec des champs aux noeuds : PROL_ZERO ne fait rien
FONCTIONNALITE
   Problème
   --------
   
   Lorsqu'on projette un champ sur un maillage plus grand en utilisant DISTANCE_MAX, le comportement par défaut est de ne pas définir
   de valeurs sur les points hors de la zone de projection.
   
   En activant PROL_ZERO='OUI', on s'attend à ce que sur ces points la valeur 0 soit affectée.
   Ce n'est pas le cas.
   
   
   Correction
   ----------
   
   Première explication : dans op0166 (PROJ_CHAMP), on appelle pjxxch systématiquement avec prol0='NON'.
   Maintenant, on lit le mot-clé PROL_ZERO dans ce cas (dire dans la doc pour quels cas PROL_ZERO est utilisé).
   
   Ensuite dans cnscno, on ne retenait que les équations (couple noeud, composante) où on avait projeté une valeur. Donc si cnscno
   était appelé avec prol0='OUI', on ne prolongeait jamais.
   
   Lors du parcours des valeurs initiales, si le champ n'est pas défini et que prol0=='OUI', on l'initialise à la valeur nulle (selon
   le type).
   
   On vérifie sur zzzz304b qu'on prolonge bien avec des zéros avec PROJ_CHAMP(CHAM_GD + DISTANCE_MAX + PROL_ZERO='OUI').
   
   Le chemin étant différent, zzzz304c vérifie la même chose avec PROJ_CHAMP(RESULTAT).
   
   La plupart des tests XFEM sont alors cassés. Dans xstan2 (MODI_MODELE_XFEM), il ne faut pas appeler cnscno avec prol0='OUI'.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : u4.72.05, v1.01.304
VALIDATION
    verification
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 27082 DU 24/10/2017
AUTEUR : BÉREUX Natacha
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    Plantage erreur développeur BLOC_LAGR
FONCTIONNALITE
   Problème:
   =========
   Lorsqu'on utilise la combinaison ALGORITHME = 'GCPC' et PRE_COND='BLOC_LAGR', on est arrêté par un ASSERT.
   
   Analyse :
   ========
   Théoriquement, BLOC_LAGR pourrait être utilisé avec GCPC (gradient conjugué préconditionné natif de code_aster), mais il faudrait
   des développements supplémentaires. 
   Ces développements n'ont pas été réalisés, mais l'utilisation de BLOC_LAGR avec GCPC n'a pas été interdite. 
   Le calcul finit par échouer brutalement.
   
   Correction:
   ==========
   J'interdis BLOC_LAGR avec GCPC dans le catalogue. L'erreur sera plus claire.
   
   Remarque:
   ========
   Pour utiliser BLOC_LAGR avec un gradient conjugué, il faut utiliser l'implémentation de PETSc de cet algorithme.
   Avec METHODE='PETSC', ALGORITHME='CG', PRE_COND='BLOC_LAGR', le calcul attaché à la fiche fonctionne.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    calcul joint
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25028 DU 06/04/2016
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Couverture de avgrma
FONCTIONNALITE
   La routine avgrma n'est pas validée par un test de la liste vérification. Cette routine est appelée quand on utilise CALC_FATIGUE,
   avec TYPE_CALCUL ='FATIGUE_MULTI' et 'OPTION' = 'DOMA_ELGA' pour un chargement non périodique.
   
   Elle est couverte par sslv135b sslv135e sslv135f qui sont dans la base de validation. Ces trois cas-tests sont trop longs.
   J'ai donc créé une nouvelle modélisation du cas-test : sslv135i. Cette modélisation reprend le cas élastique traité dans sslv135f et
   permet de couvrir la routine avgrma.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : V3.04.135
VALIDATION
    cas test sslv135i
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26503 DU 29/05/2017
AUTEUR : POTAPOV Serguei
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TMA : Necs
TITRE
    En version 13.3.22 les cas tests plexu07a, plexu10e et plexu10f sont NOOK sur Eole
FONCTIONNALITE
   Travail effectué :
   ================
   On ajoute des TOLE_MACHINE dans les 3 tests concernés en précisant que la variabilité 
   vient d'Europlexus et non de code_aster et en citant le numéro de cette fiche.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    plexus07a, plexu10e, plexu10f

--------------------------------------------------------------------------------
RESTITUTION FICHE 26292 DU 20/03/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    couverture de INFO_FONCTION/NORME/FONCTION
FONCTIONNALITE
   Problème :
   ----------
   Suite à la restitution de [#25152] Delete operators GENE_MATR_ALEA and GENE_VARI_ALEA, les mots clés suivants ne sont plus testés :
   INFO_FONCTION/NORME/FONCTION
   
   
   Solution :
   ----------
   J'ajoute un appel à INFO_FONCTION/NORME/FONCTION dans zzzz100a et j'ajoute un TEST_TABLE pour vérifier le résultat.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz100a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26761 DU 27/07/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Bug dans CREA_LIB_MFRONT
FONCTIONNALITE
   Problème :
   ----------
   Quand on se trompe dans CREA_LIB_MFRONT en inversant les unités logiques entre UNITE_MFRONT et UNITE_LIBRAIRIE, on a le message
   (surprenant) : ImportError: No module named sympy
   
   
   Solution :
   ----------
   Cela est dû au fait que dans CREA_LIB_MFRONT, en cas d'absence du fichier de sortie de MFront, je faisais un raise seul.
   
   Une autre chose n'était pas correctement programmé dans cette macro : Je n'utilisais pas le module UniteAster pour faire le lien
   entre un mot-clé UNITE et un nom de fichier.
   
   Je modifie donc ces 2 éléments. Maintenant dans cette situation, l'utilisateur aura le message suivant :
   """
   Le fichier de sortie de MFront libAsterBehaviour.so n'a pas pu être produit.
   
   Conseil : Vérifiez l'existence du fichier fourni au mot-clé UNITE_MFRONT.
   """
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    mfron02c modifie
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26843 DU 30/08/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    [FORUM] Problème lecture maillage MED
FONCTIONNALITE
   Problème :
   ----------
   Le maillage ci-joint provoque un plantage dans aster à la lecture des groupes.
   
   
   Solution :
   ----------
   Le problème vient du fait que je n'avais pas prévu qu'un nœud ou qu'une maille pouvaient être attachés à une famille MED qui
   elle-même n'était liée à aucun groupe.
   
   Concrètement, ça veut dire que ce nœud ou cette maille n'est en fait pas dans un groupe. J'avais mis un assert parce que pour moi,
   ça voulait dire qu'il devait y avoir un problème. Mais en fait, non.
   
   Je modifie lrmmfa.F90 pour que cela ne provoque pas de plantage.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    maillage joint
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27047 DU 17/10/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    CALC_CHAMP ne recalcule pas bien les REAC_NODA
FONCTIONNALITE
   Problème :
   ----------
   Dans le fichier de commandes joint, on produit un champ de déplacement.
   
   On réalise ensuite 2 CALC_CHAMP pour produire des REAC_NODA sur 2 groupes différents. Lors du 2ème CALC_CHAMP, le champ de réactions
   est nul.
   
   Or, si on commente le 1er CALC_CHAMP et que l'on effectue le 2ème CALC_CHAMP, le champ de réactions n'est pas nul.
   
   Il semble que le re-calcul des réactions nodales n'est pas bien réalisé.
   
   
   Analyse :
   ---------
   Le premier CALC_CHAMP est réalisé sur un groupe de maille, il déclenche le calcul de SIEF_ELGA. On se retrouve avec un champ
   incomplet géométriquement parlant.
   
   Au deuxième CALC_CHAMP, on ne recalcule pas SIEF_ELGA puisqu'il est présent dans la sd_resultat. Le calcul des réactions nodales se
   fait donc à partir d'un champ SIEF_ELGA incomplet.
   
   
   Solution :
   ----------
   Dans calcop lorsqu'une option est demandée par l'utilisateur, elle est sauvegardée sur la base globale. Par contre si on déclenche
   le calcul d'un champ, celui-ci est ajouté sur la volatile.
   
   Malheureusement, ici le calcul de SIEF_ELGA est déclenché par ccfnrn.F90 (qui est la routine dédiée au calcul de forces et réactions
   nodales). Ce champ est ajouté sur la base globale.
   
   Ce qui nous sauve ici, c'est que ce calcul de SIEF_ELGA passe par calcop. J'ajoute donc un argument à calcop pour permettre de
   préciser si on veut que le calcul demandé doit être mis sur la globale ou sur la volatile.
   
   En demandant le calcul sur la base volatile, on ne garde pas SIEF_ELGA et lorsqu'on revient dans CALC_CHAMP, le recalcul correct est
   effectué.
   
   Résultats faux : Lorsque l'utilisateur réalise 1 CALC_CHAMP pour REAC_NODA sur un groupe de nœuds ou de mailles à la suite d'un
   premier CALC_CHAMP de la même option mais sur un autre groupe de nœuds ou de mailles, le champ obtenu dans le deuxième cas peut
   potentiellement être nul.
RESU_FAUX_VERSION_EXPLOITATION    :  OUI   DEPUIS : 11.0
RESU_FAUX_VERSION_DEVELOPPEMENT   :  OUI   DEPUIS : 11.0
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test joint
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26852 DU 01/09/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    CREA_LIB_MFRONT en parallèle
FONCTIONNALITE
   Problème :
   ----------
   L'utilisateur a tenté d'effectuer un calcul parallèle avec une loi non native Mfront. Il utilise la macro CREA_LIB_MFRONT pour
   générer la librairie mais Le calcul s'arrête en erreur car il ne connait pas la loi.
   
   
   Solution :
   ----------
   Le problème vient du fait qu'il manque le répertoire courant dans le LD_LIBRARY_PATH.
   
   En suivant le correctif proposé par Mathieu, je modifie profile.sh.tmpl pour ajouter "." au LD_LIBRARY_PATH.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    CREA_LIB_MFRONT en parallele
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 25989 DU 24/01/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Problème avec le mot-clef SALOME/EXEC_LOGICIEL
FONCTIONNALITE
   Problème :
   ========
   
   Aucun test ne passe dans la commande EXEC_LOGICIEL/SALOME
   
   Analyse :
   =======
   
   Pour pouvoir utiliser le mot clé SALOME sous EXEC_LOGICIEL, il est nécessaire qu'une instance de Salome soit lancée sur le seveur où
   on veut faire passer le test. 
   
   Correction :
   ==========
   
   On enrichit le test zzzz151a avec un fichier de commande supplémentaire pour :
     1 - lancer une instance de Salome par un os.system et récupérer le port
     2 - lancer la commande EXEC_LOGICIEL mot clé SALOME pour créer un maillage (on reprend forma01d.datg)
     3 - on arrête proprement Salome 
     4 - on relit le maillage et on teste quelques valeurs.
   
   On corrige au passage bibpyt/med2.py : le message d'alarme 8 est incomplet.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    zzzz151a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27172 DU 27/11/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Contact et ssnv504b
FONCTIONNALITE
   Problème
   ========
   
   Dans ssnv504b, lorsqu'on modifie REAC_GEOM = 'CONTROLE' par REAC_GEOM    = 'AUTOMATIQUE', le test se met à avoir des difficultés à
   converger et il finit par s'arrêter en écrasement mémoire aval.
   
   Solution
   ========
   
   L'écrasement aval ne vient pas d'un objet mal dimensionné mais par le fait que le contact discret dans ce mode ne s'arrête pas en
   erreur fatale lorsqu'on a atteint le maximum d'itération de point fixe de géométrie.
   On corrige cfconv.F90
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssnv504b
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 27160 DU 20/11/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    [FORUM] Bug LIAISON_SOLIDE en grandes rotations
FONCTIONNALITE
   Problème
   ========
   
   Un utilisateur du forum utilise LIAISON_SOLIDE sur une configuration aprticulière.
   Le message d'erreur est peu explicite.
   
   Solution
   ========
   
   1/ Le cas considéré est effectivement exclu des cas traité par LIAISON_SOLIDE (cas 2DA et 3DA dans la doc R3.03.02).
   => on modifie le message d'erreur pour expliquer. Le texte dans R3.03.02 est franchement pas clair
   
   2/ La modélisation proposée ne peut pas fonctionner
   C'est un modèle C_PLAN qu'on cherche à faire tourner rigidement autour de DRZ. L'idée de l’utilisateur était de rigidifier la
   section par LIAISON_SOLIDE avec un discret en rotation. Puis faire tourner le discret.
   En 2D, ce modèle n'est pas possible.
   On avait "découvert" il y a quelques années que la rotation rigide d'une section n'était pas non-linéaire (c'est le test dit de la
   "machine à laver", ssnv507).
   EN modifiant le fichier de l'utilisateur, on arrive à faire tourner de manière rigide.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : r3.03.02
VALIDATION
    le test joint
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26571 DU 09/06/2017
AUTEUR : GRANET Sylvie
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    D111.17 - Incohérences sur saturation/pression
FONCTIONNALITE
   *On modifie la doc de defi_materiau afin de faire référence à LIQU_AD_GAZ 
   
   * Pour le couplage LIQU_VAPE et LIQU_GAZ_ATM on interdit via  DEFI_MATERIAU le couplage HYDR_VGM/VGC. Pour LIQU_GAZ_ATM, cela se
   justifie car VGM ou VGC impliquent une perméabilité relative au gaz qui n'a pas de sens ici.
   On pourra néanmoins se poser la question pour le future d'autoriser cette loi de couplage (pour la partie succion) ou non
   (éventuellement faire une EL si c'est pertinent). 
   On modifie les doc de DEFI_MATERIAU ainsi que la doc d'utilisation de la THM U2.04.05.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : U2.04.05, U4.43.01
VALIDATION
    passage cas test
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 27125 DU 07/11/2017
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 13.8)
TITRE
    DYNA_VIBRA avec plusieurs EXCIT_RESU en transitoire
FONCTIONNALITE
   Problème :
   ==========
   Actuellement, dans le cas transitoire en base physique, DYNA_VIBRA empêche d'utiliser plus d'un EXCIT_RESU. 
   
   Solution proposée :
   ===================
   Le verrou sur le nombre de mot clés EXCIT_RESU étant dans le catalogue, ce verrou est supprimé et une validation est faite sur le
   cas test sdls119a.
   Cette validation consiste à appeler deux fois le même EXCIT_RESU en mettant COEF_MULT = 0 dans le deuxième appel.
   On vérifie aussi dans sdls140a la présence en compte de plusieurs occurrences.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdls119a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26857 DU 04/09/2017
AUTEUR : ALVES-FERNANDES Vinicius
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.5, les tests zzzz412a zzzz412b zzzz412c et zzzz412d sont NOOK sur clap0f0q et sur Eole
FONCTIONNALITE
   Problème:
   -------------
   
   Tests zzz412a,b,c,d en NOOK sur clap0f0q et eole, avec une erreur sur les tests de non régression de l'ordre de 4.E-4%
   
   Correction:
   --------------
   
   On modifie la valeur de la raideur du cerclage utilisé pour les conditions aux limites du calcul de colonne de sol 1D - 3
   composantes. La valeur retenue est de 5.E+13 à la place de 1.E+15 initialement utilisée (valeur prise à partir des calculs de
   massifs de sol 3D, note H-T64-2016-00659-FR). 
   
   
   Validation
   -------------
   
   La valeur de raideur du cerclage proposée permet d'atteindre le critère d'erreur de non régression pour l'ensemble de tests (1.E-4%)
   sur calibre 9, eole et clap0f0q.
   
   Cette modification requiert la mise à jour des valeurs de non régression, de l'ordre de la variabilité machine observée.
   
   Impact documentaire:
   -----------------------
   
   Mise à jour des valeurs de non régression de V1.01.412 (pas encore soumis dans la base documentaire)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas tests
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 27118 DU 06/11/2017
AUTEUR : COURTOIS Mathieu
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.14, le cas test sdnv142b est CPU_LIMIT et NOOK sur AthosDev_Valid
FONCTIONNALITE
   Le test sdnv142b est très instable soit en arrêt CPU, soit NOOK sur les machines de validation, et parfois OK.
   De plus, la modélisation B de ce test n'a jamais été documentée en 2 ans.
   
   On résorbe le test (on arrête de l'exécuter toutes les semaines).
   L'historique de la base de validation permettra de le restaurer dans le cas où ce serait nécessaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnv142b
NB_JOURS_TRAV  : 0.2

--------------------------------------------------------------------------------
RESTITUTION FICHE 27171 DU 27/11/2017
AUTEUR : FILIOT Astrid
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.17-f3ca68e5d9a8, le cas test ssnv248a est NO_TEST_RESU sur AthosDev_Valid
FONCTIONNALITE
   Problème 
   ========
   
   En version 14.0.17-f3ca68e5d9a8, le cas test ssnv248a est NO_TEST_RESU sur AthosDev_Valid.
   Il n'y a pas des TEST_RESU avec des valeurs de REFERENCE: il y a seulement des TEST_RESU sur des VALE_CALC qui ne sont pas testés
   pour les tests de validation.
   
   Solution
   ========
   
   On rajoute des VALE_REFE dans les TEST_RESU de ssnv248a (REFERENCE='AUTRE_ASTER'), avec les mêmes valeurs que dans VALE_CALC.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    validation
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 26394 DU 18/04/2017
AUTEUR : ABBAS Mickael
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Utilisation de THER_NON_LINE_MO
FONCTIONNALITE
   Problème
   ========
   
   Dans le cas de l'opérateur THER_NON_LINE_MO (doc réf. U4.54.03), nous appliquons via un 
   AFFE_CHAR_THER_F :
        - un champ de vitesse sur l'ensemble du solide,
        - un flux de chaleur fonction de la température --> FLUX_NL afin de modéliser un impact de 
   laser sur une zone surfacique du solide
   
   Le résultat en fichier med semble complètement erroné (cf. capture d'écran de Paravis dans le 
   fichier zippé). Nous obtenons des températures comprises entre 0 et 1,5°C alors que le solide était 
   à 20°C avec une zone chauffée au laser.
   
   Solution
   ========
   
   THER_NON_LINE_MO résout un problème de thermique avec convection en stationnaire.
   En particulier, il est impératif que les conditions limites soient cohérentes avec la source de chaleur imposée pour que l'équation
   soit correctement résolue (c'est bien expliqué dans la doc R mais surtout dans la doc U4).
   C'est possible dès lors que le chargement est en translation uniforme puisque les faces d'arrivée et de départ sont faciles à
   identifier.
   
   Dans le cas considéré (géométrie de type cylindrique pour un rotor), je pense qu'il est très compliqué de trouver ces conditions
   limites.
   En l'occurrence, ici, elles sont mal définies et le problème n'est pas résolu correctement, d'où les résultats aberrants.
   
   Malheureusement, je pense que THER_NON_LINE_MO dans son état actuel ne peut pas résoudre le problème proposé ("tache" laser sur un
   rotor tournant) et qu'il faut résoudre le problème en transitoire "pur" (chaînage pour modifier le maillage, aps mal de Python à
   prévoir)
   
   Il n'y a donc pas de bug.
   
   NB: le problème est vraiment difficile et je pense pas être suffisamment compétent sur le sujet mais je suis à peu près certain
   qu"il n'y a pas de bug
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    le test lui-même
NB_JOURS_TRAV  : 5.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26627 DU 22/06/2017
AUTEUR : GRANET Sylvie
TYPE aide utilisation concernant Code_Aster (VERSION 11.8)
TITRE
    Problème de calcul de flux hydrique (LIQU_SATU)
FONCTIONNALITE
   Suite à échange mail de Juillet 2017 :
   Un flux, ne peut être imposé sur un bord. Imposer un flux sur un nœud n'a pas de sens car il s'agit d'un flux surfacique en kg/s/m2 
   Pour post-traiter il faut regarder directement les composants au point de Gauss FH11X et FH11Y.
   Il est également possible d'interpoler (si le maillage est assez fin) et d'intégrer sur une surface :  voir ce qui est fait dans le
   test wtnp127 par exemple.
   Je clos cette fiche. En cas de de questions complémentaires ne pas hésiter à émettre une nouvelle fiche.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage étude
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26495 DU 26/05/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    [perform] Plantage jeveux1_55
FONCTIONNALITE
   Problème :
   ----------
   Dans l'étude jointe, en utilisant MATR_DISTRIBUEE='OUI' dans STAT_NON_LINE, je tombe sur un message d'erreur développeur JEVEUX1_55.
   
   
   Solution :
   ----------
   La fiche issue27146 va interdire la méthode LAC avec matrice distribuée. Cela résout donc ce problème de cette étude qui utilise ces
   2 fonctionnalités.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    aucune
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 26840 DU 29/08/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    LIRE_RESU
FONCTIONNALITE
   Problème :
   ----------
   L'utilisateur édite sous python un fichier MED. Il utilise ensuite une fonction python utilisant MEDCoupling qui produit un champ
   visualisable sous Salomé mais dont la lecture est impossible sous Aster avec LIRE_RESU.
   
   
   Analyse :
   ---------
   Il s'agit d'une erreur d'utilisation. L'utilisateur a écrit le mot-clé NOM_CHAM_MED à la place de NOM_CMP_MED.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    aucune
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 26591 DU 13/06/2017
AUTEUR : SELLENET Nicolas
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Erreur MED dans l'impression d'un résultat avec PMF
FONCTIONNALITE
   Problème :
   ----------
   L'utilisateur a obtenu une erreur MED en voulant imprimer un résultat contenant des poutres multi-fibres dans le cas-test SSLL11F.
   S'il restreint l'impression au champ 'DEPL', cela fonctionne.
   
   
   Solution :
   ----------
   Aujourd'hui, l'impression fonctionne sans problème. Cela est sans doute dû aux corrections apportées en début d'années à
   l'impression en présence des sous-points.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test joint
NB_JOURS_TRAV  : 0.01

--------------------------------------------------------------------------------
RESTITUTION FICHE 27104 DU 31/10/2017
AUTEUR : POTAPOV Serguei
TYPE anomalie concernant Code_Aster (VERSION 13.4)
TITRE
    En version 14.0.14 et 13.4.9, les cas tests plexu01a, plexu02a, plexu02b, plexu03a, plexu03b, plexu03c, plexu04a, plexu05a, plexu06a, plexu07a, plexu08a, plexu08b, plexu08c, plexu08d, plexu08e, plexu08f, plexu08g, plexu09a, plexu09b, plexu10a, plexu10b, plexu10d, plexu10e, plexu10f, plexu11a, plexu11b échouent sur Aster5
FONCTIONNALITE
   Problème
   --------
   
   En version 14.0.14 et 13.4.19, les cas test plexu* échouent sur Aster5 depuis le redémarrage d'Aster5.
   Ces échecs sont liés à la version d'Europlexus.
   
   Analyse
   -------
   
   Il n'est pas prévu dans le cadre de FAST d'installer la version stabilisée 2017 d'EPX sur Aster5, l'installation et la qualification
   de cette version pour SEPTEN étant faites sur EOLE. SEPTEN s'apprête à mettre en exploitation cette version sur EOLE. Ils n'ont pas
   exprimé de besoin d'avoir également la version EPX 2017 sur Aster5.
   
   Action
   ------
   
   On exclut désormais CALC_EUROPLEXUS du périmètre vérifié sur Aster5 : les cas-tests plexu* sont donc retirés du bilan quotidien de
   passage des tests.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 26904 DU 18/09/2017
AUTEUR : MARCHENKO Arina
TYPE evolution concernant Documentation (VERSION 11.4)
TITRE
    RUPT : Nouvelle doc R Beremin
FONCTIONNALITE
   Doc R7.02.04, R7.02.06 et U2.05.08, relues, validées et publiées
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : r7.02.04,r7.02.06,u2.05.08
VALIDATION
    sans objet
NB_JOURS_TRAV  : 20.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 27002 DU 09/10/2017
AUTEUR : DE SOZA Thomas
TYPE aide utilisation concernant Code_Aster (VERSION 12.8)
TITRE
    Problème de fonctionnement de CALC_EUROPLEXUS
FONCTIONNALITE
   ++++++++++++
   + Contexte +
   ++++++++++++
   
   '''
   Problèmatique:
   ==============
   
   Convertir un fichier de données Code Aster en fichier de données Europlexus via la commande CALC_EUROPLEXUS.
   
   Message d'erreur:
   =================
   
   Le maillage ne fait qu'environ 100000 nœuds.
   En 12.8, on obtient l'erreur suivante même après 5 heures de calcul:
   
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   ! Exception utilisateur levee mais pas interceptee. !
   ! Les bases sont fermees. !
   ! Type de l'exception : ArretCPUError !
   ! !
   ! ARRET PAR MANQUE DE TEMPS CPU !
   ! Les commandes suivantes sont ignorées, on passe directement dans FIN !
   ! La base globale est sauvegardée 
   !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! 
   
   En 13.4 :
   CALC_EUROPLEXUS(...
   COMPORTEMENT=_F(RELATION='ELAS',
   			GROUP_MA='TOUT',),...);
   
   Le GROUP_MA 'TOUT' n'est pas défini par défaut, et en copiant tous les groupes un par un, on réobtient la lenteur excessive (ou le
   problème) rencontré en version 12.8
   
   Analyse de l'AT:
   ================
   
   La commande CALC_EUROPLEXUS convertit un jeu de donnée Code Aster en jeu de donnée Europlexus.
   Il serait surprenant que cette conversion de fichiers puisse prendre plus de 30 minutes.
   L'étude test de l'erreur est jointe.
   '''
   
   +++++++++++
   + Analyse +
   +++++++++++
   
   Remarque préliminaire
   =====================
   
   Le profil ASTK transmis active une option utilisée par les développeurs pour le débogage (Menu Options > debugjeveux), ce qui a pour
   conséquence de dégrader très fortement les performances.
   ==> il faut absolument la retirer.
   
   Observations
   ============
   
   1. En version 13, la documentation de CALC_EUROPLEXUS indique qu'il ne faut pas utiliser d'INCLUDE pour définir les commandes qui
   seront transcrites en instructions Europlexus (U7.03.10, page 5)
   ==> par précaution, je mets toutes les commandes au premier niveau.
   
   2. Avec les modifications ci-dessus, le temps de calcul en version 13 reste très important. En regardant les affichages et ce que
   fait CALC_EUROPLEXUS, on se rend compte que la partie qui bloque concerne la transcription du modèle (AFFE_MODELE). La programmation
   actuelle contient de multiples boucles imbriquées pour cette partie, dont la première d'entre elles concerne la taille de la liste
   fournie derrière AFFE_MODELE/AFFE/GROUP_MA. C'est une première explication du temps de calcul important car dans le fichier de
   commandes fourni, il y a près de 10000 groupes de mailles pour l'affectation des éléments plaques, poutres, barres et discrets.
   ==> il faut donc faire les affectations sur un seul groupe de mailles.
   
   3. En créant des groupes de mailles 'plaque', 'poutre', 'barre' avec DEFI_GROUP (pour les discrets ce n'est pas possible car il doit
   y avoir bijection entre l'affectation AFFE_MODELE et AFFE_CARA_ELEM), le calcul avance nettement plus vite mais finit par planter
   avec l'erreur suivante au bout d'environ six heures CPU :
   
      !---------------------------------------------------------------------------!
      ! <EXCEPTION> <PLEXUS_19>                                                   !
      !                                                                           !
      ! Le type de charge VERI_NORM n'est pas pris en compte par CALC_EUROPLEXUS' !
      !---------------------------------------------------------------------------!
   
   4. J'ai désactivé le mot-clé VERI_NORM='NON' dans AFFE_CHAR_MECA et relancé.
   
   Résultats
   =========
   
   Le calcul "optimisé" aboutit :
   - il a pris 5h30 ;
   - il émet trois types d'alarmes différentes à contrôler : 
   ...* un noeud orphelin,
   ...* 845 mailles incluses les unes dans les autres,
   ...* paramètre ALPHA (coefficient de dilatation thermique) présent dans 58 matériaux et non transcrits dans le jeu de données EPX ;
   - VERI_NORM='NON' a été désactivé dans AFFE_CHAR_MECA (mais c'est sans conséquence).
   
   Le jeu de données EPX ainsi produit fait environ 150000 lignes.
   
   Remarque : le maillage généré par CALC_EUROPLEXUS avec l'extension .msh est en fait un maillage au format MED que l'on peut ouvrir
   avec Salome-Meca.
   
   ++++++++++++++
   + Conclusion +
   ++++++++++++++
   
   Le calcul reste donc long par rapport aux temps de 30 minutes annoncés par le passé mais il faudrait regarder quels étaient les
   fichiers qui ont été transcrits en un temps si court. En effet, le temps passé ici est essentiellement dû à la transcription de
   AFFE_CARA_ELEM qui constitue l'essentiel de l'étude.
   ==> je n'ai pas essayé de désactiver AFFE_CARA_ELEM (ou d'en faire un bidon très simplifié) mais je pense que la transcription
   prendra alors une vingtaine de minutes.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    étude jointe
NB_JOURS_TRAV  : 1.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23855 DU 28/05/2015
AUTEUR : COURTOIS Mathieu
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    OMARISI2016 - Dossier V&V DHRC - SMART2013
FONCTIONNALITE
   La modélisation B du test sdnv142 n'est pas documentée.
   Le test est en erreur (dernière des 8 fiches: issue27118).
   On supprime cette modélisation.
   
   On résorbe le test (on arrête de l'exécuter toutes les semaines).
   L'historique de la base de validation permettra de le restaurer dans le cas où ce serait nécessaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    cas test
DEJA RESTITUE DANS : 13.0.11
NB_JOURS_TRAV  : 20.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 23854 DU 28/05/2015
AUTEUR : FAYOLLE Sebastien
TYPE evolution concernant Code_Aster (VERSION )
TITRE
    OMARISI2016 - Dossier V&V DHRC - P4
FONCTIONNALITE
   La modélisation B du test sdnv140 n'est pas documentée.
   Le test est en erreur depuis février 2016 (issue24804).
   On supprime cette modélisation en vertu des règles d'exploitation de la base de validation.
   
   À noter : on résorbe le test (on arrête de l'exécuter toutes les semaines).
   L'historique de la base de validation permettra de le restaurer dans le cas où ce serait nécessaire.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  OUI
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
DEJA RESTITUE DANS : 13.0.9
NB_JOURS_TRAV  : 0.5

