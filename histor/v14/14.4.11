==================================================================
Version 14.4.11 (révision c4e9c319b306) du 2019-11-19 07:40 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29250 ABBAS Mickael            En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur
 28982 COURTOIS Mathieu         En version 14.3.14, le cas test ssnv147b est NOOK sur clap0f0q
 29008 GUILLOUX Adrien          LIST_FREQ pris partiellement en compte seulement dans CALC_FONCTION/LISS_ENVELOP
 29296 ABBAS Mickael            En version 15.0.14 le cas-test mfron01j est en ERROR sur Calibre 9 et Scibian9
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29250 DU 28/10/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.12 sur Gaia, le test hsnv101d s'arrête en erreur.
  | 
  | 
  | Correction
  | ----------
  | 
  | Les routines nzcifw, nzisfw, nzedga et nzcizi ont toutes le même défaut.
  | Au début de ces routines, les variables suivantes sont initialisées à zéro:
  | vip (variables internes)
  | dsidep (matrice tangente)
  | sigp (contraintes)
  | 
  | Or l'accès à ces vecteurs n'existe pas vraiment lors de la prédiction (qui ne calcule que la matrice) et lors de RAPH_MECA (qui ne
  | calcule que vip/sigp)
  | 
  | Pour se "protéger" d'un risque d'écrasement, on avait l'habitude de transférer un vecteur de réels basé sur l'adresse "1" dans ZR
  | (common JEVEUX). Voir par exemple le te0139 et te0100. C'était bizarre et globalement peu prudent !
  | 
  | Apparemment, sur Gaia, le comportement est différent, un tel accès est interdit. Il faut donc initialiser ces vecteurs seulement
  | dans les cas où c'est nécessaire, sous risque d'un vilain accès mémoire défaillant.
  | 
  | Il est FORTEMENT probable que d'autres lois de comportement soient aussi peu prudentes. En fait, paradoxalement, c'est l'excès
  | d’initialisations qui provoque un problème).
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Cet écrasement mémoire peut produire des résultats faux.
  | 
  | Configuration:
  | Lois META_*_* pour l'acier et le zircaloy

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 15.0.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.0.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : hsnv101d
- DEJA RESTITUE DANS : 15.0.15
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28982 DU 15/07/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 14.3.14, le cas test ssnv147b est NOOK sur clap0f0q
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Le test est actuellement NOOK sur Gaia en v15 et clap0f0q en v14.
  | Dans issue28710, la machine clap0f0q n'avait pas été prise en compte.
  | 
  | 
  | Correction
  | ----------
  | 
  | En passant la précision du calcul (RESI_GLOB_RELA) à 2E-7 et en mettant la valeur d'Eole
  | en référence, le test est OK sur toutes les machines.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssnv147b
- DEJA RESTITUE DANS : 15.0.15


================================================================================
                RESTITUTION FICHE 29008 DU 26/07/2019
================================================================================
- AUTEUR : GUILLOUX Adrien
- TYPE : anomalie concernant code_aster (VERSION 14.3)
- TITRE : LIST_FREQ pris partiellement en compte seulement dans CALC_FONCTION/LISS_ENVELOP
- FONCTIONNALITE :
  | Le problème portait sur des problèmes de précision, notamment à cause d'aller retour entre les domaines lin et log. Les 
  | modifications nécessaires ont été apportées dans la prise en compte de FREQ_MIN/FREQ_MAX et dans le calcul de l'enveloppe. 
  | La prise en compte des arrondis à travers la gestion d'une précision déjà préalablement présente mais incomplète a été 
  | étendue.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas stagiaire
- DEJA RESTITUE DANS : 15.0.15
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29296 DU 13/11/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : En version 15.0.14 le cas-test mfron01j est en ERROR sur Calibre 9 et Scibian9
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.14 le cas-test mfron01j est en ERROR sur Calibre 9 et Scibian9 (*.mess en copie)
  | 
  | 
  |    !--------------------------------------------------------------!
  |    ! <F> <DVP_2>                                                  !
  |    !                                                              !
  |    ! Erreur numérique (floating point exception).                 !
  |    !                                                              !
  |    ! --------------------------------------------                 !
  |    ! Contexte du message :                                        !
  |    !    Option         : FULL_MECA_ELAS                           !
  |    !    Type d'élément : MEDKQU4                                  !
  |    !    Maillage       : MAIL_B                                   !
  |    !    Maille         : M4                                       !
  |    !    Type de maille : QUAD4                                    !
  |    !    Cette maille appartient aux groupes de mailles suivants : !
  |    !       TOUT_ELT SURF                                          !
  |    !    Position du centre de gravité de la maille :              !
  |    !       x=0.344715 y=0.185615 z=0.075000                       !
  |    !                                                              !
  |    !                                                              !
  |    ! Cette erreur est fatale. Le code s'arrête.                   !
  |    ! Il y a probablement une erreur dans la programmation.        !
  |    ! Veuillez contacter votre assistance technique.               !
  |    !--------------------------------------------------------------!
  | 
  | 
  | Correction
  | ----------
  | 
  | Le problème vient de la préparation des déformations élastiques dans Mfront, on a dans mfrontPrepareStrain:
  | 
  | if ((neps .eq. 6) .or. (neps .eq. 4)) then
  |    call dscal(3, rac2, dstran(4), 1) 
  |    call dscal(3, rac2, stran(4), 1)
  | endif
  | 
  | avec stran(neps), dstran(neps) en entrée de la routine
  | 
  | Dans ce test, on est en DKT (==contraintes planes), donc stran/dstran sont de longueur 4.
  | Ce n'est pas une bonne idée de faire un dscal de stran/dstran(4:6) quand stran/dstran sont de longueur 4 ...
  | 
  | 
  | On distingue les deux cas (neps=6 et neps = 4)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mfron01f
- NB_JOURS_TRAV  : 0.5

