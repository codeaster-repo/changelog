==================================================================
Version 14.2.14 (révision bfbae6e11593) du 2018-10-18 21:10 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 28122 COURTOIS Mathieu         Mise à jour de waf
 28106 LEFEBVRE Jean-Pierre     floating point exception avec CREA_CHAMP
 28041 LEFEBVRE Jean-Pierre     Faire la montée de version de PETSc 3.9.4
 28097 ABBAS Mickael            Message d'erreur pas clair
 28128 FLEJOU Jean Luc          [Adelahyd] Maintenance loi BETON_RAG
 28123 COURTOIS Mathieu         CREA_TABLE : les noms de paramètres sont troqués à 16 caractères
 28120 COURTOIS Mathieu         CREA_RESU/MULT_ELAS crée toujours un nouveau cas
 27751 BOITEAU Olivier          Expertise HPC et optimisation sur étude CBNA 3x3x3 (brique fissurée dans co...
 27740 BOITEAU Olivier          Problèmes avec un très gros modèle
 28090 GEOFFROY Dominique       [AOM] - HAYHURST - no convergence - Projet Accidents Graves
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 28122 DU 09/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Mise à jour de waf
- FONCTIONNALITE :
  | Mise à jour de waf en 2.0.12 pour rester au contact des évolutions récentes et bénéficier des mises à jour faites pour Python3.
  | 
  | Peu de modifications nécessaires :
  | 
  | - changement du défaut de PREFIX ('/' au lieu de '')
  | 
  | - add_os_flags : dup=False est la nouvelle valeur par défaut.
  | 
  | Script 'waf.engine' construit avec '--tools=boost,use_config'.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : build
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28106 DU 03/10/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : floating point exception avec CREA_CHAMP
- FONCTIONNALITE :
  | Problème
  | --------
  | L'étude en pièce-jointe présente une erreur type "floating point exception" dans la commande CREA_CHAMP si on la lance sur aster5 ou
  | eole. Par contre, l'erreur n'apparaît sur les versions Calibre9 station. 
  | 
  | Voici la commande qui présente l'erreur :
  | 
  | GAMMX_0 = CREA_CHAMP(INFO=1,
  |                      TYPE_CHAM='NOEU_EPSI_R',
  |                       OPERATION='EXTR',
  |                       RESULTAT=DYNTEMP,
  |                       NOM_CHAM='EPSI_NOEU',
  |                       TYPE_MAXI='MAXI_ABS',
  |                       LIST_INST=L_INSTh,
  |                       PRECISION=1.E-06,
  |                       TYPE_RESU='VALE',
  |                       CRITERE='RELATIF',)
  | 
  |  
  | !-------------------------------------------------------!
  | ! <EXCEPTION> <DVP_2>                                   !
  | !                                                       !
  | ! Erreur numérique (floating point exception).          !
  | !                                                       !
  | !                                                       !
  | !                                                       !
  | ! Il y a probablement une erreur dans la programmation. !
  | ! Veuillez contacter votre assistance technique.        !
  | !-------------------------------------------------------!
  | 
  | Analyse du problème
  | -------------------
  | 
  | Le CREA_CHAMP "MAXI_ABS" est réalisé à partir d'un champ incomplet construit sur des groupes de mailles et non sur la totalité du
  | maillage, des valeurs sont donc restées à NaN. Lors du traitement dans la commande CREA_CHAMP on effectue des comparaisons de
  | valeurs sur l'ensemble des valeurs sur le transitoire donné. Sur les clusters la version est construite avec le compilateur Intel et
  | l'option -fpe0, cela provoque une erreur dès que l'on cherche à faire un test avec une valeur NaN. Sur les versions locales
  | compilées avec gfortran/gcc la comparaison ne provoque pas d'erreur. D'où le comportement différent de l'étude sur les serveurs. 
  | Un fichier de test est fourni en pièce jointe pour montrer la différence de comportement avec les deux compilateurs.   
  | 
  | Correction
  | ----------
  | 
  | Pour éviter cet arrêt brutal, on modifie la routine chmima.F90 en utilisant l'objet .CNSL des cham_no_s et .CESL des cham_elem_s en
  | effectuant uniquement les opérations de comparaison sur les valeurs licites. 
  | Concernant le traitement des cham_elem, l'objet .CESL n'existant pas dans la SD, on effectue la comparaison des valeurs comme
  | auparavant et donc avac risque d'erreur.  
  | Cela affecte les options MAXI, MAXI_ABS, MINI, MINI_ABS.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : étude fournie + tests ssla200b sslp200b sslp201b et sslv200b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28041 DU 17/09/2018
================================================================================
- AUTEUR : LEFEBVRE Jean-Pierre
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Faire la montée de version de PETSc 3.9.4
- FONCTIONNALITE :
  | Objectif
  | --------
  | Montée de version de PETSc
  | 
  | Développement
  | -------------
  | Après avoir tenté d'utiliser sans succès  la version 3.10.1, je propose de mettre à disposition la version 3.9.4 en embarquant superlu. 
  | Le dépôt des pré-requis est mis à jour avec cette nouvelle version et le fichier
  | superlu-a0819410c9eb779f9b296cdd95fbdfd96986ae10.tar.gz est copié sous le répertoire 3rd. 
  | Le fichier yamm/src/yamm/projects/salome_meca/softwares/prerequisitespetsc_mpi.py est mis à jour dans YAMM pour prendre en compte
  | cette version(merge request effectué dans le dépôt //gitlab.pleiade.edf.fr/yamm/yamm).
  | 
  | Concernant le code source, il est nécessaire de modifier waftools/petsc.py et
  | bibfor/matrix/augmented_lagrangian_context_type.F90

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tests petsc
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 28097 DU 01/10/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 13.6)
- TITRE : Message d'erreur pas clair
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | L'erreur suivante est levée: 
  |    !------------------------------------------------------------------------------------------------------------------------------!
  |    ! <EXCEPTION> <DISCRETISATION_2>                                                                                               !
  |    !                                                                                                                              !
  |    !  L'instant initial de la liste est plus grand que le deuxième instant.                                                       !
  |    !  Si vous faites une reprise de calcul (REUSE), vous pouvez utiliser le mot-clef ETAT_INIT/INST_ETAT_INIT pour corriger cela. !
  |    !------------------------------------------------------------------------------------------------------------------------------!
  |    
  | par cette mise en donnée : 
  | 
  | 
  | DEBUT()
  | 
  | INSTS = (0.0, 1.0, 3.0, 3.1, 5.0)
  | 
  | L_R = DEFI_LIST_REEL(VALE = INSTS)
  | 
  | L_I = DEFI_LIST_INST(METHODE = 'MANUEL',
  |                      DEFI_LIST = _F(LIST_INST = L_R),
  |                      ECHEC = _F(SUBD_PAS_MINI = 0.5),
  |                  )
  | 
  | FIN()
  | 
  | où dans la gestion des erreurs on impose un SUBD_PAS_MINI (0.5) supérieur au plus petit pas de la liste (0.1)
  | 
  | 
  | Correction
  | ----------
  | 
  | Le message d'erreur est incorrect. On appelle un numéro de message qui ne correspond pas à la situation. Ce message est réservé au
  | cas des reprises, il n'a rien à avoir avec le cas ici.
  | On change donc le message d'erreur

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : submit
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28128 DU 11/10/2018
================================================================================
- AUTEUR : FLEJOU Jean Luc
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : [Adelahyd] Maintenance loi BETON_RAG
- FONCTIONNALITE :
  | Cas test de validation de BETON_RAG :
  | -------------------------------------
  | 
  | * overflow rencontré : une valeur propre du tenseur de traction légèrement négative, et plouf lors de sa mise à la puissance.
  | Théoriquement et mathématiquement parlant, de part sa construction le tenseur de traction ne peut avoir que des valeurs propres
  | positives. Lors du cas test, il y en a une qui vaut -1.0E-34. Il y a un exponentiel après, il faut également le protéger contre un
  | argument trop grand.
  | 
  | On limite l'endommagement a "0.999955" cela revient à limiter également les "bi" avec l'argument de l'exponentiel à 10.
  | 
  | Actuellement on a :
  |    b1 = exp( (vp1^a)/a )
  | Correction
  |    b1 = exp( min(10, (max(0,vp1)^a)/a) )
  | 
  | Passage en revu des "**" et exp et blindage

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : passage cas tests
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28123 DU 09/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CREA_TABLE : les noms de paramètres sont troqués à 16 caractères
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Si on crée une table avec un nom de paramètres de plus de 16 caractères, il est tronqué.
  | Si deux paramètres ont les 16 premiers caractères en commun, CREA_TABLE s'arrête en erreur avec le message "Les noms de paramètres
  | doivent être différents."
  | 
  | 
  | Correction
  | ----------
  | 
  | Le problème vient uniquement du vecteur de travail et de variables locales qui stockent temporairement les noms de paramètres.
  | Ils sont déclarés en k16 au lieu de k24 comme l'objet '.TBLP' de la SD table.
  | 
  | On valide avec zzzz177a : on ajoute deux paramètres dont les noms ne diffèrent qu'au 17ème caractère.
  | Avant la correction, ça plante.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz177a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28120 DU 08/10/2018
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : CREA_RESU/MULT_ELAS crée toujours un nouveau cas
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Quand on fait CREA_RESU/AFFE sur un mult_elas, on ajoute systématiquement un nouveau cas même si NOM_CAS fait référence à un cas
  | existant.
  | 
  | 
  | Analyse
  | -------
  | 
  | Sous CREA_RESU/AFFE pour un mult_elas, il n'y a pas de mot-clé NUME_ORDRE.
  | Si on ne renseigne pas NOM_CAS, un nouveau numéro d'ordre est ajouté, c'est normal.
  | Si on renseigne NOM_CAS avec un cas qui n'existe pas déjà, idem.
  | 
  | Si on renseigne NOM_CAS avec un nom existant, on s'attend à ce que le numéro d'ordre existant soit enrichi du champ affecté.
  | 
  | Dans crtype.F90, on ne traitait pas le cas de mult_elas (seule une exception est faite pour mode_meca, mais pas mode_meca_c).
  | 
  | 
  | Correction
  | ----------
  | 
  | Dans le cas d'un mult_elas, si NOM_CAS est renseigné et que le cas existe déjà dans la sd, on utilise ce numéro d'ordre.
  | Ensuite, si le champ existe déjà dans la sd, on émet l'alarme qui prévient de l'écrasement. Sinon, le champ est ajouté comme attendu.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ssll101c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27751 DU 11/06/2018
================================================================================
- AUTEUR : BOITEAU Olivier
- TYPE : aide utilisation concernant code_aster (VERSION 14.2)
- TITRE : Expertise HPC et optimisation sur étude CBNA 3x3x3 (brique fissurée dans coeur graphite AGR) pour EDF UK
- FONCTIONNALITE :
  | AOM pour une étude sur les briques de graphite AGR
  | ==================================================
  | 
  |   SYNTHESE
  |   ~~~~~~~~
  |       En optimisant le calcul sur 1 pas de temps, passe de 43h à 57min sur 2 noeuds d'EOLE. Sachant que l'étude complète comporte au
  | moins 80 pas de temps.
  |       Ce n'est pas la peine d'utiliser plus de parallélisme pour l'instant. Pour aller plus loin:
  |         - L'équipe de dévt code_aster va améliorer la programmation des routines de contact (Issue27790) et y propager les 2 niveaux
  | de parallélisme.
  |         - L'équipe de dévt MUMPS va continuer à expertiser les raisons de l'échec des compressions BLR sur ces matrices.
  | 
  |      Surtout cette fiche a été l'occasion de nouer des contacts directs avec l'équipe UK, d'échanger du REX et de partager sur les
  | bonnes pratiques en termes de HPC dans code_aster (cf. fiche VEOL jointe). 
  | 
  |   ANALYSE
  |   ~~~~~~
  |   Il s'agit d'un très gros calcul (N=3.6M ddls) avec beaucoup de zones de contact (250000 noeuds). Donc l'essentiel des coûts réside
  | dans l'opérateur STAT_NON_LINE.
  | 
  |   Les dernières fois qu'il avait été lancé à Manchester, le calcul avait été paramétré avec les valeurs par défaut coté  solveur
  | linéaire et parallélisme, donc avec MULT_FRONT et en séquentiel. Le calcul d'un pas de temps prend alors de l'ordre de 2 jours
  | (43h), sachant qu'il faut au moins 80 pas de temps pour l'étude complète !
  |   
  |   RQ. C'est déjà très bien que le solveur maison fonctionne sans encombre sur un modèle de cette taille. Il montre  ainsi qu'il peut
  | apporter des services même sur des modèles assez gros (validation de MUMPS ou de PETSc, solution de secours en cas de bugs dans
  | MUMPS ou de pbs d'installation...).
  | 
  |   Sur 1 pas de temps les coûts sont alors ventilés comme suit:
  |      - 1,25h pour construire les systèmes linéaires,
  |      - 1h pour le contact (préparation et calcul élémentaire)
  |      - 41,25h pour résoudre les systèmes linéaires.
  | 
  |   En utilisant MUMPS (mettre SOLVEUR/METHODE='MUMPS' en v12 et v13, solveur par défaut depuis v14.1), toujours en séquentiel, le
  | calcul se réduit à 3,5h.
  | 
  |   En activant les deux niveaux de parallélisme (2 noeuds d'Aster ou d'EOLE suffisent), par exemple avec le paramétrage astk
  | nb_noeud=2, mpi_nbcpu=16, nbcpu=3 (pour paralléliser sur 16 processus MPI et chacun appelle 3 threads) le paramétrage solveur linéaire 
  |         METHODE='MUMPS', RENUM='PARMETIS', ACCELERATION='FR+'
  |   le calcul d'un pas de temps tombe en dessous de l'heure (57min).
  | 
  |    On ne peut pas faire mieux pour descendre en dessous de ces 57min sur 2 noeuds pour l'instant car:
  | 
  |    Coté solveur linéaire (22min)
  |     - les compressions BLR de MUMPS ne fonctionnent pas sur ce problème pourtant déjà assez volumineux. Une des matrices a été
  | transmise à l'équipe MUMPS afin qu'ils continuent à investiguer. Mais à court terme, il n'y aura pas de gains de ce coté là. Le
  | renuméroteur PARMETIS à déjà décomposé idéalement tous les termes. Il n'y a plus rien à réduire.
  |     ===> Travaux en cours coté équipe MUMPS.
  | 
  |    Coté code_aster hors contact (6min)
  |      - les calculs élémentaires et assemblages se parallélisent très bien. Ils pourraient encore être réduits en parallélisant sur
  | plus de noeuds mais cela mobiliserait plus de ressources machine pour pas grand chose.
  | 
  |    Coté code_aster contact (28min)
  |      - de gros gains parallèles ont déjà été récemment effectués en contact mais il reste à étendre le périmètre // de cette
  | fonctionnalité à d'autres routines. Voire en modifier un peu la programmation car elles n'ont pas toujours été prévues pour traiter
  | de grandes zones de contacts (mutualisation des JEVEUO, ré-écriture des SD internes au contact...).
  |    Par exemple dans la routine mmmbca.F90 (15 min du temps passé ici).
  |      ===> Fiche Issue27790 émise sur le sujet.
  | 
  |   RQ
  |   ~~
  |       * L'usage de PETSc (+MUMPS) n'amène rien ici. Le calcul a alors du mal à converger. Le pb est sans doute trop mal conditionné.
  |       * Rajouter du parallélisme (passer de 2 à 4 noeuds par exemple) n'amène plus grand chose: on ne gagne que 20% de temps elapsed
  | tout au plus. 
  | 
  | Récapitulatif de quelques temps de calcul (1er STAT_NON_LINE, 1 pas de temps, machine EOLE, code_aster v14.2)
  | 
  | Type de calcul________ seq___________seq________2noeuds (8MPIx7threads)________2noeuds (16MPIx3threads)
  | solveur lineaire_____MULT_FRONT___MUMPS_____PARMETIS/FR+/MATR_DISTRIBUEE_______idem
  | #Newton_________________32__________32_______________34___________________________33
  | Coût_aster_systeme//___65min_______66,5min______10,0min__________________________6min
  | _____aster_contact//___27min_______27,5min_______5,0min__________________________2min
  | _____aster_contactseq__33min_______26,0min______26,0min_________________________26min
  | _____solveur_analyse____2min_______29,0min______19,0min_________________________10min
  | _____solveur_facto___41h03min______65,0min______10,0min_________________________12min
  | _____solveur_solve_______7min______2,0min_______0,5min_________________________0,5min
  | _____TOTAL___________43h26min______3h36min______1h05min_________________________57min
  | 
  | _____JEVEUXmin/opt____18/137Go______6/29Go_______6/16Go_________________________6/15Go
  | _____Vmpeak____________138Go________109Go_________33Go___________________________32Go

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : informatique
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 27740 DU 06/06/2018
================================================================================
- AUTEUR : BOITEAU Olivier
- TYPE : aide utilisation concernant code_aster (VERSION 14.2)
- TITRE : Problèmes avec un très gros modèle
- FONCTIONNALITE :
  | CONTEXTE
  | ========
  | Ces calculs ont été effectués sur 2 nœuds aster5 en 3jours (au lieu d'un mois sans HPC).
  | A noter qu'ils ont été effectués en interne UTO et avec presque pas d'assistance R&D.
  | 
  | 
  | PROBLEME
  | ========
  | Il s'agit ici de la continuité de cette étude d'analyse limite d'un fond sphérique (maillage en éléments massifs à cause de la
  | présence de piquages). 
  | Il s'agit de vérifier la bonne stabilité de la solution en faisant converger le modèle EF.
  | Le modèle initial, utilisé pour justifier l'affouillement, conduit à N=3.6M ddls.
  | Une version raffinée à 7.8M a tourné à l'UTO en 78h sur 4 nœuds et aster13.4 + MUMPS solveur direct.
  | 
  | Il s'agit ici de montrer la faisabilité de la même étude sur un modèle encore plus gros conduisant à N=20.6M d'inconnues et de
  | proposer, si possible, un paramétrage HPC plus optimisé (parallélisme, solveur linéaire).
  | 
  | RESULTAT
  | ========
  | En bref, le paramétrage HPC le plus intéressant ici semble être:
  |         Total memory=240Go
  |         Dans Astk/Options: nb_noeud=4, nb_mpi=8 et nbcpus=12   ---> calculs sur 96 coeurs
  |         Dans .comm: remplacer SOLVEUR par SOLVEUR=_F(MATR_DISTRIBUEE='OUI', METHODE='PETSC'). --> MUMPS appelé en sous-main comme
  | préconditionneur (via l'option PRECOND='LDLT_SP')
  | 
  | Résultat sur une portion de transitoire avec 4 noeuds d'Aster5 en parallélisme hybride: 8MPI x 12 threads.
  | Pour 6 itérations de Newton, 55 de PETSc (donc PETSc+MUMPS convergent en moyenne en 9 it) on met alors 2h42min.
  | Ces temps se découpent comme suit:
  | 
  |   Coût total MUMPS= analyse (5.5min) + facto (12min) + solve (55x5s=4,5min)= 22min
  |   Coût PETSc                                                               = 10.5min
  |   Coût construction matrice+RHS aster                                      = 44min
  |   Temps STAT_NON_LINE éléments finis + algèbre linéaire                    =1h13min
  |   Temps STAT_NON_LINE "Autres" comptabilisés                               =1h24min
  |   Total STAT_NON_LINE effectif                                             =3h55min (dont 20min système)
  |   
  |   => Donc il y'a beaucoup de temps perdu hors MUMPS et construction système linéaire. A creuser.
  |      Le //isme et les options MUMPS/PETSc ne nous feraient gagner ici idéalement qu'une 1h26min de plus (-35%).
  |      
  |   => Néanmoins, ci-joint deux propositions de petits chantiers logiciels (ds les cartons depuis longtemps) qui ici pourraient
  |      sans doute permettre de gratter 48min de plus.
  | 
  |   Piste 1:
  |      On pourrait réduire les coûts MUMPS ici en activant permettant à l'usage préconditionneur de MUMPS d'être (presque) aussi riche
  | que celui de
  |      l'usage solveur direct et d'activer les options avancées: ACCELERATION='LR+'  et RENUM='PARMETIS'. La première permet réduire
  | la partie analyse
  |      de MUMPS et la seconde réduit la partie factorisation. Peu de gains possible sur la partie "solve" trop émiettée.
  |      Solution testée sur l'usage solveur direct.
  |      Ici cela conduit à un gain > 50% de cette étape
  |         ------> coût MUMPS potentiel passerait alors de 22 à 11min.
  | 
  |   Piste 2:
  |       Profiter du parallélisme hybride déployé ici (sur 96 cœurs) non pas uniquement ds PETSc/MUMPS mais aussi ds les parties
  | calculs élémentaires
  |       + assemblages pures code_aster. Ici elles ne tirent partie que de... 8 cœurs.
  |       Ici cela pourrait permettre de réduire cette étape d'un facteur au moins X6 (si on suppose une efficacité // pessimiste à 50%)
  |          ------> coût MUMPS potentiel passerait alors de 44 à 7min.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : non-régression
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 28090 DU 28/09/2018
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : aide utilisation concernant code_aster (VERSION 14.2)
- TITRE : [AOM] - HAYHURST - no convergence - Projet Accidents Graves
- FONCTIONNALITE :
  | Problème
  | *****************
  | L'utilisateur fait une étude thermo-mécanique et aucune itération du calcul ne converge.
  | 
  | 
  | Analyse et solution
  | ===================
  | Le calcul mécanique est en poursuite d'un calcul thermique transitoire non-lineaire. La température finale du calcul thermique est
  | employée tout au long du calcul mécanique.
  | 
  | Problème 1
  | ==========
  | Il n'y a pas de convergence, car l'incrément de déformation thermique est important et comme le champ de température est constant
  | dans le temps, le rédcoupage n'apporte rien. En effet, on part d'un état où les déformations thermiques (non-nulles) ne sont pas
  | compatibles avec les contraintes (nulles).
  | 
  | Solution au Problème 1
  | ==========
  | On impose dans l'état initial du calcul thermique une carte de température à la température de référence. Par conséquent on est
  | maintenant en présence d'un champ initial où il y a compatibilité en les contraintes et la déformation thermique.
  | 
  | 
  | Problème 2
  | ==========
  | Malgré ce changement, aucun incrément de déformation n'arrive à converger, même en présence pas de temps minime. On génère donc une
  | étude sur le point matériau avec la loi de comportement concernée (HayHurst). Il s'avère que la loi ne converge pas (jamais) sur le
  | point matériau. Les paramètres matériaux semblent réalistes à l'exception d'un seul paramètre : sigma_0 dont la valeur de 30 Pa par
  | rapport à des contraintes de l'ordre de quelques centaines de MPa. Après vérification, ce paramètre n'est pas sensé avoir une
  | influence sur le calcul dans cette étude car le paramètre d'endommagement de loi A_0 a été choisi nul: 
  | 
  | dA/dt = A_0 * sinh( G/sigma_0 ) où G est un terme fonction des contraintes  (issu de la doc)
  | 
  | Hors, en vérifiant dans le source, il y a un "flag" sur le résultat du sinus hyperbolique : si la valeur du sinus hyperbolique est
  | trop élevée, on force un échec de l'intégration de la loi de comportement.
  | 
  | 
  | Solution au Problème 2
  | ==========
  | On force une valeur de sigma_0 élevée, malgré son effet nul sur la loi de comportement, pour éviter que le sinus hyperbolique ne
  | force l'échec de l'intégration de la loi de comportement.
  | 
  | 
  | Résulats
  | ==========
  | Le calcul fonctionne une fois les deux problèmes résolus et l'utilisateur obtient des résultats.
  | passage de enregiste -> resolu
  | 
  | Nota
  | =====
  | issue28138 a été proposée afin de modifier la doc de la loi de comportement de Hayhurst (R5.03.13) et spécifier que sigma_0 doit
  | quand même être renseigné "convenablement" malgré un A_0=0..

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : L'utilsiateur obtient un résultat
- NB_JOURS_TRAV  : 3.0

