==================================================================
Version 14.4.14 (révision 0a8e9b48fbee) du 2019-12-03 10:32 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 29298 CHERKI EL IDRISSI Anass  En version 14.4.10 le cas test ssnv244d est en NOOK_TEST_RESU sur GAIA
 29308 CHERUBINI Stefano        Petite anomaile dans sdld103a.comm ligne 472
 29049 GEOFFROY Dominique       En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
 29058 GEOFFROY Dominique       En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e son...
 29349 PIGNET Nicolas           DEFI_GROUP: comportements différents en fonction de l'option
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 29298 DU 13/11/2019
================================================================================
- AUTEUR : CHERKI EL IDRISSI Anass
- TYPE : anomalie concernant code_aster (VERSION 14.4)
- TITRE : En version 14.4.10 le cas test ssnv244d est en NOOK_TEST_RESU sur GAIA
- FONCTIONNALITE :
  | Suite à EDA : 
  | 
  | Problème:
  | ---------
  | Le test ssnv244d (NON_REGRESSION) est NOOK sur Gaia, avec une valeur erronée sur le 2ème instant de vérification (sur 4 instants
  | testés). Anomalie soulevée auparavant en v15 (voir [#29057] ). L''origine du problème est une différence de convergence entre les
  | machines : GAIA fournit un résultat plus "précis" qu'EOLE.
  | 
  | Correction:
  | -----------
  | La solution initiale était de réduire le critère afin d'aligner les résultats des différentes machines. Cette solution a toutefois
  | conduit à un doublement du temps de calcul. Etant un test de non-régression, la solution adoptée finalement est de supprimer la
  | vérification sur l'instant à l'origine du NOOK et de garder les 3 autres instants.
  | 
  | Cet instant est donc mis en commentaire avec une explication dans le .comm
  | 
  | Modification à reporter en v14 et v15

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas test
- DEJA RESTITUE DANS : 15.0.18
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 29308 DU 18/11/2019
================================================================================
- AUTEUR : CHERUBINI Stefano
- TYPE : anomalie concernant code_aster (VERSION 15.1)
- TITRE : Petite anomaile dans sdld103a.comm ligne 472
- FONCTIONNALITE :
  | La solution proposée par François était correcte. J'ai vérifié que les tests sont OK après modification.
  | 
  | Par contre, j'ai remarqué que les concepts PSEUDO et MODESTAT (modes statiques pour correction des bases modales à accélération
  | imposée et à force imposée) ne sont pas utilisés dans la suite. Ils doivent être restés d'une version précédente d'un cas-test. Le
  | calcul dynamique sur base modale corrigée par des modes statiques est fait directement avec DYNA_LINE à l'aide du mot-clé ENRI_STAT,
  | qui calcule automatiquement les modes statiques et enrichi la base modale dynamique. Pour l'instant, donc, j'ai commenté PSEUDO et
  | MODESTAT.
  | 
  | En général il y a un peu de confusion dans la cas-test car parfois on utilise DYNA_LINE, parfois DYNA_VIBRA. On pourra préciser un
  | peu plus le cadre d'utilisation (avec modification de la doc aussi). Je pourrai m'en occuper. Je propose de fermer cette fiche en
  | corrigeant l'anomalie et d'ouvrir une autre fiche pour faire quelques modification mineures du cas-test.
  | 
  | 
  | 
  | -----------------------------------------------------------------------------------------------------------------------------------------
  | 
  | Bonjour 
  | vu dans sdld103a.comm petite anomalie
  | 
  | lignes 342 sq. :
  | 
  | TRA_RK32=DYNA_VIBRA(TYPE_CALCUL='TRAN',BASE_CALCUL='GENE',
  |                  MATR_MASS=MASS_GEN,   MATR_RIGI=RIGI_GEN,
  |                  SCHEMA_TEMPS=_F(SCHEMA='RUNGE_KUTTA_32',TOLERANCE=1.E-7, PAS_MAXI = 0.0001),
  | 
  | puis lignes 464 à 477 :
  | RESARK54=REST_GENE_PHYS(  RESU_GENE=TRA_RK54,
  |                           INTERPOL='LIN',
  |                           PRECISION=4.E-3,
  |                           LIST_INST=L_RECU,
  |                          TOUT_CHAM='OUI',
  |                          MULT_APPUI='OUI'
  |                         )
  |  
  | RESARK32=REST_GENE_PHYS(  RESU_GENE=TRA_RK54,
  |                           INTERPOL='LIN',
  |                           PRECISION=4.E-3,
  |                           LIST_INST=L_RECU,
  |                          TOUT_CHAM='OUI',
  |                          MULT_APPUI='OUI'
  | puis lignes  786 à 788 :
  | N2DXA_32=RECU_FONCTION(  NOEUD='NO2',  NOM_CMP='DX',   NOM_CHAM='DEPL',
  |                           RESULTAT=RESARK32,
  |                           INTERP_NUME='LIN',      INTERPOL='LIN',
  |                           LIST_INST=L_RECU    )
  | 
  | puis aux lignes 1186 sq. l'ensemble des tests dont N2DXA_32 :
  |  TEST_FONCTION
  | 
  | Je pense qu'il faut corriger à la ligne 472 :
  | RESARK32=REST_GENE_PHYS(  RESU_GENE=TRA_RK54,
  | par 
  | RESARK32=REST_GENE_PHYS(  RESU_GENE=TRA_RK32,
  | ________________________________
  | Merci
  | F.Voldoire

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Test OK après correction anomalie
- DEJA RESTITUE DANS : 15.0.18
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 29049 DU 20/08/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.3, le cas test sdls109b est en <F>_ERROR sur Gaia
  | La commande en jeu est CALC_MODES, le mode à 530.014469Hz a une norme d'erreur très importante.
  | 
  | !--------------------------------------------------------------------------------------------------------------------------!
  |    ! <E> <ALGELINE5_15>                                                                                                       !
  |    !                                                                                                                          !
  |    !    pour le concept  MODE_DKT, le mode numÃ©ro  10                                                                         !
  |    !                                                                                                                          !
  |    !   de frÃ©quence  530.014469                                                                                               !
  |    !                                                                                                                          !
  |    !   a une norme d'erreur de  0.998329  supÃ©rieure au seuil admis  0.000001.                                                !
  |    !                                                                                                                          !
  |    ! Conseils :                                                                                                               !
  |    ! Si vous utilisez METHODE='SORENSEN' ou 'TRI_DIAG' ou 'JACOBI', vous pouvez amÃ©liorer cette norme :                       !
  |    !  - Si la dimension de l'espace rÃ©duit est infÃ©rieure Ã  (nombre de degrÃ©s de libertÃ© actifs - 2), augmenter la valeur de  !
  |    !    COEF_DIM_ESPACE (la valeur par dÃ©faut est 4 pour 'TRI_DIAG' et 2 pour 'SORENSEN' et 'JACOBI').                        !
  |    !  - DÃ©couper le calcul en plusieurs appels de maniÃ¨re Ã  rÃ©duire le nombre de modes propres recherchÃ©s simultanÃ©ment       !
  |    !    (NMAX_FREQ ou taille de la BANDE).                                                                                    !
  |    !                                                                                                                          !
  |    !                                                                                                                          !
  |    ! Cette erreur sera suivie d'une erreur fatale.                                                                            !
  |    !--------------------------------------------------------------------------------------------------------------------------!
  | 
  | 
  | Correction
  | ----------
  | 
  | On suit la recommandation d'augmenter le paramètre COEF_DIM_ESPACE. En passant de 4 (par defaut) à 5, le test passe
  | sur Gaia ainsi que sur les autres machines.
  | 
  | On ajoute une remarque dans le .comm faisant référence à cette fiche.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test OK


================================================================================
                RESTITUTION FICHE 29058 DU 20/08/2019
================================================================================
- AUTEUR : GEOFFROY Dominique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e sont  CPU_LIMIT sur Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | En version 15.0.3, les cas tests fdlv112f, sdnx100a, sdnx100b et sdnx100e sont  CPU_LIMIT sur Gaia
  | 
  | Analyse :
  | -------
  | 
  | On remarque que ce problème n'est pas présent avec lancement interactif. En regardant les temps de calcul
  | il apparait que plusieurs threads sont utilisés en interactif (par la commande CALC_MISS) alors qu'un seul
  | est demandé dans le .export. Ce qui n'est pas le cas en batch.
  | 
  | En imposant plusieurs threads (ncpus) dans l'export, les calculs en batch sont OK.
  | 
  | Le problème est également présent sur eole.
  | 
  | On en conclut que l'appel à Miss3D via CALC_MISS en interactif ne tient pas compte du paramètre ncpus.
  | 
  | 
  | Correction
  | ----------
  | 
  | Pour fdlv112f, on passe à ncpus 16 et on double la mémoire (pour gaia).
  | Pour sdnx100a, b et e on passe à 8 ncpus.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : fdlv112f, sdnx100a,b,e


================================================================================
                RESTITUTION FICHE 29349 DU 03/12/2019
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_GROUP: comportements différents en fonction de l'option
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Dans le fichier joins, je souhaite créer un group_ma (DEFI_GROUP) à partir d'une liste de noeud.
  | 
  | Si je mets OPTION='APPUI', TYPE_APPUI='SOMMET'. J'ai une alarme disant qu'il ne créait pas le groupe car il est vide
  | 
  |    !----------------------------------------------------------------!
  |    ! <A> <SOUSTRUC_36>                                              !
  |    !                                                                !
  |    !  le GROUP_MA : Arete_Symm_Y est vide. on ne le crée pas.       !
  |    !                                                                !
  |    !                                                                !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !         !
  |    !----------------------------------------------------------------!
  | 
  | 
  | Si je mets OPTION='APPUI', TYPE_APPUI='AU_MOINS_UN'. J'ai une exception disant qu'il ne créait pas le groupe car il est vide et
  | s'arrête.
  | 
  |    !--------------------------------------------------------------------------!
  |    ! <EXCEPTION> <SOUSTRUC2_7>                                                !
  |    !                                                                          !
  |    !  -> Le groupe de mailles Arete_Symm_Y est vide. On ne le crée donc pas ! !
  |    !  -> Risque & Conseil:                                                    !
  |    !     Veuillez vous assurer que le type de mailles souhaité soit cohérent  !
  |    !     avec votre maillage.                                                 !
  |    !--------------------------------------------------------------------------!
  | 
  | Cette différence de comportement est embêtante. Je propose d'émettre seulement une alarme dans les deux cas.
  | 
  | Je peux m'en charger
  | 
  | Correction
  | ------------------------
  | 
  | On remplace l’erreur fatale par l’émission de l’alarme
  | 
  |    !----------------------------------------------------------------!
  |    ! <A> <SOUSTRUC_36>                                              !
  |    !                                                                !
  |    !  le GROUP_MA : Arete_Symm_Y est vide. on ne le crée pas.       !
  |    !                                                                !
  |    !                                                                !
  |    ! Ceci est une alarme. Si vous ne comprenez pas le sens de cette !
  |    ! alarme, vous pouvez obtenir des résultats inattendus !         !
  |    !----------------------------------------------------------------!
  | 
  | 
  | Résultat faux
  | -------------
  | RAS

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test joins
- NB_JOURS_TRAV  : 0.2

