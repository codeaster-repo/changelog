==================================================================
Version 14.0.16 (révision bda6d2672a7c) du 2017-11-15 19:22 +0100
==================================================================


--------------------------------------------------------------------------------
RESTITUTION FICHE 27119 DU 06/11/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.14, le cas test ssls144a est NOOK sur clap0f0q et Eole
FONCTIONNALITE
   Caractéristique du test : 
   ========================
   On teste un cyclindre avec le modèle COQUE_AXIS. Il s'agit d'un cylindre pour lequel le rapport ep/L=1/40 (coque mince), il faut
   neutraliser l'énergie de def de cisaillement transverse. Cela se fait via A_CIS qui (pour les modèles de COQUE_AXIS) permet de
   neutraliser cette énergie (R3.07.02, remarque de la page 8).  La solution de référence est analytique sur DRZ : ddl de rotation du
   cylindre autour du vecteur ortho-radial (ez). 
   
   En général, pour les modèles de COQUE_AXIS, plus il est grand mieux l'énergie de cisaillement est pénalisée mais cela entraîne des
   problèmes numériques au niveau précision du solveur.
   
   Exemple : sur ce test et sur eole, si on prend A_CIS=2.E5, on a l'erreur suivante : 
   """
   
      !-------------------------------------------------------------!
      ! <F> <FACTOR_57>                                             !
      !                                                             !
      ! Solveur MUMPS :                                             !
      !   La solution du système linéaire est trop imprécise :      !
      !   Erreur calculée   : 1.00019e-06                           !
      !   Erreur acceptable : 1e-06   (RESI_RELA)                   !
      !                                                             !
      ! Conseils :                                                  !
      !   On peut augmenter la valeur du mot clé SOLVEUR/RESI_RELA. !
      !                                                             !
      !                                                             !
      ! Cette erreur est fatale. Le code s'arrête.                  !
      !-------------------------------------------------------------!
   """
   
   Il faut trouver un compromis pour A_CIS. Dans le test A_CIS=2.5E4. Il est trop grand. 
   En prenant 10E5, le test est OK sur clap0f0q mais pas sur eole. 
   
   Solution :
   ==========
   
   Prendre A_CIS=1.E3. Toutes les valeurs sont OK sur toutes les machines.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    ssls144a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27072 DU 23/10/2017
AUTEUR : PLESSIS Sarah
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.12, la fonctionnalité POST_RCCM/SY_MAX n'est plus couverte
FONCTIONNALITE
   En version 14.0.12, la fonctionnalité POST_RCCM/SY_MAX n'était plus couverte.
   Cette fonctionnalité était couverte par le cas test rccm01b modifié par la révision [#27018] POST_RCCM : evolutions 2017
   
   J'ai remis le test qui valide cette fonction. Pour l'instant je ne la résorbe pas, elle peut servir pour les calculs de rochet
   thermique.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    passage de cas tests rccm*
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 25384 DU 04/07/2016
AUTEUR : TAMPANGO Yannick
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    Matrices de rigidité composite dans DYNA_VIBRA
FONCTIONNALITE
   Problème :
   ==========
   Lorsque l'on construit une matrice "composite" et qu'on l'utilise dans DYNA_VIBRA, on plante dans un dismoi
   
   Une matrice de rigidité est dite "composite" quand elle est la combinaison linéaire de deux matrices de rigidité qui reposent sur
   des champs de matériaux différents.
   
   Analyse :
   =========
   L'erreur de DYNA_VIBRA apparaît lors de l'appel à 'dismoi' pour le stockage de CHAM_MATER de la matrice de rigidité dans le
   sd_resultat. Dans les matrices composites construites avec des matrices qui reposent sur des champs de matériaux différents, on
   sauvegarde toutes les références en faisant une concaténation des objets .LIME des matrices combinées (voir issue21327). Par
   construction, en présence d'une matrice composite, "dismoi" fait une lecture de l'objet concaténé et ne récupère le CHAM_MATER que
   dans le cas où il est le même pour tous les objets. Dans le cas contraire il retourne une valeur vide, avec une erreur fatale par
   défaut. L'erreur pouvant être levée par le
   développeur lors de l'appel à "dismoi".
   
   DYNA_VIBRA ne plante que dans le cas d'une résolution sur base PHYSIQUE. En effet, en base généralisée on ne stocke pas CHAM_MATER.
   
   Proposition :
   =============
   
   1/ Que faire lorsqu'une matrice repose sur plusieurs matériaux (CHAM_MATER) ?
   On sauvegarde un CHAM_MATER vide. Pour cela, lever l'erreur liée au retour de CHAM_MATER vide et le remplacer par une alarme dans
   DYNA_VIBRA. Cette alarme aura pour objectif d'informer l'utilisateur qu'aucun CHAM_MATER n'a été sauvegardé car on se retrouve dans
   le cas de plusieurs CHAM_MATER. ainsi aucun post-traitement lié à CHAM_MATER ne sera possible.
   
   2/ Est-ce vraiment utile de sauvegarder cette information dans la SD RESULTAT au fait ?
   Oui dans le cas où on a un seul CHAM_MATER, des post traitement utilisant ce champs peuvent être faits sur le 'RESULTAT' de
   DYNA_VIBRA. Par exemple remonter aux contraintes à partir des déplacements.
   
   Solution proposée :
   ===================
   On résout le problème en harmonique et en transitoire base physique. 
   
   Pour les matrices composites, dans DYNA_VIBRA on stocke un CHAM_MATER vide et on remplace l’erreur fatale par une alarme : "Le
   modèle contient plusieurs 'CHAM_MATER'. Aucun n'a été stocké dans le concept resultat. Aucun post-traitement lié à CHAM_MATER ne
   sera possible".
   
   On supprime l'erreur fatale en remplaçant : 
   call dismoi('CHAM_MATER', raide, 'MATR_ASSE', repk=mate)
   par 
   call dismoi('CHAM_MATER', raide, 'MATR_ASSE', repk=mate, arret = 'C', ier = ierc)
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    adls102a; adlv301a
NB_JOURS_TRAV  : 2.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 27117 DU 06/11/2017
AUTEUR : ESCOFFIER Florian
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.14-7705d6c862f5, les cas tests sdnv111a et ssnp172a échouent sur AthosDev_Valid et Eole_Valid
FONCTIONNALITE
   Problème :
   =============
   
   En version 14.0.14-7705d6c862f5, les cas tests sdnv111a et ssnp172a échouent sur 
   AthosDev_Valid et Eole_Valid
   en raison d'un erreur de syntaxe suite à la révision 
    	[#26809] modification of MEMBRANE and GRILLE keywords in AFFE_CARA_ELEM
   
   
   sdnv111a :
            GRILLE
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!
               ! RÃ¨gle(s) non respectÃ©e
   (s) :                                                    !
               ! - Il faut un et un seul mot-clÃ© parmi : 
   ('ANGL_REP_1', 'ANGL_REP_2', 'VECT_1', !
               ! 'VECT_2')                                                             
            !
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               ! Mots clÃ©s inconnus : ANGL_REP !
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            Fin GRILLE
   
   ssnp172a :
            GRILLE
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!
               ! RÃ¨gle(s) non respectÃ©e
   (s) :                                                    !
               ! - Il faut un et un seul mot-clÃ© parmi : 
   ('ANGL_REP_1', 'ANGL_REP_2', 'VECT_1', !
               ! 'VECT_2')                                                             
            !
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   !!!!!!!!!!
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
               ! Mots clÃ©s inconnus : ANGL_REP !
               !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   
   Solution :
   =============
   
   L'erreur est causée par la révision de la fiche issue26809 car le mot-clé ANGL_REP 
   des mots clé facteur GRILLE et MEMBRANE dans AFFE_CARA_ELEM ont a été renommé en 
   ANGL_REP_1 pour plus de lisibilité.
   De ce fait pour corriger les erreurs des tests sdnv111a et ssnp172a il suffit de 
   remplacer ANGL_REP par ANGL_REP_1
   
   Impact doc :
   ===============
   
   Néant
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sdnv111a ssnp172a
NB_JOURS_TRAV  : 0.5

--------------------------------------------------------------------------------
RESTITUTION FICHE 27037 DU 17/10/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 11.8)
TITRE
    En version 14.0.11, les cas tests fdlv112b, fdlv112e, fdlv112g, fdlv112k, sdlx105a, sdlx106a, sdnx100a, sdnx100b, sdnx100c, sdnx100d, sdnx100e, sdnx100f, sdnx100g sont NOOK sur Eole
FONCTIONNALITE
   Problème :
   ========
   En version 14.0.11, les cas tests fdlv112b, fdlv112e, fdlv112g, fdlv112k, sdlx105a, sdlx106a, sdnx100a, sdnx100b, sdnx100c,
   sdnx100d, sdnx100e, sdnx100f, sdnx100g sont NOOK sur Eole
   
   Analyse :
   =======
   Ces différents tests font appel à MISS3D qui a été compilé avec l'option -xCORE-AVX-I qui ne fonctionne pas sur l'architecture
   processeur d'Eole.
   
   Correction :
   ==========
   La correction a été effectuée dans issue27045
   
   Concernant le test sdnx100a sur clap0f0q, le facteur multiplicatif des temps d'exécution a été modifié (5 -> 5.8), le test va
   maintenant jusqu'au bout et termine en OK (1577.34 secondes !).
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    test MISS3D
NB_JOURS_TRAV  : 0.1

--------------------------------------------------------------------------------
RESTITUTION FICHE 27103 DU 31/10/2017
AUTEUR : LEFEBVRE Jean-Pierre
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.13, le cas test mumps05c échoue sur Eole_mpi
FONCTIONNALITE
   Problème :
   ========
   En version 14.0.13, le cas test mumps05c échoue sur Eole_mpi. Cette erreur est apparue lors du passage aux outils Intel version 2017
   (issue27008). 
   
   Analyse :
   =======
   L'erreur semble due au compilateur Intel2017 et concerne l'appel au solveur Mumps avec le renuméroteur SCOTCH (fonctionnement
   correct avec METIS et PORD). 
   La nécessité d'utiliser la version Intel2017 n'est pas remise en cause suite aux problèmes de performance rencontrés. Le support
   contacté par DSP-IT nous a communiqué la réponse suivante : 
   
   "Intel MPI 2016 vs Intel MPI 2017 sur OPA
   
   Nous avions ouvert un ticket chez ATOS/BULL à ce sujet en plus de l'analyse de Pierre et leur conclusion confirme son analyse. Intel
   MPI version 2016 est sur certaines communications et tailles de messages peu performant sur de l'OPA. Intel MPI 2017 a été optimisé
   pour améliorer ses performances sur OPA. "
   
   Correction :
   ==========
   On revient en arrière sur issue27008 concernant les compilateurs ifort et icc, ainsi que sur les librairies mathématiques MKL. Par
   contre on conserve la version IntelMPI 2017.
   Liste des modules utilisés sur Eole :
   ifort/2016.0.047 icc/2016.0.047 mkl/2016.0.047 et impi/2017.0.098.
   
   La correction est déjà effectuée en version de développement : reconstruction des prérequis et modification des scripts de devtools
   et des fichiers de configuration src.  
   
   On peut se poser la question de reporter les modifications en version d'exploitation.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  OUI
IMPACT_DOCUMENTAIRE : 
VALIDATION
    mumps05c et liste MPI
NB_JOURS_TRAV  : 3.0

--------------------------------------------------------------------------------
RESTITUTION FICHE 26971 DU 02/10/2017
AUTEUR : KUDAWOO Ayaovi-Dzifa
TYPE anomalie concernant Code_Aster (VERSION 14.1)
TITRE
    En version 14.0.9-47dc2776d1b5, le cas test ssnv128y est NOOK sur Eole
FONCTIONNALITE
   Fiche classée sans suite parce que le NOOK n'apparaît plus.
RESU_FAUX_VERSION_EXPLOITATION    :  NON
RESU_FAUX_VERSION_DEVELOPPEMENT   :  NON
RESTITUTION_VERSION_EXPLOITATION  :  NON
RESTITUTION_VERSION_DEVELOPPEMENT :  NON
IMPACT_DOCUMENTAIRE : 
VALIDATION
    sans objet
NB_JOURS_TRAV  : 0.1

