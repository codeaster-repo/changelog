==================================================================
Version 14.2.10 (révision f8ea1990bca2) du 2018-09-17 12:56 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27899 KUDAWOO Ayaovi-Dzifa     Amélioration de ALGO_CONT='STANDARD', ALGO_FROT='PENALISATION'+ADAPT_COEF.
 27834 KUDAWOO Ayaovi-Dzifa     problème avec ADAPTATION='CYCLAGE'
 27930 KUDAWOO Ayaovi-Dzifa     D124.18 - Ré-écriture du catalogue DEFI_CONTACT
 27977 DEVESA Georges           réactiver la possibilité d'utiliser le second gradient en dynamique
 27971 DEVESA Georges           modification DEFI_CHAR_SOL pour utiliser REAC_NODA pour le transfert des forces
 27924 PIGNET Nicolas           D115.18 - Déformations GDEF_LOG: il faut augmenter le controle sur le jacobien
 26883 KUDAWOO Ayaovi-Dzifa     En version 14.0.5, le test icare04a échoue sur  Eole_Valid. En version 13.4....
 24722 DE SOZA Thomas           B301 - Mise à jour des manuels A et SA
 27967 MICHEL-PONNELLE Sylvie   Mise à jour des docs U2 - GC
 27522 FILIOT Astrid            A204.xx - Récupération de nouvelles variables en sortie des lois de comport...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27899 DU 21/07/2018
================================================================================
- AUTEUR : KUDAWOO Ayaovi-Dzifa
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : Amélioration de ALGO_CONT='STANDARD', ALGO_FROT='PENALISATION'+ADAPT_COEF.
- FONCTIONNALITE :
  | Objets de la fiche :
  | ====================
  | Cette fiche propose deux évolutions. 
  | 
  | Évolution 1 :
  | °°°°°°°°°°°°
  | Améliorer le mode adaptatif de DEFI_CONTACT lorsqu'on branche la méthode pénalisée pour le frottement.
  | 
  | L'algorithme actuel peut être inéfficace à cause d'un mécanisme adaptatif qui existait déjà dans le code depuis 2012 et qui pouvait
  | polluer le nouveau mode adaptatif. 
  | 
  | Évolution 2 : 
  | °°°°°°°°°°°°°
  | Nouveau mot-clef : RESI_CONT
  | Un nouveau critère sur les pressions de contact. Si en moyenne, la pression de contact sur les zones sur deux itérations successives
  | est < RESI_CONT alors on déclare la convergence sur les statuts. 
  | 
  | Par défaut, RESI_CONT=-1 (ancien mode de convergence sur les statuts).
  | 
  | Validation : 
  | ===========
  | src (cal9,clap0f0q,aster5,eole).
  | ssnv504k ==> pour RESI_CONT.
  | ssnv128p ==> pour la correction sur GLIS_MAXI. 
  | ssnv249c ==> Test sur le réservoir avec RESI_CONT=-1 et avec RESI_CONT=1.E-6. 
  | 
  | Doc : 
  | =====
  | Impact en V14.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U2.04.04, U4.44.11, V6.04.504, V6.04.249,V6.04.128
- VALIDATION : src+ssnv128p+ssnv504k
- NB_JOURS_TRAV  : 10.0


================================================================================
                RESTITUTION FICHE 27834 DU 22/06/2018
================================================================================
- AUTEUR : KUDAWOO Ayaovi-Dzifa
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : problème avec ADAPTATION='CYCLAGE'
- FONCTIONNALITE :
  | Cette fiche fait suite à un constat sur la dégradation de la qualité des résultats en contact  entre a V13.3 et la V13.4. 
  | 
  | Le problème se produit avec le mot-clef ADAPTATION='CYCLAGE'.
  | 
  | Correction : 
  | ===========
  | 
  | - Débrancher dans le source ce basculement en mode automatique en pénalisation V13. 
  | Le test ssnv128z sera supprimé
  | - En V14, on le laisse car il y a un critère automatique qui vérifie la pénétration maximum.
  | - Impact Doc en V13 : U2.04.04, U4.44.11

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U2.04.04, U4.44.11
- VALIDATION : src
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 27930 DU 01/08/2018
================================================================================
- AUTEUR : KUDAWOO Ayaovi-Dzifa
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : D124.18 - Ré-écriture du catalogue DEFI_CONTACT
- FONCTIONNALITE :
  | Cette fiche corrige le catalogue de DEFI_CONTACT. 
  | 
  | Le mot-clef ADAPTATION apparaît plusieurs fois dans les blocs_conditions : b_cont_std, b_frot_std, b_cont_pena, b_lac_adapt afin de
  | choisir suivant l'algorithme de résolution le bon paramétrage de ce mot-clef. 
  | Ex : Si l'utilisateur choisit ALGO_CONT='PENALISATION', il a automatiquement ADAPTATION='ADAPT_COEF' tandis que s'il choisit
  | ALGO_CONT='STANDARD' il a automatiquement ADAPTATION='CYCLAGE'. 
  | 
  | Le problème est que dans lors de l'évaluation de la commande DEFI_CONTACT, ADAPTATION pouvait apparaître plusieurs fois dans le même
  | mot-clef facteur avec deux valeurs differentes 'CYCLAGE'/'ADAPT_COEF'. Le superviseur n'arrête pas ce genre de situation mais
  | asterstudy si : il y a un warning asterstudy qui prévient l'utilisateur d'un potentiel conflit : ADAPTATION='CYCLAGE'(COEF_PENA_CONT
  | obligatoire) et ADAPTATION='ADAPT_COEF'.  
  | 
  | """
  |                       ZONE=_F(ALGO_FROT='PENALISATION',
  |                               COULOMB=0.3,
  |                               COEF_CONT=100.0,
  |                               ADAPTATION='CYCLAGE',
  |                               COEF_PENA_FROT=1.E7,
  |                               ADAPTATION='ADAPT_COEF',
  |                               GROUP_MA_MAIT='Master',
  |                               GROUP_MA_ESCL='Slave',
  |                               NORMALE='MAIT',
  |                               CONTACT_INIT='INTERPENETRE',
  |                               VECT_ESCL='AUTO',
  |                               APPARIEMENT='MAIT_ESCL',
  |                               RESOLUTION='OUI',
  |                               INTEGRATION='AUTO',
  |                               DIST_POUTRE='NON',
  |                               GLISSIERE='NON',
  |                               VECT_MAIT='AUTO',
  |                               DIST_COQUE='NON',
  |                               TOLE_PROJ_EXT=0.5,
  |                               TYPE_PROJECTION='ORTHOGONALE',
  |                               DIST_APPA=-1.0,
  |                               GRAND_GLIS='NON',
  |                               ALGO_CONT='STANDARD',),
  |                       )
  | """
  | 
  | Correction : 
  | ============
  | 
  | 1. On enlève le mot-clef ADAPTATION des blocs et il reste facultatif avec 'CYCLAGE' comme valeur par défaut.
  | 
  | Validation : 
  | ============
  | 
  | 396 cas-tests de DEFI_CONTACT

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src + submit
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 27977 DU 28/08/2018
================================================================================
- AUTEUR : DEVESA Georges
- TYPE : anomalie concernant code_aster (VERSION 13.7)
- TITRE : réactiver la possibilité d'utiliser le second gradient en dynamique
- FONCTIONNALITE :
  | Problème :
  | --------
  | il s'agit de réactiver la possibilité d'utiliser le second gradient en dynamique, notamment pour des 
  | études avec la loi Hujeux en 2D. C'est lié à la non prise en compte de MASS_MECA dans les éléments 
  | modélisés *_DIL qui empêche l'utilisation en dynamique dans tout le reste du modèle.
  | Réponse :
  | -------
  | Il n'y a pas d'adhérence particulière avec la loi Hujeux, cela concerne toute la dynamique avec les 
  | modélisations *_DIL, d'où la nature de la correction avec seulement CONDCALCUL en négatif pour ces 
  | modélisations dans le catalogue de l'option MASS_MECA.
  | Il faut étendre cette condition calcul à AMOR_MECA si on veut aussi mettre de l'amortissement dans le 
  | modèle, en particulier de l'amortissement dû aux frontières absorbantes en dynamique.
  | Validation :
  | ----------
  | On enrichit le cas test WTNV132B d'une colonne de sol avec Hujeux avec un calcul d'évolution dynamique par 
  | DYNA_NON_LINE en remplaçant le blocage à la base en statique par une frontière absorbante et en utilisant 
  | la sollicitation sismique des tests WDNP101. On teste en non régression le premier maximum relatif de 
  | réponse sur un intervalle très court. A remarquer que le calcul sur l'intervalle beaucoup plus long des 
  | tests WDNP101 aurait donné des niveaux de réponse de même ordre que pour ces tests.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V7.31.132
- VALIDATION : cas test WTNV132B
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 27971 DU 24/08/2018
================================================================================
- AUTEUR : DEVESA Georges
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : modification DEFI_CHAR_SOL pour utiliser REAC_NODA pour le transfert des forces
- FONCTIONNALITE :
  | Problème :
  | --------
  | Formulé par Emmanuel Robbe (CIH) : 
  | Dans le cadre du calcul des barrages en béton au séisme, avec un chargement onde plane. On a testé les 
  | nouveaux développements de la commande DEFI_CHAR_SOL pour utiliser le chargement onde plane sur un sol non 
  | homogène et/ou avec amortissement.
  | Dès lors qu'on veut introduire de l'amortissement, ou des couches de sol, on peut réaliser la procédure 
  | suivante dans le cas ou la colonne de sol n'est pas précédemment calculée par DEFI_SOL_EQUI mais obtenue 
  | par une évolution renseignée par RESU_INIT : 
  |  - calcul d'une colonne de sol représentative de la fondation (stratification et amortissement)
  |  - extraction des vitesses / déplacements et forces sur les bords latéraux de la colonne de sol
  |  - projection des vitesses / déplacement et forces sur le modèle 2D de fondation
  |  - calcul des forces sur les bords latéraux du modèle 2D à partir des éléments précédents (c'est là 
  | qu'intervient DEFI_CHAR_SOL)
  |  - résolution du modèle 2D.
  | Un cas test développé au CIH a mis en évidence les limites de la programmation actuelle : 
  |  - seules les FORC_NODA peuvent être utilisées dans DEFI_CHAR_SOL pour le calcul des forces. Or FORC_NODA 
  | n'est pas la bonne grandeur : le calcul d'une colonne de sol sous une onde de cisaillement montre qu'on 
  | récupère des forces horizontales sur les bords non nulles, ce qui n'est pas correct. Il faudrait plutôt 
  | utiliser REAC_NODA à la place qui donne des résultats plus cohérents.
  | 
  | Les modifications suivantes peuvent être proposées : 
  |  - soit de remplacer FORC_NODA par REAC_NODA dans DEFI_CHAR_SOL.
  |  - soit de 'mettre à zéro' certaines composantes de forces selon l'onde calculée :
  |       + dans le cas d'une onde de cisaillement, on met a zéro les composantes horizontales de FORC_NODA
  |       + dans le cas d'une onde de compression, on met a zéro les composantes verticales de FORC_NODA
  | 
  | La première option semble la plus simple mais il peut subsister un problème dans les coins inférieurs du 
  | modèle. Il faudrait donc tester également la seconde option pour vérifier si une amélioration est apportée.
  | Enfin, le mot clef COEF de la commande DEFI_CHAR_SOL est à discuter : avec la modification proposée et la 
  | stratégie de modélisation utilisée, il vaut mieux le fixer à 1 par défaut en tout cas.
  | 
  | Analyse:
  | -------
  | Suite à une instruction préalable et conjointe de la fiche avec Emmanuel, il s'avère que certaines 
  | demandes peuvent être affinées et reformulées :
  | - l'utilisation de REAC_NODA ne résout pas tout, car si elle supprime des valeurs parasites à certaines 
  | composantes, elle peut faire conserver d'autres valeurs parasites à la base. C'est donc bien aussi de 
  | garder FORC_NODA, avec la possibilité de sélectionner des composantes comme le fait DEFI_CHAR_SOL à partir 
  | de DEFI_SOL_EQUI. D'autant plus que seul FORC_NODA convient pour certains calculs harmoniques comme dans 
  | DEFI_SOL_EQUI.
  | - On peut effectivement changer la valeur par défaut de COEF à +1 dans CREA_RESU/CONV_RESU transposée dans 
  | DEFI_CHAR_SOL/RESU_INIT. Même si selon la direction de projection d'un résultat initial sur les bords du 
  | domaines, COEF=-1 peut convenir comme dans le cas test SDLS140B.
  | - Les investigations faites par Emmanuel ont conduit à faire corriger un coefficient 0.5 par +1 dans le 
  | corps de DEFI_CHAR_SOL quand on veut récupérer les forces nodales issues des ondes de cisaillement.
  | 
  | Action et validation :
  | --------------------
  | Les modifications suivantes concernent CREA_RESU/CONV_RESU, ensuite DEFI_CHAR_SOL/RESU_INIT :
  | - Possibilité d'ajouter les champs REAC_NODA et ACCE comme entrées de NOM_CHAM_INIT avec FORC_NODA par 
  | défaut. Le champ ACCE permet d'introduire des forces d'inertie par des masses pénalisées.
  | - Possibilité d'exclure des composantes du résultat en entrée, nécessaire surtout avec le champ FORC_NODA, 
  | au moyen du mot clé DDL_EXCLUS.
  | - La valeur par défaut de COEF passe à +1.
  | Les modifications précédentes, avec la correction du coefficient 0.5 par +1 pour les forces nodales issues 
  | des ondes de cisaillement dans DEFI_CHAR_SOL, ont un impact dans les tests SDLV121H ( par ajustement de 
  | valeurs testées) et surtout SDLS140B. Dans ce test, elles permettent de coller encore mieux aux références 
  | du test SDLS140A et on y ajoute les cas de calcul suivants : exclusion de composante dans FORC_NODA, 
  | utilisation de REAC_NODA et utilisation de ACCE pour des forces d'inertie par des masses pénalisées.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.44.12 U4.84.32 V2.03.140 V2.04.121
- VALIDATION : tests sdls140b sdlv121h
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 27924 DU 30/07/2018
================================================================================
- AUTEUR : PIGNET Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : D115.18 - Déformations GDEF_LOG: il faut augmenter le controle sur le jacobien
- FONCTIONNALITE :
  | Problème:
  | dans poslog.F90, il y a un contrôle sur la valeur du jacobien de F. Pour l'instant, il ne peut pas être supérieur à 100 mais ceci
  | peut arriver quand on fait des grandes déformations ou que l'on passe par une configuration un peu mal foutu lors du Newton. La
  | physique ne l'interdit pas contrairement à une valeur négative
  | 
  | Correction:
  | On a supprimé la valeur supérieure car elle n'a pas de sens physique. Elle servait en général à stopper la non convergence du
  | newton. On ajoute également ce test à prelog car ces critères étaient testés après le calcul du comportement et non avant
  | 
  | On en profite pour mettre en cohérence les critères qui sont utilisés par les routines gdef_log (prelog, poslof, deflog et gdlog_module)
  | 
  | Vérification:
  | Tout astest
  | 
  | 
  | Rmq: A terme, il serait mieux de n'utiliser que le module gdlog_module à la place de prelog et poslog qui est plus générique

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : v6.08.107
- VALIDATION : ssnd107a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 26883 DU 11/09/2017
================================================================================
- AUTEUR : KUDAWOO Ayaovi-Dzifa
- TYPE : evolution concernant code_aster (VERSION 14.1)
- TITRE : En version 14.0.5, le test icare04a échoue sur  Eole_Valid. En version 13.4.3, il est NOOK sur Eole_Valid
- FONCTIONNALITE :
  | Après investigation, il y a deux choses qui font défaut dans ce test :
  | - absence d'une valeur de référence
  | - paramétrage forcé de DEFI_CONTACT/XFEM. 
  | 
  | Pour le premier, le test icare04a était rentré sans réel valeur de test de référence. Comme les tests icare étaient du couplage
  | nonintrusif global-local j'avais opté pour une vérification des valeurs des forces aux interfaces. Floc/glob=Fglob/loc. Je propose
  | de maintenir cette vérification telle qu'elle est. 
  | 
  | Ensuite, pour le NOOK très élevé, je pense que le problème vient du paramétrage de XFEM : 
  | REAC_GEOM=SANS+PENALISATION.
  | """
  | 
  |         l[AsterIter] =DEFI_CONTACT(MODELE         = self.modele,
  |                       FORMULATION    = 'XFEM',
  |                       FROTTEMENT     = 'COULOMB',
  |                       ITER_CONT_MAXI = 5,
  |                       ITER_FROT_MAXI = 12,
  |                       REAC_GEOM      = 'SANS',
  |                       ELIM_ARETE     = 'ELIM',
  |                       RESI_FROT = 5.0,
  |                       ZONE=(
  |                             _F(
  |                                FISS_MAIT      = self.FISS,
  |                                INTEGRATION    = 'NOEUD',
  |                                CONTACT_INIT   = 'OUI',
  |                                COULOMB        = 0.0,
  |                                ALGO_CONT      = 'PENALISATION',
  |                                COEF_PENA_CONT = 1.E6,
  |                                ALGO_FROT      = 'PENALISATION',
  |                                COEF_PENA_FROT = 1.0E4,
  |                              ),
  |                          ),
  |                    );
  | """
  | 
  | Il se trouve que lorsque je remets des valeurs de convergence correctes, le modèle local XFEM ne converge plus. 
  | 
  | Solution :
  | =========
  | 
  | - J'opte pour un remplacement du modèle local XFEM par un modèle simplifié élasto-plastique.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V1.03.136
- VALIDATION : icare04a
- NB_JOURS_TRAV  : 5.0


================================================================================
                RESTITUTION FICHE 24722 DU 18/01/2016
================================================================================
- AUTEUR : DE SOZA Thomas
- TYPE : evolution concernant Documentation (VERSION 11.4)
- TITRE : B301 - Mise à jour des manuels A et SA
- FONCTIONNALITE :
  | Cette fiche traçait les actions réalisées dans le cadre du livrable principal B301 du projet PSM "Manuel AQ de Code_Aster et
  | Salome-Meca mis à jour".
  | 
  | Les manuels AQ de code_aster et salome_meca ont été mis à jour une première fois en 2015 puis en 2018.
  | 
  | Le passage sans écart de l'audit de développement du 23 janvier 2018 valide cette mise à jour.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 15.0


================================================================================
                RESTITUTION FICHE 27967 DU 23/08/2018
================================================================================
- AUTEUR : MICHEL-PONNELLE Sylvie
- TYPE : evolution concernant Documentation (VERSION 11.8)
- TITRE : Mise à jour des docs U2 - GC
- FONCTIONNALITE :
  | On propose une mise à jour de 2 docs U2 liées au Génie Civil :
  | U2.03.07 : panorama des outils de GC : mise à niveau + intégration des liens hypertextes vers les autres docs U2 et les supports de
  | formation.
  | 
  | U2.03.06 : calcul de Génie Civil avec câble de précontrainte : remise à niveau importante avec notamment l'intégration des conseils
  | d'utilisation sur les éléments CABLE_GAINE.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U2.03.07 ; U2.03.06
- VALIDATION : sans objet
- NB_JOURS_TRAV  : 4.0


================================================================================
                RESTITUTION FICHE 27522 DU 30/03/2018
================================================================================
- AUTEUR : FILIOT Astrid
- TYPE : evolution concernant code_aster (VERSION 14.4)
- TITRE : A204.xx - Récupération de nouvelles variables en sortie des lois de comportement
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Dans le cadre du chantier d'issue26800, on souhaite améliorer la prédiction de Newton en communiquant plus avec la loi de 
  | comportement, en particulier lorsque celle-ci implique de la viscosité et/ou de l'endommagement, et notamment en présence de 
  | variable de commande.
  | 
  | Cette fiche propose de mettre en place récupération des nouvelles quantités en sortie de la loi de comportement, décrites dans 
  | le CR en PJ d'issue26800. 
  | 
  | 
  | Développement
  | -------------
  | 
  | SANS SOURCE : c'est le développeur de la loi de comportement qui se charge de transmettre le terme d'ordre 0 attendu en 
  | prédiction à travers la variable sigp (qui n'était pas réutilisée en prédiction dans le mécanisme actuel 'OLD'). 
  | On documente bien dans D5.04.01 les variables d'entrée/sortie que doit fournir le développeur suivant les mécanismes, et avec 
  | une distinction claire en prédiction et correction
  | 
  | -> Doc déjà impactée à l'occasion de issue27464 et issue27504

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : D5.04.01
- VALIDATION : aucune (sans source)
- NB_JOURS_TRAV  : 1.0

