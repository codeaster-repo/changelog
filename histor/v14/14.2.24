==================================================================
Version 14.2.24 (révision 6fe734431c49) du 2019-01-10 15:29 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 27994 YU Ting                  [FAUX] Pilotage PRED_ELAS avec ENDO_ISOT_BETON
 28166 ABBAS Mickael            FAUX - DURT_ELNO est faux avec le Zircaloy
 28337 ABBAS Mickael            Interdire POU_D_EM avec les options CRIT_STAB  ou MOD_VIBR
 28393 ABBAS Mickael            LIRE_RESU/MED plante avec INFO=2
 27910 ABBAS Mickael            D115.19 - L'ajout de nouvelles mailles est une calamité dans aster
 28062 ABBAS Mickael            A302.19 - [MFRONT] Transmission de l'instant de calcul à une loi de comporte...
 28345 ABBAS Mickael            Ajout d'un test pour le mélange de modélisation MECA/HM
 28395 ABBAS Mickael            DEFI_BASE_MODALE plante salement
 28353 ABBAS Mickael            Non couverture de code : AFFE_CHAR_MECA / ARETE_IMPO / PHI
 26973 YU Ting                  Créer une nouvelle commande pour IMPR_RESU/CONCEPT
 28390 COURTOIS Mathieu         Notice utilisation aster5
 28400 YU Ting                  mettre à jour des commandes de l'exemple 3 dans la Doc U2.01.11
 28398 YU Ting                  incohérence pour EVOL_CHAR dans la doc LIRE_RESU
 18937 ABBAS Mickael            Faire le ménage dans l'arboresence de mecalm
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 27994 DU 31/08/2018
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [FAUX] Pilotage PRED_ELAS avec ENDO_ISOT_BETON
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | L’équation de pilotage PRED_ELAS pour ENDO_ISOT_BETON est mal résolue.
  | Il manque les classiques sqrt(2) sur les termes de cisaillement de déformation
  | 
  | RESU_FAUX: tout calcul engendrant du cisaillement (par exemple nu != 0) et utilisant ENDO_IOST_BETON avec le pilotage PRED_ELAS
  | conduit à des résultats faux. L'équation de pilotage est mal résolue et l’endommagement ne progresse pas comme attendu.
  | 
  | C'est faux depuis l'introduction du modèle (au moins V6)
  | 
  | 
  | Correction
  | ------------------------
  | 
  | On a corrigé les termes avec sqrt(2) dans critet.F90.
  | 
  | Puis avec cette correction, par TMA :
  | Installation de la révision présente dans le bundle.
  | Passage de 43 cas-tests de src. Rien de cassé.
  | Pas de tests trouvés dans validation.
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Depuis l'introduction du modèle (au moins V6)

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 6.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 6.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage des tests utilisants ENDO_ISOT_BETON


================================================================================
                RESTITUTION FICHE 28166 DU 19/10/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : FAUX - DURT_ELNO est faux avec le Zircaloy
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans le cadre du développement de la métallurgie du revenu (de l'acier), nous avons découvert  que le calcul de la dureté
  | métallurgique (DURT_ELNO) fait l'hypothèse implicite qu'on est avec un acier (à 5 phases).
  | 
  | Mais rien n’interdit à aster de tenter de le calculer pour le Zircaloy.
  | 
  | Les résultats seront complètement faux (mauvais décalages des tableaux)
  | 
  | Il faut interdire le calcul de DURT_ELNO avec le Zircaloy
  | 
  | 
  | Correction
  | ----------
  | 
  | On vérifie dans op0194 qu'on ne calcule par DURT_ sur autre chose que de l'acier
  | 
  | Il y a également un problème dans les messages d'erreur: META1_46 existe deux fois. On corrige mais ça ne concerne que la V14
  | Pour la V13, l'impact est limité à ce qui a été fait dans op0194.F90 avec le message d'erreur META1_3
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Le calcul des options DURT_* donne des résultats faux avec le Zircaloy quelque soient les conditions

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 13.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 14.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : test manuel
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28337 DU 10/12/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 14.2)
- TITRE : Interdire POU_D_EM avec les options CRIT_STAB  ou MOD_VIBR
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Avec des POU_D_EM, lorsque que l'on calcule l'option CRIT_STAB (STAT_NON_LINE) ou MOD_VIBR (DYNA_NON_LINE) cela peut donner des
  | résultat faux indépendamment du comportement matériaux (élastique ou non-linéaire).
  | 
  | C'est un problème qui vient du calcul de la position du nœud utilisé pour la condensation statique.  
  | 
  | 
  | Correction
  | ----------
  | 
  | On interdit POU_D_EM avec les options CRIT_STAB  ou MODE_VIBR.
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Avec des POU_D_EM, lorsque que l'on calcule l'option CRIT_STAB (STAT_NON_LINE) ou MODE_VIBR (DYNA_NON_LINE) cela peut donner des
  | résultat faux indépendamment du comportement matériaux (élastique ou non-linéaire).

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 7.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 7.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28393 DU 04/01/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : LIRE_RESU/MED plante avec INFO=2
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Si on ajoute INFO=2 à LIRE_RESU au format MED (ci-dessous dans zzzz389a), le code plante avec l'erreur :
  |  . !--------------------------------------------!
  |  . ! <F> <UTILITAI6_77> . . . . . . . . . . . . !
  |  . ! . . . . . . . . . . . . . . . . . . . . . .!
  |  . ! Concept résultat 00000003 : . . . . . . . .!
  |  . ! le numéro d'ordre 0 est inconnu. . . . . . !
  |  . ! . . . . . . . . . . . . . . . . . . . . . .!
  |  . ! . . . . . . . . . . . . . . . . . . . . . .!
  |  . ! Cette erreur est fatale. Le code s'arrête. !
  |  . !--------------------------------------------!
  | 
  | Correction
  | ----------
  | 
  | Le problème vient de l’utilisation de TOUT_ORDRE='OUI'
  | Au format MED, on pré-dimensionne la SD résultat avec une valeur du nombre de pas de temps à 100 (qu'on agrandit si nécessaire)
  | Malheureusement, si le nombre de pas de temps est inférieur à 100, on n'utilise pas le nouveau nombre de pas de temps.
  | 
  | On modifie lrfmed et op0150 pour boucler sur le nombre de pas de temps _réel_.
  | 
  | Vérif.: on modifie zzzz389a avec INFO=2

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz389a
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 27910 DU 25/07/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : D115.19 - L'ajout de nouvelles mailles est une calamité dans aster
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | L'ajout  de nouvelles mailles est une calamité dans aster
  | 
  | On pourrait croire qu'il suffit de changer le catalogue mesh_types.py, mais non ! On a le temps de faire beaucoup plus compliqué
  | 
  | Il faut changer pas moins de 22 (vingt-deux) routines (F90 mais aussi .h car les tableaux ne sont même pas en "*" ! ) parce qu'on a
  | mis en dur dans le code le nombre de type_maille
  | 
  | 
  | Développement
  | -------------
  | 
  | On remplace toutes les occurrences du nombre de mesh_types par un #define donné dans l'include MeshTypes_type.h
  | 
  | MT_NTYMAX pour ce nombre
  | On ajoute MT_NNOMAX pour le nombre de nœuds maxi par élément.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : tout src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 28062 DU 20/09/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : A302.19 - [MFRONT] Transmission de l'instant de calcul à une loi de comportement rédigée sous MFront
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Certaines loi de comportement peuvent nécessiter de connaître l'instant de calcul courant. C'est par exemple la cas de la loi
  | META_LEMA_ANI.
  | Cette loi devant être intégrée dans code_aster au formalisme MFront (issue25018) , il peut être intéressant de transmettre cette
  | variable entre aster et MFront (au même titre que la température).
  | 
  | 
  | Développement
  | -------------
  | 
  | On ajoute la variable TIME comme ExternalStateVariable gérable par MFront. L'idée est de lui passer le temps courant.
  | 
  | Pour l'utiliser, il suffit donc d'ajouter @ExternalStateVariable real TIME dans le fichier MFront. Cette variable n'est active que
  | dans MFront, car, pour aster, l'instant est bien géré comme une variable de commande (utilisation dans rcvalb en particulier pour
  | les paramètres matériaux)
  | 
  | On en profite pour améliorer la gestion des comportements
  | 
  | 1/ Quand la carte CARCRI (ou COMPOR) est modifiée, il faut penser à la modifier partout dans le Fortran (en particulier, on oublie
  | systématiquement SIMU_POINT_MAT ! ). Le fichier Behaviour_type.h utilise des #DEFINE pour repérer la taille de la carte et les
  | "slots" disponibles, c'est beaucoup plus lisible.
  | 
  | 2/ On revoit la gestion de CARCRI. Actuellement, on mélange  _lecture_ des données, _vérification_ puis _écriture_ dans CARCRI
  | On modifie donc la programmation pour bien séparer les trois phases, comme pour la carte COMPOR
  | 
  | 3/ Cette nouvelle variable d'état externe est la 31 ème. Or il se trouve qu'on utilisait un entier codé dans la carte CARCRI pour
  | détecter l'activation des variables d'état. Il faut donc ajouter un DEUXIEME entier codé. Ce qui a impacté le Fortran (lecture dans
  | les comportements) mais, avec les modifications de 1 et 2, c'est désormais facile à faire. On est désormais tranquille jusqu'à 120
  | variables.
  | 
  | 
  | Vérification:
  | on modifie le test zzzz387c et son fichier MFront pour valider que TIME est bien transmis, comme on l'a déjà fait dans ce même test
  | pour HYDR et ELTSIZE1 (issue24129)
  | On crée le coefficient suivant dans MFront:
  | coefverif = (2.-0.25*ELTSIZE1-0.75*HYGR+log(TIME/TIME));
  | 
  | Ce coefficient vaut toujours 1, ce qui ne modifie pas le vrai comportement (ici BETON_Ageing)
  | 
  | Une vraie validation sera réalisée par TM lors de l'intégration de META_LEMA_ANI en mode full-Mfront

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz387c
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28345 DU 13/12/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ajout d'un test pour le mélange de modélisation MECA/HM
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | La fiche issue28101 a corrigé un problème mais nous n'avons pas introduit de test pour blinder cette situation
  | 
  | 
  | Correction
  | ----------
  | 
  | On modifie wtnl101a pour passer de STAT_NON_LINE en DYNA_NON_LINE. Ca ne change pas les résultats (de non-régression) mais ça permet
  | de valider qu'on puisse mélanger THM et non THM dans un calcul dynamique non-linéaire

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V7.30.101
- VALIDATION : wtnl101a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28395 DU 04/01/2019
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : DEFI_BASE_MODALE plante salement
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | DEFI_BASE_MODALE/RITZ sert à construire une base de Ritz en complétant une base de modes propres (issue de CALC_MODES) avec des
  | modes statiques.
  | Quand on utilise RITZ, il est obligatoire de renseigner BASE_MODALE et MODE_INTF simultanément comme l'indique la documentation.
  | Si on ne le fait pas, on a un sale message d'erreur d'objet JEVEUX inconnu : Objet inexistant dans les bases ouvertes : .DESC 
  | 
  | Correction
  | ----------
  | 
  | Ca vient du fait qu'on tente de lire l'objet qui est _censé_ être derrière MODE_INTF alors qu'il n'est pas renseigné puisque le
  | mot-clef n'est pas obligatoire dans le catalogue
  | On peut pas modifier le catalogue vu comment il est fait. Il faut intervenir dans le Fortran avec un beau message
  | d'erreur EXPLICITE.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Tout src
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28353 DU 14/12/2018
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Non couverture de code : AFFE_CHAR_MECA / ARETE_IMPO / PHI
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans AFFE_CHAR_MECA, On peut faire
  | ARETE_IMPO =_F (GROUP_MA = xxx ,  PHI= yyyy)
  | FACE_IMPO =_F (GROUP_MA = xxx ,  PHI= yyyy )
  | 
  | Il n'y a pas de test vérification/validation, ni macro qui utilisent ce chargement.
  | 
  | 
  | Correction
  | ----------
  | 
  | On modifie le cas-test adlv100a (maillage et fichier de commandes) pour passer dans ces options

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : adlv100a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 26973 DU 03/10/2017
================================================================================
- AUTEUR : YU Ting
- TYPE : evolution concernant code_aster (VERSION 14.1)
- TITRE : Créer une nouvelle commande pour IMPR_RESU/CONCEPT
- FONCTIONNALITE :
  | Objectif
  | -----------------
  | 
  | Actuellement IMPR_RESU comprend 2 parties qui sont assez indépendantes :
  |     - impression des résultats
  |     - impression des concepts 
  | 
  | En particulier, dans Asterstudy, le choix facultatif pour RESU conduit souvent un oubli et les utilisateurs se font piéger par un
  | fichier vide.
  | 
  | 
  | Développement
  | ------------------------
  | 
  | J'ai séparé la partie concernant CONCEPT dans IMPR_RESU. En la modifiant, j'ai créé une nouvelle procédure IMPR_CONCEPT et je lui ai
  | attribuée op0021.
  | 
  | - maintenant l'option RESU dans IMPR_RESU est obligatoire
  | - un message d'erreur (PREPOST_1, concernant PROC0 pour concept dans impr_resu) est supprimé
  | - 2 cas test concernant (zzzz200c mfron04b) : les commandes sont remplacées par IMPR_CONCEPT et les cas test sont validés
  | 
  | Pour les doc en dessous, je supprime la partie concernant concept : 
  | U4.91.01  - IMPR_RESU au format RESULTAT et ASTER
  | U7.05.01  - IMPR_RESU au format IDEAS
  | U7.05.21  - IMPR_RESU au format MED
  | U7.05.32  - IMPR_RESU au format GMSH
  | 
  | Il faut créer une nouvelle doc U4.91.04 pour IMPR_CONCEPT

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.91.01, U4.91.04, U7.05.21
- VALIDATION : cas test
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 28390 DU 03/01/2019
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant Documentation (VERSION 11.*)
- TITRE : Notice utilisation aster5
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | On s'est rendu compte d'une coquille dans le document du site web sur l'utilisation des clusters SU4.01.01.
  | p.5 il est écrit qu'on peut faire 
  | 
  | ssh nni @aster5-1.hpc.edf.fr
  | et
  | username@claut682:~$ ssh -X nni @aster5-1.hpc.edf.fr
  | 
  | Il faut remplacer par :
  | ssh nni@aster5-1.hpc.edf.fr
  | et
  | username@claut682:~$ ssh -X nni@aster5-1.hpc.edf.fr
  | 
  | Il n'y a en effet pas d'espace entre nni et @ sinon la commande ne fonctionne pas. On obtient un message de type 
  | 
  | ssh: Could not resolve hostname nni: Name or service not known
  | 
  | 
  | Correction
  | ----------
  | 
  | On corrige la mise en forme du document.
  | 
  | A noter : Comme il est indiqué en *rouge*, il faut remplacer 'nni' pour le NNI de l'utilisateur sinon la connexion n'ira pas loin.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : doc
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 28400 DU 08/01/2019
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : mettre à jour des commandes de l'exemple 3 dans la Doc U2.01.11
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | En cherchant des exemples d'utilisation PROJ_CHAMP dans la doc, j'ai trouvé l'exemple 3 [Comment appliquer une pression sur une
  | partie restreinte (non maillée exactement) du bord d'une structure 3D] dans U2.01.11 et l'option de METHODE = 'ELEM' n’existe plus
  | dans la version actuelle .
  | 
  | 
  | Correction
  | ------------------------
  | 
  | J'ai fait un petit calcul test comme exemple 3 en modifiant METHODE = 'COLLOCATION', et il marche. 
  | Donc je l'ai corrigé dans la doc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U2.01.11
- VALIDATION : test dans asterstudy
- NB_JOURS_TRAV  : 0.2


================================================================================
                RESTITUTION FICHE 28398 DU 07/01/2019
================================================================================
- AUTEUR : YU Ting
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : incohérence pour EVOL_CHAR dans la doc LIRE_RESU
- FONCTIONNALITE :
  | Problème
  | -----------------
  | 
  | Pour un fichier MED avec des champs aux nœuds PRES, j'ai utilisé LIRE_RESU pour créer un resu de type EVOL_CHAR. La commande est
  | exécutée sans erreur. 
  | 
  | Mais ce resu créé se trouve vide lors de la lecture dans AFFE_CHAR_MECA.
  | 
  | 
  | Correction
  | ------------------------
  | 
  | En fait, LIRE_RESU force que le nom de champ PRES correspond au type de cham_elem au format MED. 
  | 
  | Donc j'ai modifié la partie concernante dans la doc LIRE_RESU (U7.02.01) qui n'est pas claire pour le type EVOL_CHAR :
  | 
  | * Format MED : 
  |     - PRES, cham_elem
  |     - FSUR_3D, cham_elem
  |     - FVOL_2D/3D, FSUR_2D, VITE_VENT, VECT_ASSE n'existent pas
  |     - FORC_NODA, cham_noeu
  | 
  | * Format IDEAS :
  |     - PRES, cham_elno
  |     - FVOL_2D/3D,FSUR_2D/3D, VITE_VENT, T_EXT, COEF_H, cham_noeu
  |     - VECT_ASSE n'existent plus
  | 
  | 
  | En même temps, je profites aussi de modifier la partie concernant EVOL_CHAR dans le doc CREA_RESU :
  |     - Ajout du nom de champ FORC_NODA
  |     - Pour les autres champs, remplacement de 'champs aux noeuds' à 'champs' : 
  |       parce que j'ai testé que les champs aux éléments marchent aussi, par exemple, pour PRES.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U7.02.01   U4.44.12
- VALIDATION : non
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 18937 DU 04/06/2012
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Faire le ménage dans l'arboresence de mecalm
- FONCTIONNALITE :
  | Cette fiche peut maintenant être fermée grâce à la réalisation de issue28302.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : rien
- NB_JOURS_TRAV  : 0.5

