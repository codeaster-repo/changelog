==================================================================
Version 17.0.12 (révision 7b27ac4605) du 2024-03-28 21:23:49 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33694 GANGNANT Alexandre       Performances GDEF_LOG
 33702 ALVES-FERNANDES Vinicius Nouvelles fonctionnalités non testées dans DEFI_SOL_EQUI
 33713 ALVES-FERNANDES Vinicius Traitement du cas d'égalité entre critère d'arrêt et variation du module pour...
 33724 ABBAS Mickael            Calcul de charge limite sur Té
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33694 DU 05/03/2024
================================================================================
- AUTEUR : GANGNANT Alexandre
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Performances GDEF_LOG
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | On passe beaucoup de temps dans "intégration de la loi de comportement" dans le cas GDEF_LOG,
  | même avec un comportement linéaire
  | 
  | Analyse
  | ---
  | 
  | On perd beaucoup de temps dans le calcul des déformations logarithmiques (surtout deflg3)
  | 
  | Solution
  | ---
  | 
  | On factorise/développe les boucles, pour simplifier le nombre d'opérations et
  | aussi supprimer les 'if' en plein milieu des boucles
  | 
  | gain :
  | - ~20% dans "intégration de la loi de comportement" (ex perf02a)
  | - 7% en moyenne sur l'ensemble des tess GDEF_LOG

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33702 DU 11/03/2024
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Nouvelles fonctionnalités non testées dans DEFI_SOL_EQUI
- FONCTIONNALITE :
  | Problème :
  | -----------
  | Nouvelles fonctionnalités non testées dans DEFI_SOL_EQUI suite à issue33639
  | 
  | Solution:
  | ----------
  | La fiche issue33639 teste les nouvelles fonctionnalités uniquement pour un l'option CHARGEMENT='MONO_APPUI'. On ajoute les nouvelles options au cas-test
  | zzzz412b pour vérification avec l'option CHARGEMENT='ONDE_PLANE'.
  | 
  | Impact cas-test:
  | ----------------
  | zzzz412b
  | 
  | Impact sources:
  | ----------------
  | astest/zzzz412b.comm

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz412b
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 33713 DU 14/03/2024
================================================================================
- AUTEUR : ALVES-FERNANDES Vinicius
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Traitement du cas d'égalité entre critère d'arrêt et variation du module pour DEFI_SOL_EQUI
- FONCTIONNALITE :
  | Problème :
  | -----------
  | La macro-commande DEFI_SOL_EQUI ne traite pas correctement le cas d'égalité entre le critère d'arrêt (RESI_RELA) et la variation de module d'Young calculé dans
  | la macro (valeur affichée sur le fichier .mess deltaE).
  | 
  | Développement proposé:
  | -----------------------
  | On propose de considérer le cas d'égalité comme "critère atteint". Pour cela, il suffit de modifier le condition de comparaison de < à <=.
  | 
  | Cas-tests
  | ----------
  | Passage de tous les cas-tests avec DEFI_SOL_EQUI
  | 
  | Impact sources:
  | ---------------
  | code_aster/MacroCommands/defi_sol_equi_ops.py

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : cas-tests DEFI_SOL_EQUI
- NB_JOURS_TRAV  : 0.1


================================================================================
                RESTITUTION FICHE 33724 DU 21/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : aide utilisation concernant code_aster (VERSION 11.8)
- TITRE : Calcul de charge limite sur Té
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Il s'agit d'un calcul de charge limite (par la borne inf) réalisée sur un Té.
  | Le calcul a du mal à converger et il y a trois chargements (une pression et deux torseurs d'effort).
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Il vaut mieux appliquer les trois chargements en même temps sous la forme Q(t) = t*(Q1 + Q2 + Q3) et voir si on peut arriver jusqu’à t=1. En effet, par
  | convexité du domaine, si on atteint un bord des chargements admissibles pour t = tc<1, tous les chargements pour t>tc sont non admissibles, et en particulier
  | t=1. Evidemment, si tc>1, on a démontré que t=1 est admissible. En revanche, si on fait un trajet plus exotique pour arriver à t=1, rien ne garantit qu'on ne va
  | pas être amené à traverser une zone non admissible.*
  | On voit une illustration sur les slides joints.
  | 
  | 
  | Si on prends le trajet Q1=Q2, en vert, le point extrémité est bien admissible (le bord du domaine admissible est en rouge). En revanche, si on prend le trajet
  | orange, on va sortir du domaine (le calcul ne convergera plus) avant d’y rentrer de nouveau.
  | 
  | On propose deux stratégies pour résoudre le problème:
  | - celle utilisant du pilotage par DEFORMATION (malheureusement le pilotage par PRED_ELAS ne fonctionne pas pour de la plasticité => développement nécessaire)
  | - celle utilisant un contrôle "à la main" du chargement, 
  | 
  | Par ailleurs des recommandations générales sont faites pour faire converger plus facilement (recherche linéaire), plus vite (LDLT_DP + FMGRES) et plus
  | facilement (introduction de la notion de déplacement généralisé pour détecter quant on atteint l'asymptote)
  | 
  | Enfin, un fichier complet pour la mise en oeuvre a été fourni.
  | 
  | Les slides joints donnent également des pistes pour améliorer la mise en oeuvre en faisant un certains nombre d'évolutions. Propositions intégrées dans l'EO des
  | projets PSM/3M pour le nouveau cycle (à partir de 2025)

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : l'étude cible
- NB_JOURS_TRAV  : 2.0

