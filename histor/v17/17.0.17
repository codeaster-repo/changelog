==================================================================
Version 17.0.17 (révision 421f6bc81e) du 2024-05-16 17:40:01 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33649 FERTÉ Guilhem            PTEMPSR => PINSTR
 32729 COURTOIS Mathieu         supprimer getvectjev, putvectjev, getcolljev et putcolljev suite
 33337 FERTÉ Guilhem            IMPR_RESU et CARA_ELEM
 33779 LE CORVEC Véronique      [CSI2] Cloison gv - Comportement LIAISON_MAIL différent entre 14.8 et 17
 33788 LE CORVEC Véronique      Erreur DVP_1 - Contact
 33725 ROSSAT Donatien          Erreur dans la construction de macro-éléments dynamiques avec matrices non-sy...
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33649 DU 12/02/2024
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : PTEMPSR => PINSTR
- FONCTIONNALITE :
  | Développement
  | -------------
  | 
  | Dans le cadre du chantier de nettoyage de la thermique,
  | on renomme les champs locaux d'instants PTEMPSxR en PINSTxR.
  | 
  | Validation
  | ----------
  | 
  | src

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 32729 DU 29/03/2023
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : supprimer getvectjev, putvectjev, getcolljev et putcolljev suite
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | Il reste des appels à getvectjev, putvectjev
  | 
  | Solution
  | ---
  | 
  | * Supprimer/remplacer les appels restant (sauf asojb)
  | * Supprimer/remplacer les appels via .sdj.xxx.get() (tests et macros)
  | * Ménage des méthodes putvectjev / putcolljev dans le C et le Fortran
  | 
  | Travail effectué
  | ---
  | 
  | 1/ suppression getvectjev / putvectjev de ObjectExt 
  | 
  | VECT_ASSE_GENE
  | 
  | On remplace par des méthodes getValues() et setValues()
  | On type correctement les résultat en FieldOnNodesComplex et non FieldOnNodesReal dans certaines
  | commandes puis ménage des méthodes RECU_VECT_GENE_R()/RECU_VECT_GENE_C() en RECU_VECT_GENE() qui
  | retourne automatiquement un array complexe si FieldOnNodesComplex ou un array reel si FieldOnNodesReal
  | 
  | MACR_ELEM_DYNA
  | 
  | On remplace par getGeneralizedStiffnessMatrix() / getGeneralizedMassMatrix() / getGeneralizedDampingMatrix()
  | 
  | 3/ suppression getvectjev / putvectjev / .sdj des cas tests
  | 
  | zzzz510[a-e] : putvectjev -> setValues()
  | sdll123d : réécriture à l'aide de matr.toNumpy()
  | sslv002a, ssns106a : remplace mesh.sdj et material.sdj
  | zzzz117a, zzzz314[a-f] : fissure.sdj, ajout de méthodes nécessaires dans Crack.cxx
  | 
  | 4/ suppression .sdj des Macrocommandes
  | 
  | Calc_Essai calc_Miss dyna_visco_harm calc_gp
  | comb_sism_modal dyna_iss_vari post_dyna_alea
  | post_jmod post_k1_k2_k3 propa_fiss test_fonction
  | 
  | ajout de méthode nécessaires
  | 
  | AssemblyMatrix : getCalculOption()
  | ConstantFieldValues : getValues()
  | Crack, XfemCrack : getCrackTipCoords() getCrackTipBasis() getCrackTipMultiplicity()
  |                     getTipType() getCrackTipNodeFacesField() getCrackFrontRadius() getNormal()
  |                     getLowerNormNodes() getUpperNormNodes() getLowerNormNodes2() getUpperNormNodes2();
  | GeneralizedResult : getDisplacement()
  | MechanicalLoad : getMechanicalLoadDescription().getConstantLoadField(str)
  | Model : getXfemContact()
  | 
  | 7/ divers
  | 
  | correction dans ConstantFieldOnCells
  | suppression dans table_ext
  | 
  | 8/ bilan
  | Il ne reste plus que 2 getvectjev qui servent dans l'appel à sdj (cf issue32808).
  | Qui sera définitivement supprimé par la fiche issue32808.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src


================================================================================
                RESTITUTION FICHE 33337 DU 03/11/2023
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : IMPR_RESU et CARA_ELEM
- FONCTIONNALITE :
  | Problème
  | ---
  | 
  | L'alarme MED2_13 précise "On ignore le CARA_ELEM du type d'élément pour ce champ"
  | si un type d'élément n'en a pas besoin (pas de sous-points)
  | 
  | "Conseil : Supprimer le mot-clé CARA_ELEM"
  | Mais si on supprime le mot clé de l’occurrence on plante en erreur MED2_14 car le CARA_ELEM
  | peut être nécessaire pour un autre type d'élément (à sous-points).
  | 
  | Analyse
  | ---
  | 
  | Il est tout à fait possible d'imprimer un champ qui est à la fois à sous-points pour certains éléments
  |  et pas à sous-points pour d'autres éléments dans la même occurrence du MC facteur RESU.
  | Dans ce cas IMPR_RESU divise automatiquement le champ:
  | * les éléments sans sous-points s'appuient sur le maillage d'origine
  | * les éléments à sous-points s'appuient sur le maillage de structure
  | 
  | Travail effectué
  | ---
  | 
  | Quand on traite un champ donné dans ircmpe, on boucle sur toutes les familles de pt de Gauss.
  | pour chacune des famille on regarde si le cara_ele est utilisé
  | 
  | - Si au moins une famille a utilisé le cara_ele, pas d'alarme
  | 
  | - Si aucune des familles n'a utilisé cara_ele, l'alarme MED2_13 est émise
  | L'utilisateur peut supprimer le mot clé CARA_ELEM pour ce champ

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src + ssls101m modifié joint


================================================================================
                RESTITUTION FICHE 33779 DU 30/04/2024
================================================================================
- AUTEUR : LE CORVEC Véronique
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [CSI2] Cloison gv - Comportement LIAISON_MAIL différent entre 14.8 et 17
- FONCTIONNALITE :
  | Lors de la refactorisation de AFFE_CHAR_MECA/LIAISON_MAIL (issue31593), la vérification que les surface maître et 
  | esclave sont disjointes n'a pas été reporté pour le cas MASSIF (VoVo) et MASSIF_COQUE (VoSh) mais pour les 
  | autres cas COQUE_MASSIF et COQUE (ShVo,ShSh). 
  | On rajoute la vérification des noeuds en commun et on supprime la relation linéaire si besoin.
  | 
  | Ajout de cas dans zzzz127b pour vérifier l'émission de l'alarme et la résolution dans le cas 
  | TYPE_RACCORD=MASSIF , MASSIF_COQUE et COQUE_MASSIF.
  | 
  | Pas de de résultats faux : le calcul donnait une matrice singulière car la relation linéaire non supprimée 
  | conduit à un pivot nul lors de la factorisation

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz127b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33788 DU 06/05/2024
================================================================================
- AUTEUR : LE CORVEC Véronique
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : Erreur DVP_1 - Contact
- FONCTIONNALITE :
  | Problème
  | ---------
  | 
  | L'utilisateur qui a fourni des groupes de volume dans la définition des surfaces de contact obtient une erreur DVP_1.
  | 
  | Correction
  | ----------
  | La vérification des éléments des zones de contact est faite dans une routine en aval. On ne peut pas simplement inverser les routines.
  | On ajoute un message d'alarme fatal CONTACT2_10 pour arrêter proprement l'utilisateur dans la routine mmelty.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : etude utilisateur
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33725 DU 22/03/2024
================================================================================
- AUTEUR : ROSSAT Donatien
- TYPE : anomalie concernant code_aster (VERSION 17.4)
- TITRE : Erreur dans la construction de macro-éléments dynamiques avec matrices non-symétriques
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Dans le cadre d'un calcul modal par sous-structuration classique impliquant une machine tournante et son 
  | support (cas-test sdlv132), on constate une erreur dans les précessions du rotor obtenues : en effet, 
  | dans le calcul avec sous-structuration, les sens de précession (direct ou indirect) sont inversés par 
  | rapport au calcul "complet" (rotor + support sans sous-structuration). 
  | 
  | 
  | Ceci est dû à une erreur dans la routine calpro.F90 utilisée dans MACR_ELEM_DYNA, pour projeter les 
  | matrices assemblées (raideur, amortissement et masse) dans la base de Ritz (modes propres + modes 
  | statiques) de chaque composant. En effet, dans le cas de matrices non-symétriques (en particulier, 
  | toujours dans le cas de la matrice d'amortissement à vitesse de rotation non-nulle, du fait de la 
  | gyroscopie), les parties triangulaires inférieure et supérieure des matrices résultats de la projection 
  | (i.e. des matrices généralisées) sont échangées. 
  | 
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On corrige la double boucle dans la routine calpro.F90 visant à calculer les coefficients d'une matrice 
  | projetée, en distinguant les cas symétrique et non-symétrique. On vérifie également que les modes 
  | obtenus avec sous-structuration présentent des précessions cohérentes avec celles issues d'un calcul 
  | modal sur structure complète, sans sous-structuration.  
  | 
  | 
  | En parallèle de cette fiche, j'inclus à la branche (déjà existante) issue32806 
  | (https://aster.retd.edf.fr/rex/issue32806) 
  | de l'outil MT un nouveau test (variante de sdlv132a) permettant de tester les sens de précession du 
  | rotor dans le cas d'un calcul modal avec sous-structuration, mettant en jeu un support généralisé.
  | 
  | 
  | 
  | Résultat faux
  | -------------
  | 
  | Le sens de précession de la partie tournante du modèle est inversé par rapport au sens théorique, du 
  | fait de l'erreur décrite ci-dessus. Cette erreur est a priori présente dans le code depuis sa première 
  | version.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 1.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 1.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdlv132a
- NB_JOURS_TRAV  : 1.0

