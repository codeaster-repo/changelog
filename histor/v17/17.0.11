==================================================================
Version 17.0.11 (révision 2ccce70f68) du 2024-03-21 19:52:51 +0100
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 33680 ABBAS Mickael            [3M.2024.CF] Mauvais usage du mot-clef SEUIL_INIT
 33706 ABBAS Mickael            Impression META_ELNO_NOMME incorrecte avec le revenu
 33705 ABBAS Mickael            mtlp104b ne passe pas en mode DEBUG
 33693 COURTOIS Mathieu         taille de fichier d'échange en poursuite
 33616 COURTOIS Mathieu         Ajouter un adapt_syntaxe pour RESI_INTE_RELA/RESi_INTE
 33602 COURTOIS Mathieu         Vérification consistance des paramétres élastiques pour la géomécanique
 33716 BETTONTE Francesco       En version 17.0.10, les cas test htna101a arcad01a hsnv141a et rccm002a sont ...
 33686 FERTÉ Guilhem            En version 17.0.8, le cas test sdlv125a est en erreur sur Cronos & Gaia
 31357 COURTOIS Mathieu         Saisie de caractéristiques élastiques isotropes autres que E et nu
 33661 TARDIEU Nicolas          Comportements différents entre 1 et 4 procs MPI avec parallélisme distribué
 27897 Bonnes idées             EO2019 - Calcul des matrices élementaires en IFS - CALC_MATR_ELEM/GROUP_MA
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 33680 DU 01/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [3M.2024.CF] Mauvais usage du mot-clef SEUIL_INIT
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le mot-clef SEUIL_INIT dans DEFI_CONTACT sert à déterminer une pression de contact initiale, utile dans le cas du frottement (point fixe).
  | 
  | 1/ Dans DEFI_CONT, le mot-clef est rattaché à PairingParameters
  | 2/ Il n'est pas certain qu'on utilise ce mot-clef dans la nouvelle algorithmie
  | 
  | Correction/Développement
  | ------------------------
  | 
  | On propose donc de le supprimer (au moins provisoirement)
  | Il n'est pas testé dans l'algorithme.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : src
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33706 DU 13/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Impression META_ELNO_NOMME incorrecte avec le revenu
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le nouveau modèle de revenu dans CALC_META ne produit pas correctement le champ META_ELNO_NOMME dans IMPR_REsu, il manque les phases de revenu
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | La carte COMPORMETA n'est pas remplacée dans la SD EVOL_THER quand on fait le revenu, ce qui empêche IMPR_RESU de retrouver le nom des variables internes.
  | On corrige

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : viseulle
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33705 DU 13/03/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : mtlp104b ne passe pas en mode DEBUG
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Le cas-test mtlp104b plante en mode DEBUG
  | 
  | De plus, les résultats sont faux: les phases de revenu sont différentes sur les noeuds d'un élément alors que la température est uniforme
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | Un des champs en entrée du calcul de revenu était mal dimensionné. Ce qui fait qu'on "tapait" en dehors. Ca donnait des résultats faux, voir parfois des erreurs
  | à l'exécution (particulièrement en débug)
  | 
  | On corrige.
  | On modifie aussi mtlp104b pour ajouter des test_resu sur les 8 noeuds de l'élement au lieu d'un seul.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mtlp104b
- NB_JOURS_TRAV  : 1.0


================================================================================
                RESTITUTION FICHE 33693 DU 05/03/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : taille de fichier d'échange en poursuite
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Depuis issue32423, max_base vaut 2 To par défaut.
  | Mais ça ne fonctionne pas en poursuite il passe à 12 Go avec l'info :
  | 
  | Ajustement de la taille maximale des bases à 12.00 Go.
  | 
  | 
  | Correction
  | ----------
  | 
  | La taille maximale est depuis issue32423 de 2 To, fixée dans DEBUT.
  | On calcule alors nblmax (nombre maximum d'enregistrements total) et
  | nbenrg (nombre d'enregistrements dans un fichier de la base).
  | 
  | En poursuite, on recalcule la taille maximale (totale) de la base à partir de ces paramètres
  | stockés dans le 1er enregistrement de la base :
  | 
  | mfic_prev = longbl(ic)*nbenrg(ic)*lois
  | 
  | Cette valeur est la taille d'un fichier seulement.
  | Il faut utiliser nblmax à la place.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : étude
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33616 DU 31/01/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Ajouter un adapt_syntaxe pour RESI_INTE_RELA/RESi_INTE
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Dans issue24065, RESI_INTE_RELA et RESI_INTE_MAXI ont été remplacés par RESI_INTE.
  | 
  | Ce changement doit être géré automatiquement et prévénir l'utilisateur avec un DeprecationWarning.
  | 
  | 
  | Développement
  | -------------
  | 
  | On ajoute, dans c_comportement.py, une fonction compat_syntax pour faire le changement.
  | 
  | Cette fonction est ajoutée dans 19 catalogues (dont 2 macros + 2 commandes cachées).
  | 
  | Certaines commandes ont déjà une fonction compat_syntax.
  | On ajoute un utilitaire compat_union qui permet d'appliquer plusieurs fonctions.
  | 
  | Dans THER_NON_LINE :
  | compat_syntax=compat_union(compat_syntax, compat_comport),
  | 
  | 
  | Vérifier sur comp001b et mfron03d avec l'ancienne syntaxe.
  | Dans supv003i, on utilise l'ancienne syntaxe pour afficher le warning.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : supv003i + comp001b/mfron03d ancienne syntaxe
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33602 DU 24/01/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Vérification consistance des paramétres élastiques pour la géomécanique
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | Dans DEFI_MATERIAU, avec mots-clé ELAS et ELAS_FO on ne peut saisir que E et nu.
  | Pour de nombreux matériaux (sols, roches, etc), on "connaît" mieux K et G=mu
  | (modules de compressibilité et de cisaillement), ou les deux célérités des ondes P
  | et S : Vp et Vs.
  | 
  | Par exemple lorsque l'on souhaite décrire une incertitude sur ces paramètres avec GENE_FONC_ALEA.
  | 
  | 
  | Développement
  | -------------
  | 
  | Sous ELAS (on ne fait pas pour les fonctions pour le moment), on peut fournir :
  | - soit (E, NU)
  | - soit (K, MU)
  | - soit (CELE_P, CELE_S, RHO)
  | 
  | Dans les 2 derniers cas, on calcule (E, NU) à partir des paramètres fournis et on les renseigne
  | sous ELAS.
  | 
  | On a alors ce type de message :
  | 
  | (dans comp012d)
  | Les valeurs des paramètres E et NU calculées à partir de (K, MU) sont :
  |     E = 1.88476e+08
  |     NU = 0.298303
  | 
  | ou bien :
  | 
  | (dans ssnv160c)
  | Les valeurs des paramètres E et NU calculées à partir de (CELE_P, CELE_S, RHO) sont :
  |     E = 1e+07
  |     NU = 0.3
  | 
  | On vérifie que K, MU, CELE_P, CELE_S sont strictement positifs
  | et que CELE_P/CELE_S est supérieur à 2*sqrt(3)/3.
  | 
  | Si ce n'est pas le cas, on a le message d'erreur suivant :
  | 
  |  ║ Le rapport CELE_P / CELE_S doit être supérieur à 2*sqrt(3)/3 (= 1.154701).                     ║
  |  ║ Or il vaut : 0.851025                                                                          ║
  | 
  | 
  | Par ailleurs, on ajoute la vérification de la cohérence des paramètres E, NU avec YoungModulus, PoissonRatio
  | fournis sous les mots-clés MFront.
  | 
  | Si ce n'est pas le cas (vérifié dans comp012d, on a ajoute artificiellement 1% d'écart), le message
  | d'erreur est :
  | 
  |  ║ <F> <MATERIAL1_13>                                                                             ║
  |  ║                                                                                                ║
  |  ║ Les valeurs des paramètres Iwan/YoungModulus et ELAS/E ne sont pas cohérents (à 0.001 près) :  ║
  |  ║     Iwan/YoungModulus = 1.86859e+08                                                            ║
  |  ║     ELAS/E = 1.88476e+08                                                                       ║

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : U4.43.01
- VALIDATION : comp012d, ssnv160c
- NB_JOURS_TRAV  : 2.0


================================================================================
                RESTITUTION FICHE 33716 DU 18/03/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 17.0.10, les cas test htna101a arcad01a hsnv141a et rccm002a sont en erreur sur Cronos et Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | En version 17.0.10, les cas test htna101a arcad01a hsnv141a et rccm002a sont en erreur sur Cronos et Gaia
  | 
  | Mess
  | -----
  | 
  |  ╔════════════════════════════════════════════════════════════════════════════════════════════════╗
  |  ║ <EXCEPTION> <SUPERVIS_4>                                                                       ║
  |  ║                                                                                                ║
  |  ║ Erreur de syntaxe dans CREA_RESU                                                               ║
  |  ║                                                                                                ║
  |  ║ For keyword NOM_CHAM: Unauthorized keyword: 'NOM_CHAM'                                         ║
  |  ║                                                                                                ║
  |  ║ Exception détaillée ci-dessous.                                                                ║
  |  ╚════════════════════════════════════════════════════════════════════════════════════════════════╝
  | 
  | Correction
  | ----------
  | Changement de syntaxe depuis issue33432, NOM_CHAM déplacé dans AFFE

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : htna101a arcad01a hsnv141a et rccm002a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33686 DU 04/03/2024
================================================================================
- AUTEUR : FERTÉ Guilhem
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : En version 17.0.8, le cas test sdlv125a est en erreur sur Cronos & Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | En version 17.0.8, le cas test sdlv125a est en erreur sur Cronos & Gaia
  | 
  | 
  | Correction :
  | ----------
  | 
  | "SAV" de la fiche issue33209 sur la suppression du .LIME
  | 
  | On ajoute le CARA_ELEM à la commande CALC_MODES

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdlv125a


================================================================================
                RESTITUTION FICHE 31357 DU 01/09/2021
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Saisie de caractéristiques élastiques isotropes autres que E et nu
- FONCTIONNALITE :
  | Fait dans issue33602

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : comp012d, ssnv160c
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 33661 DU 20/02/2024
================================================================================
- AUTEUR : TARDIEU Nicolas
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : Comportements différents entre 1 et 4 procs MPI avec parallélisme distribué
- FONCTIONNALITE :
  | Problème
  | --------
  | Dans l'étude jointe, j'utilise l'option LIRE_MAILLAGE("PTSCOTCH").
  | Le comportement est différent selon que j'utilise un processus MPI ou 4 processus MPI.
  | 
  | Sur 1 processus MPI, le calcul est OK (cf output/snl_seq.log)
  | 
  | Sur 4 processus MPI, j'ai 2 alarmes dans STAT_NON_LINE (cf output/snl_par.log) qui m'indiquent que les 2 groupes 
  | de mailles VGOUPSUP et VGOUPINF n'ont pas de
  | comportement visiblement. Je ne comprends pas pourquoi.
  | 
  | 
  | Solution
  | --------
  | Mon diagnostic est qu'il n'y a pas de pb. 
  | Les alarmes sont pertinentes : du fait de la découpe des domaines, un comportement peut ne pas être utilisé.
  | Le problème est en fait d'origine numérique et, pour preuve, le calcul ne passe pas si l'on utilise MUMPS, qui 
  | indique qu'il y a un pivot nul. Cela est dû au matériau rigide dont le module d'Young est bien trop élevé et 
  | cause une singularité. En le passant à 1.e9 (soit 1000 fois plus rigide que l'autre matériau), tout se passe 
  | bien et les résultats sont identiques sur 1 ou 4 procs.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : Passage du cas
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 27897 DU 20/07/2018
================================================================================
- AUTEUR : Bonnes idées
- TYPE : evolution concernant code_aster (VERSION 11.8)
- TITRE : EO2019 - Calcul des matrices élementaires en IFS - CALC_MATR_ELEM/GROUP_MA
- FONCTIONNALITE :
  | Déjà fait dans issue32673

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : ras
- NB_JOURS_TRAV  : 0.1

