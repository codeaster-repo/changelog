==================================================================
Version 17.1.11 (révision 4661657ebb) du 2024-10-18 16:28:27 +0200
==================================================================

------ ------------------------ --------------------------------------------------------------------------------
FICHE  AUTEUR                   TITRE
------ ------------------------ --------------------------------------------------------------------------------
 31186 ABBAS Mickael            Calcul sismique des réservoirs : pression équivalente EC8/DERESMA
 33568 BETTONTE Francesco       [TEST] En version 17.0.2, les cas-tests wdnp101a et mfron01j sont en erreur s...
 34017 COURTOIS Mathieu         Warnings sur debian 11
 34098 COURTOIS Mathieu         run_ctest par morceaux
 34097 COURTOIS Mathieu         [TEST] zzzz509f est cassé sur Cronos & Gaia
 34090 COURTOIS Mathieu         setPhysicalQuantity : de NEUT_R vers SIEF_R
 34092 BETTONTE Francesco       Dépassement de tableau dans POST_DYNA_MODA_T
 34071 BETTONTE Francesco       [MAC3] Erreurs LIRE_RESU champ de contraintes
 33683 MEUNIER Sébastien        [RUPT] POST_BEREMIN - Amélioration des performances
 34066 COURTOIS Mathieu         [TEST] En versions 17.1.7 et 16.6.3, les cas-tests htna101a mac3c08d ssnl201v...
 34063 ABBAS Mickael            Nouvelle documentation STAT_NON_LINE
------ ------------------------ --------------------------------------------------------------------------------


================================================================================
                RESTITUTION FICHE 31186 DU 21/06/2021
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : Calcul sismique des réservoirs : pression équivalente EC8/DERESMA
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Pour mener les calculs de tenue sismique des réservoirs, l'approche habituelle est basée sur 
  | l'EuroCode 8 partie 4 (alias EC8 norme NF EN 1998-4).
  | Cette méthode est la même que ce qui est préconisé par la note DT DERESMA (DÉmarche de 
  | RÉévaluation Sismique des MAtériels ENGSDS110381 révision D).
  | 
  | Les effets, sous séisme, du fluide sur la paroi du réservoir sont représentés par un champ 
  | de pression équivalent défini par la superposition de plusieurs
  | fonctions définies en coordonnées cylindriques (pour les réservoirs cylindriques verticaux).
  | Ces champs sont ensuite imposés sur la paroi et on peut alors mener une analyse sismique 
  | statique équivalente de type push-over.
  | 
  | La DT dispose d'un outil MagicEC8 (développé en python) qui permet de générer une version 
  | tabulée de ce champ.
  | 
  | A UTO on a redéveloppé les routines en python directement dans un fichier de commande Aster 
  | (pour la version 13.4) cf. note D450718024173, avec validation par
  | comparaison avec les résultats de l'EC8/DERESMA et de MagiEC8.
  | 
  | On propose de développer une macro-commande réalisant ce calcul
  | 
  | 
  | Correction/Développement
  | ------------------------
  | 
  | La macro-commande DEFI_PRES_EC8 produit une formule qui peut être fournie à 
  | AFFE_CHAR_MECA/FORCE_COQUE_FO (issue33522)
  | 
  | 
  | DEFI_PRES_EC8 = MACRO(
  |     nom="DEFI_PRES_EC8",
  |     op=OPS("code_aster.MacroCommands.defi_pres_ec8_ops.defi_pres_ec8_ops"),
  |     sd_prod=formule,
  |     fr=tr("Création d'un formule de pression pour réservoir (EC8)"),
  |     reentrant="n",
  |     #
  |     Z_FOND=SIMP(statut="o", typ="R", min=1, max=1),
  |     RAYON=SIMP(statut="o", typ="R", min=1, max=1),
  |     HAUT_EAU=SIMP(statut="o", typ="R", min=1, max=1),
  |     RHO_EAU=SIMP(statut="o", typ="R", min=1, max=1),
  |     #
  |     ACCE_SP_H=SIMP(statut="o", typ="R", min=1, max=1),
  |     ACCE_FLEX_H_N=SIMP(statut="o", typ="R", min=1, max=1),
  |     ACCE_FLEX_V=SIMP(statut="o", typ="R", min=1, max=1),
  |     ACCE_SP_V=SIMP(statut="o", typ="R", min=1, max=1),
  |     ACCE_CONV_H=SIMP(statut="o", typ="R", min=1, max=1),
  |     #
  |     GRAVITE=SIMP(statut="f", typ="R", min=1, max=1, defaut=0.0, val_min=0.0),
  |     PRES_SURF_LIBR=SIMP(statut="f", typ="R", min=1, max=1, defaut=0.0),
  |     NEWMARK=SIMP(statut="f", typ="TXM", into=("PC+", "PC-"), defaut="PC+"),
  |     EVAL=FACT(
  |         regles=(UN_PARMI("LIST_H", "LIST_R_FOND")),
  |         statut="f",
  |         max="**",
  |         RHO=SIMP(statut="o", typ=("R")),
  |         LIST_H=SIMP(statut="f", typ="R", val_min=0.0, max="**"),
  |         LIST_R_FOND=SIMP(statut="f", typ="R", val_min=0.0, max="**"),
  |         LIST_EPAIS=SIMP(statut="o", typ="R", max="**"),
  |         THETA=SIMP(statut="f", typ=("R"), defaut=0.0),
  |         TABLE=SIMP(statut="o", typ=CO),
  |     ),
  | )
  | 
  | Modifs par rapport à la RTA:
  | 
  | 1/ Une fois cette formule créée on perd le détail des différentes composantes de la 
  | pression, c'est pourquoi le mot-clé EVAL a été mis en place. Il permet de
  | stocker dans une table les différentes composantes de pressions pour une liste de points 
  | choisis.
  | 2/ On a aussi ajouté le mot-clé NEWMARK permettant de choisir en la combinaison PC+ et PC-.
  | 
  | Dans la macro, on a ajouté les choses suivantes :
  | 
  | - si les coordonnées d'un point donnent un r plus grand que RAYON*1.01 => on s'arrête en 
  | erreur fatale
  | - si les coordonnées d'un point donnent un z plus petit que Z_FOND => on s'arrête en erreur 
  | fatale
  | 
  | Les fonds non-plats ne sont pas interdits, mais ne sont pas validés.
  | 
  | Sur le fond, on a ajouté (par rapport au script initial) une dépendance linéaire à r pour la 
  | composante Pif (pression impulsive flexible), c'est à dire un
  | facteur r/R.
  | 
  | Validation: sslv305
  | 
  | 
  | La modélisation A valide le calcul des différentes composantes de la pression et de la 
  | pression totale sur la base de référence Framatome, pour 3 altitudes
  | différentes (moins de 0.1% de différence à chaque fois), sur la ligne Y=0 (theta=0).
  | Elle valide que la table issue de EVAL donne la même pression totale que l'évaluation de la 
  | formule.
  | Elle valide le calcul des pressions pour theta différent de 0.
  | Elle valide le calcul des différentes composantes sauf PIR sur le fond.
  | Elle valide que PRES_SURF_LIBR s'ajoute partout (en dessous de H et au dessus)
  | Elle valide le mot-clé NEWMARK.
  | 
  | La modélisation B valide le calcul de PIR en fonction de r (sur le fond)
  | 
  | La modélisation C valide (informatiquement) l'application de la formule de pression via 
  | AFFE_CHAR_MECA/FORCE_COQUE_FO.
  | On applique le chargement sur un maillage de réservoir de même dimension que ce qui est 
  | déclaré dans DEFI_PRES_EC8.
  | Les références seront uniquement en non-régression mais on a vérifié que la déformée avait 
  | globalement la bonne allure (à regarder dans la doc).

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : V3.04.305, U4.63.03,
- VALIDATION : sslv305


================================================================================
                RESTITUTION FICHE 33568 DU 15/01/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : [TEST] En version 17.0.2, les cas-tests wdnp101a et mfron01j sont en erreur sur Cronos & Gaia
- FONCTIONNALITE :
  | Problème
  | ---------
  | En version 17.0.2, les cas test wdnp001a est en erreur sur Cronos & Gaia
  | 
  | Correction
  | ----------
  | Le test est rempli de commentaires qui alertent sur la variabilité de ses résultats; toutefois le critère de convergence n'est pas homogène dans tous les
  | opérateurs de calcul (STAT_NON_LINE, DYNA_NON_LINE).
  | 
  | Je mets tous les critères sur la valeur la plus petite parmi celles renseignées : RESI_GLOB_RELA=1.0e-12

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : wdnp101a sur pipeline et cronos
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34017 DU 30/08/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : Warnings sur debian 11
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | GCC 10 sur debian 11, GCC 12 sur debian 12 lèvent ce type de warning:
  | 
  | bibfor/hho/HHO_gradrec_module.F90:C9992: Unexpected warning at 609,None: Array 'bg' at (1) is larger than limit set by 
  | '-fmax-stack-var-size=', moved from stack to static storage. This makes the procedure unsafe when called recursively, 
  | or concurrently from multiple threads. Consider using '-frecursive', or increase the '-fmax-stack-var-size=' limit, or 
  | change the code to use an ALLOCATABLE array. [-Wsurprising]
  | 
  | 
  | Correction
  | ----------
  | 
  | Avant d'utiliser des tableaux ALLOCATABLE, il faudrait évaluer l'impact sur les performances.
  | Il n'y a pas d'accès concurrent à ces tableaux. Ils peuvent être alloués statiquement.
  | On fait cela explicitement en ajoutant un 'save'.
  | 
  | Un commentaire rappelle l'origine de ce changement dans les modules concernés.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : aslint
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34098 DU 09/10/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : run_ctest par morceaux
- FONCTIONNALITE :
  | Objectif
  | --------
  | 
  | run_ctest exécute une liste de cas-tests avec ctest.
  | L'utilisation courante est de lancer run_ctest en batch sur un noeud complet.
  | 
  | On souhaite pouvoir dispatcher simplement le lancement sur plusieurs noeuds.
  | 
  | 
  | Développement
  | -------------
  | 
  | On ajoute une option --nlist=N :
  | 
  | $ run_ctest --resutest=/local00/tmp/results -L verification --nlist=4
  | 
  | Command lines for each batch job:
  |   cd /local00/tmp/results/001 && ctest '-j' '14' '-L' 'verification'
  |   cd /local00/tmp/results/002 && ctest '-j' '14' '-L' 'verification'
  |   cd /local00/tmp/results/003 && ctest '-j' '14' '-L' 'verification'
  |   cd /local00/tmp/results/004 && ctest '-j' '14' '-L' 'verification'
  | Build consolidated report:
  |   cd /local00/tmp/results
  |   run_ctest --resutest=/local00/tmp/results -L verification --nlist=4 --report
  | 
  | 
  | Quand on utilise --testlist=file, le fichier CTestTestfile.cmake ne contient que les tests de la liste.
  | Quand on utilise les filtres -R et/ou -L, le fichier CTestTestfile.cmake contient tous les tests
  | qui sont ensuite lancés ou non en fonction des filtres.
  | Dans le 2ème cas de figure, le nombre de tests lancés par chaque commande peut être déséquilibrés.
  | 
  | La dernière ligne de commande permet de produire un fichier run_testcases.xml consolidé, regroupant
  | les résultats de tous les sous-répertoires.
  | 
  | Remarque :
  | Il existe un script dans admintools (intranet) qui produit un fichier html de bilan.
  | À lancer hors conteneur puisque, par défaut, il ouvre le navigateur.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : lignes de commandes
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34097 DU 09/10/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [TEST] zzzz509f est cassé sur Cronos & Gaia
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | zzzz509f est en erreur sur Cronos et Gaia.
  | 
  | 
  | Correction
  | ----------
  | 
  | Ce test a fait exprès pour poser des problèmes de convergence et vérifier que la découpe
  | des pas de temps se passe bien et qu'on sort proprement après NB_PAS_MAXI pas de temps
  | calculés.
  | 
  | Sur Cronos/Gaia, on tombe sur une FPE au lieu (ou avant) d'échouer à intégrer le comportement.
  | C'est *normal* que l'intégration échoue, on a multiplié par 50 le chargement.
  | 
  | Cependant, on peut s'assurer de ne pas arriver aux FPE en vérifiant l'argument des exponentielles.
  | On le fait pour 2 exponentielles supplémentaires dans @Integrator.
  | Il en reste 2 dans @Predictor mais je ne sais pas trop en sortir proprement pour indiquer l'échec...

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz509f
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34090 DU 07/10/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : evolution concernant code_aster (VERSION 17.1)
- TITRE : setPhysicalQuantity : de NEUT_R vers SIEF_R
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | setPhysicalQuantity de NEUT_R vers SIEF_R ne semble pas modifier la physical quantity.
  | 
  | 
  | Correction
  | ----------
  | 
  | Effectivement, `setPhysicalQuantity()` de modifie pas le champ sur laquelle elle s'applique
  | et retourne un nouveau champ en renommant les composantes.
  | La docstring le dit bien. C'est donc une évolution.
  | 
  | On renomme `setPhysicalQuantity()` en `asPhysicalQuantity()`.
  | C'est semblable à `numpy.astype()` qui retourne un tableau modifié.
  | 
  | De même, `setLocalization()` devient `asLocalization()`.
  | 
  | La méthode `changeLocalization()` est marquée dépréciée (existe depuis 16.4.0) :
  | 
  | DeprecationWarning: This feature is obsoleted, 'changeLocalization' will be removed in the future. Use 
  | 'asLocalization()' instead, it returns a new field.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : zzzz505d
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34092 DU 08/10/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : Dépassement de tableau dans POST_DYNA_MODA_T
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Valgrind détecte un dépassement de tableau dans impact.F90 sur un calcul MAC3_Dynamique qui utilise l'opérateur POST_DYNA_MODA_T
  | 
  | 
  | Correction
  | ----------
  | 
  | Modification des "extrémités" de la boucle for effectuée afin de rester dans la taille du tableau.
  | 
  | 
  | -            do j = 1, nbpas
  | +            do j = i, min(i+nbpas, nbpt)
  | -                if (abs(fn(i+j)) .gt. offset) idech = 1
  | +                if (abs(fn(j)) .gt. offset) idech = 1
  |              end do
  | 
  | Report en v16

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : sdnl106a
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34071 DU 24/09/2024
================================================================================
- AUTEUR : BETTONTE Francesco
- TYPE : anomalie concernant code_aster (VERSION 11.8)
- TITRE : [MAC3] Erreurs LIRE_RESU champ de contraintes
- FONCTIONNALITE :
  | Analyse
  | ===
  | 
  | 1/ Alarme <MED_83>
  | ---
  | 
  | Il est normal d'avoir cette alarme, parce que les éléments n'ont pas toutes les mêmes
  | composantes : "mélange de modélisations qui ne portent pas les mêmes composantes"
  | 
  | Les composantes non présentes pour un élément donné sont mises à zéro dans le MED par IMPR_RESU.
  | Les valeurs "en trop" lues par LIRE_RESU sont ces zéros qui ne sont pas affecté au champ
  | 
  | => RESU et RELOAD devrait être identiques si le COMPORTEMENT et CHAM_MATER sont les mêmes
  | 
  | 2/ comparaison RESU et RELOAD
  | ---
  | 
  | Le problème concerne effectivement les éléments multifibres
  | 
  | J'utilise getValuesWithDescription pour récupérer les valeurs des groupes PMF et non PMF
  | 
  | RESU.values(non_PMF) == RELOAD.values(non_PMF)
  | RESU.values(PMF) != RELOAD.values(PMF)
  | 
  | Sur la maille 34 par exemple (PMF), voici les valeurs de RESU pour SIXX
  | 
  |                sous point 1        sous point 2       sous point 3         sous point 4
  | 
  | pt gauss 1  [-227307362.27936408, -243726327.7753577, -235516845.02735966, -235516845.02736208,
  | pt gauss 2   -228667072.67119056, -243804106.55109024, -236235589.61113933, -236235589.61114177,
  | pt gauss 3   -230026618.9144088 , -243881874.28901997, -236954246.6017137 , -236954246.60171542]
  | 
  | Mais dans RELOAD
  | 
  | gauss 1, ss pt 1   [-227307362.27936408,
  | gauss 1, ss pt 4    -235516845.02736208, 
  | gauss 2, ss pt 3    -236235589.61113933, 
  | gauss 3, ss pt 2    -243881874.28901997, 
  | 
  | gauss 1, ss pt 2    -243726327.7753577,
  | gauss 2, ss pt 1    -228667072.67119056,
  | gauss 2, ss pt 4    -236235589.61114177,
  | gauss 3, ss pt 3    -236954246.6017137
  | 
  | gauss 1, ss pt 3    -235516845.02735966, 
  | gauss 2, ss pt 2    -243804106.55109024,
  | gauss 3, ss pt 1    -230026618.9144088, 
  | gauss 3, ss pt 4    -236954246.60171542]
  | 
  | => Les valeurs affectées à la mailles sont les bonnes, mais l'ordre est incorrect
  | 
  | 3/ Valeurs dans le MED
  | ---
  | 
  | Voici les infos de mdump3
  | 
  | profile
  | -------
  | 
  |           pt gauss 1              pt gauss 2             pt gauss 3 
  | (sp 1, sp 2, sp 3, sp 4), (sp 1, sp 2, sp 3, sp 4), (sp 1, sp 2, sp 3, sp 4)
  |  0.556 0.556 0.556 0.556   0.889 0.889 0.889 0.889   0.556 0.556 0.556 0.556
  | 
  | valeurs pour M34
  | -------
  | 
  |                sous point 1        sous point 2       sous point 3         sous point 4
  | 
  | pt gauss 1   -227307362.279364   -243726327.775358   -235516845.027360   -235516845.027362
  | pt gauss 2   -228667072.671191   -243804106.551090   -236235589.611139   -236235589.611142
  | pt gauss 3   -230026618.914409   -243881874.289020   -236954246.601714   -236954246.601715
  | 
  | => MED OK, donc IMPR_RESU OK, LIRE_RESU KO
  | 
  | 4/ Problème dans LIRE_RESU
  | 
  | dans lrcmve, on affecte 
  | 
  | (sous-point 1, point 1), (sous-point 1, point 2), ...
  | (sous-point 2, point 1), (sous-point 2, point 2), ...
  | ...
  | 
  | Il faut faire
  | 
  | (point 1, sous-point 1), (point 1, sous-point 2), ...
  | (point 2, sous-point 1), (point 2, sous-point 2), ...
  | 
  | 
  | 
  | Tests
  | -----
  | Modification du test mac3c03a pour prévoir un cas avec poursuite depuis un fichier med imprimé qui aboutit sur des résultats identiques au cas sans impression
  | sur disque.
  | 
  | Résultats Faux
  | --------------
  | Oui en cas de poursuite à partir d'un IMPR_RESU + LIRE_RESU en présence d’éléments PMF, depuis toujours.

- RESU_FAUX_VERSION_EXPLOITATION : OUI   DEPUIS : 1.0
- RESU_FAUX_VERSION_DEVELOPPEMENT : OUI   DEPUIS : 1.0
- RESTITUTION_VERSION_EXPLOITATION : OUI
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : mac3c03a  + test perso


================================================================================
                RESTITUTION FICHE 33683 DU 01/03/2024
================================================================================
- AUTEUR : MEUNIER Sébastien
- TYPE : evolution concernant code_aster (VERSION )
- TITRE : [RUPT] POST_BEREMIN - Amélioration des performances
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | Les performances de POST_BEREMIN, en mémoire et en temps sont perfectibles.
  | C'est pénible pour l'utilisation dans les études industrielles, par exemple dans le cadre du benchmark international du projet MODERN sur éprouvettes CT.
  | 
  | Développement
  | -------------
  | Je remplace certains CREA_CHAMP par les méthodes de manipulation python récentes et plus efficaces : restrict par exemple.
  | Je diminue la taille des champs manipulés en remplaçant les champs NEUT_R (30 composantes) par DEPL_R (2 ou 3 composantes), quand c'est possible.
  | 
  | Gain observé
  | ------------
  | Les chiffres donnés correspondent aux 2 POST_BEREMIN dans chaque test.
  | Pour la mémoire, j'écris le chiffre VmPeak de la commande.
  | 
  | Les calculs sont faits sur cronos.
  | 
  | Nom test | Temps avant (s) | Mémoire avant (Mo) |Temps après (s) | Mémoire après (Mo) |
  | ---------|-----------------|--------------------|----------------|--------------------|
  | ct_psi   |   105, 100      |  4132, 4651        |  101, 97       | 4281, 4436         |
  | ct_psi_f |   185, 192      |  4408, 5527        |  133, 139      | 4004, 4753         |
  | 
  | Le gain le plus notable est dans l'utilisation de POST_BEREMIN/WEIBULL_FO

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : gitlab beremin : ct_psi, ct_psi_f
- NB_JOURS_TRAV  : 3.0


================================================================================
                RESTITUTION FICHE 34066 DU 23/09/2024
================================================================================
- AUTEUR : COURTOIS Mathieu
- TYPE : anomalie concernant code_aster (VERSION 17.1)
- TITRE : [TEST] En versions 17.1.7 et 16.6.3, les cas-tests htna101a mac3c08d ssnl201v sdnv140e ssns113a ssns502d tpnl301b en CPU_LIMIT + Dégradation perf de SNL
- FONCTIONNALITE :
  | Problème
  | --------
  | 
  | Tests en manque de temps sur Gaia.
  | 
  | 
  | Analyse
  | -------
  | 
  | htna101a : ras (*)
  | mac3c08d : ras
  | ssnl201v : ras
  | sdnv140e : ras
  | ssns113a : ras
  | ssns502d : varie pas mal (+/-10%) mais n'a jamais été aussi rapide que les 3 dernières semaines (*).
  | tpnl301b : ras (*)
  | 
  | 
  | "ras" signifie qu'il n'y a pas de variation significative du temps d'exécution.
  | 
  | (*) : assez proche du time_limit, on augmente dans le .export pour avoir plus de marge.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : OUI
- IMPACT_DOCUMENTAIRE : 
- VALIDATION : bilan suivant
- NB_JOURS_TRAV  : 0.5


================================================================================
                RESTITUTION FICHE 34063 DU 23/09/2024
================================================================================
- AUTEUR : ABBAS Mickael
- TYPE : evolution concernant Documentation (VERSION )
- TITRE : Nouvelle documentation STAT_NON_LINE
- FONCTIONNALITE :
  | Problème/Objectif
  | -----------------
  | 
  | Mise à jour de la documentation U4 de STAT_NON_LINE sur le même principe que MECA_NON_LINE. On organise  par fonctionnalités et non par catalogue de mots-clefs.
  | On répète la syntaxe pour chaque bloc.

- RESU_FAUX_VERSION_EXPLOITATION : NON
- RESU_FAUX_VERSION_DEVELOPPEMENT : NON
- RESTITUTION_VERSION_EXPLOITATION : NON
- RESTITUTION_VERSION_DEVELOPPEMENT : NON
- IMPACT_DOCUMENTAIRE : U4.53.01
- VALIDATION : rien
- NB_JOURS_TRAV  : 1.0

